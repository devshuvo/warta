<?php
/**
 * @package Warta
 */

global $friskamax_warta;

$format                 = get_post_format() ? get_post_format() : 'standard'; // Set default post format
$hide_featured_image    = get_post_meta( get_the_ID(), 'friskamax_hide_featured_image_main', true );
$hide_post_meta_all     = get_post_meta( get_the_ID(), 'friskamax_hide_post_meta_all', true );
$hide_post_meta_main    = get_post_meta( get_the_ID(), 'friskamax_hide_post_meta_main', true );
$hide_post_meta         = $hide_post_meta_all || $hide_post_meta_main;
?>

<article id="post-<?php the_ID(); ?>" 
<?php   post_class(
                'article-large entry-content clearfix ' . 
                ( !get_the_content() ? 'no-padding-bottom no-border-bottom' : '' 
        ) ); 
?>>
        
<?php   
        /**
         * Featured image
         * ---------------------------------------------------------------------
         */
        if( has_post_thumbnail() && !$hide_featured_image ) :
                warta_featured_image( array( 'page' => 'singular' ) ); ?>
                <div class="margin-bottom-15"></div>        
<?php   
        /**
         * Doesn't have featured image
         * ---------------------------------------------------------------------
         */
        else : // has_post_thumbnail() 
                if( !$hide_post_meta ) {
                        echo '<hr class="no-margin-top">';
                        echo warta_post_meta('singular');
                        echo '<hr>';
                } // post meta

        endif; // has_post_thumbnail() 
        
        // Review box
        if( get_post_meta( get_the_ID(), 'friskamax_review_total', true ) ) {    
                preg_match('/^\[carousel.+?ids="([0-9 ,]+?)".*?\]/is', get_the_content(), $matches_carousel); 
                
                if( $matches_carousel ) {
                        $content = str_replace( $matches_carousel[0], '', get_the_content());
                        $content = apply_filters( 'the_content', $content );
                        $content = str_replace( ']]>', ']]&gt;', $content );
                        
                        echo do_shortcode( $matches_carousel[0] );                        
                        warta_review_box(); 
                        echo $content;
                } else {
                        warta_review_box();
                        the_content(); 
                }
        } else {
                the_content();
        }
        
        wp_link_pages( array(
                'before'        => '<div class="page-links">' . __( 'Pages:', 'warta' ),
                'after'         => '</div>',
                'link_before'   => '<span>',
                'link_after'    => '</span>'
        ) ); // Pages
    ?>

</article><!-- .article-large -->