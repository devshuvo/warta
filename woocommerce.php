<?php
/**
 * The template for displaying Woocommerce pages.
 * 
 * @package Warta
 */

get_header(); ?>

<?php if( warta_is_full_width_carousel_enabled() ) : // Display the carousel if the "Full Width Carousel" option is enabled. ?>
        
        <?php warta_carousel_large(); ?>
        
<?php else : // Otherwise display the normal header. ?>       
       
        <?php warta_page_title(); ?>

<?php endif; ?>

</header>

<div id="content">
        <div class="container">    
                <div class="row">
                        <main id="main-content" class="col-md-8" role="main"> 
                                <?php woocommerce_content(); ?>
                        </main>

                        <?php get_sidebar() ?>
                </div>
        </div>
</div><!-- #content -->

<?php  get_footer(); 