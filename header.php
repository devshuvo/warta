<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Warta
 */

// Get Warta Theme Variables
global  $friskamax_warta, 
        $friskamax_warta_var;

$html_class = '';

if( isset( $friskamax_warta['style']['boxed_style'] ) && $friskamax_warta['style']['boxed_style'] == 1 ) 
        $html_class .= 'boxed-style ';
if( isset( $friskamax_warta['style']['flat_style'] ) && $friskamax_warta['style']['flat_style'] == 1 ) 
        $html_class .= 'flat-style ';
if( isset( $friskamax_warta['style']['image_light'] ) && $friskamax_warta['style']['image_light'] != 1 ) 
        $html_class .= 'no-image-light ';

?><!DOCTYPE html>
<html <?php language_attributes(); ?> 
        <?php echo isset( $friskamax_warta_var['html_id'] ) ? "id='{$friskamax_warta_var['html_id']}'" : '' ?>
        class="<?php echo $html_class ?>"
>
<head>

<meta name="ahrefs-site-verification" content="89c08ab3e8cc1ac62feed99f395bb8ad90568bc12b40daaa34d4e0ecdd6e7710">

<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-5465231055702143",
          enable_page_level_ads: true
     });
</script>

</head>

<body <?php body_class(); ?>>
    <?php do_action( 'before' ); ?>
    
    <!-- HEADER
    ======================================================================== --> 
    <header>
        
        <!-- TOP NAVIGATION BAR
        ==================================================================== -->
        <?php if( isset( $friskamax_warta['top_menu'] ) && !!$friskamax_warta['top_menu'] ): ?>
        <nav class="navbar navbar-inverse <?php if( $friskamax_warta['top_menu_hide_mobile'] ) echo 'hidden-xs hidden-sm' ?>" id="top-nav" role="navigation">

            <div class="container">

                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <a class="sr-only" href="#content"><?php _e( 'Skip to content', 'warta' ); ?></a>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#top-nav-collapse">
                        <span class="sr-only"><?php _e( 'Menu', 'warta' ) ?></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="top-nav-collapse">

                    <?php
                        wp_nav_menu( array(
                            'menu'              => 'top',
                            'theme_location'    => 'top',
                            'depth'             => 2,
                            'container'         => false,
                            'menu_class'        => 'nav navbar-nav',
                            'fallback_cb'       => 'Warta_Bootstrap_Navwalker::fallback',
                            'walker'            => new Warta_Bootstrap_Navwalker())
                        );
                    ?>

                    <?php if( $friskamax_warta['top_menu_search_form'] ): ?>                    
                        <form class="navbar-form navbar-right" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
<?php                           if( is_rtl() ) : ?>
                                        <button type="submit" >
                                            <span class="fa fa-search"></span>
                                            <span class="sr-only"><?php echo esc_attr_x( 'Search', 'submit button', 'warta' ); ?></span>
                                        </button>
                                        <label class="sr-only" for="top_search_form"><?php _ex( 'Search for:', 'label', 'warta' ); ?></label>
                                        <input id="top_search_form" 
                                               name="s"
                                               type="search" 
                                               placeholder="<?php echo esc_attr_x( 'Search&hellip;', 'placeholder', 'warta' ); ?>" 
                                               value="<?php echo esc_attr( get_search_query() ); ?>">
<?php                           else : // is_rtl ?>
                                        <label class="sr-only" for="top_search_form"><?php _ex( 'Search for:', 'label', 'warta' ); ?></label>
                                        <input id="top_search_form" 
                                               name="s"
                                               type="search" 
                                               placeholder="<?php echo esc_attr_x( 'Search&hellip;', 'placeholder', 'warta' ); ?>" 
                                               value="<?php echo esc_attr( get_search_query() ); ?>">
                                        <button type="submit" >
                                            <span class="fa fa-search"></span>
                                            <span class="sr-only"><?php echo esc_attr_x( 'Search', 'submit button', 'warta' ); ?></span>
                                        </button>
<?php                           endif // is_rtl ?>
                        </form>
                    <?php endif ?>
                                        
                    <?php if( $friskamax_warta['top_menu_social_media'] ) warta_social_media( $friskamax_warta, 'nav navbar-nav navbar-right', 'sc-sm sc-dark') ?>

                </div><!-- .navbar-collapse -->

            </div><!--.container-->

        </nav><!--#top-nav-->
        <?php endif ?>

        <!-- MAIN NAVIGATION BAR
        ================================================================ -->
        <nav class="navbar navbar-default" id="main-nav" role="navigation">

            <div class="container">

                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <a class="sr-only" href="#content"><?php _e( 'Skip to content', 'warta' ); ?></a>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav-collapse">
                        <span class="sr-only"><?php _e( 'Menu', 'warta' ) ?></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <?php warta_logo() ?>                        
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="main-nav-collapse">
                    <?php
                        wp_nav_menu( array(
                            'menu'              => 'main',
                            'theme_location'    => 'main',
                            'depth'             => 2,
                            'container'         => false,
                            'menu_class'        => 'nav navbar-nav navbar-right',
                            'fallback_cb'       => 'Warta_Bootstrap_Navwalker::fallback',
                            'walker'            => new Warta_Bootstrap_Navwalker())
                        );
                    ?>
                </div><!-- .navbar-collapse -->

            </div><!--.container-->

        </nav><!--#main-nav-->