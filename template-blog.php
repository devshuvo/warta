<?php
/**
 * Template Name: Blog
 *
 * @package Warta
 */ 
global  $friskamax_warta, 
        $friskamax_warta_var, 
        $content_width, 
        $wp_query;
    
$friskamax_warta_var['page'] = 'archive';    
warta_set_default_option_values();          

if( $friskamax_warta['archive_layout'] == 1) {
        $friskamax_warta_var['html_id'] = 'blog-version-1';
        $content_width                  = 717;
} elseif( $friskamax_warta['archive_layout'] == 2 ) {
        $friskamax_warta_var['html_id'] = 'blog-version-2';
        $content_width                  = 360;
}

get_header();

if( is_page() ) 
        warta_page_title( get_the_title( get_queried_object_id() ), '' ); 
else
        warta_page_title(get_bloginfo( 'name' ), get_bloginfo('description') );

?>

    </header><!--header-->

        <div id="content">

            <div class="container">
                
                <div class="row">

                    <!-- Main Content
                    ============================================================ -->
                    <main id="main-content" class="col-md-8" role="main">
                        
                        <div class="row">
<?php 
                                // Breaking News
                                if( isset( $friskamax_warta['breaking_news']['archive'] ) && !!$friskamax_warta['breaking_news']['archive'] ) 
                                        warta_breaking_news();

                                // Before Content Widgets Area
                                dynamic_sidebar('archive-before-content-section'); 
?>                    
                        </div>

                        <?php if ( have_posts() ) : ?>

                                <?php /* Start the Loop */ ?>
                                <?php while ( have_posts() ) : the_post(); ?>

                                        <?php
                                                /* Include the Post-Format-specific template for the content.
                                                 * If you want to override this in a child theme, then include a file
                                                 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                                 */
                                                get_template_part( "content-{$friskamax_warta['archive_layout']}", get_post_format() );
                                        ?>

                                <?php endwhile; ?>

                        <?php else : ?>

                                <?php get_template_part( 'content', 'none' ); ?>

                        <?php endif; ?>
                        
                        <div class="row">
<?php
                                /**
                                 * After Content Widgets Area
                                 * -----------------------------------------------------
                                 */
                                dynamic_sidebar('archive-after-content-section');
?>                        
                        </div>

                        <?php warta_paging_nav(); ?>

                    </main><!-- #main -->

                    <!-- SIDEBAR 
                    ======================================================== -->
                    <?php get_sidebar(); ?>
                    
                </div><!--.row-->

            </div><!--.container -->

        </div><!--#content-->

    <?php get_footer();