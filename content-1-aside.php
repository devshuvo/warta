<?php
/**
 * @package Warta
 */

global $friskamax_warta, $wp_embed;

$format             = get_post_format() ? get_post_format() : 'standard'; // Set default post format
$hide_post_meta_all = get_post_meta( get_the_ID(), 'friskamax_hide_post_meta_all', true );
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'article-large ' . ( !has_post_thumbnail() ? 'no-image' : '' ) ); ?>>
<?php   
        /**
         * Featured image
         * =====================================================================
         */
        if( has_post_thumbnail() ) : 
                warta_featured_image();

        /**
         * No featured image
         * =====================================================================
         */
        else: // has_post_thumbnail()  
                
                if( !$hide_post_meta_all ) {
                        echo '<hr class="no-margin">';

                        echo warta_post_meta('archive');
                } // post meta            
            
        endif; // has_post_thumbnail() 
    
        /**
         * Content
         * -------------------------------------------------------------------------
         */
        the_content();

        /** 
         * Footer
         * -------------------------------------------------------------------------
         */
        warta_article_footer( array( 
                'read_more' => FALSE
        ) );
?>

</article><!--.article-large-->