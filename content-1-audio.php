<?php
/**
 * @package Warta
 */

global $friskamax_warta;

$hide_post_meta_all = get_post_meta( get_the_ID(), 'friskamax_hide_post_meta_all', true );
$featured_media     = warta_match_featured_media();

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'article-large ' . ( !has_post_thumbnail() ? 'no-image' : '' ) ); ?>>
<?php   
        /**
         * Featured image
         * =========================================================================
         */
        if( has_post_thumbnail() ) : 
                warta_featured_image();

        /**
         * No featured image
         * =====================================================================
         */
        else: // has_post_thumbnail()  ?>

                <a href="<?php the_permalink() ?>" class="title"><h4><?php the_title() ?></h4></a><!--title-->
<?php           if( !$hide_post_meta_all ) {
                        echo warta_post_meta('archive');
                } // post meta

                if($featured_media) : ?>
                        <div class="featured-media"><?php echo $featured_media ?></div>
<?php           endif; // $featured_media 

        endif; // has_post_thumbnail()  ?>
    
        <p><?php echo warta_the_excerpt_max_charlength( $friskamax_warta['archive_excerpt_length'] ) ?></p><!--Content-->
            
<?php   warta_article_footer() // footer ?>
        
</article><!--.article-large-->