<?php
/**
 * Template Name: Archive Version 2
 * 
 * Template file used to render a custom Category Archive Index page.
 *
 * @package Warta
 */ 

get_header(); ?>

<?php if( warta_is_full_width_carousel_enabled() ) : // Display the carousel if the "Full Width Carousel" option is enabled. ?>
        
        <?php warta_carousel_large(); ?>
        
<?php else : // Otherwise display the normal header. ?>       
       
        <?php warta_page_title(); ?>

<?php endif; ?>

</header>

<div id="content">
        <div class="container">
                <div class="row">
                        <main id="main-content" class="col-md-8" role="main">
                                
                                <!-- Widget Area: "Before Content - Archive Page" -->
                                <div class="row aside"><?php dynamic_sidebar( 'archive-before-content-section' );  ?></div>
                                
                                        <?php 
                                                //  Setup custom query.
                                                $warta_query = new WP_Query( array( 
                                                        'post_type'     => 'post',
                                                        'paged'         => get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : absint( get_query_var( 'page' ) ),
                                                        'cat'           => get_post_meta( get_the_ID(), 'friskamax_archive_category', true )
                                                ) );
                                                
                                                if ( $warta_query->have_posts() ) : 
                                                        // Start the Loop.
                                                        while ( $warta_query->have_posts() ) : 
                                                                $warta_query->the_post();       
                                                
                                                                // Include the template for the content.
                                                                // If you want to override this in a child theme, then include a file
                                                                // called content-2.php and that will be used instead. 
                                                                get_template_part( 'content', 2 );
                                                        endwhile; 
                                                        // End the loop.
                                                else : 
                                                        // Include template part for displaying a message that posts cannot be found.
                                                        get_template_part( 'content', 'none' ); 
                                                endif; 
                                                
                                                // Get the pagination
                                                $warta_pagination = warta_get_paging_nav();
                                                
                                                // Restore original Post Data
                                                wp_reset_postdata();
                                                
                                                unset( $warta_query );
                                        ?>
                                
                                <!-- Widget Area: "After Content - Archive Page" -->
                                <div class="row aside"><?php dynamic_sidebar( 'archive-after-content-section' ); ?></div>

                                <?php echo $warta_pagination; // Prints the pagination. ?>
                                
                        </main><!-- #main-content -->

                        <?php get_sidebar(); // Load sidebar template. ?>
                </div><!-- .row -->
        </div><!-- .container -->
</div><!-- #content -->

<?php 

get_footer();