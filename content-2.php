<?php
/**
 * @package Warta
 */

global $friskamax_warta;

$format             = get_post_format() ? get_post_format() : 'standard'; // Set default post format
$hide_post_meta_all = get_post_meta( get_the_ID(), 'friskamax_hide_post_meta_all', true );

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('article-medium'); ?>>

        <div class="row">

                <div class="col-sm-6">
<?php                   
                        warta_featured_image( array(
                                'size' => 'medium'
                        )); // featured image
?>                                                            
                </div><!--.col-sm-6-->

                <div class="col-sm-6">            

                        <a href="<?php the_permalink() ?>" class="title"><h4><?php the_title() ?></h4></a>
<?php                   if( !$hide_post_meta_all ) {
                                echo warta_posted_on(array(
                                        'date_format'       => $friskamax_warta['archive_date_format'],
                                        'meta_date'         => $friskamax_warta['archive_post_meta']['date'],
                                        'meta_format'       => $friskamax_warta['archive_post_meta']['format'],
                                        'meta_comments'     => $friskamax_warta['archive_post_meta']['comments'],
                                        'meta_views'        => $friskamax_warta['archive_post_meta']['views'],
                                        'meta_category'     => $friskamax_warta['archive_post_meta']['category'],
                                        'meta_categories'   => $friskamax_warta['archive_post_meta']['categories'],
                                        'meta_author'       => $friskamax_warta['archive_post_meta']['author'],
                                        'meta_edit'         => TRUE
                                ) );
                        } 
?>      
                        <p><?php echo warta_the_excerpt_max_charlength( $friskamax_warta['archive_excerpt_length'] ) ?></p>    

                </div><!--.col-sm-6-->

        </div><!--.row-->

<?php   warta_article_footer() // footer ?>

</article>    