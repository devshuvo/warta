<?php
/**
 * @package Warta
 */

global $friskamax_warta;

$hide_post_meta_all = get_post_meta( get_the_ID(), 'friskamax_hide_post_meta_all', true );
$matches_gallery    = warta_match_gallery();
$featured_media     = warta_match_featured_media();

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('article-medium'); ?>>

        <div class="row">

                <div class="col-sm-6">    
<?php                 
                        $attachment_src = wp_get_attachment_image_src( $matches_gallery['image_ids'][0], 'large');
                        
                        warta_featured_image( array(
                                'size'              => 'medium',
                                'featured_media'    => $featured_media,
                                'image'             => wp_get_attachment_image( $matches_gallery['image_ids'][0], 'medium'),
                                'image_url'         => $attachment_src[0]
                        )); // featured image
?>                                 
                </div><!--.col-sm-6-->                                                    

                <div class="col-sm-6">
            
                        <a href="<?php the_permalink() ?>" class="title"><h4><?php the_title() ?></h4></a><!--.title-->
<?php           
                        /**
                         * Post meta
                         * -------------------------------------------------------------
                         */
                        if( !$hide_post_meta_all ) {
                                echo warta_posted_on(array(
                                        'date_format'       => $friskamax_warta['archive_date_format'],
                                        'meta_date'         => $friskamax_warta['archive_post_meta']['date'],
                                        'meta_format'       => $friskamax_warta['archive_post_meta']['format'],
                                        'meta_comments'     => $friskamax_warta['archive_post_meta']['comments'],
                                        'meta_views'        => $friskamax_warta['archive_post_meta']['views'],
                                        'meta_category'     => $friskamax_warta['archive_post_meta']['category'],
                                        'meta_categories'   => $friskamax_warta['archive_post_meta']['categories'],
                                        'meta_author'       => $friskamax_warta['archive_post_meta']['author'],
                                        'meta_edit'         => TRUE
                                ) );
                        } 
?>                    
                        <p><?php echo warta_the_excerpt_max_charlength( $friskamax_warta['archive_excerpt_length'] ) ?></p><!--excerpt-->   

                </div><!--.col-sm-6-->

        </div><!--.row-->

<?php   warta_article_footer() // footer ?>

</article>    