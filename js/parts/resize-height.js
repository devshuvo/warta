/*
 * RESIZE HEIGHT 
 * =============================================================================
 */
+function ($) { "use strict";
    
    /**
     * Adjust container image height
     * =========================================================================
     * @param {string} parent
     * @returns {$}
     */
    $.fn.resizeHeight = function ( parent ) {   
            $(this).imagesLoaded(function () {
                        $(this.element).each(function () {
                                var $this           = $(this),
                                    $img            = $this.find('img'),
                                    containerHeight = $this.height(),
                                    containerWidth  = $this.width(),
                                    imgHeight       = $img[0].naturalHeight,
                                    imgWidth        = $img[0].naturalWidth;

                                // IE8
                                if(!imgHeight | !imgWidth) {
                                        var tmpImg = new Image();

                                        tmpImg.src      = $img.attr('src');
                                        imgHeight       = tmpImg.height;  
                                        imgWidth        = tmpImg.width;
                                }

                                if(h < w)   $(this).height(aW).css('left', ( aW - $(this).width() ) / 2 );
                                else        $(this).width(aW).css('top', ( aW - $(this).height() ) / 2 );
                        });
            });
            
            
        var $this = $( this ); 
        
        $this.each( function () {
            var $this = $(this);
            
            $(this).imagesLoaded(function () {                
                setTimeout(function () {                        
                        var     naturalHeight   = $this[0].naturalHeight,
                                naturalWidth    = $this[0].naturalWidth;
                 
                        // IE8
                        if(!naturalHeight | !naturalWidth) {
                                var tmpImg = new Image();
                                        
                                tmpImg.src      = $this.attr('src');
                                naturalHeight   = tmpImg.height;  
                                naturalWidth    = tmpImg.width;
                        }
                        
                        var newH = container.width() * naturalHeight / naturalWidth;                
                        container.height( newH );
                }, 400);  
            });
        } );
        
        return $( this );        
    };
    
    /*
     * Initialize
     * -------------------------------------------------------------------------
     */
    var resizeTimer;
    $( '.image' ).resizeHeight();
    
    $(window).on( 'resize', function () { 
        clearTimeout(resizeTimer); 
        resizeTimer = setTimeout(function() {
                $( '.image' ).resizeHeight();
        }, 300);
    });     
         
} ( jQuery );