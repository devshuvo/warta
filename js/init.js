+function ($) { "use strict"; 
               
    /*
     * Carousel Caption Animation
     * -------------------------------------------------------------------------
     */
    new $.CarouselAnimation('.carousel-large, .carousel-medium');
    
   /*
    * Slider Tabs
    * --------------------------------------------------------------
    */
   $('.slider-tabs').each(function() {
      
        var widget = $(this).attr('id');
        
        $(this).find('.tab-content .tab-pane').each(function () {
            new $.Slider({
                widget  : '#' + widget,             // The widget ID
                tab     : '#' + $(this).attr('id')  // The tab ID
           });
        });
       
   });
   
   /*
    * HOVER DIRECTION AWARE
    * --------------------------------------------------------------
    */
   $(' .da-thumbs > li ').each( function() { $(this).hoverdir({
       hoverDelay : 75
   }); } );
   
   /*
    * Nivo Lightbox 
    * --------------------------------------------------------------
    */
   $(
            '#gallery a,' +
            '.image a:has(.fa-search-plus),' + 
            '.da-thumbs a:not(.link-to-attachment-page),' +  
            '#main-content .frame > a:has(img)'
    ).nivoLightbox({
       effect: 'fadeScale'
    });
   
   /*
    * Flickr
    * --------------------------------------------------------------
    */
   $('.flickr-feed').each(function() {
        var $this = $(this);
        
        new $.FlickrFeed({
            element : $this, // Your element id to place the photos.
            items   : $this.data('count'), // How many items do you want to show.
            id      : $this.data('id'), // A single user ID. This specifies a user to fetch for. eg: '685365@N25'.
            ids     : $this.data('ids'), // A comma delimited list of user IDs. This specifies a list of users to fetch for.
            tags    : $this.data('tags'), // A comma delimited list of tags to filter the feed by.
            tagmode : $this.data('tagmode')  // Control whether items must have ALL the tags (tagmode=all), or ANY (tagmode=any) of the tags. Default is ALL.
        });
   });
        
   /*
    * jQuery Marquee 
    * --------------------------------------------------------------
    */
    $('.breaking-news .content').marquee({   
        delayBeforeStart: 0,
        duplicated      : true,
        gap             : 0,
        pauseOnHover    : true
    });
    
    if( $(window).width() >= 768 ) {  // Don't run on small devices              

        /*
         * JQUERY ZOOM
         * ---------------------------------------------------------------------
         */
        $('.article-large .frame [data-zoom]').each(function () {
                var     $this   = $(this), 
                        href    = $this.attr('href'),
                        img     = $('<img>', { src: href });
                        
                img.imagesLoaded(function() {
                        if( img[0].width > 750 ) 
                                $this.zoom({ url: href });
                });
                
            
        });

    }
    
   /*
    * gmaps.js 
    * --------------------------------------------------------------
    */
    if( typeof GMaps != "undefined" ) {
        var mapContainer = $('#map').height( $(window).height() / 2 ); // set the map container's height

        new GMaps({
            div: '#map',
            lat: mapContainer.data('lat'), 
            lng: mapContainer.data('long'),
            scrollwheel: false
        }).addMarker({
            lat: mapContainer.data('lat'), 
            lng: mapContainer.data('long'),
            title: mapContainer.data('marker')
        });
    }
    
   /**
    * RateIt
    * --------------------------------------------------------------------------
    */
    $(".rateit").rateit({
        step        : .1,
        resetable   : false
    } ).on('over', function ( event, value ) { 
        $(this).attr('title', value ? parseFloat(value).toFixed(1) : 0.0 ); 
    } ).on('rated', function ( event, value ) {
        var $this = $(this);
        
        value   = value 
                ? parseFloat(value).toFixed(1) 
                : 0.0;
        
        $.getJSON( ajax_object.ajax_url, {
            action  : 'warta_review_user_rating',
            id      : $this.data('id'),
            value   : value
        }, function( response ) {            
            var wrapper = $this.closest('.user-rating');
                        
            $this
                .rateit('readonly', true)
                .attr('title', value );
            
            wrapper.find('.after-vote').css('display', 'block');
            wrapper.find('.vote-value').text( response.value );
            wrapper.find('.vote-count').text( response.count );
        } );
            
    } );
    
   /**
    * Twitter Feed
    * --------------------------------------------------------------
    */               
   $('.twitter-feed').each( function() {
       var $this = $(this);
       
       $this.twittie({
            username     : $this.data('username'),
            apiPath      : ajax_object.ajax_url,
            template     : $this.find('script[type="text/template"]').html(),
            dateFormat   : $this.data('date-format'),
            count        : $this.data('count'),       
            loadingText  : $this.data('loading-text')       
        });
   } );
   
   $('.twitter-feed').on('click', 'a:has(.fa-reply)', function (event) {
        event.preventDefault();
        window.open( $(this).attr('href'), '', 'width=710,height=540' );
   });
   
   /**
    * Share buttons
    * --------------------------------------------------------------------------
    */
   var shareButtonsWrapper = $('[data-share-buttons]');
   
   if( shareButtonsWrapper.length > 0 ) {
            $.post( ajax_object.ajax_url, {
                    action      : 'warta_share_buttons',
                    permalink   : shareButtonsWrapper.data('permalink'),
                    title       : shareButtonsWrapper.data('title')
            }, function ( response ) {       
                    if( response ) {
                            shareButtonsWrapper.addClass('share-post').append( response ); 

                            $('.share-post a').click(function ( event ) {
                                    var href = $(this).attr('href');

                                    if( /^http.+$/.test( href ) ) {   
                                            window.open( href, '', 'width=800,height=550' );
                                            event.preventDefault();     
                                    }
                            });
                    }
            } );
                    
   }
   
    
} (jQuery);