+function($) { 'use strict';
    
    var $body = $('body');
        
    var WartaReview = function() {        
        this.total();
    };
    
    WartaReview.prototype.total = function() {
        var self = this;
        
        $body.on('change', '[name="warta_review_scores[]"]', function() {
            self.calculateTotal.call( $(this).closest('.warta-widget') );
        } );
        
        $body.on('warta-tab-add warta-tab-delete', '.warta-widget', function() {
            self.calculateTotal.call(this);
        });
        
    };
    
    WartaReview.prototype.calculateTotal = function() {
        var $this   = $( this ),
            scores  = $this.find( '[name="warta_review_scores[]"]' ),
            total   = 0.0;

        scores.each( function(index, element) {
            var value = parseFloat( $(this).val() );
            value = value ? value : 0.0;
            
            if( value > 5 ) { 
                value = 5.0;
                $(this).val(5.0);
            }

            total += value;
        } );

        total = total / scores.length;

        $this.find('.review-total-score').val( total.toFixed(1) );
    };
    
    new WartaReview();
    
}(jQuery);
            