+function($) { 'use strict';
    
    var body = $('body');
        
    /**
     * TABS WIDGET
     * #########################################################################
     */
    var WartaTabs = function() {
        this.add();
        this.remove();
        this.checkbox();
    };
    
    /**
     * Add tab
     * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     */
    WartaTabs.prototype.add = function() {
        body.on('click', '.warta-tab-add', function(e) {
            var tab = $(this).prev('.warta-tab').clone();
            
//            tab.find('input:not([type=submit]):not([type=reset]), select, textarea').val('');
                    
            tab.insertBefore(this).trigger('warta-tab-add');
            e.preventDefault();
        });
    };
    
    /**
     * Remvoe tabs
     * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     */
    WartaTabs.prototype.remove = function() {
        body.on('click', '.warta-tab-delete', function(e) {
            var widget = $(this).closest('.warta-widget');
            
            $(this).closest('.warta-tab').remove(); 
            
            widget.trigger('warta-tab-delete');
            
            e.preventDefault();            
        });
    };
    
    WartaTabs.prototype.checkbox = function() {
        body.on('change', '.warta-tab input[type=checkbox]', function() {
            $(this).next().attr('value', this.checked ? 1 : 0);
        });
    };
    
    new WartaTabs();
    
    /**
     * SHOW/HODE OPTIONS
     * #########################################################################
     */
    var WartaOptions = function() {
        this.option();
    };
    
    /**
     * Show/Hide Options
     * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     */
    WartaOptions.prototype.option = function() {
        body.on('change', '.warta-option-choice', function() {
            var $this   = $(this),
                option  = $this.closest('div').find('.warta-option').hide();
                
            option.filter('.warta-option-' + $this.val().replace(/_/, '-') ).show();
        });
    };
    
    new WartaOptions();
    
}(jQuery);
            