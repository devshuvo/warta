/**
 * Post Views Counter
 */
+function($) { 'use strict';
        
        if( $('html').is('.ajax-post-views') ) {                
                var     $body           = $('body'),
                        $postViewCount  = $('.post-view-count'),
                        ids             = [];

                if( $body.is('.single-post') ) {
                        $.post( ajax_object.ajax_url, {
                                action  : 'warta_set_post_views',
                                id      : $body.attr('class').match(/postid-(\d*)/)[1]
                        } );
                }
        }        
        
}(jQuery);