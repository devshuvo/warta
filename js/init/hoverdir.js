/*
 * HOVER DIRECTION AWARE
 * =====================
 */
+function($) { 'use strict';
        $(' .da-thumbs > li ').each( function() { 
                $(this).hoverdir({
                        hoverDelay : 75
                }); 
        } );
        
        // Make the height of the images wrapper to be the same.
        $( '.entry-content, .featured-media.gallery' ).find( '.images .da-thumbs' ).each( function( index, element ) {
                $( element ).find( 'li a' ).imagesLoaded( function() {
                        var     $elements       = $( this.elements ),
                                height          = 0;

                        $elements.each( function( index, element ) {
                                var elementHeight = $( element ).height();

                                if ( height === 0 || height > elementHeight ) {
                                        height = elementHeight;
                                }
                        } );

                        if ( height > 0 ) {
                                $elements.height( height );
                        }
                } );
        } );
                
}(jQuery);