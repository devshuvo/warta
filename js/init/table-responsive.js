/*
 * Create responsive tables by wrapping any table inside post content and 
 * comment content with <div class="table-responsive"></div> to make them scroll 
 * horizontally on small devices. When viewing on anything medium and large
 * devices, you will not see any difference in these tables. 
 */
; ( function ( $, window, document, undefined ) { 'use strict';
        
        $( '.entry-content table, .post-comments .comment table' ).wrap( '<div class="table-responsive"></div>' );
        
} ) ( jQuery, window, document );