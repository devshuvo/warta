/*
 * Nivo Lightbox 
 * =============
 */
+function($) { 'use strict';
        $( 'a[data-lightbox-gallery], a[data-lightbox]' ).nivoLightbox({
                effect: 'fadeScale'
         }); 
}(jQuery);