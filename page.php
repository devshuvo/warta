<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Warta
 */

$friskamax_warta_var['page']      = 'page';
$friskamax_warta_var['html_id']   = 'blog-detail';
warta_set_default_option_values();

get_header(); 

// print page title
warta_page_title( get_bloginfo('name'), get_bloginfo('description') ); // print page title

?>

</header><!--header-->

<div id="content">

    <div class="container">

        <div class="row">
            
            <!-- Main Content
            ============================================================ -->
            <main id="main-content" class="col-md-8" role="main">        
                
                <div class="row">
                    <!-- BREAKING NEWS
                    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ --> 
                    <?php if( $friskamax_warta['breaking_news'] && $friskamax_warta['breaking_news']['page'] ) warta_breaking_news() ?>
                </div>
                <h1><?php single_post_title(); ?></h1>
</br>
                <?php while ( have_posts() ) : the_post(); ?>

                        <?php get_template_part( 'content', 'page' ); ?>

                        <?php
                                // If comments are open or we have at least one comment, load up the comment template
                                if ( comments_open() || '0' != get_comments_number() ) :
                                        comments_template();
                                endif;
                        ?>

                <?php endwhile; // end of the loop. ?>

            </main><!-- #main -->

            
            <?php 
                /**
                 * Sidebar
                 * -------------------------------------------------------------
                 */
                $custom_sidebar = get_post_meta( get_the_ID(), 'friskamax_custom_sidebar', true ); 
                
                if( $custom_sidebar ) :
            ?>
            
                    <aside class="col-md-4">
                        <div class="entry-content">
                            <?php 
                                $content    = apply_filters('the_content', $custom_sidebar);
                                $content    = str_replace(']]>', ']]&gt;', $content);
                                
                                echo $content 
                            ?>
                        </div>
                    </aside>
                    
            <?php else : get_sidebar(); endif; ?>
           
        </div><!--.row-->

    </div><!--.container -->

</div><!--#content-->

<?php get_footer(); ?>