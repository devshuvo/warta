<?php
/**
 * Warta functions and definitions
 *
 * @package Warta
 */



/**
 * Load Warta Theme variables
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
require get_template_directory() . '/inc/variable.php';



/**
 * Set the content width based on the theme's design and stylesheet.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
if ( ! isset( $content_width ) ) {
	$content_width = 750; /* pixels */
}



if ( ! function_exists( 'warta_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function warta_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Warta, use a find and replace
	 * to change 'warta' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'warta', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'top'   => __( 'Top Menu', 'warta' ),
		'main'  => __( 'Main Menu', 'warta' ),
		'footer'=> __( 'Footer Menu', 'warta' ),
	) );

	// Enable support for Post Formats.
	add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' ) );

        // Enabling Support for Post Thumbnails
        add_theme_support( 'post-thumbnails' ); 
        
        // Update Reserved Image Sizes
        update_option('thumbnail_size_w', 95);
        update_option('thumbnail_size_h', 75);
        update_option('thumbnail_crop', 1);
        update_option('medium_size_w', 350);
        update_option('medium_size_h', 185);
        update_option('medium_crop', 1);
        update_option('large_size_w', 730);
        update_option('large_size_h', 370);
        update_option('large_crop', 1);
        
        // Registers a new image size
        add_image_size('small', 165, 90, TRUE);
        add_image_size('gallery', 180, 180, TRUE);
        add_image_size('huge', 1366, 768, TRUE);
}
endif; // warta_setup
add_action( 'after_setup_theme', 'warta_setup' );



if( !function_exists('warta_scripts') ) :
/**
 * Enqueue scripts and styles.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
function warta_scripts() {
        $friskamax_warta = get_option('friskamax_warta');
        
        // color option
        if( !empty( $friskamax_warta['color'] ) ) {
                if( $friskamax_warta['color_option'] == 'both' ) 
                        $style  = "style-color-{$friskamax_warta['color']}-2.css";
                else
                        $style  = $friskamax_warta['color'] == 0 
                                ? 'style.css' 
                                : "style-color-{$friskamax_warta['color']}.css";                        
        } else {
                $style = 'style.css';
        }
    
        wp_enqueue_script( 'warta-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

        if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
            wp_enqueue_script( 'comment-reply' );
        }
        
        wp_enqueue_style( 'warta-style', get_template_directory_uri() . "/css/{$style}"  );
        wp_enqueue_style( 'warta-style-wp', get_template_directory_uri() . '/css/wp/wp.css'  );
        if(is_rtl())
                wp_enqueue_style( 'warta-style-rtl', get_template_directory_uri() . '/css/rtl.css', array('warta-style', 'warta-style-wp')  );
        
        wp_enqueue_script( 'warta-script', get_template_directory_uri() . '/js/script.js' , array('jquery', 'jquery-ui-core', 'jquery-effects-core'), time(), TRUE);
        wp_enqueue_script( 'warta-script-init', get_template_directory_uri() . '/js/init.js' , array(), time(), TRUE);
        
        // Ajax
        wp_localize_script( 'warta-script', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}
endif; // warta_scripts
add_action( 'wp_enqueue_scripts', 'warta_scripts' );



if( !function_exists('warta_editor_style') ) :
/**
 * Adds editor style
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
function warta_editor_style() { 
        add_editor_style( 'css/wp/editor.css');
}
endif; // warta_editor_style
add_action('init','warta_editor_style');



if( !function_exists('warta_start_session') ) :
/**
 * Start session
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
function warta_start_session() {
        if( !session_id() ) 
                session_start();
}
endif; // warta_start_session
add_action('init','warta_start_session');



/**
 * Load filters
 */
require get_template_directory() . '/inc/filters/load.php';

/**
 * Register widgetized area.
 */
require get_template_directory() . '/inc/widgets_init.php';

/**
 * Register widgets.
 */
require get_template_directory() . '/inc/widgets/init.php';

/**
 * Add meta boxes.
 */
require get_template_directory() . '/inc/meta-boxes/init.php';

/**
 * Add shortcodes.
 */
require get_template_directory() . '/inc/shortcodes/init.php';

/**
 * Initialize ReduxFramework.
 */
if ( !class_exists( 'ReduxFramework' ) && file_exists( get_template_directory() . '/inc/admin/redux-framework/ReduxCore/framework.php' ) ) {
    require_once( get_template_directory() . '/inc/admin/redux-framework/ReduxCore/framework.php' );
}
if ( !isset( $friskamax_warta ) && file_exists( get_template_directory() . '/inc/admin/redux-config.php' ) ) {
    require_once( get_template_directory() . '/inc/admin/redux-config.php' );
}

/**
 * Load custom navwalker.
 */
require get_template_directory() . '/inc/warta_bootstrap_navwalker.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags/load.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras/load.php';
require get_template_directory() . '/inc/extras.php';

/**
 * Load post views counter.
 */
require get_template_directory() . '/inc/post-views.php';

/**
 * Load ajax functions
 */
require get_template_directory() . '/inc/ajax/load.php';

/**
 * Load theme customizer
 */
if( file_exists( get_template_directory() . '/customizer/init.php' ) )
        require_once get_template_directory() . '/customizer/init.php';