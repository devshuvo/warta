<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package Warta
 */

$friskamax_warta_var['page']      = 'archive';
warta_set_default_option_values();

$friskamax_warta_var['html_id']   = $friskamax_warta['archive_layout'] == 1 ? 'blog-version-1' : 'blog-version-2';

get_header(); 

warta_page_title( get_search_query(), __( 'Search Results for:', 'warta'), 1 ); // print page title
?>

</header><!--header-->

<div id="content">

    <div class="container">

        <div class="row">

            <!-- Main Content
            ============================================================ -->
            <main id="main-content" class="col-md-8" role="main">
                
                <div class="row">
                    <!-- BREAKING NEWS
                    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ --> 
                    <?php if( $friskamax_warta['breaking_news'] && $friskamax_warta['breaking_news']['archive'] ) warta_breaking_news() ?>
                </div>

		<?php if ( have_posts() ) : ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content-' . ( $friskamax_warta['archive_layout'] ? $friskamax_warta['archive_layout'] : 1 ), get_post_format() ); ?>

			<?php endwhile; ?>

			<?php warta_paging_nav(); ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

            </main><!-- #main-content -->
            
            <!-- SIDEBAR 
            ======================================================== -->
            <?php get_sidebar(); ?>

        </div><!--.row-->

    </div><!--.container -->

</div><!--#content-->

<?php get_footer(); ?>
