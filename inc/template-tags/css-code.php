<?php

if( !function_exists('warta_css_code')):
function warta_css_code() {
        global $friskamax_warta;
        
        if( isset( $friskamax_warta['css_code'] ) && !!trim( $friskamax_warta['css_code'] ) ) {
                echo "<style>{$friskamax_warta['css_code']}</style>";
        }
}
endif; // warta_ie8_support
add_action('wp_head', 'warta_css_code', 666);

