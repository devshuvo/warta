<?php

/* -------------------------------------------------------------------------- *
 * Featured Image
 * -------------------------------------------------------------------------- */

if ( ! class_exists( 'Warta_Featured_Image' ) ) :
/**
 * Helper class for warta_get_featured_image().
 */
class Warta_Featured_Image {
        public          $r              = array();      // arguments
        protected       $options        = array(),      // Warta theme options
                        $format         = '';           // post format
                
        public function __construct( $args = array() ) {
                $this->r                = apply_filters( 'Warta_Featured_Image__args', $args );
                $this->format           = get_post_format();

                $this->r                = wp_parse_args( $this->r, array(
                                                'size'                  => 'large',     // thumbnail size, ex: .article-large = large, .article-medium = medium, ...
                                                'page'                  => 'archive',   // current page type
                                                'featured_media'        => '',          // featured media html code
                                        ) );
                $this->options          = friskamax_get_options( array(
                                                'archive_icons'         => array( 'author' => 1 ),
                                                'archive_icons_aside'   => array( 'author' => 1 ),
                                                'archive_icons_gallery' => array( 'author' => 1 ),
                                                'archive_icons_link'    => array( 'author' => 1 ),
                                                'archive_icons_image'   => array( 'author' => 1 ),
                                                'archive_icons_quote'   => array( 'author' => 1 ),
                                                'archive_icons_status'  => array( 'author' => 1 ),
                                                'archive_icons_video'   => array( 'author' => 1 ),
                                                'archive_icons_audio'   => array( 'author' => 1 ),
                                                'archive_icons_chat'    => array( 'author' => 1 ),
                        
                                                'archive_post_meta'    => array( 
                                                                                'date'          => 1,
                                                                                'categories'    => 1,
                                                                                'tags'          => 1,
                                                                                'author'        => 1,
                                                                                'comments'      => 1,
                                                                                'views'         => 1, 
                                                                        ),
                                                'singular_post_meta'    => array( 
                                                                                'date'          => 1,
                                                                                'categories'    => 1,
                                                                                'tags'          => 1,
                                                                                'author'        => 1,
                                                                                'comments'      => 1,
                                                                                'views'         => 1, 
                                                                        ),
                                        ), 'Warta_Featured_Image__options', false ); 
        }
                
        /**
         * Large image's icons
         */
        public function display_large_icons() {
                $_format        = $this->r['page'] == 'archive' && $this->format ? '_' . $this->format : '';
                $icons          = wp_parse_args( $this->options[ $this->r['page'] . '_icons' . $_format ], array(
                                        'author'    => 0,
                                        'comments'  => 0,
                                        'format'    => 0,
                                ) ); 
                $icons['format']= $this->r['page'] == 'singular' && !$this->format ? 0 : $icons['format'];
                
                ?>

                <div class="icons">
                        
                        <?php if ( $icons['author'] ): ?>
                                <span class="vcard">
                                        <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta('ID') ) ) ?>" 
                                           class="url fn"
                                           title="<?php echo sprintf( __('View all posts by %s', 'warta'), get_the_author() ) ?>">
                                                <?php echo get_avatar( get_the_author_meta('ID'), 65 ) ?>
                                                <span class="hidden"><?php the_author() ?></span>
                                        </a>
                                </span>
                        <?php endif; ?>

                        <?php if ( $icons['format'] ) : ?>
                                <a href="<?php echo esc_url( get_post_format_link( $this->format ) ) ?>" 
                                   title="<?php echo esc_attr( sprintf( __('View all %s posts', 'warta'), $this->format ) ) ?>">
                                        <i class="dashicons dashicons-format-<?php echo $this->format ?>"></i>
                                </a>
                        <?php endif; ?>

                        <?php if( $icons['comments'] && get_comments_number() ) : ?>
                                <a href="<?php echo esc_url( get_comments_link() ) ?>" 
                                   title="<?php echo esc_attr( sprintf( _n( "%d comment", "%d comments", get_comments_number(), 'warta' ), get_comments_number() ) ) ?>">
                                        <i class="fa fa-comments"></i><span class="comment"><?php echo warta_format_counts( get_comments_number() ) ?></span>
                                </a>
                        <?php endif; ?>
                        
                </div>

                <?php
        }
                
        /**
         * Large image's content
         */
        public function display_large_content() {
                $post_meta      = wp_parse_args( $this->options["{$this->r['page']}_post_meta"], array(
                                        'date'          => 0,
                                        'format'        => 0,
                                        'category'      => 0,
                                        'categories'    => 0,
                                        'tags'          => 0,
                                        'author'        => 0,
                                        'comments'      => 0,
                                        'views'         => 0, 
                                        'review_score'  => 0,
                                ) );

                ?>

                <?php if( $this->r['featured_media'] ) : ?>
                        <div class="featured-media <?php echo $this->format ?>"><?php echo $this->r['featured_media']; ?></div>
                <?php elseif( has_post_thumbnail() ) : ?>
                        <?php $att_img_src = warta_get_attachment_image_src_without_jetpack_photon( get_post_thumbnail_id(), 'full'); ?>
                        <a href ="<?php echo $this->r['page'] == 'archive' ? esc_url( get_permalink() ) : esc_url( $att_img_src[0] ) ?>" 
                           title="<?php the_title_attribute() ?>" class="image" 
                           <?php echo $this->r['page'] == 'archive' ? '' : 'data-lightbox' ?>
                        >
                                <figure class="image-holder"><?php the_post_thumbnail('large') ?></figure>
                                <div class="image-light"></div>
                        </a><!--thumbnail image-->
                <?php endif; ?> 

                <?php if( $this->r['page'] == 'archive' && $this->format != 'aside' ) : ?>
                        <h4><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h4><!--title-->
                <?php endif; ?>

                <?php echo warta_get_post_meta(
                        array(
                                'date_format'           => isset( $this->options["{$this->r['page']}_date_format"] ) 
                                                        ? $this->options["{$this->r['page']}_date_format"] 
                                                        : 'F j, Y',
                                'meta_date'             => $post_meta['date'],
                                'meta_format'           => $post_meta['format'],
                                'meta_comments'         => $post_meta['comments'],
                                'meta_views'            => $post_meta['views'],
                                'meta_category'         => $post_meta['category'],
                                'meta_categories'       => $post_meta['categories'],
                                'meta_author'           => $post_meta['author'],
                                'meta_review_score'     => $post_meta['review_score'],
                                'meta_edit'             => true,
                                'class'                 => $this->r['page'] == 'singular' ? 'pull-right' : ''
                        )
                ); ?>
                        
                <?php
        }
                
        /**
         * Large image
         */
        public function display_large() {
                ?>
                        
                <div class="frame thick clearfix">
                        <?php if( $this->options['archive_layout'] != '3' || $this->r['page'] == 'singular' ) $this->display_large_icons(); ?>
                        <?php $this->display_large_content(); ?>
                </div>
                        
                <?php echo warta_image_shadow(); ?>
                        
                <?php
        }
                
        /**
         * Medium image
         */
        public function display_medium() {
                ?>
                      
                <?php if( $this->r['featured_media'] ) : ?>
                        <div class="frame">
                                <div class="featured-media <?php echo $this->format ?>"><?php echo $this->r['featured_media']; ?></div>
                        </div>
                        <?php echo warta_image_shadow(); ?>
                <?php elseif( has_post_thumbnail() ) : ?>
                        <div class="frame">
                                <a class="image" href="<?php the_permalink() ?>">
                                        <figure class="image-holder">
                                                <?php the_post_thumbnail( 'medium' ) ?>
                                        </figure>
                                        <div class="image-light"></div>
                                        
                                        <?php if( $this->format ) : ?>
                                                <span class="dashicons dashicons-format-<?php echo $this->format ?>"></span>
                                        <?php endif ?>
                                </a>
                        </div>
                        <?php echo warta_image_shadow(); ?> 
                <?php endif; ?>
                        
                <?php
        }
                
        /**
         * Small image
         */
        public function display_small() {
                ?>
                
                <?php if ( has_post_thumbnail() ) : ?> 
                        <a href="<?php the_permalink() ?>" class="image">
                                <figure class="image-holder">
                                        <?php the_post_thumbnail( $this->r['size'] == 'small' ? 'small' : 'thumbnail' ) ?>
                                </figure>
                                <div class="image-light"></div>
                                
                                <?php if( $this->format ) : ?>
                                        <span class="dashicons dashicons-format-<?php echo $this->format ?>"></span>
                                <?php endif ?>
                        </a>
                <?php endif; ?>

                <?php
        }
                
        /**
         * Display the featured image
         */
        public function display() {
                switch ($this->r['size']) { 
                        case 'large':
                                $this->display_large();
                                break;
                        case 'medium':
                                $this->display_medium();
                                break;
                        default:
                                $this->display_small();
                                break;
                }
        }
}
endif;

if ( ! function_exists('warta_get_featured_image') ) :
/**
 * Return featured image HTML
 * 
 * @param array $args 
 */
function warta_get_featured_image( $args = array() ) {     
        $args           = apply_filters( 'warta_get_featured_image_args', $args );
        $featured_image = new Warta_Featured_Image( $args );

        ob_start();
        $featured_image->display();

        return apply_filters( 'warta_get_featured_image', ob_get_clean(), $featured_image->r );
}
endif; // warta_featured_image

if ( ! function_exists( 'warta_featured_image' ) ) :
/**
 * Display featured image
 * 
 * @param array $args 
 */
function warta_featured_image( $args = array() ) {         
        echo warta_get_featured_image( $args );
}
endif; // warta_featured_image

/* -------------------------------------------------------------------------- *
 * Shadow
 * -------------------------------------------------------------------------- */

if ( ! function_exists( 'warta_image_shadow' ) ) :
/**
 * Returns image shadow HTML.
 * 
 * @param array $args                   Optional. Arguments.
 * @param string $args[image_url]       Optional. The Image URL.
 * @param string $args[class]           Optional. Classname for the <img>.
 * 
 * @return string
 */
function warta_image_shadow( $args = array() ) {
        $r      = friskamax_parse_args( $args, array(
                        'image_url'     => get_template_directory_uri() . '/img/shadow.png',
                        'class'         => 'shadow'
                ), 'warta_image_shadow_r' );        
        $return = '<img src="' . $r->image_url . '" class="' . $r->class . '" alt="shadow">';
        
        return apply_filters( 'warta_image_shadow', $return, $r );
}
endif; // ! function_exists( 'warta_image_shadow' )

/* -------------------------------------------------------------------------- *
 * Carousel
 * -------------------------------------------------------------------------- */

if ( ! function_exists( 'warta_get_carousel_large' ) ) :
/**
 * Returns HTML carousel large for the current post.
 * @return string
 */
function warta_get_carousel_large() {        
        $carousel               = new Warta_Posts_Carousel();
        $carousel_options       = get_post_meta( get_the_ID(), 'warta_full_width_carousel_options', TRUE );
        
        ob_start();
        $carousel->widget( array( 'is_large' => true ), $carousel_options );
        
        return apply_filters( 'warta_get_carousel_large', ob_get_clean(), $carousel_options, $carousel );
}
endif; 

if ( ! function_exists( 'warta_carousel_large' ) ) :
/**
 * Display carousel large.
 */
function warta_carousel_large() {
        echo warta_get_carousel_large();
}
endif;

/* ========================================================================== *
 * Deprecated
 * ========================================================================== */

if ( ! function_exists( 'warta_image_links' ) ) :
/**
 * Returns links on article medium's image.
 * 
 * @deprecated since version 1.6.2.1
 */
function warta_image_links() {    
        _deprecated_function( __FUNCTION__, '1.6.2.1' );
}
endif;