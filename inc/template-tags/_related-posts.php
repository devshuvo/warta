<?php
/**
 * Display related posts
 * 
 * @package Warta
 */

if ( ! function_exists('warta_get_related_posts') ) :
        /**
         * Returns related posts.
         * 
         * @param array $args                           Optional. Arguments.
         * @param string $args[before]                  Optional. Opening tag.
         * @param string $args[after]                   Optional. Closing tag.
         * @param string $args[before_title]            Optional. String before title.
         * @param string $args[after_title]             Optional. String after title.
         * @param string $args[row_open]                Optional. Opening row.
         * @param string $args[row_close]               Optional. Closing row.
         * @param string $args[before_article]          Optional. Opening article wrapper.
         * @param string $args[after_article]           Optional. Closing article wrapper.
         * @param string $args[before_article_title]    Optional. String before post title.
         * @param string $args[after_article_title]     Optional. String after post title.
         * @param array $args[featured_image_r]         Optional. Argument for warta_get_featured_image().
         * 
         * @return string
         */
        function warta_get_related_posts( $args = array() ) {
                $r      = friskamax_parse_args( $args, array(
                                'before'                => '<section class="widget related-posts">',
                                'after'                 => '</section>',
                                'before_title'          => '<header class="clearfix"><h4>',
                                'after_title'           => '</h4></header>',
                                'row_open'              => '<div class="row">',
                                'row_close'             => '</div>',
                                'before_article'        => '<div class="article-small col-md-3 col-sm-6">',
                                'after_article'         => '<hr class="visible-sm visible-xs"></div>',
                                'before_article_title'  => '<h5><a href="%s">',
                                'after_article_title'   => '</a></h5>',
                                'featured_image_r'      => array( 'size' => 'small' )
                        ), 'warta_get_related_posts_r' );
                $o      = friskamax_get_options( array(
                                'singular_related'              => true,
                                'singular_related_title'        => __( 'Related Posts', 'warta' ),
                                'singular_related_post_meta'    => array(),
                                'singular_related_date_format'  => 'M j, Y',
                        ), 'warta_get_related_posts_o' );
                
                if( ! $o->singular_related ) return; // Return if disabled from Theme Option.
                
                // Setup query.
                // -----------------------------------------------------------------------------
                
                $tags           = get_the_tags();
                $cats           = get_the_category();
                $query_args     = array(  
                                        'post__not_in'          => ( array ) get_the_ID(),  
                                        'posts_per_page'        => 4, // Number of related posts to display.  
                                        'ignore_sticky_posts'   => true  
                                );
                
                if ( $tags ) {  // Related by tags.  
                    foreach( $tags as $tag ) {
                            $tag_ids[] = $tag->term_id; 
                    }       

                    $query_args['tag__in'] = $tag_ids;
                } else if ($cats) {        // Related by categories.
                    foreach ( $cats as $cat ) {
                            $cat_ids[] = $cat->term_id;
                    }

                    $query_args['category__in'] = $cat_ids; 
                } else {
                    return;
                }

                $the_query = new WP_Query( apply_filters( 'warta_get_related_posts_var_WP_Query_args', $query_args ) ); 
                
                if ( ! $the_query->have_posts() ) {
                    return; // Return if there's no posts to display.
                }
                
                // Get the articles.
                // -----------------------------------------------------------------------------
                
                $articles = '';
                
                while( $the_query->have_posts() ) :                 
                        $meta   = wp_parse_args( 
                                        $o->singular_related_post_meta, 
                                        array(
                                                'date'          => 0,
                                                'format'        => 0,
                                                'category'      => 0,
                                                'author'        => 0,
                                                'comments'      => 0,
                                                'views'         => 0,
                                        )
                                );
                
                        $the_query->the_post(); 
                        
                        $articles .=    $r->before_article;
                        $articles .=            warta_get_featured_image( $r->featured_image_r ); 
                        $articles .=            sprintf( $r->before_article_title, esc_url( get_permalink() ) ); 
                        $articles .=                    get_the_title(); 
                        $articles .=            $r->after_article_title; 
                        $articles .=            warta_get_post_meta( array(
                                                        'meta_date'     => $meta['date'],
                                                        'meta_format'   => $meta['format'],
                                                        'meta_category' => $meta['category'],
                                                        'meta_author'   => $meta['author'],
                                                        'meta_comments' => $meta['comments'],
                                                        'meta_views'    => $meta['views'],
                                                        'date_format'   => $o->singular_related_date_format,
                                                ) );
                        $articles .=    $r->after_article;                        
                endwhile; // $the_query->have_posts()  

                wp_reset_postdata(); // Restore original Post Data  
                
                // The output.
                // -----------------------------------------------------------------------------

                $return = $r->before;
                $return .=      $o->singular_related_title ? $r->before_title . $o->singular_related_title . $r->after_title : '';
                $return .=      $r->row_open;
                $return .=              $articles;
                $return .=      $r->row_close;
                $return .= $r->after;

                return apply_filters( 'warta_get_related_posts', $return, $r, $o, $query_args );
        }
endif; // warta_get_related_posts

if( !function_exists('warta_related_posts') ) :
        /**
         * Display related posts.
         * 
         * @param array $args                           Optional. Arguments.
         * @param string $args[before]                  Optional. Opening tag.
         * @param string $args[after]                   Optional. Closing tag.
         * @param string $args[before_title]            Optional. String before title.
         * @param string $args[after_title]             Optional. String after title.
         * @param string $args[row_open]                Optional. Opening row.
         * @param string $args[row_close]               Optional. Closing row.
         * @param string $args[before_article]          Optional. Opening article wrapper.
         * @param string $args[after_article]           Optional. Closing article wrapper.
         * @param string $args[before_article_title]    Optional. String before post title.
         * @param string $args[after_article_title]     Optional. String after post title.
         * @param array $args[featured_image_r]         Optional. Argument for warta_get_featured_image().
         */
        function warta_related_posts( $args = array() ) {
                echo warta_get_related_posts( $args );
        }
endif;