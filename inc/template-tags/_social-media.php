<?php
/**
 * Display social media icons
 * 
 * @package Warta
 */

if( !function_exists('warta_get_social_media')) :
        /**
         * Return list of social media icons
         * 
         * @param array $urls Key value pairs of the urls array('facebook' => 'http://facebook.com')
         * @param array $args Arguments          
         * @return string
         */
        function warta_get_social_media($urls = array(), $args = array()) {                
                $icons  = $GLOBALS['friskamax_warta_var']['social_media'];
                $urls   = apply_filters('warta_get_social_media_arg_urls', $urls);
                $args   = apply_filters('warta_get_social_media_args', $args);
                
                if( is_string($args) ) {
                        // v1.6.1 or older
                        $r = array(
                                'before'        => '',
                                'after'         => '',
                                'i_class'       => $args
                        );
                } else {
                        $r = wp_parse_args( $args, array(
                                'before'        => '<ul class="social clearfix">',
                                'after'         => '</ul>',
                                'i_class'       => 'sc-sm'
                        ) );
                }
                
                ob_start();
                
                echo $r['before'];
                foreach ( array_keys($icons) as $key ) : 
                        if( !empty($urls[$key]) ) : ?>
                                <li>
                                        <a href="<?php echo esc_url($urls[$key]) ?>" title="<?php echo esc_attr( $icons[$key] ) ?>" target="_blank">
                                                <i class="<?php echo $r['i_class'] .' sc-'. esc_attr($key) ?>"></i>
                                        </a>
                                </li>
<?php                   endif; 
                endforeach;
                echo $r['after'];

                return apply_filters('warta_get_social_media', ob_get_clean(), $urls, $r); 
        }
endif;

if( !function_exists('warta_social_media') ) :
        /**
         * Display list of social media icons
         * 
         * @param array $urls
         * @param array $args
         */
        function warta_social_media($urls, $args) {
                echo warta_get_social_media($urls, $args);
        }
endif;




if( !function_exists('warta_get_social_media_author') ) :
        /**
         * Return list of social media icons from an author 
         * 
         * @param array $args
         * @return string
         */
        function warta_get_social_media_author( $args = array() ) {  
                $args   = apply_filters( 'warta_get_social_media_author_args', $args );
                $r      = wp_parse_args( $args, array( 
                                'user_id' => FALSE,                                
                                'before'  => '<ul class="social clearfix">',
                                'after'   => '</ul>',
                                'i_class' => 'sc-sm'
                        ) );    
                $urls   = array();
                
                foreach ( $GLOBALS['friskamax_warta_var']['social_media'] as $key => $value ) : 
                        $urls[$key] = get_the_author_meta( "friskamax_$key", $r['user_id'] );  
                endforeach;
                
                $urls = apply_filters( 'warta_get_social_media_author_var_urls', $urls );
                
                return apply_filters( 'warta_get_social_media_author', warta_get_social_media( $urls, $args ), $urls, $r );
        }
endif;

if( !function_exists('warta_social_media_author') ) :
        /**
         * Display list of social media icons from an author 
         * 
         * @param array $args
         */
        function warta_social_media_author( $args = array() ) {
                echo warta_get_social_media_author( $args );
        }
endif;