<?php
/**
 * Display social media icons
 * 
 * @package Warta
 */

if( !function_exists('warta_social_media')) :
/**
 * Display social media icons
 * 
 * @global array $friskamax_warta Warta variables
 * @param string $option_name Option name on database
 * @param string $ul_class Classes for the &lt;ul&gt; container
 * @param string $li_class Classes for the &lt;li&gt;
 */
function warta_social_media($data = array(), $ul_class = '', $i_class = '') {
    global  $friskamax_warta_var; 
    
    echo "<ul class='$ul_class'>";
    foreach ($friskamax_warta_var['social_media'] as $key => $value) : if(!empty($data[$key])) : ?>
        <li>
            <a href="<?php echo esc_url($data[$key]) ?>" title="<?php echo esc_attr( $friskamax_warta_var['social_media'][$key] ) ?>">
                <i class="<?php echo $i_class .' sc-'. esc_attr($key) ?>"></i>
            </a>
        </li>
    <?php  endif; endforeach;    
    echo '</ul>';
    
}
endif;