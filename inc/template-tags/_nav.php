<?php

/* -------------------------------------------------------------------------- *
 * Pagination
 * -------------------------------------------------------------------------- */

if ( ! function_exists( 'warta_get_paging_nav' ) ) :
/**
 * Returns HTML pagination.
 * 
 * @return string
 */
function warta_get_paging_nav() {        
    global  $wp_query,
            $warta_query;

    $query = $warta_query ? $warta_query : $wp_query;

    /** Stop execution if there's only 1 page */
    if( $query->max_num_pages <= 1 ) {
        return;
    }

    $paged          = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : absint( get_query_var( 'page' ) );
    $paged          = $paged ? $paged : 1;
    $max            = intval( $query->max_num_pages );
    $direction_prev = is_rtl() ? 'right' : 'left';
    $direction_next = is_rtl() ? 'left' : 'right';
    $return         = '';

    /**	Add current page to the array */
    if ( $paged >= 1 ) {
        $links[] = $paged;
    }

    /**	Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    $return .= '<ul class="pagination">';

    // Previous Post Link
    if ( get_previous_posts_link() ) {
        $return .= sprintf( 
                    '<li>%s</li>', 
                    get_previous_posts_link( '<i class="fa fa-angle-double-' . $direction_prev . '"></i>' ) 
                );
    } else {
        $return .= '<li><span><i class="fa fa-angle-double-' . $direction_prev . '"></i></span></li>';
    }

    // Divider
    $return .= '<li class="divider"></li>';

    // Link to first page, plus ellipses if necessary
    if ( ! in_array( 1, $links ) ) {
        $return .= 1 == $paged
                ? '<li class="active"><span>1</span></li>'
                : sprintf( 
                    '<li><a href="%s">%s</a></li>', 
                    esc_url( get_pagenum_link( 1 ) ), 
                    number_format_i18n( 1 )
                );

        if ( ! in_array( 2, $links ) ) {
            $return .= '<li><span class="hellip">&hellip;</span></li>';
        }
    }
    
    // Link to current page, plus 2 pages in either direction if necessary
    sort( $links );
    foreach ( (array) $links as $link ) {
        $return .= $paged == $link
                ? '<li class="active"><span>' . $link . '</span></li>'
                : sprintf( 
                    '<li><a href="%s">%s</a></li>', 
                    esc_url( get_pagenum_link( $link ) ), 
                    number_format_i18n( $link )
                );
    }

    // Link to last page, plus ellipses if necessary
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) ) {
            $return .= '<li><span class="hellip">&hellip;</span></li>';
        }

        $class  = $paged == $max ? ' class="active"' : '';

        $return .= sprintf( 
                    '<li%s><a href="%s">%s</a></li>', 
                    $class, 
                    esc_url( get_pagenum_link( $max ) ), 
                    number_format_i18n( $max )
                );
    }

    // Divider
    $return .= '<li class="divider"></li>';

    // Next Post Link
    if ( get_next_posts_link() ) {
        $return .= sprintf( 
                    '<li>%s</li>', 
                    get_next_posts_link('<i class="fa fa-angle-double-' . $direction_next . '"></i>') 
                );
    } else {
        $return .= '<li><span><i class="fa fa-angle-double-' . $direction_next . '"></i></span></li>';
    }

    $return .= '</ul>';

    return apply_filters( 'warta_get_paging_nav', $return );
}
endif; 

if( ! function_exists( 'warta_paging_nav' ) ) :
/**
 * Prints pagination.
 */
function warta_paging_nav() {
        echo warta_get_paging_nav();
}
endif;

/* -------------------------------------------------------------------------- *
 * Post navigation
 * -------------------------------------------------------------------------- */

if ( ! function_exists( 'warta_get_post_nav' ) ) :
/**
 * Returns navigation to next/previous post when applicable.
 * 
 * @param array $args                   Optional. Arguments.
 * @param string $args[before]          Optional. Opening wrapper tag.
 * @param string $args[after]           Optional. Closing wrapper tag.
 * @param string $args[before_title]    Optional. String to prepend to the navigation title.
 * @param string $args[after_title]     Optional. String to append to the navigation title.
 * @param string $args[title]           Optional. The navigation title.
 * @param string $args[before_links]    Optional. String to prepend to the links.
 * @param string $args[after_links]     Optional. String to append to the links.
 * @param string $args[prev_format]     Optional. Link anchor format.
 * @param string $args[prev_link]       Optional. Link permalink format.
 * @param string $args[next_format]     Optional. Link anchor format.
 * @param string $args[next_link]       Optional. Link permalink format.
 * @param string $args[default]         Optional. Default content when there's nowhere to navigate or it has been disabled from Theme Options.
 * 
 * @return string
 */
function warta_get_post_nav( $args = array() ) {
        $r              = friskamax_parse_args( $args, array(
                                'before'        => '<nav class="navigation post-navigation" role="navigation">',
                                'after'         => '</nav>',
                                'before_title'  => '<h1 class="sr-only">',
                                'after_title'   => '</h1>',
                                'title'         => __( 'Post navigation', 'warta' ),
                                'before_links'  => '<div class="nav-links">',
                                'after_links'   => '</div>',
                                'prev_format'   => '<div class="nav-previous">%link</div>',
                                'prev_link'     => _x( '<span class="meta-nav">Previous Post:</span> <h5>%title</h5>', 'Previous post link', 'warta' ),
                                'next_format'   => '<div class="nav-next">%link</div>',
                                'next_link'     => _x( '<span class="meta-nav">Next Post:</span> <h5>%title</h5>', 'Next post link', 'warta' ),
                                'default'       => '<div class="margin-bottom-45"></div>'
                        ), 'warta_get_post_nav_r' );
        $o              = friskamax_get_options( array( 'singular_post_nav' => true ), 'warta_get_post_nav_o' );
        $previous       = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
        $next           = get_adjacent_post( false, '', false );
        
        if ( ( ! $next && ! $previous ) || ! $o->singular_post_nav ) return $r->default;
        
        $return = $r->before;
        $return .=      $r->before_title . $r->title . $r->after_title;
        $return .=      get_previous_post_link( $r->prev_format, $r->prev_link );
        $return .=      get_next_post_link( $r->next_format,  $r->next_link );
        $return .= $r->after;
        
        return apply_filters( 'warta_get_post_nav', $return, $r, $o );
}
endif; 

if ( ! function_exists( 'warta_post_nav' ) ) :
/**
 * Display navigation to next/previous post when applicable.
 * 
 * @param array $args                   Optional. Arguments.
 * @param string $args[before]          Optional. Opening wrapper tag.
 * @param string $args[after]           Optional. Closing wrapper tag.
 * @param string $args[before_title]    Optional. String to prepend to the navigation title.
 * @param string $args[after_title]     Optional. String to append to the navigation title.
 * @param string $args[title]           Optional. The navigation title.
 * @param string $args[before_links]    Optional. String to prepend to the links.
 * @param string $args[after_links]     Optional. String to append to the links.
 * @param string $args[prev_format]     Optional. Link anchor format.
 * @param string $args[prev_link]       Optional. Link permalink format.
 * @param string $args[next_format]     Optional. Link anchor format.
 * @param string $args[next_link]       Optional. Link permalink format.
 * @param string $args[default]         Optional. Default content when there's nowhere to navigate or it has been disabled from Theme Options.
 * 
 * @return void
 */
function warta_post_nav( $args = array() ) {
        echo warta_get_post_nav( $args );
}
endif; // warta_post_nav