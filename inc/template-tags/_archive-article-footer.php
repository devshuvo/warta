<?php

/* 
 * Display article footer
 * 
 * @package Warta
 */

if( !function_exists( 'warta_get_article_footer') ) :
        /**
         * Return article footer
         * 
         * @param array $args
         */
        function warta_get_article_footer( $args = array() ) {   
                global $friskamax_warta;
                    
                $args           = apply_filters('warta_get_article_footer_args', $args);
                $r              = wp_parse_args( $args, array(
                                        'page' => 'archive',
                                ) );
                
                $o              = wp_parse_args( (array) $friskamax_warta, array(
                                        $r['page'] . '_excerpt_length'  => 320,
                                        $r['page'] . '_post_meta'       => array( 'tags' => 1 )
                                ) ); 
                
                $format         = get_post_format();
                $content        = trim( get_the_content() );
                $excerpt_length = $o[ $r['page'] . '_excerpt_length' ];
                $display_tags   = $o[ $r['page'] . '_post_meta']['tags'] && get_the_tags();

                switch ($format) { 
                        case 'aside':
                        case 'status':
                                $display_more = FALSE;
                                break;

                        case 'quote':
                                $match_quote    = warta_match_quote();
                                $display_more   = $match_quote 
                                                ? strlen($match_quote) < strlen($content)
                                                : FALSE;
                                break;

                        case 'chat':
                                $match_chat     = warta_match_chat();                                
                                $display_more   = strlen($match_chat) < strlen( wpautop($content) );
                                break;

                        case 'link':
                                $match_link     = warta_match_link();
                                $display_more   = $match_link 
                                                ? strlen($match_link) < strlen($content)
                                                : FALSE;
                                break;

                        default:
                                $display_more   = strlen($content) > $excerpt_length
                                                || preg_match('/class="more-link"/is', $content );
                                break;
                }

        ?>
                <div class="footer <?php if( !$display_tags && !$display_more ) { echo 'no-padding'; } ?>">
                        <ul class="tags"><?php  if( $display_tags ) : the_tags('<li>', '</li><li>', '</li>'); endif ?></ul>
        <?php
                        if( $display_more ) : ?>
                                <div class="read-more">
                                        <a class="btn btn-primary btn-sm" href="<?php the_permalink() ?>"><?php esc_attr_e('Read More', 'warta') ?></a> 
                                </div>
        <?php           endif // read more 
        ?>                
                </div>
        <?php
        }
endif; // warta_get_article_footer

if( !function_exists( 'warta_article_footer') ) :
        /**
         * Display article footer
         * 
         * @param array $args
         */
        function warta_article_footer( $args = array() ) {
                echo warta_get_article_footer( $args );
        }
endif;

