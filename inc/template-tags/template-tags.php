<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package warta\template-tags
 */

require dirname(__FILE__) . '/_nav.php';
require dirname(__FILE__) . '/_comment.php';
require dirname(__FILE__) . '/_meta.php';
require dirname(__FILE__) . '/_social-media.php';
require dirname(__FILE__) . '/_image.php';
require dirname(__FILE__) . '/_header.php';
require dirname(__FILE__) . '/_related-posts.php';
require dirname(__FILE__) . '/_review-box.php';
require dirname(__FILE__) . '/_body-bg.php';
require dirname(__FILE__) . '/_logo.php';
require dirname(__FILE__) . '/_menu-cart.php';
require dirname(__FILE__) . '/_favicon.php';
require dirname(__FILE__) . '/_archive-large.php';
require dirname(__FILE__) . '/_archive-medium.php';
require dirname(__FILE__) . '/_archive-article-footer.php';
require dirname(__FILE__) . '/_single.php';

require dirname(__FILE__) . '/_walker-bootstrap-navwalker.php';