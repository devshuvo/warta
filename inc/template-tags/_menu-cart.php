<?php

/* ========================================================================== *
 * Deprecated
 * ========================================================================== */

if(!function_exists('warta_add_to_cart_fragment')) :
/**
 * Update menu cart title.
 * 
 * @deprecated since version 1.8.6
 * @global WooCommerce $woocommerce
 * @param array $fragments
 * @return array
 */
function warta_add_to_cart_fragment( $fragments ) {                
    global $woocommerce;

    _deprecated_function(__FUNCTION__, '1.8.6');

    ob_start(); 
    
    ?>

    <a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'warta'); ?>">
        <i class="fa fa-shopping-cart"></i> 
        <?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'warta'), $woocommerce->cart->cart_contents_count);?> - 
        <?php echo $woocommerce->cart->get_cart_total(); ?>
    </a>

    <?php
    
    $fragments['a.cart-contents'] = ob_get_clean();

    return $fragments;
}
endif;

if( !function_exists('warta_get_menu_cart') ) :
/**
 * Return cart for top menu
 * 
 * @deprecated since version 1.8.6
 * @global WooCommerce $woocommerce
 */
function warta_get_menu_cart() {
    global $woocommerce;
    
    _deprecated_function(__FUNCTION__, '1.8.6');

    ob_start();

    if ( !!$woocommerce && !is_cart() && !is_checkout() ) : ?>
            <li class="dropdown menu-cart">
                    <a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'warta'); ?>">
                            <i class="fa fa-shopping-cart"></i> 
                            <?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'warta'), $woocommerce->cart->cart_contents_count);?> - 
                            <?php echo $woocommerce->cart->get_cart_total(); ?>
                    </a>

                    <div class="dropdown-menu">
                        <?php the_widget( 'WC_Widget_Cart', 'title= ' ); ?>
                    </div>
            </li>
    <?php endif;

    return apply_filters('warta_get_menu_cart', ob_get_clean());
}
endif;

if( !function_exists('warta_menu_cart') ) :
/**
 * Display cart for top menu.
 * 
 * @deprecated since version 1.8.6
 */
function warta_menu_cart() {    
    _deprecated_function(__FUNCTION__, '1.8.6');
    echo warta_get_menu_cart();
}
endif;