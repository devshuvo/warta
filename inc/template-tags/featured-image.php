<?php

/**
 * Display featured image
 * 
 * @package Warta
 */

if( !function_exists('warta_featured_image') ) :
/**
 * Display featured image
 * 
 * @global type $friskamax_warta Warta theme option
 * @param array $args : 
 *      - size          : thumbnail size, ex: .article-large = large, .article-medium = medium, ...
 *      - page          : current page type
 *      - image         : custom image html code
 *      - image_url     : custom image url
 *      - featured_media: featured media html code
 *      - caption       : (boolean) display caption
 */
function warta_featured_image( $args = array() ) {
        global $friskamax_warta;   
        
        $format             = get_post_format() ? get_post_format() : 'standard'; // Set default post format
        $hide_post_meta_all = get_post_meta( get_the_ID(), 'friskamax_hide_post_meta_all', true );
                
        extract( wp_parse_args( 
                $args, 
                array(
                        'size'                  => 'large',
                        'page'                  => 'archive',
                        'image'                 => '',
                        'image_url'             => '',
                        'featured_media'        => '',
                        'caption'               => '',
                ) 
        ) );
        
        $image          = $image ? $image : get_the_post_thumbnail( NULL, $size );
        $attachment_src = wp_get_attachment_image_src( get_post_thumbnail_id(), 'huge');
        $image_url      = $image_url 
                        ? esc_url( $image_url ) 
                        : esc_url( $attachment_src[0] );
        
        /**
         * Article large
         * =====================================================================
         */
        if( $size == 'large' ) : ?>

                <div class="frame thick clearfix">

                        <a href="<?php echo $image_url ?>" 
                           title="<?php the_title() ?>" data-zoom>
<?php                                   echo $image ?>
                                <div class="image-light"></div>
                        </a><!--thumbnail image-->

                        <div class="icons">
<?php                   
                                if( $friskamax_warta["{$page}_icons"]['author'] ): ?>
                                        <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta('ID') ) ) ?>" 
                                           title="<?php echo sprintf( __('View all posts by %s', 'warta'), get_the_author()) ?>">
                                                <?php echo get_avatar( get_the_author_meta('ID'), 65 ) ?>
                                        </a>
<?php                           endif; // author 

                                if( $friskamax_warta["{$page}_icons"]['format'] ) : ?>
                                        <a href="<?php echo esc_url( get_post_format_link($format) ) ?>" 
                                           title="<?php echo esc_attr( sprintf( 'View all %s posts', $format ), 'warta') ?>">
                                                <i class="dashicons dashicons-format-<?php echo $format ?>"></i>
                                        </a>
<?php                           endif; // post format 

                                if( $friskamax_warta["{$page}_icons"]['comments'] && get_comments_number() > 0 ) : ?>
                                        <a href="<?php echo esc_url( get_comments_link() ) ?>" 
                                           title="<?php echo esc_attr( sprintf( _n( "%d comment", "%d comments", get_comments_number(), 'warta' ), get_comments_number() ) ) ?>">
                                                <i class="fa fa-comments"></i><span class="comment"><?php echo warta_format_counts( get_comments_number() ) ?></span>
                                        </a>
<?php                           endif // comments ?>

                        </div><!--.icons-->

<?php                   if( $page != 'singular' ) : ?>
                                <a href="<?php the_permalink() ?>"><h4><?php the_title() ?></h4></a><!--title-->
<?php                   endif;

                        if( !$hide_post_meta_all ) {
                                echo warta_posted_on(
                                        array(
                                                'date_format'       => $friskamax_warta["{$page}_date_format"],
                                                'meta_date'         => $friskamax_warta["{$page}_post_meta"]['date'],
                                                'meta_format'       => $friskamax_warta["{$page}_post_meta"]['format'],
                                                'meta_comments'     => $friskamax_warta["{$page}_post_meta"]['comments'],
                                                'meta_views'        => $friskamax_warta["{$page}_post_meta"]['views'],
                                                'meta_category'     => $friskamax_warta["{$page}_post_meta"]['category'],
                                                'meta_categories'   => $friskamax_warta["{$page}_post_meta"]['categories'],
                                                'meta_author'       => $friskamax_warta["{$page}_post_meta"]['author'],
                                                'meta_review_score' => $friskamax_warta["{$page}_post_meta"]['review_score'],
                                                'meta_edit'         => TRUE
                                        ),
                                        $page == 'singular' ? 'pull-right' : ''
                                );
                        } // post meta
?>
                </div><!--.frame-->                            
<?php           echo warta_image_shadow(); // shadow
        
        /**
         * Article medium
         * =====================================================================
         */
        elseif( $size == 'medium' ) : 
                            
                /**
                 * Featured image
                 * -------------------------------------------------------------
                 */
                if( has_post_thumbnail() ) : ?>
                        <div class="frame">
                                <div class="image">
<?php                                   the_post_thumbnail('medium') ?>
                                        <div class="image-light"></div>
<?php                                   if( $caption ) : ?>
                                                <div class="caption">
                                                        <span class="dashicons dashicons-format-<?php echo $format ?>"></span>
                                                        <h5><?php the_title() ?></h5>
                                                </div>  
<?php                                   endif; // caption
                                        echo warta_image_links() // hover links ?>
                                </div><!--.image-->
                        </div><!--.frame-->
<?php                   echo warta_image_shadow(); // shadow
                
                /**
                 * Custom image
                 * -------------------------------------------------------------
                 */
                elseif( $image ) : ?>
                        <div class="frame">
                                <div class="image">
                                        <?php echo $image ?>
                                        <div class="image-light"></div>
<?php                                   if( $caption ) : ?>
                                                <div class="caption">
                                                        <span class="dashicons dashicons-format-<?php echo $format ?>"></span>
                                                        <h5><?php the_title() ?></h5>
                                                </div>  
<?php                                   endif; // caption
                                        echo warta_image_links( $image_url ) // hover links ?>
                                </div><!--.image-->
                        </div><!--.frame-->
<?php                   echo warta_image_shadow(); // shadow

                /**
                 * Featured media
                 * -------------------------------------------------------------
                 */
                elseif( $featured_media ) : // has_post_thumbnail() ?>
                        <div class="featured-media"><?php echo $featured_media; ?></div><!--.featured-media-->                                
<?php                   if( $caption ) : ?>
                                <h5><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h5>
<?php                   endif; // caption 
                     
                /**
                 * Post format placeholder
                 * -------------------------------------------------------------
                 */
                else : ?>
                        <div class="frame">
                                <div class="image">
                                        <div class="dashicons format-placeholder dashicons-format-<?php echo $format ?>"></div>
                                        <div class="image-light"></div>
<?php                                   if( $caption ) : ?>
                                                <div class="caption">
                                                        <h5><?php the_title() ?></h5>
                                                </div>  
<?php                                   endif; // caption
                                        echo warta_image_links( '', TRUE ) // hover links ?>
                                </div><!--.image-->
                        </div><!--.frame-->
<?php                   echo warta_image_shadow(); // shadow
                endif; // has_post_thumbnail
            
        /**
         * Article tiny
         * =====================================================================
         */
        elseif( $size == 'tiny' || $size == 'small' ) :
                
                if(has_post_thumbnail()) : ?>
                        <a href="<?php the_permalink() ?>" class="image">
                                <?php the_post_thumbnail( $size == 'small' ? 'small' : 'thumbnail' ) ?>
                                <div class="image-light"></div>
                                <div class="link">
                                        <span class="dashicons dashicons-format-<?php echo $format ?>"></span>
                                </div>
                        </a>
<?php           else : ?>
                        <div class="image">
                                <div class="dashicons format-placeholder dashicons-format-<?php echo $format ?>"></div>
                                <div class="image-light"></div>
                        </div>
<?php           endif; // has_post_thumbnail
            
        endif; // article type
}
endif; // warta_featured_image