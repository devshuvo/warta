<?php

/* -------------------------------------------------------------------------- *
 * Page title
 * -------------------------------------------------------------------------- */

if ( ! function_exists( 'warta_get_page_title' ) ) :
/**
 * Returns HTML page title.
 * 
 * @since   1.0.0
 * 
 * @param   array   $args   {
 *      An array of arguments.
 * 
 *      @type   string  $primary    Title.
 *      @type   string  $secondary  Subtitle.
 *      @type   boolean $one_line   True if want to display 1 line and false if 2 lines.
 * }
 */
function warta_get_page_title( $args = array() ) {
        $r      = friskamax_parse_args( $args, array(
                        'primary'       => get_bloginfo( 'name' ), 
                        'secondary'     => get_bloginfo( 'description' ),
                        'one_line'      => false
                ), 'warta_get_page_title_r' );
        
        ob_start();
?> 

        <div id="title">
                <div class="image-light"></div>
                
                <div class="container">
                        <div class="title-container">

                                <?php if ( $r->one_line ) : ?>

                                        <h1>
                                                <?php if ( ! empty( $r->secondary ) ) : ?>
                                                        <span class="secondary"><?php echo $r->secondary ?></span>
                                                <?php endif ?>

                                                <?php if ( ! empty( $r->primary ) ) : ?>
                                                        <span class="primary"><?php echo $r->primary ?></span>
                                                <?php endif ?>
                                        </h1>

                                <?php else : ?>

                                        <?php if ( ! empty( $r->primary ) ) : ?>
                                                <h1 class="primary"><?php echo $r->primary ?></h1>
                                        <?php endif ?>

                                        <?php if ( ! empty( $r->secondary ) ) : ?>
                                                <p class="secondary"><?php echo $r->secondary ?></p>
                                        <?php endif ?>

                                <?php endif ?>

                        </div>
                </div>
        </div>

        <?php
        
        return apply_filters( 'warta_get_page_title', ob_get_clean(), $r );
}
endif; // ! function_exists( 'warta_get_page_title' )

if ( ! function_exists( 'warta_page_title' ) ) :
/**
 * Prints page title.
 * 
 * @since   1.0.0
 * @see     warta_get_page_title()
 * 
 * @param   string  $args           Arguments.
 * @param   string  $deprecated     Deprecated.
 * @param   boolean $deprecated_2   Deprecated.
 */
function warta_page_title( $args = array(), $deprecated = null, $deprecated_2 = null ) {
        
        if ( is_string( $args ) ) {                
                $args = array( 'primary' => $args );
                
                if ( $deprecated ) {     
                        $args[ 'secondary' ] = $deprecated;
                        _deprecated_argument( __FUNCTION__, '1.6.9.2' );
                }
                
                if ( $deprecated_2 ) {  
                        $args[ 'one_line' ] = $deprecated_2;
                        _deprecated_argument( __FUNCTION__, '1.6.9.2' );
                }
        }
        
        echo warta_get_page_title( $args );
        
}
endif;

if ( ! function_exists( 'warta_title_bg' ) ) :    
/**
 * Set page title background.
 */
function warta_title_bg() {        
        global $content_width;
        
        $o              = friskamax_get_options( array(
                                'title_bg'              => array( 'id' => 0 ),
                                'singular_title_bg'     => array( 'id' => 0 ),
                                'archive_title_bg'      => array( 'id' => 0 ),
                                'page_title_bg'         => array( 'id' => 0 ),
                        ), 'warta_title_bg_o' );
        $attachment_id  = 0;
                        
        // custom img
        if( is_single() || is_page() || ( function_exists('is_woocommerce') && is_woocommerce() ) || ( function_exists('is_bbpress') && is_bbpress() ) ) {                
                if( has_post_thumbnail() && get_post_meta( warta_get_page_id(), 'friskamax_page_title_op', true ) == 'featured' ) {
                        $attachment_id = get_post_thumbnail_id();
                } else  {
                        $attachment_id = (int) get_post_meta( warta_get_page_id(), 'friskamax_page_title_bg', true );    
                }
        } elseif ( is_category() ) {
                $cat_id         = get_query_var( 'cat' );
                $cat_meta       = friskamax_parse_args( get_option( "warta_cat_meta_$cat_id", array() ), array( 'page_title_bg' => 0 ), 'warta_title_bg_cat_meta' );
                $attachment_id  = (int) $cat_meta->page_title_bg;
        }
                
        
        // img from theme option
        if( ! $attachment_id ) {
                switch ( $GLOBALS['friskamax_warta_var']['page'] ) {     
                        case 'singular':
                                $attachment_id = $o->singular_title_bg['id']    ? (int) $o->singular_title_bg['id']     : 0;
                                break;
                        case 'archive':
                                $attachment_id = $o->archive_title_bg['id']     ? (int) $o->archive_title_bg['id']      : 0;
                                break;
                        case 'page':
                                $attachment_id = $o->page_title_bg['id']        ? (int) $o->page_title_bg['id']         : 0;
                                break;                                 
                } 
                
                $attachment_id = ! $attachment_id && $o->title_bg['id']         ? (int) $o->title_bg['id']              : $attachment_id;
        }

        ?>

        <?php if ( $attachment_id ) : ?>

                <?php                
                        /**
                         * Temporary change $content_width value.
                         * 
                         * @see http://wordpress.stackexchange.com/questions/167525/why-is-wp-get-attachment-image-src-returning-wrong-dimensions
                         */
                        $tmp_content_width      = $content_width;
                        $content_width          = 1920;
                
                        $attachment_large       = wp_get_attachment_image_src( $attachment_id, 'large' );
                        $attachment_huge        = wp_get_attachment_image_src( $attachment_id, 'huge' );
                        $attachment_full        = wp_get_attachment_image_src( $attachment_id, 'full' ); 
                        
                        $content_width          = $tmp_content_width;
                ?>

                <style>
                        #title { 
                                background-image: url('<?php echo esc_url( $attachment_large[0] ) ?>');
                                height          : <?php echo $attachment_large[2] ?>px; 
                        }

                        @media( min-width: 731px ) {
                                #title { 
                                        background-image: url('<?php echo esc_url( $attachment_huge[0] ) ?>');
                                        height          : <?php echo $attachment_huge[2] ?>px;
                                }
                        }

                        @media( min-width: 1367px ) { 
                                #title { 
                                        background-image: url('<?php echo esc_url( $attachment_full[0] ) ?>');
                                        height          : <?php echo $attachment_full[2] ?>px; 
                                }
                        }
                </style>

        <?php   endif; // $attachment_id ?>
                
        <?php
}    
endif; // warta_title_bg
add_action( 'wp_head', 'warta_title_bg', 20 );

/* -------------------------------------------------------------------------- *
 * Entry header
 * -------------------------------------------------------------------------- */

if ( ! function_exists( 'warta_get_entry_header' ) ) :
/**
 * Return post title and subtitle.
 * 
 * @param array $args                           Optional. Arguments.
 * @param string $args['before']                Optional. Opening tag.
 * @param string $args['after']                 Optional. Closing tag.
 * @param string $args['before_title']          Optional. Content to prepend to the title.
 * @param string $args['after_title']           Optional. Content to append to the title.
 * @param string $args['before_subtitle']       Optional. Content to prepend to the subtitle.
 * @param string $args['after_subtitle']        Optional. Content to append to the subtitle.
 * @param string $args['subtitle']              Optional. Post subtitle.
 * 
 * @return string
 */
function warta_get_entry_header( $args = array() ) {
        $o      = friskamax_get_options( array( 'title_content' => 'post_title' ), 'warta_get_entry_header_o' );        
        $r      = friskamax_parse_args( $args, array(
                        'before'                => '<header class="entry-header' . ( $o->title_content == 'post_title' ? ' hidden' : '' ) . '">',
                        'after'                 => '</header>',
                        'before_title'          => '<h1 class="entry-title">',
                        'after_title'           => '</h1>',
                        'before_subtitle'       => '<h2 class="entry-subtitle">',
                        'after_subtitle'        => '</h2>',
                        'subtitle'              => get_post_meta( get_the_ID(), 'friskamax_page_subtitle', true ),
                ), 'warta_get_entry_header_r' );
        
        $return = $r->before;
        $return .=      $r->before_title . get_the_title() . $r->after_title;
        $return .=      $r->subtitle ? $r->before_subtitle . $r->subtitle . $r->after_subtitle : '';
        $return .= $r->after;
        
        return apply_filters( 'warta_get_entry_header', $return, $r, $o );
}
endif;

if ( ! function_exists( 'warta_entry_header' ) ) :
/**
 * Display post title and subtitle.
 * 
 * @param array $args                           Optional. Arguments.
 * @param string $args['before']                Optional. Opening tag.
 * @param string $args['after']                 Optional. Closing tag.
 * @param string $args['before_title']          Optional. Content to prepend to the title.
 * @param string $args['after_title']           Optional. Content to append to the title.
 * @param string $args['before_subtitle']       Optional. Content to prepend to the subtitle.
 * @param string $args['after_subtitle']        Optional. Content to append to the subtitle.
 * @param string $args['subtitle']              Optional. Post subtitle.
 */
function warta_entry_header( $args = array() ) {
        echo warta_get_entry_header( $args );
}
endif; 

/* -------------------------------------------------------------------------- *
 * Page header
 * -------------------------------------------------------------------------- */

if ( ! function_exists( 'warta_get_page_header' ) ) :
/**
 * Return HTML page header.
 * 
 * @param array $args                   Optional. Arguments.
 * @param string $args[before]          Optional. Opening tag.
 * @param string $args[after]           Optional. Closing tag.
 * @param string $args[before_title]    Optional. Content to prepend to the title.
 * @param string $args[after_title]     Optional. Content to append to the title.
 * @param string $args[before_subtitle] Optional. Content to prepend to the subtitle.
 * @param string $args[after_subtitle]  Optional. Content to append to the subtitle.
 * @param string $args[title]           Optional. The title.
 * @param string $args[subtitle]        Optional. The subtitle.
 * 
 * @return string
 */
function warta_get_page_header( $args = array() ) {
        if ( ! warta_page_title_is_site_title() ) return;
        
        $r = friskamax_parse_args( $args, array(
                'before'                => '<header class="page-header">',
                'after'                 => '</header>',
                'before_title'          => '<h1 class="page-title">',
                'after_title'           => '</h1>',
                'before_subtitle'       => '<div class="page-subtitle">',
                'after_subtitle'        => '</div>',
                'title'                 => '',
                'subtitle'              => ''
        ), 'warta_get_page_header_r' );
        
        $return = $r->before;
        $return .=      $r->title ? $r->before_title . $r->title . $r->after_title : '';
        $return .=      $r->subtitle ? $r->before_subtitle . $r->subtitle . $r->after_subtitle : '';
        $return .= $r->after;
        
        return apply_filters( 'warta_get_page_header', $return, $r );
}        
endif; 

if ( ! function_exists( 'warta_page_header' ) ) :
/**
 * Prints the page header.
 * 
 * @param array $args                   Optional. Arguments.
 * @param string $args[before]          Optional. Opening tag.
 * @param string $args[after]           Optional. Closing tag.
 * @param string $args[before_title]    Optional. Content to prepend to the title.
 * @param string $args[after_title]     Optional. Content to append to the title.
 * @param string $args[before_subtitle] Optional. Content to prepend to the subtitle.
 * @param string $args[after_subtitle]  Optional. Content to append to the subtitle.
 * @param string $args[title]           Optional. The title.
 * @param string $args[subtitle]        Optional. The subtitle.
 */
function warta_page_header( $args = array() ) {
        echo warta_get_page_header( $args );
}        
endif; 