<?php
/**
 * Set body background
 * 
 * @package Warta
 */

if( !function_exists('warta_body_bg') ) :    
/**
 * Set body background
 */
function warta_body_bg() {    
    $o = friskamax_get_options( array(
            'body_bg' => array(
                'id' => 0,
            ),
    ), 'warta_body_bg_o' );
            
    if(isset($o->body_bg['id']) && $o->body_bg['id']) { 
        $attachment_src_huge    = wp_get_attachment_image_src($o->body_bg['id'], 'huge');
        $attachment_src_full    = wp_get_attachment_image_src($o->body_bg['id'], 'full');
        
        ?>

        <!--Body Bg-->
        <style>
            <?php if (isset($o->body_bg['width']) && $o->body_bg['width'] >= 1367) : ?>
                .boxed-style body  { background-size: cover; }
            <?php endif ?>
            
            @media(min-width: 828px) {
                .boxed-style body  { background-image: url('<?php echo esc_url( $attachment_src_huge[0] ) ?>') !important }
            }

            @media(min-width: 1367px) { 
                .boxed-style body  { background-image: url('<?php echo esc_url( $attachment_src_full[0] ) ?>') !important }
            }
        </style>

        <?php
    }
}    
endif; // warta_body_bg
add_action('wp_head', 'warta_body_bg', 666);
