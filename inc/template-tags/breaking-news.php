<?php
/**
 * Display breaking news
 * 
 * @package Warta
 */

if( !function_exists('warta_breaking_news') ) :
/**
 * Display breaking news
 * 
 * @global array $friskamax_warta Theme option values
 */
function warta_breaking_news() {
    global $friskamax_warta;
    
    $args = array(
        'posts_per_page'        => $friskamax_warta['breaking_news_count'],
        'ignore_sticky_posts'   => $friskamax_warta['breaking_news_ignore_sticky']
    );
        
    switch ($friskamax_warta['breaking_news_data']) {
        case 'cats':
            $args['category__in'] = $friskamax_warta['breaking_news_categories'];
            break;
        
        case 'tags':
            $args['tag__in'] = $friskamax_warta['breaking_news_tags'];
            break;
                
        case 'posts':
            $args['post__in'] = $friskamax_warta['breaking_news_posts'];
            break;
    }
    
    $the_query = new WP_Query( $args );

    if ( $the_query->have_posts() ) : ?>
        
        <section class="col-md-12 breaking-news">
            <header>
                <h4><?php echo $friskamax_warta['breaking_news_title'] ? strip_tags( $friskamax_warta['breaking_news_title'] ) : __('Breaking News', 'warta') ?></h4>
                <i class="triangle"></i>
            </header>
            <div class="content" 
                 data-duration="<?php echo (int) $friskamax_warta['breaking_news_duration'] ?>"
                 data-direction="<?php echo esc_attr( $friskamax_warta['breaking_news_direction']) ?>"
                 <?php if(is_rtl()) echo 'dir="ltr"' ?>>
                <ul <?php if(is_rtl()) echo 'dir="rtl"' ?>>
                    
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    
                        <li><a href="<?php the_permalink() ?>">
<?php                           if( $friskamax_warta['breaking_news_icon'] ) : ?>
                                        <i class="<?php echo esc_attr( $friskamax_warta['breaking_news_icon'] ) ?>"></i> 
<?php                           endif; // breaking_news_icon
                                the_title(); ?>
                            </a>
                        </li>
                    
                    <?php endwhile ?>
                        
                </ul>
            </div>
        </section>
            
    <?php endif;
    
    wp_reset_postdata(); // Restore original Post Data
    
} 
endif; // warta_breaking_news
