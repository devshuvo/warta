<?php

if ( ! function_exists( 'warta_get_post_meta' ) ) :
/**
 * Returns HTML with meta information for the current post-date/time and author.
 * 
 * @param array $args                           Arguments.
 * @param string $args[before]                  Optional. Before the post meta.
 * @param string $args[after]                   Optional. After the post meta.
 * @param boolean $args[meta_date]              Optional. Whether to display the date.
 * @param boolean $args[meta_format]            Optional. Whether to display the post format.
 * @param boolean $args[meta_category]          Optional. Whether to display the first category.
 * @param boolean $args[meta_categories]        Optional. Whether to display the categories.
 * @param boolean $args[meta_tags]              Optional. Whether to display the tags.
 * @param boolean $args[meta_author]            Optional. Whether to display the author.
 * @param boolean $args[meta_comments]          Optional. Whether to display the comments count.
 * @param boolean $args[meta_views]             Optional. Whether to display the view count.
 * @param boolean $args[meta_review_score]      Optional. Whether to display the review score.
 * @param boolean $args[meta_edit]              Optional. Whether to display the edit link.
 * @param string $args[date_format]             Optional. PHP date format.
 * @param string $args[class]                   Optional. Additional css class for the container.
 * 
 * @return string
 */
function warta_get_post_meta( $args = array() ) {
        $r      = friskamax_parse_args( $args, array( 
                        'before'                => '<p class="post-meta entry-meta %s">',
                        'after'                 => '</p>',
                        'meta_date'             => false,
                        'meta_format'           => false,
                        'meta_category'         => false,
                        'meta_categories'       => false,
                        'meta_author'           => false,
                        'meta_comments'         => false,
                        'meta_views'            => false, 
                        'meta_review_score'     => false,
                        'meta_edit'             => false,
                        'date_format'           => 'F j, Y',
                        'class'                 => ''
                ), 'warta_get_post_meta_r' );
        $return = '';
                
        if ( $r->meta_review_score )    $return .= warta_get_post_meta_review_score();
        if ( $r->meta_date )            $return .= warta_get_post_meta_date( array( 'date_format' => $r->date_format ) );
        if ( $r->meta_format )          $return .= warta_get_post_meta_format();
        if ( $r->meta_category )        $return .= warta_get_post_meta_category();
        if ( $r->meta_categories )      $return .= warta_get_post_meta_categories();
        if ( $r->meta_author )          $return .= warta_get_post_meta_author();
        if ( $r->meta_comments )        $return .= warta_get_post_meta_comments();
        if ( $r->meta_views )           $return .= warta_get_post_meta_views();
        if ( $r->meta_edit )            $return .= warta_get_edit_link();
        
        if( $return ) {
                $return = sprintf( $r->before, $r->class ) . $return . $r->after;
        }

        return apply_filters( 'warta_get_post_meta', $return, $r ); 
}
endif; // ! function_exists( 'warta_get_post_meta' )




if ( ! function_exists( 'warta_post_meta' ) ) :
/**
 * Display post meta for archive page and single post page.
 * 
 * @param string $class Custom class
 * @return string
 */
function warta_post_meta( $class='' ) {
        $page   = $GLOBALS['friskamax_warta_var']['page'];          
        $o      = friskamax_get_options( array(
                        $page . '_post_meta'    => array(),
                        $page . '_date_format'  => 'F j, Y'
                ), 'warta_post_meta_o' );        
        $meta   = friskamax_parse_args( $o->{$page . '_post_meta'}, array(
                        'date'          => 0,
                        'format'        => 0,
                        'category'      => 0,
                        'categories'    => 0,
                        'tags'          => 0,
                        'author'        => 0,
                        'comments'      => 0,
                        'views'         => 0, 
                        'review_score'  => 0,
                ), 'warta_post_meta_meta' ); 
                                                
        return  warta_get_post_meta( array(
                        'meta_date'             => $meta->date,
                        'meta_views'            => $meta->views,
                        'meta_review_score'     => $meta->review_score,
                        'meta_category'         => $meta->category,
                        'meta_categories'       => $meta->categories,
                        'meta_format'           => $meta->format,
                        'meta_comments'         => $meta->comments,
                        'meta_author'           => $meta->author,
                        'meta_edit'             => true,
                        'date_format'           => $o->{$page . '_date_format'},
                        'class'                 => $class
                ) );
}
endif;

if ( ! function_exists( 'warta_get_post_meta_date' ) ) :
/**
 * Returns HTML with meta information for the current post-date/time.
 * 
 * 
 * @param array $args                   Optional. Arguments
 * @param string $args[date_format]     Optional. PHP date format.
 * @param string $args[before]          Optional. Before the date text.
 * @param string $args[after]           Optional. After the date text.
 * @param string $args[link_open]       Optional. Opening <a> tag. %1$s represents url to the day archive. %2$s represents text in the title attribute.
 * @param string $args[link_close]      Optional. Closing </a> tag.
 * @param string $args[icon]            Optional. Date icon.
 * @param string $args[string]          Formatted string. %1$s represents the date.
 * 
 * @return string
 */
function warta_get_post_meta_date( $args = array() ) {
        $r      = friskamax_parse_args( $args, array(
                        'date_format'   => 'M j, Y',
                        'before'        => '',
                        'after'         => '',
                        'link_open'     => '<a href="%1$s" title="%2$s">',
                        'link_close'    => '</a> ',
                        'icon'          => '<i class="fa fa-clock-o"></i> ',
                        'string'        => _x( '<span class="sr-only">Posted on </span>%1$s', 'Post date. Text that wrapped with <span class="sr-only"></span> will only be visible to screen readers.', 'warta' ),
                ), 'warta_get_post_meta_date_r' );
        
        $date   = '<time class="published" datetime="' . esc_attr( get_the_time('c') ) . '">' . esc_html( get_the_time( $r->date_format ) ) . '</time>';
        $date   .= '<time class="updated hidden" datetime="' . esc_attr( get_the_modified_date( 'c' ) ) . '">' . esc_html( get_the_modified_date() ) . '</time>';
        
        $return = $r->before;
        $return .=       sprintf( 
                                $r->link_open, 
                                esc_url( get_day_link( 
                                        get_the_time( 'Y' ), 
                                        get_the_time( 'm' ), 
                                        get_the_time( 'd' )
                                ) ), 
                                esc_attr( sprintf( 
                                        __( "View all posts in %s", 'warta' ), 
                                        get_the_date( 'Y/m/d' ) 
                                ) ) 
                        );
        $return .=              $r->icon;
        $return .=              sprintf( $r->string, $date ); 
        $return .=      $r->link_close;
        $return .= $r->after;
        
        return apply_filters( 'warta_get_post_meta_date', $return, $r );
}
endif; // ! function_exists( 'warta_get_post_meta_date' )

if ( ! function_exists( 'warta_get_post_meta_format' ) ) :
/**
 * Returns HTML with meta information for the current post format.
 * 
 * @param array $args                   Optional. Arguments
 * @param string $args[before]          Optional. Before the post format text.
 * @param string $args[after]           Optional. After the post format text.
 * @param string $args[link_open]       Optional. Opening <a> tag. %1$s represents url to the post format archive. %2$s represents text in the title attribute.
 * @param string $args[link_close]      Optional. Closing </a> tag.
 * @param string $args[icon]            Optional. Post format icon.
 * @param string $args[string]          String format for sprintf(). %1$s represents the post format.
 * 
 * @return string
 */
function warta_get_post_meta_format( $args = array() ) {
        if ( ! get_post_format() ) return;
        
        $r              = friskamax_parse_args( $args, array(
                                'before'        => '',
                                'after'         => '',
                                'link_open'     => '<a href="%1$s" title="%2$s" class="entry-format">',
                                'link_close'    => '</a> ',
                                'icon'          => '<i class="dashicons dashicons-format-%1$s"></i> ',
                                'string'        => _x( '<span class="sr-only">Post format </span>%1$s', 'Post format.  Text that wrapped with <span class="sr-only"></span> will only be visible to screen readers.', 'warta' ),
                        ), 'warta_get_post_meta_format_r' );
        $post_format    = get_post_format();
        
        $return = $r->before;
        $return .=      sprintf( 
                                $r->link_open, 
                                esc_url( get_post_format_link( $post_format ) ), 
                                esc_attr( sprintf( __( "View all %s posts", 'warta' ), get_post_format_string( $post_format ) ) )
                        );
        $return .=              sprintf( $r->icon, $post_format );
        $return .=              sprintf( $r->string, get_post_format_string( $post_format ) ); 
        $return .=      $r->link_close;
        $return .= $r->after;
        
        return apply_filters( 'warta_get_post_meta_format', $return, $r );
}
endif; // ! function_exists( 'warta_get_post_meta_format' )

if ( ! function_exists( 'warta_get_post_meta_category' ) ) :
/**
 * Returns HTML with meta information for the current first post category.
 * 
 * @param array $args                   Optional. Arguments
 * @param string $args[before]          Optional. Before the category text.
 * @param string $args[after]           Optional. After the category text.
 * @param string $args[link_open]       Optional. Opening <a> tag. %1$s represents url to the category archive. %2$s represents text in the title attribute.
 * @param string $args[link_close]      Optional. Closing <a> tag.
 * @param string $args[icon]            Optional. Category icon.
 * @param string $args[string]          Optional. String format for sprintf(). %1$s represents the category.
 * 
 * @return string
 */
function warta_get_post_meta_category( $args = array() ) {
        if ( ! in_array( 'category', get_object_taxonomies( get_post_type() ) ) ) return;
        if ( ! warta_categorized_blog() ) return;
        
        $r              = friskamax_parse_args( $args, array(
                                'before'        => '<span class="cat-links">',
                                'after'         => '</span> ',
                                'link_open'     => '<a href="%1$s" title="%2$s" rel="category tag">',
                                'link_close'    => '</a>',
                                'icon'          => '<i class="fa fa-folder"></i> ',
                                'string'        => _x( '<span class="sr-only">Posted in </span>%1$s', 'Post categories. Text that wrapped with <span class="sr-only"></span> will only be visible to screen readers.', 'warta' ),
                        ), 'warta_get_post_meta_category_r' );        
        $categories     = get_the_category();
        
        $return = $r->before;
        $return .=      sprintf( 
                                $r->link_open, 
                                esc_url( get_category_link( $categories[0]->term_id ) ),
                                esc_attr( sprintf( __( "View all posts under %s category", 'warta' ), $categories[0]->name ) )
                        ); 
        $return .=              $r->icon;
        $return .=              sprintf( $r->string, $categories[0]->cat_name ); 
        $return .=      $r->link_close;
        $return .= $r->after;
        
        return apply_filters( 'warta_get_post_meta_category', $return, $r );        
}
endif; // ! function_exists( 'warta_get_post_meta_category' )

if ( ! function_exists( 'warta_get_post_meta_categories' ) ) :
/**
 * Returns HTML with meta information for the current post categories.
 * 
 * @param array $args                   Optional. Arguments
 * @param string $args[before]          Optional. Before the category links.
 * @param string $args[after]           Optional. After the category links.
 * @param string $args[link_open]       Optional. Opening <a> tag. %1$s represents url to the category archive. %2$s represents text in the title attribute.
 * @param string $args[link_close]      Optional. Closing <a> tag.
 * @param string $args[icon]            Optional. Category icon.
 * @param string $args[string]          Optional. String format for sprintf(). %1$s represents the categories.
 * 
 * @return string
 */
function warta_get_post_meta_categories( $args = array() ) {
        if ( ! in_array( 'category', get_object_taxonomies( get_post_type() ) ) ) return;
        if ( ! warta_categorized_blog() ) return;
        
        $r              = friskamax_parse_args( $args, array(
                                'before'        => '<span class="cat-links">',
                                'after'         => '</span> ',
                                'link_open'     => '<a href="%1$s" title="%2$s" rel="category tag">',
                                'link_close'    => '</a>',
                                'icon'          => '<i class="fa fa-folder"></i> ',
                                'separator'     =>  _x( ', ', 'Used between list items, there is a space after the comma.', 'warta' ),
                                'string'        => _x( '<span class="sr-only">Posted in </span>%1$s', 'Post categories. Text that wrapped with <span class="sr-only"></span> will only be visible to screen readers.', 'warta' ),
                        ), 'warta_get_post_meta_categories_r' );        
        $categories     = get_the_category();
        $counter        = 0;

        $category_links = '';
        
        foreach ( $categories as $category ) {      
                $category_links .= sprintf( 
                                        $r->link_open, 
                                        esc_url( get_category_link( $category->term_id ) ),
                                        esc_attr( sprintf( __( "View all posts under %s category", 'warta' ), $category->name ) )
                                );
                $category_links .=      $counter++ == 0 ? $r->icon : '';
                $category_links .=      $category->cat_name; 
                $category_links .= $r->link_close;
                $category_links .= $r->separator;
        }
        
        $category_links = trim( $category_links, $r->separator );
        
        $return = $r->before;
        $return .=      sprintf( $r->string, $category_links );
        $return .= $r->after;
        
        return apply_filters( 'warta_get_post_meta_meta_categories', $return, $r );
}
endif; // ! function_exists( 'warta_get_post_meta_categories' )

if ( ! function_exists( 'warta_get_post_meta_author' ) ) :
/**
 * Returns HTML with meta information for the current post author.
 * 
 * @param array $args                   Optional. Arguments
 * @param string $args[before]          Optional. Before the author text.
 * @param string $args[after]           Optional. After the author text.
 * @param string $args[link_open]       Optional. Opening <a> tag.  %1$s represents url to the author archive. %2$s represents text in the title attribute.
 * @param string $args[link_close]      Optional. Closing </a> tag.
 * @param string $args[icon]            Optional. Author icon.
 * @param string $args[string]          Optional. Formatted string for sprintf(). %1$s represent the author link.
 * 
 * @return string
 */
function warta_get_post_meta_author( $args = array() ) {
        $r      = friskamax_parse_args( $args, array(
                        'before'        => '',
                        'after'         => '',
                        'before'        => '<span class="byline"><span class="author vcard">',
                        'after'         => '</span></span>',
                        'link_open'     => '<a href="%1$s" title="%2$s" rel="author" class="url fn">',
                        'link_close'    => '</a> ',
                        'icon'          => '<i class="fa fa-user"></i> ',
                        'string'        => _x( '<span class="sr-only">by </span>%1$s', 'Post author. Text that wrapped with <span class="sr-only"></span> will only be visible to screen readers.', 'warta' ),
                ), 'warta_get_post_meta_author_r' );
        
        $author_link    = sprintf(
                                $r->link_open, 
                                esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
                                esc_attr( sprintf( __( "View all posts by %s", 'warta' ), get_the_author() ) )
                        );
        $author_link    .=      $r->icon;
        $author_link    .=      get_the_author_meta( 'nickname' ); 
        $author_link    .= $r->link_close;
        
        $return = $r->before;
        $return .=      sprintf( $r->string, $author_link ); 
        $return .= $r->after;
        
        return apply_filters( 'warta_get_post_meta_author', $return, $r );
}
endif; // ! function_exists( 'warta_get_post_meta_author' )

if ( ! function_exists( 'warta_get_post_meta_comments' ) ) :
/**
 * Returns HTML with meta information for the current post comments.
 * 
 * @param array $args                   Optional. Arguments
 * @param string $args[before]          Optional. Before the comment text.
 * @param string $args[after]           Optional. After the comment text.
 * @param string $args[link_open]       Optional. Opening <a> tag.  %1$s represents url to the current post comments. %2$s represents text in the title attribute.
 * @param string $args[link_close]      Optional. Closing </a> tag.
 * @param string $args[icon]            Optional. Comment icon.
 * @param string $args[string]          Optional. Formatted string for sprintf(). %d represent the comment count.
 * 
 * @return string
 */
function warta_get_post_meta_comments( $args = array() ) {
        if ( post_password_required() ) return;
        if ( ! comments_open() || ! get_comments_number() ) return;
        
        $r      = friskamax_parse_args( $args, array(
                        'before'        => '',
                        'after'         => '',
                        'link_open'     => '<a href="%1$s" title="%2$s">',
                        'link_close'    => '</a> ',
                        'icon'          => '<i class="fa fa-comments"></i> ',
                        'string'        => _n( "%s comment", "%s comments", get_comments_number(), 'warta' )
                ), 'warta_get_post_meta_comments_r' );

        $return = $r->before;
        $return .=      sprintf( 
                                $r->link_open, 
                                esc_url( get_comments_link() ),
                                esc_attr( sprintf( $r->string, get_comments_number() ) )
                        );
        $return .=              $r->icon;
        $return .=              sprintf( $r->string, warta_format_counts( get_comments_number() ) ); 
        $return .=      $r->link_close;
        $return .= $r->after;
        
        return apply_filters( 'warta_get_post_meta_comments', $return, $r );
}
endif; // ! function_exists( 'warta_get_post_meta_comments' )

if ( ! function_exists( 'warta_get_post_meta_views' ) ) :
/**
 * Returns HTML with meta information for the current post views.
 * 
 * @since 1.7.0
 * 
 * @param array $args {                   
 *      Optional. An array of arguments
 * 
 *      @type string $before            Before the view count text.
 *      @type string $after             After the view count text.
 *      @type string $link_open         Opening <a> tag.  %1$s: permalink for current post. %2$s: text in the title attribute.
 *      @type string $link_close        Closing <a> tag.
 *      @type string $icon              View icon.
 *      @type string $string            Formatted string for sprintf(). %s: view count.
 * }
 * @return string
 */
function warta_get_post_meta_views( $args = array() ) {
        $view_count     = (int) get_post_meta( get_the_ID(), 'warta_post_views_count', true );
        $r              = friskamax_parse_args( $args, array(
                                'before'        => '',
                                'after'         => '',
                                'link_open'     => '<a href="%1$s" title="%2$s">',
                                'link_close'    => '</a> ',
                                'icon'          => '<i class="fa fa-eye"></i> ',
                                'string'        => _n( "%s view", "%s views", $view_count, 'warta' ),
                        ), 'warta_get_post_meta_views_r' );
        
        if ( ! $view_count ) return;

        $return = $r->before;
        $return .=      sprintf( 
                                $r->link_open, 
                                esc_url( get_permalink() ),
                                esc_attr( sprintf( $r->string, $view_count ) )
                        );
        $return .=              $r->icon;
        $return .=              sprintf( $r->string, warta_format_counts( $view_count ) );
        $return .=      $r->link_close;
        $return .= $r->after;
        
        return apply_filters( 'warta_get_post_meta_views', $return, $r );
}
endif; // ! function_exists( 'warta_get_post_meta_views' )

if ( ! function_exists( 'warta_get_post_meta_review_score' ) ) :
/**
 * Returns HTML with meta information for the current review score.
 * 
 * @param array $args                   Optional. Arguments
 * @param string $args[before]          Optional. Before the review score.
 * @param string $args[after]           Optional. After the review score.
 * @param string $args[link_open]       Optional. Opening <a> tag. %1$s represents permalink for current post. %2$s represents text in the title attribute.
 * @param string $args[link_close]      Optional. Closing </a> tag.
 * 
 * @return string
 */
function warta_get_post_meta_review_score( $args = array() ) {
        $r      = friskamax_parse_args( $args, array(
                        'before'        => '',
                        'after'         => '',
                        'link_open'     => '<a href="%1$s" title="%2$s">',
                        'link_close'    => '</a> ',
                ), 'warta_get_post_meta_review_score_r' );
        $score  = (float) get_post_meta( get_the_ID(), 'friskamax_review_total', true );
        
        if ( ! $score ) return;

        $return = $r->before;
        $return .=      sprintf( 
                                $r->link_open,  
                                esc_url( get_permalink() ), 
                                sprintf( _x( '%s out of 5', 'Rating', 'warta' ), $score )
                        );
        $return .=              '<span data-rateit-value="' . $score . '" data-rateit-readonly="true" class="rateit"></span>';
        $return .=      $r->link_close;
        $return .= $r->after;
        
        return apply_filters( 'warta_get_post_meta_review_score', $return, $r );
}
endif; // ! function_exists( 'warta_get_post_meta_review_score' )

if ( ! function_exists( 'warta_get_edit_link' ) ) :
/**
 * Returns HTML edit post link.
 * 
 * @param array $args                   Optional. Arguments
 * @param string $args[before]          Optional. Before the link.
 * @param string $args[after]           Optional. After the link.
 * @param string $args[link_open]       Optional. Opening <a> tag. %1$s represents the edit post link. %2$s represents text in the title attribute.
 * @param string $args[link_close]      Optional. Closing </a> tag.
 * @param string $args[icon]            Optional. Edit icon.
 * @param string $args[text]            Optional. Edit text.
 * 
 * @return string
 */
function warta_get_edit_link( $args = array() ) {
        $r      = friskamax_parse_args( $args, array(
                        'before'        => '',
                        'after'         => '',
                        'link_open'     => '<a href="%1$s" title="%2$s">',
                        'link_close'    => '</a> ',
                        'icon'          => '<i class="fa fa-pencil"></i> ',
                        'text'          => __( 'Edit', 'warta' )
                ), 'warta_get_edit_link_r' );
        $link   = get_edit_post_link();
        
        if ( ! $link ) return;

        $return = $r->before;
        $return .=      sprintf( 
                                $r->link_open,  
                                esc_url( $link ), 
                                __('Edit post', 'warta')
                        );
        $return .=              $r->icon;
        $return .=              $r->text;
        $return .=      $r->link_close;
        $return .= $r->after;

        return apply_filters( 'warta_get_edit_link', $return, $r );
}
endif; // ! function_exists( 'warta_get_edit_link' )

/* ========================================================================== *
 * Helper
 * ========================================================================== */

/**
 * Find out if blog has more than one category.
 *
 * @return boolean true if blog has more than 1 category
 */
function warta_categorized_blog() {
        if ( false === ( $all_the_cool_cats = get_transient( 'warta_category_count' ) ) ) {
                // Create an array of all the categories that are attached to posts
                $all_the_cool_cats = get_categories( array(
                        'hide_empty' => 1,
                ) );

                // Count the number of categories that are attached to the posts
                $all_the_cool_cats = count( $all_the_cool_cats );

                set_transient( 'warta_category_count', $all_the_cool_cats );
        }

        if ( 1 !== (int) $all_the_cool_cats ) {
                // This blog has more than 1 category so warta_categorized_blog should return true
                return true;
        } else {
                // This blog has only 1 category so warta_categorized_blog should return false
                return false;
        }
}

/**
 * Flush out the transients used in warta_categorized_blog.
 *
 * @since Twenty Fourteen 1.0
 */
function warta_category_transient_flusher() {
        // Like, beat it. Dig?
        delete_transient( 'warta_category_count' );
}
add_action( 'edit_category', 'warta_category_transient_flusher' );
add_action( 'save_post',     'warta_category_transient_flusher' );

/* ========================================================================== *
 * Deprecated
 * ========================================================================== */

if ( ! function_exists( 'warta_posted_on' ) ) :
/**
 * Returns HTML with meta information for the current post-date/time and author.
 * 
 * @deprecated since version 1.6.9.2
 * 
 * @param array $args   Arguments.
 * @param string $class Optional. Additional css class for the container.
 * 
 * @return string
 */
function warta_posted_on( $args = array(), $class='' ) {
        $args['class'] = $class;
        
        _deprecated_function( __FUNCTION__, '1.6.9.2', 'warta_get_post_meta' );
        
        return warta_get_post_meta( $args );
}
endif; // ! function_exists( 'warta_posted_on' )