<?php
/**
 * Set page title background
 * 
 * @package Warta
 */

if( !function_exists('warta_title_bg') ) :    
/**
 * Set page title background
 * 
 * @global array $friskamax_warta Theme option values
 * @global array $friskamax_warta_var Theme variables
 */
function warta_title_bg() {
        global  $friskamax_warta, 
                $friskamax_warta_var;
        
        $attachment_id = 0;
                
        if( is_single() )
                $attachment_id = (int) get_post_meta(get_the_ID(), 'friskamax_page_title_bg', true );    
        
        if( !$attachment_id && isset( $friskamax_warta['title_bg']['id'] ) && !!$friskamax_warta['title_bg']['id'] ) {
                $attachment_id = (int) $friskamax_warta['title_bg']['id'];

                switch ( $friskamax_warta_var['page'] ) {        
                        case 'home':
                                if( $friskamax_warta['homepage_title_bg'] && $friskamax_warta['homepage_title_bg']['id'] )   
                                        $attachment_id = (int) $friskamax_warta['homepage_title_bg']['id'];
                                break;

                        case 'singular':
                                if( $friskamax_warta['singular_title_bg'] && $friskamax_warta['singular_title_bg']['id'] ) 
                                        $attachment_id = (int) $friskamax_warta['singular_title_bg']['id'];
                                break;

                        case 'archive':
                                if( $friskamax_warta['archive_title_bg'] && $friskamax_warta['archive_title_bg']['id'] ) 
                                    $attachment_id = (int) $friskamax_warta['archive_title_bg']['id'];
                                break;

                        case 'page':
                                if( $friskamax_warta['page_title_bg'] && $friskamax_warta['page_title_bg']['id'] )  
                                        $attachment_id = (int) $friskamax_warta['page_title_bg']['id'];
                                break;
                } // $friskamax_warta_var['page']
        }

        if( $attachment_id ) : 
                $attachment_large   = wp_get_attachment_image_src($attachment_id, 'large');
                $attachment_huge    = wp_get_attachment_image_src($attachment_id, 'huge');
                $attachment_full    = wp_get_attachment_image_src($attachment_id, 'full');
?>

                <style>
                        #title          { background-image: url('<?php echo esc_url( $attachment_large[0] ) ?>') !important }

                        @media(min-width: 731px) {
                                #title  { background-image: url('<?php echo esc_url( $attachment_huge[0] ) ?>') !important }
                        }

                        @media(min-width: 1367px) { 
                                #title  { background-image: url('<?php echo esc_url( $attachment_full[0] ) ?>') !important }
                        }
                </style>

<?php   endif; // $attachment_id
}    
endif; // warta_title_bg
add_action('wp_head', 'warta_title_bg', 666);
