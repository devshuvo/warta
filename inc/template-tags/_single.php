<?php
/* 
 * Template tags for Single Post Page
 * 
 * @since 1.6.5
 * @package warta\template-tags\single
 */

/* -------------------------------------------------------------------------- *
 * Featured image
 * -------------------------------------------------------------------------- */

if ( ! function_exists( 'warta_template_single_is_featured_image_included' ) ) :
/**
 * Does the user want to display the featured image?
 * 
 * @since 1.7.0
 * @return boolean
 */
function warta_template_single_is_featured_image_included() {
        global $friskamax_warta;

        $is_included    = isset( $friskamax_warta['hide_featured_image_all'] ) && ! isset( $friskamax_warta['singular_display_featured_image'] ) // old version (> 1.6.3.1)
                        ? ! $friskamax_warta['hide_featured_image_all']
                        : true;

        $o              = friskamax_get_options( array( 'singular_display_featured_image' => $is_included ), 'warta_template_single_is_featured_image_included_o' );
        
        $post_format    = get_post_format(); 
        $_post_format   = $post_format ? '_' . $post_format : '';

        return has_post_thumbnail() && $o->{ 'singular_display_featured_image' . $_post_format }; 
}
endif;

/* -------------------------------------------------------------------------- *
 * Review box
 * -------------------------------------------------------------------------- */

if ( ! function_exists( 'warta_template_single_review_box' ) ) :
/**
 * Display review box.
 * 
 * @since 1.7.0
 */
function warta_template_single_review_box() {
        $warta_review_box = new Warta_Review_Box();
                
        if ( $warta_review_box->is_review() ) {
                $warta_review_box->display();
        }
}
endif;

/* -------------------------------------------------------------------------- *
 * Share buttons
 * -------------------------------------------------------------------------- */

if ( ! function_exists( 'warta_template_single_get_share_buttons' ) ) :
/**
 * Get share buttons wrapper.
 * 
 * @since 1.7.0
 */
function warta_template_single_get_share_buttons() {
        $o = friskamax_get_options( array( 'singular_share_buttons_display' => true ), 'warta_template_single_get_share_buttons_o' );
                
        ob_start();
        
        ?>

        <?php if ( $o->singular_share_buttons_display ) : // Share buttons. ?>
                <section class="clearfix" data-share-buttons data-permalink="<?php the_permalink() ?>" data-title="<?php the_title_attribute() ?>">
                </section>
        <?php endif; ?>

        <?php
        
        return apply_filters( 'warta_template_single_get_share_buttons', ob_get_clean(), $o );
}
endif; 

if ( ! function_exists( 'warta_template_single_share_buttons' ) ) :
/**
 * Prints share buttons wrapper.
 * 
 * @see warta_template_single_get_share_buttons()
 * @since 1.7.0
 */
function warta_template_single_share_buttons() {
        echo warta_template_single_get_share_buttons();
}
endif;

/* -------------------------------------------------------------------------- *
 * Tags
 * -------------------------------------------------------------------------- */

if ( ! function_exists( 'warta_template_single_get_tags' ) ) :
/**
 * Retrieve the tags for a post formatted as a string.
 * 
 * @since 1.7.0
 * 
 * @param array $args                   Optional. Arguments.
 * @param string $args[before]          Optional. Opening tag.
 * @param string $args[after]           Optional. Closing tag.
 * @param string $args[before_title]    Optional. String to use before title.
 * @param string $args[after_title]     Optional. String to use after title.
 * @param string $args[before_tag]      Optional. String to use before tags.
 * @param string $args[separator]       Optional. String to use between the tags.
 * @param string $args[after_tag]       Optional. String to use after tags.
 * 
 * @return string
 */
function warta_template_single_get_tags( $args = array() ) {
        $r      = friskamax_parse_args( $args, array(
                        'before'        => '<section class="post-tags clearfix">',
                        'after'         => '</section>',
                        'before_title'  => '<h5>',
                        'after_title'   => '</h5>',
                        'before_tag'    => '<ul class="tags"><li>',
                        'separator'     => '</li><li>',
                        'after_tag'     => '</li></ul>'
                ), 'warta_template_single_get_tags_r' );
        $o      = friskamax_get_options( array(
                        'singular_post_meta'    => array( 'tags' => true ),
                        'singular_tag_text'     => __( 'Tags:', 'warta' )
                ), 'warta_template_single_get_tags_o' );
        
        if ( ! isset( $o->singular_post_meta['tags'] ) ||  ! $o->singular_post_meta['tags'] || ! get_the_tags() ) return;
        
        $return = $r->before;
        $return .=      $o->singular_tag_text ? $r->before_title . $o->singular_tag_text . $r->after_title : '';
        $return .=      get_the_tag_list( $r->before_tag, $r->separator, $r->after_tag );
        $return .= $r->after;
        
        return apply_filters( 'warta_template_single_get_tags', $return, $r, $o );
}
endif; 

if ( ! function_exists( 'warta_template_single_tags' ) ) :
/**
 * Prints the tags for a post formatted as a string.
 * 
 * @see warta_template_single_get_tags()
 * @since 1.7.0
 */
function warta_template_single_tags( $args = array() ) {
        echo warta_template_single_get_tags( $args );
}
endif;

/* -------------------------------------------------------------------------- *
 * Author Info
 * -------------------------------------------------------------------------- */

if ( ! function_exists( 'warta_template_single_get_author_info' ) ) :
/**
 * Get the author information.
 * 
 * @since 1.7.0
 * 
 * @param array $args                           Optional. Arguments.
 * @param string $args[before]                  Optional. Opening tag.
 * @param string $args[after]                   Optional. Closing tag.
 * @param string $args[before_title]            Optional. String to use before title.
 * @param string $args[after_title]             Optional. String to use after title.
 * @param string $args[before_avatar]           Optional. String to use before avatar. %1$s represents the post permalink. %2$s represents the title attribute of the <a>.
 * @param string $args[after_avatar]            Optional. String to use after avatar.
 * @param string $args[before_name]             Optional. String to use before name.
 * @param string $args[after_name]              Optional. String to use after name.
 * @param string $args[before_description]      Optional. String to use before description.
 * @param string $args[after_description]       Optional. String to use after description.
 * 
 * @return string
 */
function warta_template_single_get_author_info( $args = array() ) {
        $r      = friskamax_parse_args( $args, array(
                        'before'                => '<section class="widget author">',
                        'after'                 => '</section>',
                        'before_title'          => '<header class="clearfix"><h4>',
                        'after_title'           => '</h4></header>',
                        'before_avatar'         => '<a href="%1$s" class="avatar" title="%2$s">',
                        'after_avatar'          => '</a>',
                        'before_name'           => '<span class="name">',
                        'after_name'            => '</span><br>',
                        'before_description'    => '<p>',
                        'after_description'     => '</p>'
                ), 'warta_template_single_get_author_info_r' );
        $o      = friskamax_get_options( array(
                        'singular_author_info'          => true,
                        'singular_author_info_title'    => __( 'Author', 'warta' )
                ), 'warta_template_single_get_author_info_o' );
        
        if ( ! $o->singular_author_info ) return;
        
        $return = $r->before;
        $return .=      $o->singular_author_info_title ? $r->before_title . $o->singular_author_info_title . $r->after_title : '';
        $return .=      sprintf( 
                                $r->before_avatar, 
                                esc_url( get_author_posts_url( get_the_author_meta('ID') ) ),
                                esc_attr( sprintf( __( 'View all posts by %s', 'warta' ), get_the_author() ) )
                        );
        $return .=              get_avatar( get_the_author_meta( 'ID' ), 75 );
        $return .=              '<div class="image-light"></div>';
        $return .=      $r->after_avatar;
        $return .=      $r->before_name . get_the_author_link() . $r->after_name;
        $return .=      $r->before_description . get_the_author_meta( 'description' ) . $r->after_description;
        $return .=      warta_get_social_media_author();
        $return .= $r->after;    
        
        return apply_filters( 'warta_template_single_get_author_info', $return, $r, $o );
}
endif; // ! function_exists( 'warta_template_single_get_author_info' )

if ( ! function_exists( 'warta_template_single_author_info' ) ) :
/**
 * Prints the author information.
 * 
 * @see warta_template_single_get_author_info()
 * @since 1.7.0
 */
function warta_template_single_author_info( $args = array() ) {
        echo warta_template_single_get_author_info( $args );
}
endif; 

/* ========================================================================== *
 * Deprecated
 * ========================================================================== */

if ( ! function_exists( 'warta_template_single_is_display_featured_image' ) ) :
/**
 * Does the user want to display the featured image?
 * 
 * @deprecated since version 1.6.9.2
 * @since 1.6.5
 * @return boolean
 */
function warta_template_single_is_display_featured_image() {
        _deprecated_function( __FUNCTION__, '1.6.9.2', 'warta_template_single_is_featured_image_included' );
        return warta_template_single_is_featured_image_included();
}
endif;