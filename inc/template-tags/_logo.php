<?php
/**
 * Set logo 
 * 
 * @package Warta
 */

if ( ! function_exists( 'warta_get_logo' ) ) :
/**
 * Returns HTML logo.
 * @return string HTML logo.
 */
function warta_get_logo() {
        $o      = friskamax_get_options( array(
                        'logo'  => array( 'id' => 0 )
                ), 'warta_get_logo_o' );
        
        ob_start();
        
        if( $o->logo['id'] ) : ?>

                <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                        <h1 class="sr-only"><?php echo get_bloginfo( 'name' ) ?></h1>
                        <?php echo wp_get_attachment_image( $o->logo['id'], 'full' ) ?>
                </a>

        <?php endif;
        
        return apply_filters( 'warta_get_logo', ob_get_clean(), $o );
}
endif;

if ( ! function_exists( 'warta_logo' ) ) :    
/**
 * Display logo.
 */
function warta_logo() {
        echo warta_get_logo();
}    
endif; // warta_logo
