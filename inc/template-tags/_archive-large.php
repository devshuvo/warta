<?php
/* 
 * Functions for Archive Page with large image
 * 
 * @package Warta
 */

if ( ! function_exists( 'warta_get_template_archive_large_featured_media' ) ) :
/**
 * Return featured media for Archive Page with large images
 */
function warta_get_template_archive_large_featured_media() {
        $post_format = get_post_format();

        ob_start();

        switch ( $post_format ) :
                case 'audio':
                case 'video':
                case 'gallery': 
                case 'quote':
                case 'aside':                 
                case 'status':                 
                case 'chat':
                case 'link':
                        $featured_media = warta_match_featured_media();

                        if( $featured_media ) :
                                warta_featured_image( array('featured_media' => $featured_media) );
                        elseif( has_post_thumbnail() ) :
                                warta_featured_image();
                        else : ?>
                                <h4><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h4>
                                <?php echo warta_post_meta();
                        endif;
                        break;

                default: 
                        if( has_post_thumbnail() ) : 
                                warta_featured_image(); 
                        else: ?>
                                <h4><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h4>
                                <?php echo warta_post_meta();
                        endif;
                        break;
        endswitch; 

        return apply_filters( 'warta_get_template_archive_large_featured_media', ob_get_clean() );
}
endif;

if ( ! function_exists( 'warta_template_archive_large_featured_media' ) ) :
/**
 * Display featured media for Archive Page with large images
 */
function warta_template_archive_large_featured_media() {
        echo warta_get_template_archive_large_featured_media();
}
endif;




if ( ! function_exists( 'warta_get_template_archive_large_content' ) ) :
/**
 * Return content for Archive Page with large images 
 */
function warta_get_template_archive_large_content() {
        $o      = friskamax_get_options( array(
                        'archive_excerpt_length' => 320
                ), 'warta_get_template_archive_large_content_o' );

        ob_start();
        
        switch ( get_post_format() ) { 
                case 'aside':
                case 'status':                       
                case 'quote':                                        
                case 'chat':
                        break;

                case 'link':                                
                        if( ! warta_match_link() ) {
                                echo "<p>" . warta_the_excerpt_max_charlength( $o->archive_excerpt_length ) . "</p>";
                        }
                        break;

                default:
                        echo "<p>" . warta_the_excerpt_max_charlength( $o->archive_excerpt_length ) . "</p>";
                        break;
        }

        return apply_filters( 'warta_get_template_archive_large_content', ob_get_clean() );
}
endif; // ! function_exists( 'warta_get_template_archive_large_content' )

if ( ! function_exists( 'warta_template_archive_large_content' ) ) :
/**
 * Display content for Archive Page with large images 
 */
function warta_template_archive_large_content() {
        echo warta_get_template_archive_large_content();
}
endif; // ! function_exists( 'warta_template_archive_large_content' )