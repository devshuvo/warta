<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Warta
 */

require __DIR__ . '/paging-nav.php';
require __DIR__ . '/post-nav.php';
require __DIR__ . '/comment.php';
require __DIR__ . '/posted-on.php';
require __DIR__ . '/social-media.php';
require __DIR__ . '/carousel.php';
require __DIR__ . '/image-shadow.php';
require __DIR__ . '/image-links.php';
require __DIR__ . '/breaking-news.php';
require __DIR__ . '/title-bg.php';
require __DIR__ . '/page-title.php';
require __DIR__ . '/related-posts.php';
require __DIR__ . '/review-box.php';
require __DIR__ . '/featured-image.php';
require __DIR__ . '/article-footer.php';
require __DIR__ . '/body-bg.php';
require __DIR__ . '/logo.php';
require __DIR__ . '/ie8-support.php';
require __DIR__ . '/css-code.php';
require __DIR__ . '/js-code.php';