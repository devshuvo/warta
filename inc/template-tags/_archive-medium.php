<?php
/* 
 * Functions for Archive Page with medium image
 * 
 * @package Warta
 */

if( !function_exists('warta_get_template_archive_medium_featured_media') ) :
        /**
         * Return featured media for Archive Page with medium image
         */
        function warta_get_template_archive_medium_featured_media( $args = array() ) {
                $args           = apply_filters('warta_get_template_archive_medium_featured_media', $args);
                $r              = wp_parse_args( $args, array(
                                        'chat_count'                    => 3,
                                        'gallery_convert_to_carousel'   => TRUE,
                                ) );
        
                $featured_media = warta_match_featured_media( $r );
                $return         = warta_get_featured_image( array(
                                        'size'          => 'medium',
                                        'featured_media'=> $featured_media
                                )); 
                
                return apply_filters('warta_get_template_archive_medium_featured_media', $return, $r);
        }
endif;

if( !function_exists('warta_template_archive_medium_featured_media') ) :
        /**
         * Display featured media for Archive Page with medium image
         */
        function warta_template_archive_medium_featured_media() {
                echo warta_get_template_archive_medium_featured_media();
        }
endif;




if( !function_exists('warta_get_template_archive_medium_content') ) :
        /**
         * Return content for Archive Page with medium image
         */
        function warta_get_template_archive_medium_content() {
                global $friskamax_warta;
                
                $o              = wp_parse_args( (array) $friskamax_warta, array( 
                                        'archive_excerpt_length' => 320
                                ) );
                
                $post_format    = get_post_format();
                $title          = get_the_title();
                $excerpt_length = $o['archive_excerpt_length'];
                
                switch ($post_format) {  
                        case 'aside': 
                        case 'quote':                
                        case 'status':                 
                        case 'chat':
                        case 'link': 
                                $post_content   = $title ? '<h4><a href="' . esc_url( get_permalink() ) . '">' .  $title . '</a></h4>' : '';
                                $post_content   .= warta_post_meta();
                                break;

                        default:
                                $post_content   = $title ? '<h4><a href="' . esc_url( get_permalink() ) . '">' .  $title . '</a></h4>' : '';
                                $post_content   .= warta_post_meta();
                                $post_content   .= '<p>' . warta_the_excerpt_max_charlength( $excerpt_length ) . '</p>';
                                break;
                }
                
                return apply_filters('warta_get_template_archive_medium_content', $post_content);
        }
endif;

if( !function_exists('warta_template_archive_medium_content') ) :
        /**
         * Display content for Archive Page with medium image
         */
        function warta_template_archive_medium_content() {
                echo warta_get_template_archive_medium_content();
        }
endif;