<?php
/**
 * Display review box
 * 
 * @package Warta
 */

if( !function_exists('warta_review_box') ) :
/**
 * Display review box
 * 
 * @global array $friskamax_warta Warta theme options
 */
function warta_review_box() {
    global $friskamax_warta;
    
    $titles         = get_post_meta( get_the_ID(), 'friskamax_review_titles', true );
    $scores         = get_post_meta( get_the_ID(), 'friskamax_review_scores', true );
    $total          = (float) get_post_meta( get_the_ID(), 'friskamax_review_total', true );
    $summary        = get_post_meta( get_the_ID(), 'friskamax_review_summary', true );
    $user_rating    = (array) get_post_meta( get_the_ID(), 'friskamax_review_user_rating', true );
    
    $score_text     = explode(',', $friskamax_warta['review_box_score_text']);
    
    $has_voted      = isset( $_COOKIE['friskamax_review_user_rating'][ get_the_ID() ] ) 
                    || isset( $_SESSION['friskamax_review_user_rating'][ get_the_ID() ] );
    $read_only      =  '';
    
    if( !isset( $user_rating['value'] ) )
            $user_rating['value'] = 0;
    
    if( !!$has_voted ) {
        $vote_value = isset( $_COOKIE['friskamax_review_user_rating'][ get_the_ID() ] )
                    ? (float) $_COOKIE['friskamax_review_user_rating'][ get_the_ID() ]
                    : (float) $_SESSION['friskamax_review_user_rating'][ get_the_ID() ];
        
        $read_only  = 'data-rateit-readonly="true"';
    }
?>
    <section class="review no-margin-top">
        
<?php 
        if( $friskamax_warta['review_box_title'] ) {
            echo '<header><h5><strong>' . strip_tags( $friskamax_warta['review_box_title'] ) . '</strong></h5></header>'; 
        }
        
        foreach ( (array) $titles as $key => $value ) : 
            
            $score = (float) $scores[$key]; 
            
            echo strip_tags($value) . ': ' . $score;                    
            
            if( $friskamax_warta['review_box_score_type'] == 'star' ) : // Star Score 
?>
        
                <span data-rateit-value="<?php echo $score ?>" data-rateit-readonly="true" class="rateit pull-right"></span>
                <hr>
                
<?php       else : // Bar Score ?>
        
                <div class="progress progress-striped <?php if( $friskamax_warta['review_box_score_type'] == 'bar_animated') { echo 'active'; } ?>">
                    <div class="progress-bar" style="width: <?php echo $score / 5 * 100 ?>%"></div>
                </div>
        
<?php       
            endif; // $friskamax_warta['review_box_score_type']

        endforeach; // $titles 
        
        if( $friskamax_warta['review_box_score_type'] != 'star' ) { 
            echo '<hr>';
        }
?>

        <div class="review-total-score">
            <h1><?php echo $total ?></h1>
            <?php echo trim( strip_tags( $score_text[ --$total ] ) ) ?>
        </div>

        <div class="review-summary"><?php echo wp_kses_post( $summary ) ?></div>
        <hr>

<?php
        if( $friskamax_warta['review_box_enable_user_rating'] ) : 
?>
            <div class="user-rating">
                <?php
                    if( $user_rating['value'] ) {
                        echo wp_kses_post( 
                                sprintf( 
                                    $friskamax_warta['review_box_user_rating_text'], 
                                    '<span class="vote-value">' . (float) $user_rating['value'] . '</span>', 
                                    '<span class="vote-count">' . (int) $user_rating['count'] . '</span>'
                                ) );
                    } else {
                        echo wp_kses_post( $friskamax_warta['review_box_user_rating_not_rated'] );
                    }
                ?> 
                <span <?php echo $read_only ?> data-rateit-value="<?php echo (float) $user_rating['value'] ?>" class="rateit pull-right" data-id="<?php the_ID() ?>"></span>
                <small class="after-vote"><?php echo wp_kses_post( $friskamax_warta['review_box_user_rating_after_vote'] ) ?></small>

                <?php 
                    if( $has_voted ) {
                        echo '<br><small>' . wp_kses_post( sprintf( $friskamax_warta['review_box_user_rating_has_voted'], $vote_value )  ) . '</small>';
                    }
                ?>
            </div><!--.user-rating-->
            <hr>
            
<?php   endif // review_box_enable_user_rating ?>
            
    </section>   
<?php    
}
endif; // warta_review_box