<?php

/**
 * Initialize Meta Boxes.
 *
 * @package Warta
 */
 
require_once dirname( __FILE__ ) . '/_archive.php'; 
require_once dirname( __FILE__ ) . '/_full-carousel.php';
require_once dirname( __FILE__ ) . '/_page-builder.php';  
require_once dirname( __FILE__ ) . '/_review.php';  
require_once dirname( __FILE__ ) . '/_sidebar.php';
require_once dirname( __FILE__ ) . '/_title-bg.php';    
require_once dirname( __FILE__ ) . '/_page-subtitle.php';    


if ( ! function_exists( 'warta_meta_boxes_init' ) ) :        
/**
 * Calls the class on the post edit screen.
 */
function warta_meta_boxes_init() {
        $GLOBALS['warta_archive_meta_box']              = new Warta_Archive_Meta_Box();
        $GLOBALS['warta_full_width_carousel_meta_box']  = new Warta_Full_Width_Carousel_Meta_Box();
        $GLOBALS['warta_page_title_bg_meta_box']        = new Warta_Page_Title_BG_Meta_Box();
        $GLOBALS['warta_review_meta_box']               = new Warta_Review_Meta_Box();
        $GLOBALS['warta_sidebar_meta_box']              = new Warta_Sidebar_Meta_Box();
        
        wp_enqueue_script( 'warta-meta-boxes', get_template_directory_uri() . '/js/admin/meta-boxes.js' , array( 'jquery'), wp_get_theme()->get( 'Version' ), true );
}
endif; // ! function_exists( 'warta_meta_boxes_init' )
add_action( 'load-post.php', 'warta_meta_boxes_init' );
add_action( 'load-post-new.php', 'warta_meta_boxes_init' );