<?php

if ( ! function_exists( 'warta_Fsmpb_Meta_Box_Elements__tabs' ) ) :        
/**
 * Register Page Builder tabs.
 * @param array $tabs Array of tabs that contains grouped elements.
 */
function warta_Fsmpb_Meta_Box_Elements__tabs( $tabs ) {        
        $fsmpb_helper   = new Fsmpb_Helper();
        
        /* -------------------------------------------------------------------------- *
         * Warta Widgets tab
         * -------------------------------------------------------------------------- */
                
        $warta_widget_classes = array(
                'Warta_Advertisement',
                'Warta_Articles',
                'Warta_Breaking_News',
                'Warta_Feedburner',
                'Warta_Flickr_Feed',
                'Warta_Gallery_Carousel',
                'Warta_Posts_Carousel',
                'Warta_Slider_Tabs',
                'Warta_Social_Media',
                'Warta_Sub_Categories',
                'Warta_Tabs',
                'Warta_Text_Editor',
        );                
        
        if( warta_isset_twitter_api() ) {
                $warta_widget_classes[] = 'Warta_Twitter_Feed';
                asort( $warta_widget_classes ); 
        }             

        $tabs['fsmpb-tab--warta-widgets'] = array (
                'name'  => __( 'Warta Widgets', 'warta' ),                
                'items' => $fsmpb_helper->get_element_args__widget($warta_widget_classes)
        );

        /* -------------------------------------------------------------------------- *
         * WordPress Widgets tab
         * -------------------------------------------------------------------------- */
        
        $tabs['fsmpb-tab--wp-widgets']  = array(
                'name'  => __('WordPress Widgets', 'warta'),
                'items' => $fsmpb_helper->get_element_args__wp_widgets()
        );
        
        /* -------------------------------------------------------------------------- *
         * Plugin Widgets tab
         * -------------------------------------------------------------------------- */
        
        $plugin_widgets_args    = $fsmpb_helper->get_element_args__plugin_widgets();
        
        if( $plugin_widgets_args ) {
                $tabs['fsmpb-tab--plugin-widgets'] = array(
                        'name'  => __( 'Plugin Widgets', 'warta' ),
                        'note'  => __( "Note: widgets in this tab may not work properly. If it doesn't work, please use <strong>Widget Area</strong> instead. You can find it in tab <strong>Other</strong>.", 'warta' ),
                        'items' => $plugin_widgets_args
                );
        }
        
        /* -------------------------------------------------------------------------- *
         * Other tab
         * -------------------------------------------------------------------------- */
        $tabs['fsmpb-tab--other'] =  array(
                'name'  => __( 'Other', 'warta' ),                
                'items' => array( 
                                array(
                                        'type'          => 'custom',
                                        'php_class'     => 'Fsmpb_Element_Widget_Area',
                                ) 
                        )
        );        
        
        return $tabs;
}
endif; // ! function_exists( 'warta_Fsmpb_Meta_Box_Elements__tabs' )
add_filter( 'Fsmpb_Meta_Box_Elements__tabs', 'warta_Fsmpb_Meta_Box_Elements__tabs' );

/**
 * Change screen sizes variables.
 * 
 * @param array $screen_sizes
 * @return array
 */
function warta_Fsmpb_Meta_Box_Display__screen_sizes( $screen_sizes ) {        
        $screen_sizes['sm_min'] = 600;
        
        return $screen_sizes;
}
add_filter( 'Fsmpb_Meta_Box_Display__screen_sizes', 'warta_Fsmpb_Meta_Box_Display__screen_sizes' );