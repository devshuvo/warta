<?php
/**
 * Initialize Meta Boxes.
 *
 * @package Warta
 */

/**
 * Load Meta Boxes Classes
 * =============================================================================
 */
require __DIR__ . '/custom-sidebar.php';  
require __DIR__ . '/review.php';  
require __DIR__ . '/title-bg.php'; 
require __DIR__ . '/display-options.php';   

if( !function_exists('warta_meta_boxes_init') ) :
/**
 * Calls the class on the post edit screen.
 * =============================================================================
 */
function warta_meta_boxes_init() {
    new Warta_Custom_Sidebar_Meta_Box();
    new Warta_Review_Meta_Box();
    new Warta_Page_Title_BG_Meta_Box();
    new Warta_Display_Options_Meta_Box();
    
    wp_enqueue_style( 'warta-widgets', get_template_directory_uri() . '/css/admin/widgets.css'  );
    wp_enqueue_script( 'warta-widgets', get_template_directory_uri() . '/js/admin/widgets.js' , array('jquery'), '1.0', TRUE);
    wp_enqueue_script( 'warta-meta-boxes', get_template_directory_uri() . '/js/admin/meta-boxes.js' , array('jquery'), '1.0', TRUE);
}
endif;

if ( is_admin() ) {
    add_action( 'load-post.php', 'warta_meta_boxes_init' );
    add_action( 'load-post-new.php', 'warta_meta_boxes_init' );
}