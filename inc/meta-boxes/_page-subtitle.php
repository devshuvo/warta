<?php

if ( ! function_exists( 'warta_page_subtitle_metabox' ) ) :
/**
 * Prints the box content.
 * 
 * @param WP_Post $wp_post 	Post object
 */
function warta_page_subtitle_metabox( $wp_post ) {
        if ( $wp_post->post_type != 'post' && $wp_post->post_type != 'page' ) return;
        
        // Add an nonce field so we can check for it later.
        wp_nonce_field( 'warta_page_subtitle_metabox', 'warta_page_subtitle_metabox_nonce' );

        /*
         * Use get_post_meta() to retrieve an existing value
         * from the database and use the value for the form.
         */
        $subtitle = get_post_meta( get_the_ID(), 'friskamax_page_subtitle', true );
        
        ?>
                <p id="warta_page_subtitle_metabox">
                        <label>
                                <span class="screen-reader-text"><?php _e( 'Enter subtitle here', 'warta' ) ?></span>
                                <input class=           "widefat" 
                                       type=            "text" 
                                       name=            "warta_page_subtitle" 
                                       value=           "<?php echo esc_attr( htmlspecialchars( $subtitle ) ); ?>" 
                                       autocomplete=    "off" 
                                       style=           "font-size: 1.7em; padding: 3px 8px;" 
                                       placeholder=     "<?php _e( 'Enter subtitle here', 'warta' ) ?>"
                                >
                        </label>
                </p>
        <?php
}
endif;
add_action( 'edit_form_after_title', 'warta_page_subtitle_metabox' );

if ( ! function_exists( 'warta_page_subtitle_metabox_save' ) ) :
/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function warta_page_subtitle_metabox_save( $post_id ) {

        /* We need to verify this came from our screen and with proper authorization,
         * because the save_post action can be triggered at other times. */

        // Check if our nonce is set.
        if ( ! isset( $_POST['warta_page_subtitle_metabox_nonce'] ) ) return;

        // Verify that the nonce is valid.
        if ( ! wp_verify_nonce( $_POST['warta_page_subtitle_metabox_nonce'], 'warta_page_subtitle_metabox' ) ) return;
      

        // If this is an autosave, our form has not been submitted, so we don't want to do anything.
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

        // Check the user's permissions.
        if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
                if ( ! current_user_can( 'edit_page', $post_id ) ) return;
        } else {
                if ( ! current_user_can( 'edit_post', $post_id ) ) return;
        }

        /* OK, it's safe for us to save the data now. */
	
        // Make sure that it is set.
        if ( ! isset( $_POST['warta_page_subtitle'] ) ) return;

        // Sanitize user input.
        $subtitle = sanitize_text_field( $_POST['warta_page_subtitle'] );

        // Update the meta field in the database.
        update_post_meta( $post_id, 'friskamax_page_subtitle', $subtitle );
}
endif; //  ! function_exists( 'warta_page_subtitle_metabox_save' )
add_action( 'save_post', 'warta_page_subtitle_metabox_save' );