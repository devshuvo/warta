<?php

if ( ! class_exists( 'Warta_Archive_Meta_Box' ) ) :
/**
 * Archive Meta Box Class
 */
class Warta_Archive_Meta_Box {
        /**
         * Hook into the appropriate actions when the class is constructed.
         */
        public function __construct() {
                add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
                add_action( 'save_post', array( $this, 'save' ) );
        }

        /**
         * Adds the meta box container.
         */
        public function add_meta_box( $post_type ) {
                if ( $post_type != 'page' ) return;
                add_meta_box( 'warta_archive_meta_box', __( 'Template Archive Option', 'warta' ), array( $this, 'render_meta_box_content' ) ,'page', 'side', 'default' );
        }

        /**
         * Save the meta when the post is saved.
         *
         * @param int $post_id The ID of the post being saved.
         */
        public function save( $post_id ) {

                /* We need to verify this came from the our screen and with proper authorization,
                 * because save_post can be triggered at other times. */

                // Check if our nonce is set.
                if ( ! isset( $_POST['warta_archive_meta_box_nonce'] ) ) return $post_id;

                // Verify that the nonce is valid.
                if ( ! wp_verify_nonce( $_POST['warta_archive_meta_box_nonce'], 'warta_archive_meta_box' ) ) return $post_id;

                // If this is an autosave, our form has not been submitted, so we don't want to do anything.
                if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return $post_id;

                // Check the user's permissions.
                if ( 'page' == $_POST['post_type'] ) {
                        if ( ! current_user_can( 'edit_page', $post_id ) ) return $post_id;
                } else {
                        return $post_id;
                }

                /* OK, its safe for us to save the data now. */

                // Sanitize the user input.
                $category = ( int ) $_POST['warta_archive_category'];

                // Update the meta field.
                update_post_meta( $post_id, 'friskamax_archive_category', $category );
        }


        /**
         * Render Meta Box content.
         *
         * @param WP_Post $post The post object.
         */
        public function render_meta_box_content( $post ) {

                // Add an nonce field so we can check for it later.
                wp_nonce_field( 'warta_archive_meta_box', 'warta_archive_meta_box_nonce' );

                // Use get_post_meta to retrieve an existing value from the database.
                $category = get_post_meta( $post->ID, 'friskamax_archive_category', true );

                ?>

                <label>
                        <?php _e( 'Category:', 'warta' ) ?>
                        <?php wp_dropdown_categories( array(
                                'name'                  => 'warta_archive_category',
                                'selected'              => $category,
                                'hierarchical'          => true,
                                'show_option_all'       => __( 'Show all categories', 'warta' )
                        ) ); ?>
                </label>

                <?php
        }
}
endif; // ! class_exists( 'Warta_Archive_Meta_Box' )