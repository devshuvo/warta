<?php

/**
 * ReduxFramework config file.
 * 
 * @link https://docs.reduxframework.com 
 * @since 1.0.0
 * @package warta\admin\theme-options
 */

if ( ! class_exists( "Redux_Framework_warta" ) ) :
        
/**
 * ReduxFramework config class.
 * 
 * @since 1.0.0
 */
class Redux_Framework_warta {        
        public $args            = array();
        public $sections        = array();
        public $theme;
        public $ReduxFramework;

        /**
         * Initialize ReduxFramework configuration.
         * 
         * @since 1.0.0
         * @access private
         */
        public function __construct() {
                // Return if ReduxFramework plugin is not installed.
                if ( ! class_exists( 'ReduxFramework' ) ) return;

                // Set the default arguments
                $this->setArguments();

                // Create the sections and fields
                $this->setSections();

                // Adds custom css.
                add_action( 'redux/page/' . $this->args['opt_name'] . '/enqueue', array($this, 'add_panel_css') ); 
                
                // If Redux is running as a plugin, this will remove the demo notice and links
                add_action( 'redux/loaded', array( $this, 'remove_demo' ) );

                // Function to test the compiler hook and demo CSS output.
                // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
                add_filter( 'redux/options/'.$this->args['opt_name'].'/compiler', array( $this, 'compiler_action' ) );
                
                // Run the compiler when the theme has been updated.
                add_action( 'friskamax_after_update_theme', array( $this, 'compiler_action' ) );

                $this->ReduxFramework = new ReduxFramework( $this->sections, $this->args );
        }
        
        /**
         * Setup Redux arguments.
         * 
         * @link http://docs.reduxframework.com/core/arguments/
         * @since 1.0.0
         * @access private
         */
        public function setArguments() {
                $this->args = array(
                        // TYPICAL -> Change these values as you need/desire
                        'opt_name'              => 'friskamax_warta',                           // This is where your data is stored in the database and also becomes your global variable name.
                        'display_name'          => __( 'Warta Theme Options', 'warta' ),        // Name that appears at the top of your panel.
                        'display_version'       => '',                                          // Version that appears at the top of your panel.
                        'menu_type'             => 'menu',                                      // Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only).
                        'allow_sub_menu'        => true,                                        // Show the sections below the admin menu item or not.
                        'menu_title'            => 'Warta',
                        'page_title'            => __( 'Warta Theme Options', 'warta' ),
                        
                        // You will need to generate a Google API key to use this feature.
                        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
                        'google_api_key'        => 'AIzaSyBbUsxUE6vWOArBrQv82op-0a6hf9AijG0',   // Must be defined to add google fonts to the typography module.
                        //'google_update_weekly'=> false,                                       // Set it you want google fonts to update weekly. A google_api_key value is required.
                        
                        'async_typography'              => false,                               // Use a asynchronous font on the front end or font string.
                        //'disable_google_fonts_link'   => true,                                // Disable this in case you want to create your own google fonts loader.
                        'admin_bar'                     => true,                                // Show the panel pages on the admin bar.
                        //'admin_bar_icon'              => 'dashicons-portfolio',               // Show the panel pages on the admin bar.
                        //'admin_bar_priority'          => 50,                                  // Choose an icon for the admin bar menu.
                        'global_variable'               => '',                                  // Set a different name for your global variable other than the opt_name.
                        'dev_mode'                      => false,                               // Show the time the page took to load, etc.
                        'update_notice'                 => false,                               // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo.
                        'customizer'                    => true,                                // Enable basic customizer support.
                        //'open_expanded'               => true,                                // Allow you to start the panel in an expanded way initially.
                        //'disable_save_warn'           => true,                                // Disable the save warning when a user changes a field.
                        
                        // OPTIONAL -> Give you extra features
                        'page_priority'         => null,                                        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
                        'page_parent'           => 'themes.php',                                // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
                        'page_permissions'      => 'manage_options',                            // Permissions needed to access the options panel.
                        'menu_icon'             => '',                                          // Specify a custom URL to an icon.
                        'last_tab'              => '',                                          // Force your panel to always open to a specific tab (by id).
                        'page_icon'             => 'icon-themes',                               // Icon displayed in the admin panel next to your menu_title.
                        'page_slug'             => 'warta-options',                             // Page slug used to denote the panel.
                        'save_defaults'         => true,                                        // On load save the defaults to DB before user clicks save or not.
                        'default_show'          => false,                                       // If true, shows the default value next to each field that is not the default value.
                        'default_mark'          => '',                                          // What to print by the field's title if the value shown is default. Suggested: *.
                        'show_import_export'    => true,                                        // Shows the Import/Export panel when not used as a field.

                        // CAREFUL -> These options are for advanced use only
                        'transient_time'        => 60 * MINUTE_IN_SECONDS,
                        'output'                => true,                                         // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output.
                        'output_tag'            => true,                                         // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head.
                        // 'footer_credit'      => '',                                           // Disable the footer credit of Redux. Please leave if you can help it.
                
                        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
                        'database'              => '',                                          // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
                        'system_info'           => false,                                       // REMOVE.
                        
                        'ajax_save'             => false,                                       // Disable the use of ajax saving for the panel.
                );    
        }
        
        /**
         * Setup Redux sections.
         * 
         * @link http://docs.reduxframework.com/core/sections/getting-started-with-sections/
         * @since 1.0.0
         * @access private
         */
        public function setSections() {
                global $friskamax_warta_var;
                
                $dir = dirname(__FILE__);
                
                require $dir . '/_section_general.php';     
                require $dir . '/_section_appearance.php';    
                require $dir . '/_section_header.php';     
                require $dir . '/_section_footer.php';     
                require $dir . '/_section_top-menu.php';       
                //require $dir . '/_section_typography.php';      
        
                $this->sections[] = array( 'type' => 'divide' );

                require $dir . '/_section_archive.php';  
                require $dir . '/_section_singular.php';  
                require $dir . '/_section_gallery-page.php';  
                require $dir . '/_section_static-page.php';  
                require $dir . '/_section_contact-page.php';  

                $this->sections[] = array( 'type' => 'divide' );

                require $dir . '/_section_featured-media.php';     
                require $dir . '/_section_twitter.php';     

                $this->sections[] = array( 'type' => 'divide' );

                require $dir . '/_section_advanced.php';     
        }
         
        /**
         * Add custom css files.
         * 
         * @since 1.0.0
         * @access private
         */
        public function add_panel_css() {
                if( is_rtl() ) {
                       wp_enqueue_style( 'redux-custom-css-rtl', get_template_directory_uri() . '/css/admin/redux-rtl.css', array( 'redux-css' ), wp_get_theme()->get( 'Version' ) );
                }
                
                wp_enqueue_script( 'redux-custom-js', get_template_directory_uri() . '/js/admin/redux.js', array('jquery', 'iris'), wp_get_theme()->get( 'Version' ), true );
        }
        
        /**
         * Remove the demo link and the notice of integrated demo from the redux-framework plugin.
         * 
         * @since 1.0.0
         * @access private
         */
        public function remove_demo() {
                // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
                if (class_exists('ReduxFrameworkPlugin')) {
                        remove_filter('plugin_row_meta', array(ReduxFrameworkPlugin::instance(), 'plugin_metalinks'), null, 2);

                        // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                        remove_action('admin_notices', array(ReduxFrameworkPlugin::instance(), 'admin_notices'));
                }
        }
        
        /**
         * Recompile the LESS files.
         * 
         * This only runs if a field set with compiler => true is changed.
         * The compiling proses is handled by Less.php.
         * 
         * @link http://lessphp.gpeasy.com/             LESS compiler.
         * @link http://wordpress.org/plugins/lessphp/  WordPress plugin that provide the Less.php library.
         * @since 1.4.0
         */
        public function compiler_action() {                   
                global $wp_filesystem;
                
                // If lessphp plugin is not installed, stop right here.
                if ( ! class_exists( 'Less_Parser' ) ) return;
                                
                /**
                 * Check for error from last compiling process.
                 */
                
                if ( isset( $_SESSION['warta_compiler_fatal_error'] ) ) {                         
                        $_SESSION['friskamax_admin_notices'][] = array(
                                'class'         => 'error',
                                'message'       => $this->compiler_get_error_message( $_SESSION['warta_compiler_fatal_error'] ),
                                'unset'         => true
                        );
                        
                        unset( $_SESSION['warta_compiler_fatal_error'] );
                        
                        // Return. 
                        // This is important, otherwise the compiler will run again if user access the admin page and keep getting the fatal error.
                        return;
                }
                 
                /**
                 * Setup variables.
                 */
                
                $o = friskamax_get_options( array(
                        'primary-color'         => '',
                        'secondary-color'       => '',
                        'headings-link-color'   => '',
                        'body-link-color'       => '',
                        'typography_body'       => array( 'font-family' => '' ),
                        'typography_headings'   => array( 'font-family' => '' ),
                ), 'Redux_Framework_warta__compiler_action_o', false );
                $o['typography_body']['font-family']            = isset( $o['typography_body']['font-family'] )         ? $o['typography_body']['font-family']          : '';
                $o['typography_headings']['font-family']        = isset( $o['typography_headings']['font-family'] )     ? $o['typography_headings']['font-family']      : '';
                
                $input  = get_template_directory() . '/css/less/style.less';
                $output = get_template_directory() . '/css/style.min.css';
                $vars   = apply_filters( 'warta_less_variables', array(
                                'primary'               => $o['primary-color'],
                                'secondary'             => $o['secondary-color'],
                                'headings-link-color'   => $o['headings-link-color'],
                                'body-link-color'       => $o['body-link-color'],
                                'body-font'             => $o['typography_body']['font-family'],
                                'headings-font'         => $o['typography_headings']['font-family'],
                        ), $o );
                $vars   = array_filter( $vars, 'friskamax_is_not_empty' );
                
                /**
                 * Setup Less_Parser and start compiling.
                 */        
                
                $parser = new Less_Parser(array( 
                        'compress'      => true,
                        'relativeUrls'  => false
                ));
                
                $parser->parseFile( $input );
                $parser->ModifyVars( $vars );
                
                try {                        
                        register_shutdown_function( array( $this, 'compiler_fatal_handler' ) );

                        $new_css = $parser->getCss();
                } catch( Exception $e ){
                        $_SESSION['friskamax_admin_notices'][] = array(
                                'class'         => 'error',
                                'message'       => '<p>' . $e->getMessage() . '</p>',
                                'unset'         => true
                        );

                        return;
                }
                
                /**
                 * Updating CSS file.
                 */
                
                if( empty( $wp_filesystem ) ) {
                        require_once( ABSPATH .'/wp-admin/includes/file.php' );
                        WP_Filesystem();
                }                
                
                if( $wp_filesystem && $new_css ) {
                        if( ! $wp_filesystem->put_contents( $output, $new_css, FS_CHMOD_FILE ) ) {
                                $_SESSION['warta_new_css']      = $new_css;
                                $warta_update_css_url           = wp_nonce_url ( 
                                                                        add_query_arg( 'page', 'warta-update-css', admin_url( 'index.php' ) ), 
                                                                        'warta_update_css_action', 
                                                                        'warta_update_css_nonce'
                                                                );
                                
                                // Redirect to CSS updater page if $wp_filesystem cannot write the file directly.
                                // '&amp;' needs to be replaced with '&' if we want to use wp_redirect() or wp_safe_redirect()
                                $warta_update_css_url           = str_replace( '&amp;', '&', $warta_update_css_url );
                                
                                wp_safe_redirect( esc_url_raw( $warta_update_css_url ) );
                        }
                }
        }
        
        /**
         * Get compiler error message if any.
         * 
         * Additionally, add solutions for common errors.
         * 
         * @since 1.7.1
         * @access private
         * 
         * @param array $error  The last occurred error. Retrieved from error_get_last().
         * @return string
         */
        public function compiler_get_error_message( $error ) {
                if ( strpos( $error['message'], 'memory' ) !== false ) {
                        ob_start();

                        ?>
                                <p><?php _e( 'Please increase the maximum amount of memory that can be consumed by PHP. You can try one of these methods:', 'warta' ); ?></p>
                                <ol>
                                        <li>
                                                <?php printf( __( 'Add the following to your <code>wp-config.php</code> file in <code>%s</code> above the line reading <code>/* That\'s all, stop editing! Happy blogging. */</code>.', 'warta' ), ABSPATH ); ?>
                                                <pre>define( 'WP_MEMORY_LIMIT', '96M' );</pre>
                                        </li>
                                        <li>
                                                <?php _e( 'If you have access to PHP.ini file, increase the value of <code>memory_limit</code>.', 'warta' ); ?>
                                                <pre>memory_limit = 96M</pre>
                                        </li>
                                        <li>
                                                <?php printf( __( 'Add the following to your <code>.htaccess</code> file in <code>%s</code>. If the file doesn\'t exist, please create a new one.' , 'warta' ), ABSPATH ); ?>
                                                <pre>php_value memory_limit 96M</pre>
                                        </li>
                                        <li>
                                                <?php _e( 'If none of the above works, your host may not allow you change this setting. In this case, you should contact your host.', 'warta' ) ?>
                                        </li>
                                </ol>
                                <p><em><?php _e( 'Note: change the value <strong>96M</strong> to a value that suits your need as it may be different from one and another.', 'warta' ) ?></em></p>
                        <?php

                        $solution = ob_get_clean();
                } else {
                        $solution       = '<p>' 
                                                . __( 'If you continue to see this message, please contact us via <a href="http://themeforest.net/user/friskamax" target="_blank">http://themeforest.net/user/friskamax</a>.', 'warta' )                                
                                        . '</p>';
                }

                $message = '<h3>' . __( 'Error While Updating the CSS File', 'warta' ) . '</h3>';
                $message .= '<p><code>' . $error['message'] . ' in ' . $error['file'] . ' on line ' . $error['line'] . '</code></p>';  
                
                return $message . $solution;
        }
        
        /**
         * Fatal error handler when the sass compiler runs.
         * 
         * Registered with register_shutdown_function().
         * If the error type is a fatal run-time error, then save the error 
         * in a session variable.
         * 
         * @since 1.7.1
         * @access private
         */
        public function compiler_fatal_handler() {
                $error = error_get_last();
                
                if ( $error !== null && $error['type'] === E_ERROR ) {
                        $_SESSION['warta_compiler_fatal_error'] = $error;
                }
        }
}

$Redux_Framework_warta = new Redux_Framework_warta();

endif; //  ! class_exists( "Redux_Framework_warta" )