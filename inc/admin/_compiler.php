<?php 

function warta_display_submenu_page_update_css() {
        global $wp_filesystem;
        
        // Return if there's no new CSS.
        if ( ! isset( $_SESSION['warta_new_css'] ) ) return;
        
        // Verify that a nonce is correct and unexpired with the respect to a specified action. 
        if ( ! isset( $_GET['warta_update_css_nonce'] ) || ! wp_verify_nonce( $_GET['warta_update_css_nonce'], 'warta_update_css_action') ) return;
        
        echo '<h2>' . __('Updating CSS File', 'warta') . '</h2>';
        		
		// okay, let's see about getting credentials
		$url    = wp_nonce_url ( 
                        add_query_arg( 'page', 'warta-update-css', admin_url( 'index.php' ) ), 
                        'warta_update_css_action', 
                        'warta_update_css_nonce'
                );
        
		if ( false === ( $creds = request_filesystem_credentials( esc_url_raw( $url ) ) ) ) {		
                // if we get here, then we don't have credentials yet,
                // but have just produced a form for the user to fill in, 
                // so stop processing for now

                return; // stop the normal page form from displaying
		}
			
		// now we have some credentials, try to get the wp_filesystem running
		if ( ! WP_Filesystem( $creds ) ) {
                // our credentials were no good, ask the user for them again
                request_filesystem_credentials( esc_url_raw( $url ), '', true );
                
                return;
		}
        
		// by this point, the $wp_filesystem global should be working, so let's use it to create a file		
        $filename = $wp_filesystem->wp_themes_dir() . get_template() . '/css/style.min.css';
        
		if ( ! $wp_filesystem->put_contents( $filename, $_SESSION['warta_new_css'], FS_CHMOD_FILE) ) {  
                ?>
                        <div class="error">
                                <p><?php printf( __( 'Error! Cannot update %s. Please make sure the file permission is correct.', 'warta' ), $filename ); ?></p>
                        </div>
                <?php
		} else {
                unset( $_SESSION['warta_updated_css'] );
                ?>
                        <p><?php _e( 'The CSS file has been successfully updated.', 'warta' ) ?></p>
                        <p><a href="<?php echo admin_url( 'admin.php?page=warta-options' ); ?>" class="button-primary"><?php _e( 'Back to Theme Options page', 'warta' ); ?></a></p>
                <?php
        }    
	
        return true;
}

/**
 * add the admin options page
 */
function warta_add_submenu_page_update_css() {    
        add_submenu_page( 
                null, 
                __('Update CSS File', 'warta'), 
                __('Update CSS', 'warta'), 
                'manage_options', 
                'warta-update-css', 
                'warta_display_submenu_page_update_css' 
        ); 
}
add_action('admin_menu', 'warta_add_submenu_page_update_css');



