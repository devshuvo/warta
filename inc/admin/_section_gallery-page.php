<?php
/**
 * Singular Page Settings.
 *
 * @package Warta
 */

$this->sections[] = array(
        'icon'      => 'el-icon-picture',
        'title'     => __('Gallery Post', 'warta'),
        'fields'    => array(
                array(
                        'id'       => 'gallery_page_default',
                        'type'     => 'switch', 
                        'title'    => __('Use default WordPress style', 'warta'),
                        'default'  => 0,
                ),
                array(
                        'id'      => 'gallery_page_caption_length',
                        'type'    => 'slider', 
                        'title'   => __('Caption Length', 'warta'),
                        'subtitle'=> __('How many characters of the caption do you want to show?', 'warta'),
                        'default' => '60',
                        'min'     => '1',
                        'step'    => '1',
                        'max'     => '100',
                        'validate'=> 'numeric',
                        'required'  => array( 'gallery_page_default', '=', 0 )
                ),
                array(
                        'id'       => 'gallery_page_columns',
                        'type'     => 'select',
                        'title'    => __( 'Columns', 'warta' ), 
                        'subtitle' => __( 'Force all gallery posts to display with the selected columns.', 'warta' ),
                        'options'  => array(
                                '1' => 1,
                                '2' => 2,
                                '3' => 3,
                                '4' => 4,
                                '5' => 5,
                                '6' => 6,
                                '7' => 7,
                                '8' => 8,
                                '9' => 9,
                        ),
                ),
        )
);