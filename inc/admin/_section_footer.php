<?php

$this->sections[] = array(
        'icon'          => 'el-icon-stop',
        'title'         => __( 'Footer', 'warta' ),
        'fields'        => array(    
                array(
                        'id'       => 'footer_main',
                        'type'     => 'switch', 
                        'title'    => __('Main', 'warta'),
                        'default'  => 1,
                ),
                array(
                        'id'       => 'footer_bottom',
                        'type'     => 'switch', 
                        'title'    => __('Bottom', 'warta'),
                        'default'  => 1,
                ),
                array(
                    'id'       => 'footer_layout',
                    'type'     => 'image_select',
                    'title'    => __('Layout', 'warta'), 
                    'options'  => array(
                        1      => array(
                            'alt'   => '4 Column', 
                            'img'   => get_template_directory_uri() . '/img/admin/layout-footer-v1.jpg'
                        ),
                        2      => array(
                            'alt'   => '6 Column', 
                            'img'   => get_template_directory_uri() . '/img/admin/layout-footer-v2.jpg'
                        )
                    ),
                    'default'  => '2',
                     'validate_callback' => 'warta_validate_integer'
                ),
                  array(
                            'id'         =>'footer_text',
                            'type'       => 'editor',
                            'title'      => __('Copyright Text', 'warta'), 
                            'default'    => sprintf(__('Copyright &copy; %1$s - <strong>%2$s</strong>'), date('Y'), get_bloginfo('name')),
                            'validate'   => 'html'
                 ),
        )
);