<?php

$this->sections[] = array(
    'icon'      => 'el-icon-file',
    'title'     => __('Single Post Page', 'warta'),
    'fields'    => array(
        array(
                'type'     => 'switch',
                'id'       => 'singular_post_nav',
                'title'    => __( 'Post Navigation', 'warta' ),
                'subtitle' => __( 'Navigation to next/previous post.', 'warta' ),
                'default'  => true,
        ),
        array(
            'id'       => 'singular_specific_widget',
            'type'     => 'checkbox',
            'title'    => __('Custom Widget Area', 'warta'), 
            'subtitle' => __('The widget area will only be displayed on Single Post Page.', 'warta'),
            'options'  => array(
                'sidebar'   => __('Sidebar Section', 'warta'),
                'footer'    => __('Footer Section', 'warta'),
            ),
        ),
        
        array(
           'id'       => 'singular_footer_layout',
           'type'     => 'image_select',
           'title'    => __('Footer Layout', 'warta'), 
           'subtitle' => __('Choose between 4 or 6 columns layout.', 'warta'),
           'options'  => array(
               1      => array(
                   'alt'   => '4 Column', 
                   'img'   => get_template_directory_uri() . '/img/admin/layout-footer-v1.jpg'
               ),
               2      => array(
                   'alt'   => '6 Column', 
                   'img'   => get_template_directory_uri() . '/img/admin/layout-footer-v2.jpg'
               )
           ),
           'default'  => '2',
           'validate_callback' => 'warta_validate_integer'
       ),
    )
);

/* -------------------------------------------------------------------------- *
 * Featured Image
 * -------------------------------------------------------------------------- */
$this->sections[] = array(
        'title'         => __('Featured Image', 'warta'),
        'desc'          => __('Enable/disable featured image', 'warta'),
        'subsection'    => TRUE,
        'fields'        => array(                                
                                array(
                                        'id'       => 'singular_display_featured_image',
                                        'type'     => 'switch',
                                        'title'    => __('Post format standard', 'warta'),
                                        'default'  => TRUE,
                                ),
                                array(
                                        'id'       => 'singular_display_featured_image_aside',
                                        'type'     => 'switch',
                                        'title'    => __('Post format aside', 'warta'),
                                        'default'  => TRUE,
                                ),
                                array(
                                        'id'       => 'singular_display_featured_image_gallery',
                                        'type'     => 'switch',
                                        'title'    => __('Post format gallery', 'warta'),
                                        'default'  => FALSE,
                                ),
                                array(
                                        'id'       => 'singular_display_featured_image_link',
                                        'type'     => 'switch',
                                        'title'    => __('Post format link', 'warta'),
                                        'default'  => TRUE,
                                ),
                                array(
                                        'id'       => 'singular_display_featured_image_image',
                                        'type'     => 'switch',
                                        'title'    => __('Post format image', 'warta'),
                                        'default'  => FALSE,
                                ),
                                array(
                                        'id'       => 'singular_display_featured_image_quote',
                                        'type'     => 'switch',
                                        'title'    => __('Post format quote', 'warta'),
                                        'default'  => TRUE,
                                ),
                                array(
                                        'id'       => 'singular_display_featured_image_status',
                                        'type'     => 'switch',
                                        'title'    => __('Post format status', 'warta'),
                                        'default'  => TRUE,
                                ),
                                array(
                                        'id'       => 'singular_display_featured_image_video',
                                        'type'     => 'switch',
                                        'title'    => __('Post format video', 'warta'),
                                        'default'  => FALSE,
                                ),
                                array(
                                        'id'       => 'singular_display_featured_image_audio',
                                        'type'     => 'switch',
                                        'title'    => __('Post format audio', 'warta'),
                                        'default'  => FALSE,
                                ),
                                array(
                                        'id'       => 'singular_display_featured_image_chat',
                                        'type'     => 'switch',
                                        'title'    => __('Post format chat', 'warta'),
                                        'default'  => TRUE,
                                ),
                        )
);

/* -------------------------------------------------------------------------- *
 * Post Meta
 * -------------------------------------------------------------------------- */

$this->sections[] = array(
        'title'         => __( 'Post Meta', 'warta'),
        'subsection'    => true,
        'fields'        => array(
                array(
                        'id'       => 'singular_post_meta',
                        'type'     => 'checkbox',
                        'title'    => __( 'Meta', 'warta' ), 
                        'options'  => array(
                            'date'          => __('Date', 'warta'),
                            'format'        => __('Post format', 'warta'),
                            'categories'    => __('Categories', 'warta'),
                            'author'        => __('Author', 'warta'),
                            'comments'      => __('Comments', 'warta'),
                            'views'         => __('Views', 'warta'),
                            'tags'          => __('Tags', 'warta'),
                            'review_score'  => __('Review score', 'warta'),
                        ),
                        'default'   => array(
                            'date'      => 1,  
                            'tags'      => 1,
                            'views'     => 1,                      
                        ),
                ),
                array(
                        'id'       => 'singular_icons',
                        'type'     => 'checkbox',
                        'title'    => __('Icons', 'warta'), 
                        'options'  => array(
                                'author'        => __('Author', 'warta'),
                                'comments'      => __('Comments', 'warta'),
                                'format'        => __('Post Format', 'warta'),
                        ),
                        'default'   => array(
                                'author'    => 1,
                                'comments'  => 1,
                                'format'    => 1,
                        ),
                ),
                array(
                        'id'            => 'singular_date_format',
                        'type'          => 'text',
                        'title'         => __( 'Date Format', 'warta' ),
                        'subtitle'      => '<a href="http://codex.wordpress.org/Formatting_Date_and_Time#Examples" target="_blank">' . __( 'Formatting Date and Time', 'warta' ) . '</a>',
                        'validate'      => 'no_html',
                        'default'       => 'l, F j, Y g:i a'
                ),
                array(
                    'id'       => 'singular_tag_text',
                    'type'     => 'text',
                    'title'    => __('Tag Title', 'warta'),
                    'validate' => 'no_html',
                    'default'  => __('Tags:', 'warta')
                ),
        )
);

/* -------------------------------------------------------------------------- *
 * Review
 * -------------------------------------------------------------------------- */

$this->sections[] = array(
    'title'             => __( 'Review Box', 'warta' ),
    'subsection'        => true,
    'fields'            => array(
        array(
            'id'       => 'review_box_title',
            'type'     => 'text',
            'title'    => __('Title', 'warta'),
            'validate' => 'no_html',
            'default'  => __('Review Scores', 'warta'),
        ),
        array(
            'id'       => 'review_box_enable_categories',
            'type'     => 'switch', 
            'title'    => __('Enable score categories', 'warta'),
            'default'  => 1,
            'validate_callback' => 'warta_validate_integer',
        ),
        array(
            'id'       => 'review_box_score_type',
            'type'     => 'radio',
            'title'    => __('Score Type', 'warta'), 
            'options'  => array(
                'bar'           => _x('Bar', 'Review score type', 'warta'), 
                'bar_animated'  => _x('Bar animated', 'Review score type', 'warta'), 
                'star'          => _x('Star', 'Review score type', 'warta'),
            ),
            'default' => 'bar',
            'required'=> array('review_box_enable_categories', '=', 1)
        ),
        array(
            'id'       => 'review_box_score_text',
            'type'     => 'text',
            'title'    => __('Score Text', 'warta'),
            'subtitle' => __('Separated by commas, from 1 to 5.', 'warta'),
            'validate' => 'no_html',
            'default'  => __('Bad, Poor, Average, Good, Super', 'warta'),
        ),
        array(
            'id'       => 'review_box_enable_user_rating',
            'type'     => 'switch', 
            'title'    => __('Enable user rating', 'warta'),
            'default'  => 1,
             'validate_callback' => 'warta_validate_integer'
        ),
        array(
            'id'       => 'review_box_user_rating_text',
            'type'     => 'text',
            'title'    => __('User Rating Text', 'warta'),
            'validate' => 'html',
            'default'  => __('User Rating: %1$s (%2$s votes)', 'warta'),
            'desc'     => __( 'The %1$s is the total user rating, and the %2$s is the number of total voters.', 'warta' ),
            'required' => array( 'review_box_enable_user_rating', '=', 1 )
        ),
        array(
            'id'       => 'review_box_user_rating_not_rated',
            'type'     => 'text',
            'title'    => __('User Rating: Not Rated Text', 'warta'),
            'validate' => 'html',
            'default'  => __('User rating: not rated yet, be the first!', 'warta'),
            'required' => array( 'review_box_enable_user_rating', '=', 1 )
        ),
        array(
            'id'       => 'review_box_user_rating_after_vote',
            'type'     => 'text',
            'title'    => __('User Rating: After Vote Text', 'warta'),
            'validate' => 'html',
            'default'  => __('Thank you. Your vote has been saved.', 'warta'),
            'required' => array( 'review_box_enable_user_rating', '=', 1 )
        ),
        array(
            'id'       => 'review_box_user_rating_has_voted',
            'type'     => 'text',
            'title'    => __('Text for those who have voted', 'warta'),
            'validate' => 'html',
            'default'  => __('You have voted: %s.', 'warta'),
            'desc'     => __('%s is the number he/she voted.', 'warta'),
            'required' => array( 'review_box_enable_user_rating', '=', 1 )
        ),
    )
);

/* -------------------------------------------------------------------------- *
 * Share Buttons
 * -------------------------------------------------------------------------- */
$this->sections[] = array(
        'title'         => __( 'Share Buttons', 'warta' ),
        'subsection'    => true,
        'fields'        => array(
                array(
                        'id'                => 'singular_share_buttons_display',
                        'type'              => 'switch', 
                        'title'             => __('Display Share Buttons', 'warta'),
                        'default'           => true,
                        'validate_callback' => 'warta_validate_integer'
                ),
                array(
                    'id'       => 'singular_share_text',
                    'type'     => 'text',
                    'title'    => __( 'Title', 'warta' ),
                    'validate' => 'no_html',
                    'default'  => __( 'Share:', 'warta' ),
                    'required'  => array( 'singular_share_buttons_display', '=', 1 )
                ),
                array(
                    'id'       => 'singular_share_buttons',
                    'type'     => 'sortable',
                    'mode'     => 'checkbox',
                    'title'    => __( 'Buttons', 'warta' ), 
                    'options'  => array(
                        'facebook'          => $friskamax_warta_var['social_media_all']['facebook'],
                        'googleplus'        => $friskamax_warta_var['social_media_all']['googleplus'],
                        'linkedin'          => $friskamax_warta_var['social_media_all']['linkedin'],
                        'pinterest'         => $friskamax_warta_var['social_media_all']['pinterest'],
                        'stumbleupon'       => $friskamax_warta_var['social_media_all']['stumbleupon'],
                        'twitter'           => $friskamax_warta_var['social_media_all']['twitter'],

                        'digg'              => $friskamax_warta_var['social_media_all']['digg'],
                        'mail'              => $friskamax_warta_var['social_media_all']['mail'],
                        'tumblr'            => $friskamax_warta_var['social_media_all']['tumblr'],
                    ),
                    'default'   => array(
                        'facebook'      => 1,
                        'googleplus'    => 1,
                        'linkedin'      => 1,
                        'pinterest'     => 1,
                        'stumbleupon'   => 1,
                        'twitter'       => 1,

                        'digg'          => 1,
                        'mail'          => 1,
                        'tumblr'        => 1,
                    ),
                    'required'  => array( 'singular_share_buttons_display', '=', 1 ),
                ),
                array(
                        'type'          => 'switch',
                        'id'            => 'singular_share_count',
                        'title'         => __( 'Display Share Count', 'warta' ),
                        'required'      => array( 'singular_share_buttons_display', '=', 1 ),
                        'default'       => true
                ),
        )
);

/* -------------------------------------------------------------------------- *
 * Author
 * -------------------------------------------------------------------------- */
$this->sections[] = array(
        'title'         => __( 'Author', 'warta' ),
        'subsection'    => true,
        'fields'        => array(
                array(
                        'id'       => 'singular_author_info',
                        'type'     => 'switch', 
                        'title'    => __('Display Author Info', 'warta'),
                        'default'  => true,
                ),
                array(
                    'id'       => 'singular_author_info_title',
                    'type'     => 'text',
                    'title'    => __('Title', 'warta'),
                    'validate' => 'no_html',
                    'default'  => __('Author', 'warta'),
                    'required'  => array( 'singular_author_info', '=', 1 )
                ),        
        )
);

/* -------------------------------------------------------------------------- *
 * Related Posts
 * -------------------------------------------------------------------------- */
$this->sections[] = array(
        'title'         => __( 'Related Posts', 'warta' ),
        'subsection'    => true,
        'fields'        => array(
                array(
                        'id'       => 'singular_related',
                        'type'     => 'switch', 
                        'title'    => __('Display Related Posts', 'warta'),
                        'default'  => 1,
                ),
                array(
                    'id'       => 'singular_related_title',
                    'type'     => 'text',
                    'title'    => __('Title', 'warta'),
                    'validate' => 'no_html',
                    'default'  => __('Related Posts', 'warta'),
                    'required'  => array( 'singular_related', '=', 1 )
                ),    
                array(
                    'id'       => 'singular_related_post_meta',
                    'type'     => 'checkbox',
                    'title'    => __('Post Meta', 'warta'), 
                    'options'  => array(
                        'date'          => __('Date', 'warta'),
                        'format'        => __('Post format', 'warta'),
                        'category'      => __('Category', 'warta'),
                        'author'        => __('Author', 'warta'),
                        'comments'      => __('Comments', 'warta'),
                        'views'         => __('Views', 'warta'),
                    ),
                    'default'   => array(
                        'comments'  => 1,
                        'date'      => 1,
                        'views'     => 1,                      
                    ),
                    'validate'  => 'no_html',
                    'required'  => array( 'singular_related', '=', 1 )
                ),        
                array(
                        'id'       => 'singular_related_date_format',
                        'type'     => 'text',
                        'title'    => __('Date Format', 'warta'),
                        'subtitle' => '<a href="http://codex.wordpress.org/Formatting_Date_and_Time#Examples" target="_blank">' . __( 'Formatting Date and Time', 'warta' ) . '</a>',
                        'validate' => 'no_html',
                        'default'  => 'M j, Y',
                        'required' => array( 'singular_related', '=', 1 )
                ),
        )
);

/* -------------------------------------------------------------------------- *
 * Advanced
 * -------------------------------------------------------------------------- */
$this->sections[] = array(
        'title'         => __( 'Advanced', 'warta' ),
        'subsection'    => true,
        'fields'        => array(
                array(
                        'id'       => 'ajax_post_views',
                        'type'     => 'switch',
                        'title'    => __('Use AJAX on post views counter', 'warta'),
                        'subtitle' => __('Useful when using a cache plugin', 'warta'),
                        'default'  => false,
                )
        )
);