<?php
/**
 * General Settings.
 *
 * @package Warta
 */

$this->sections[] = array(
        'icon'  => 'el-icon-screen',
        'title' => __('Appearance', 'warta'),
        'fields' => array(
                array(
                        'id'                    => 'boxed_style',
                        'type'                  => 'switch',
                        'title'                 => __('Boxed style', 'warta'),
                        'default'               => 0,
                        'validate_callback'     => 'warta_validate_integer'
                ),
                array(
                        'id'       => 'body_bg',
                        'type'     => 'media', 
                        'url'      => true,
                        'title'    => __( 'Body Background', 'warta' ),
                        'url'      => false,
                        'required' => array('boxed_style','equals','1')
                ),
                array(
                        'id'                    => 'flat_style',
                        'type'                  => 'switch',
                        'title'                 => __('Flat style', 'warta'), 
                        'default'               => 0,
                        'validate_callback'     => 'warta_validate_integer'
                ),
                array(
                        'id'                    => 'image_light',
                        'type'                  => 'switch',
                        'title'                 => __('Light on images', 'warta'), 
                        'default'               => 1,
                        'validate_callback'     => 'warta_validate_integer'
                ),
                array(
                        'id'                    => 'zoom_effect',
                        'type'                  => 'switch',
                        'title'                 => __('Zoom effect', 'warta'), 
                        'default'               => 1,
                        'validate_callback'     => 'warta_validate_integer'
                ),
                 
        )                
);

//Color_________________________________________________________________________
$this->sections[] = array(
        'title'         => __('Color', 'warta'),
        'subsection'    => TRUE,
        'fields'        => array(
                array(
                         'id'            => 'primary-color',
                         'type'          => 'color',
                         'title'         => __('Primary Color', 'warta'),
                         'default'       => '#fd4a29',
                         'validate'      => 'color',
                         'transparent'   => FALSE,
                         'compiler'      => true,
                 ),
                 array(
                         'id'            => 'secondary-color',
                         'type'          => 'color',
                         'title'         => __('Secondary Color', 'warta'),
                         'default'       => '#606068',
                         'validate'      => 'color',
                         'transparent'   => FALSE,
                         'compiler'      => true
                 ),
                 array(
                         'id'            => 'headings-link-color',
                         'type'          => 'color',
                         'title'         => __('Headings Link Color', 'warta'),
                         'default'       => '#606068',
                         'validate'      => 'color',
                         'transparent'   => FALSE,
                         'compiler'      => true,
                 ),
                 array(
                         'id'            => 'body-link-color',
                         'type'          => 'color',
                         'title'         => __('Body Link Color', 'warta'),
                         'default'       => '#428bca',
                         'validate'      => 'color',
                         'transparent'   => FALSE,
                         'compiler'      => true
                 )
        )
);

//font__________________________________________________________________________
$this->sections[] = array(
        'title'         => __('Font', 'warta'),
        'subsection'    => TRUE,
        'fields'        => array(
                array(
                        'id'            => 'typography_headings',
                        'type'          => 'typography',
                        'title'         => __('Primary Font', 'warta'),
                        'compiler'      => TRUE,
                        'color'         => FALSE,
                        'font-style'    => FALSE,
                        'font-weight'   => FALSE,
                        'font-size'     => FALSE,
                        'line-height'   => FALSE,
                        'text-align'    => FALSE,
                        'google'        => TRUE,
                        'all_styles'    => TRUE,
                        'preview'       => array(
                                                'font-size'     => '40px'
                                        ),
                        'default'       => array(
                                                'font-family'   => 'Roboto Condensed'
                                        )
                ),
                array(
                        'id'            => 'typography_body',
                        'type'          => 'typography',
                        'title'         => __('Secondary Font', 'warta'),
                        'compiler'      => TRUE,
                        'color'         => FALSE,
                        'font-style'    => FALSE,
                        'font-weight'   => FALSE,
                        'font-size'     => FALSE,
                        'line-height'   => FALSE,
                        'text-align'    => FALSE,                        
                        'google'        => TRUE,
                        'all_styles'    => TRUE,
                        'preview'       => array(
                                                'font-size'     => '40px'
                                        ),
                ),
        )
);