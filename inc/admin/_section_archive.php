<?php
/**
 * Archive Page Settings.
 *
 * @package Warta
 */

$this->sections[] = array(
        'icon'      => 'el-icon-folder-close',
        'title'     => __('Archive Page', 'warta'),
        'fields'    => array(
            array(
                'id'            => 'archive_layout',
                'type'          => 'image_select',
                'title'         => __('Layout', 'warta'), 
                'desc'          => __('version 1 - version 2 - version 3', 'warta'),
                'options'       => array(
                                        1 => array(
                                            'alt'   => __('Version 1', 'warta'), 
                                            'img'   => get_template_directory_uri() . '/img/admin/layout-blog-v1.jpg'
                                        ),
                                        2 => array(
                                            'alt'   => __('Version 2', 'warta'), 
                                            'img'   => get_template_directory_uri() . '/img/admin/layout-blog-v2.jpg'
                                        ),
                                        3 => array(
                                            'alt'   => __('Version 3', 'warta'), 
                                            'img'   => get_template_directory_uri() . '/img/admin/layout-blog-v3.jpg'
                                        )
                                ),
                'default'       => 1,
            ),
            array(
                'id'       => 'archive_excerpt_length',
                'type'     => 'slider', 
                'title'    => __('Excerpt Length', 'warta'),
                'subtitle' => __('How many characters do you want to show?','warta'),
                'default'  => '320',
                'min'      => '1',
                'step'     => '1',
                'max'      => '500',
                'validate_callback' => 'warta_validate_integer'
            ),

            array(
                'id'       => 'archive_specific_widget',
                'type'     => 'checkbox',
                'title'    => __('Custom Widget Area', 'warta'), 
                'subtitle' => __('The widget area will only be displayed on Archive Page.', 'warta'),
                'options'  => array(
                    'sidebar'   => __('Sidebar Section', 'warta'),
                    'footer'    => __('Footer Section', 'warta'),
                ),
            ),

            array(
               'id'       => 'archive_footer_layout',
               'type'     => 'image_select',
               'title'    => __('Footer Layout', 'warta'), 
               'subtitle' => __('Choose between 4 or 6 columns layout.', 'warta'),
               'options'  => array(
                   1      => array(
                       'alt'   => '4 Column', 
                       'img'   => get_template_directory_uri() . '/img/admin/layout-footer-v1.jpg'
                   ),
                   2      => array(
                       'alt'   => '6 Column', 
                       'img'   => get_template_directory_uri() . '/img/admin/layout-footer-v2.jpg'
                   )
               ),
               'default'  => '2',
               'validate_callback' => 'warta_validate_integer'
           ),
        )
);

/* -------------------------------------------------------------------------- *
 * Post Meta
 * -------------------------------------------------------------------------- */
$this->sections[] = array(
        'title'         => __( 'Post Meta', 'warta'),
        'subsection'    => true,
        'fields'        => array(                 
                array(
                        'id'       => 'archive_date_format',
                        'type'     => 'text',
                        'title'    => __('Date Format', 'warta'),
                        'subtitle' => '<a href="http://codex.wordpress.org/Formatting_Date_and_Time#Examples" target="_blank">' . __( 'Formatting Date and Time', 'warta' ) . '</a>',
                        'validate' => 'no_html',
                        'default'  => 'l, F j, Y g:i a'
                ),
                array(
                        'id'       => 'archive_post_meta',
                        'type'     => 'checkbox',
                        'title'    => __('Meta', 'warta'), 
                        'options'  => array(
                            'date'         => __('Date', 'warta'),
                            'format'       => __('Post format', 'warta'),
                            'category'     => __('First category', 'warta'),
                            'categories'   => __('All categories', 'warta'),
                            'tags'         => __('Tags', 'warta'),
                            'author'       => __('Author', 'warta'),
                            'comments'     => __('Comments', 'warta'),
                            'views'        => __('Views', 'warta'),
                            'review_score' => __('Review score', 'warta'),
                        ),
                        'default'   => array(
                            'date'          => 1,
                            'format'        => 0,
                            'category'      => 0,
                            'categories'    => 1,
                            'tags'          => 1,
                            'author'        => 1,
                            'comments'      => 1,
                            'views'         => 1,     
                            'review_score'  => 0,
                        ),
                ),
        )
);

/* -------------------------------------------------------------------------- *
 * Icon
 * -------------------------------------------------------------------------- */
$this->sections[] = array(
        'title'         => __('Icons', 'warta'),
        'desc'      => __('Icon options for layout version 1 and 3. Choose what you want to display.', 'warta'),
        'subsection'    => TRUE,
        'fields'        => array(
                array(
                        'id'       => 'archive_icons',
                        'type'     => 'checkbox',
                        'title'    => __('Post format standard', 'warta'), 
                        'options'  => array(
                                'author'       => __('Author', 'warta'),
                                'comments'     => __('Comments', 'warta'),
                        ),
                        'default'   => array(
                                'author'    => 1,
                                'comments'  => 1,
                        ),
                ), 
                array(
                        'id'       => 'archive_icons_aside',
                        'type'     => 'checkbox',
                        'title'    => __('Post format aside', 'warta'), 
                        'options'  => array(
                                'author'       => __('Author', 'warta'),
                                'comments'     => __('Comments', 'warta'),
                                'format'       => __('Post Format', 'warta'),
                        ),
                        'default'   => array(
                                'format'    => 1, 
                        ),
                ), 
                array(
                        'id'       => 'archive_icons_gallery',
                        'type'     => 'checkbox',
                        'title'    => __('Post format gallery', 'warta'), 
                        'options'  => array(
                                'author'       => __('Author', 'warta'),
                                'comments'     => __('Comments', 'warta'),
                                'format'       => __('Post Format', 'warta'),
                        ),
                        'default'   => array(
                                'author'    => 1,
                                'comments'  => 1,
                                'format'    => 1, 
                        ),
                ), 
                array(
                        'id'       => 'archive_icons_link',
                        'type'     => 'checkbox',
                        'title'    => __('Post format link', 'warta'), 
                        'options'  => array(
                                'author'       => __('Author', 'warta'),
                                'comments'     => __('Comments', 'warta'),
                                'format'       => __('Post Format', 'warta'),
                        ),
                        'default'   => array(
                                'format'    => 1, 
                        ),
                ), 
                array(
                        'id'       => 'archive_icons_image',
                        'type'     => 'checkbox',
                        'title'    => __('Post format image', 'warta'), 
                        'options'  => array(
                                'author'       => __('Author', 'warta'),
                                'comments'     => __('Comments', 'warta'),
                                'format'       => __('Post Format', 'warta'),
                        ),
                        'default'   => array(
                                'author'    => 1,
                                'comments'  => 1,
                                'format'    => 1, 
                        ),
                ), 
                array(
                        'id'       => 'archive_icons_quote',
                        'type'     => 'checkbox',
                        'title'    => __('Post format quote', 'warta'), 
                        'options'  => array(
                                'author'       => __('Author', 'warta'),
                                'comments'     => __('Comments', 'warta'),
                                'format'       => __('Post Format', 'warta'),
                        ),
                        'default'   => array(
                                'format'    => 1, 
                        ),
                ), 
                array(
                        'id'       => 'archive_icons_status',
                        'type'     => 'checkbox',
                        'title'    => __('Post format status', 'warta'), 
                        'options'  => array(
                                'author'       => __('Author', 'warta'),
                                'comments'     => __('Comments', 'warta'),
                                'format'       => __('Post Format', 'warta'),
                        ),
                        'default'   => array(
                                'format'    => 1, 
                        ),
                ), 
                array(
                        'id'       => 'archive_icons_video',
                        'type'     => 'checkbox',
                        'title'    => __('Post format video', 'warta'), 
                        'options'  => array(
                                'author'       => __('Author', 'warta'),
                                'comments'     => __('Comments', 'warta'),
                                'format'       => __('Post Format', 'warta'),
                        ),
                        'default'   => array(
                                'author'    => 1,
                                'comments'  => 1,
                                'format'    => 1, 
                        ),
                ), 
                array(
                        'id'       => 'archive_icons_audio',
                        'type'     => 'checkbox',
                        'title'    => __('Post format audio', 'warta'), 
                        'options'  => array(
                                'author'       => __('Author', 'warta'),
                                'comments'     => __('Comments', 'warta'),
                                'format'       => __('Post Format', 'warta'),
                        ),
                        'default'   => array(
                                'format'    => 1, 
                        ),
                ), 
                array(
                        'id'       => 'archive_icons_chat',
                        'type'     => 'checkbox',
                        'title'    => __('Post format chat', 'warta'), 
                        'options'  => array(
                                'author'       => __('Author', 'warta'),
                                'comments'     => __('Comments', 'warta'),
                                'format'       => __('Post Format', 'warta'),
                        ),
                        'default'   => array(
                                'author'    => 1,
                                'comments'  => 1,
                                'format'    => 1, 
                        ),
                ), 
        )
        
);