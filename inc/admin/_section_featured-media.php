<?php

$this->sections[] = array(
        'title'         => __('Featured Media', 'warta'),
        'icon'          => 'el-icon-video',
        'heading'       => __('Featured Media Shortcode', 'warta'),
        'desc'          => __( '<p>If you use post format video, audio or gallery, the theme will grab the first shortcode from the post content and display it in Archive Page and Articles widget. If the theme did not find any shortcode then the featured image will be used.</p> 
                            <p>Below you can add or remove any shortcode. This is useful if you are using a plugin that provide shortcodes and you want to use those shortcodes.</p>', 'warta'),
        'fields'        => array(
                                array(
                                        'id'            => 'featured_media_video',
                                        'type'          => 'multi_text',
                                        'title'         => __('Post format video', 'warta'),
                                        'validate'      => 'no_html',
                                        'default'       => array(
                                                                '[video]',
                                                                '[embed]'
                                                        )
                                ),
                                array(
                                        'id'            => 'featured_media_audio',
                                        'type'          => 'multi_text',
                                        'title'         => __('Post format audio', 'warta'),
                                        'validate'      => 'no_html',
                                        'default'       => array(
                                                                '[audio]',
                                                                '[playlist]',
                                                                '[embed]'
                                                        )
                                ),
                                array(
                                        'id'            => 'featured_media_gallery',
                                        'type'          => 'multi_text',
                                        'title'         => __('Post format gallery', 'warta'),
                                        'validate'      => 'no_html',
                                        'default'       => array(
                                                                '[gallery]',
                                                                '[carousel]'
                                                        )
                                ),
                                array(
                                        'id'       => 'featured_media_convert_gallery_to_carousel',
                                        'type'     => 'switch',
                                        'title'    => __('Convert shortcode [gallery] to [carousel]', 'warta'),
                                        'subtitle' => __('If shortcode [gallery] is found, it will be displayed as carousel', 'warta'),
                                        'default'  => true,
                                )
                        ),
);