<?php

require_once dirname( __FILE__ ) . '/_compiler.php';
require_once dirname( __FILE__ ) . '/_custom-validation.php';
require_once dirname( __FILE__ ) . '/_theme-options.php';
require_once dirname( __FILE__ ) . '/_tgmpa.php';