<?php
/**
 * General Settings.
 *
 * @package Warta
 */

$this->sections[] = array(
        'icon'  => 'el-icon-cog',
        'title' => __('General Settings', 'warta'),
        'fields'=> array(                
                array(
                    'id'       => 'logo',
                    'type'     => 'media', 
                    'url'      => true,
                    'title'    => __('Logo', 'warta'),
                    'url'      => false
                ),
                array(
                    'id'       => 'favicon',
                    'type'     => 'media', 
                    'url'      => true,
                    'title'    => __('Favicon', 'warta'),
                    'url'      => false
                ),
        )
);