<?php

$this->sections[] = array(
        'icon'          => 'el-icon-photo',
        'title'         => __( 'Header', 'warta' ),
        'fields'        => array(                
                array(
                        'type'     => 'button_set',
                        'id'       => 'title_content',
                        'title'    => __( 'Header content', 'warta' ),
                        'options' => array(
                                'site_title' => __( 'Site Title and Tagline', 'warta' ), 
                                'post_title' => __( 'Post/Page Title and Subtitle', 'warta' ), 
                         ), 
                        'default' => 'post_title'
                ),
                array(
                        'type'          => 'button_set',
                        'id'            => 'title_content_front_page',
                        'title'         => __( 'Header content on Front Page', 'warta' ),
                        'options'       => array(
                                'site_title' => __( 'Site Title and Tagline', 'warta' ), 
                                'post_title' => __( 'Page Title and Subtitle', 'warta' ), 
                         ), 
                        'default'       => 'site_title',
                        'required'      => array( 'title_content', '=', 'post_title' )
                ),
        )
);

$this->sections[] = array(
        'subsection'    => true,
        'title'         => __( 'Background', 'warta' ),
        'heading'       => __( 'Header Background', 'warta' ),
        'desc'          => __( 'Minimum size: 1920x400 pixels for full width layout and 1260x260 pixels for boxed layout.', 'warta' ),
        'fields'        => array(                
                array(
                        'id'       => 'title_bg',
                        'type'     => 'media', 
                        'url'      => true,
                        'title'    => __( 'Default', 'warta' ),
                        'url'      => false
                ),
                array(
                        'id'       => 'archive_title_bg',
                        'type'     => 'media', 
                        'url'      => true,
                        'title'    => __( 'Archive Page', 'warta' ),
                        'url'      => false
                ),
                array(
                        'id'       => 'singular_title_bg',
                        'type'     => 'media', 
                        'url'      => true,
                        'title'    => __( 'Single Post Page', 'warta' ),
                        'url'      => false
                ),
                array(
                        'id'       => 'page_title_bg',
                        'type'     => 'media', 
                        'url'      => true,
                        'title'    => __( 'Static Page', 'warta' ),
                        'url'      => false
                ),
        ),
);