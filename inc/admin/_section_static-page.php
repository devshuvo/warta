<?php
/**
 * Singular Page Settings.
 *
 * @package Warta
 */

$this->sections[] = array(
    'icon'      => 'el-icon-file',
    'title'     => __('Static Page', 'warta'),
    'fields'    => array(       
        array(
            'id'       => 'page_specific_widget',
            'type'     => 'checkbox',
            'title'    => __('Custom Widget Area', 'warta'), 
            'subtitle' => __('The widget area will only be displayed on Static Page.', 'warta'),
            'options'  => array(
                'sidebar'   => __('Sidebar Section', 'warta'),
                'footer'    => __('Footer Section', 'warta'),
            ),
        ),
        
        array(
           'id'       => 'page_footer_layout',
           'type'     => 'image_select',
           'title'    => __('Footer Layout', 'warta'), 
           'subtitle' => __('Choose between 4 or 6 columns layout.', 'warta'),
           'options'  => array(
               1      => array(
                   'alt'   => '4 Column', 
                   'img'   => get_template_directory_uri() . '/img/admin/layout-footer-v1.jpg'
               ),
               2      => array(
                   'alt'   => '6 Column', 
                   'img'   => get_template_directory_uri() . '/img/admin/layout-footer-v2.jpg'
               )
           ),
           'default'  => '2',
           'validate_callback' => 'warta_validate_integer'
       ),
    )
);