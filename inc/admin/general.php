<?php
/**
 * General Settings.
 *
 * @package Warta
 */

$this->sections[] = array(
   'icon' => 'el-icon-cog',
   'title' => __('General Settings', 'warta'),
   'fields' => array(
       array(
            'id'       => 'style',
            'type'     => 'checkbox',
            'title'    => __('Appearance Style', 'warta'), 
            'options'  => array(
                    'boxed_style'   => __('Boxed style', 'warta'), 
                    'flat_style'    => __('Flat style', 'warta'), 
                    'image_light'   => __('Display light on images', 'warta'), 
            ),
            'validate' => 'no_special_chars',
       ),
        array(
                'id'       => 'color',
                'type'     => 'image_select',
                'title'    => __('Color', 'warta'), 
                'options'  => array(
                        '0'      => array(
                                'alt'   => '0', 
                                'img'   => get_template_directory_uri() . '/img/admin/color-0.jpg'
                        ), 
                        '1'      => array(
                                'alt'   => '1', 
                                'img'   => get_template_directory_uri() . '/img/admin/color-1.jpg'
                        ), 
                        '2'      => array(
                                'alt'   => '2', 
                                'img'   => get_template_directory_uri() . '/img/admin/color-2.jpg'
                        ), 
                        '3'      => array(
                                'alt'   => '3', 
                                'img'   => get_template_directory_uri() . '/img/admin/color-3.jpg'
                        ), 
                        '4'      => array(
                                'alt'   => '4', 
                                'img'   => get_template_directory_uri() . '/img/admin/color-4.jpg'
                        ), 
                        '5'      => array(
                                'alt'   => '5', 
                                'img'   => get_template_directory_uri() . '/img/admin/color-5.jpg'
                        ), 
                        '6'      => array(
                                'alt'   => '6', 
                                'img'   => get_template_directory_uri() . '/img/admin/color-6.jpg'
                        ), 
                        '7'      => array(
                                'alt'   => '7', 
                                'img'   => get_template_directory_uri() . '/img/admin/color-7.jpg'
                        ), 
                        '8'      => array(
                                'alt'   => '8', 
                                'img'   => get_template_directory_uri() . '/img/admin/color-8.jpg'
                        ), 
                ),
                'default' => '0',
                'validate_callback'=> 'warta_validate_integer'
       ),
       array(
                'id'       => 'color_option',
                'type'     => 'radio',
                'title'    => __('Color Option', 'warta'), 
                'subtitle' => __('Use the selected color above for primary color only '
                            . 'or both primary and secondary color.', 'warta'),
                'options'  => array(
                        'primary'   => __('Primary', 'warta'), 
                        'both'      => __('Primary and secondary', 'warta'), 
                ),
                'default' => 'primary',
                'validate'=> 'no_special_chars'
       ),
       array(
           'id'       => 'logo',
           'type'     => 'media', 
           'url'      => true,
           'title'    => __('Logo', 'warta'),
           'subtitle' => __('Upload 30px tall image.', 'warta'),
           'url'      => false
       ),
       array(
           'id'       => 'body_bg',
           'type'     => 'media', 
           'url'      => true,
           'title'    => __('Body Background', 'warta'),
           'subtitle' => __('Upload 1920x wide image.', 'warta'),
           'url'      => false
       ),
       array(
           'id'       => 'title_bg',
           'type'     => 'media', 
           'url'      => true,
           'title'    => __('Default Title Background', 'warta'),
           'subtitle' => __('Upload 1920x150px image.', 'warta'),
           'url'      => false
       ),
       array(
           'id'       => 'footer_layout',
           'type'     => 'image_select',
           'title'    => __('Footer Layout', 'warta'), 
           'subtitle' => __('Choose between 4 or 6 columns layout.', 'warta'),
           'options'  => array(
               1      => array(
                   'alt'   => '4 Column', 
                   'img'   => get_template_directory_uri() . '/img/admin/layout-footer-v1.jpg'
               ),
               2      => array(
                   'alt'   => '6 Column', 
                   'img'   => get_template_directory_uri() . '/img/admin/layout-footer-v2.jpg'
               )
           ),
           'default'  => '2',
            'validate_callback' => 'warta_validate_integer'
       ),
         array(
                   'id'         =>'footer_text',
                   'type'       => 'editor',
                   'title'      => __('Footer Text', 'warta'), 
                   'default'    => 'Copyright &copy; 2014 - <strong>WarTa</strong>',
                   'validate'   => 'html'
        ),
   )
);