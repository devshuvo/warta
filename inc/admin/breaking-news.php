<?php
/**
 * Breaking News Settings.
 *
 * @package Warta
 */

/**
 * Retrieve last 100 posts
 * -----------------------------------------------------------------------------
 */
$the_query  = new WP_Query( array( 
                'posts_per_page'    => 100,
                'post_status'       => 'publish'      
            ) );
$posts      = array();
$counter    = 0;

if ( $the_query->have_posts() ) {
    while ( $the_query->have_posts() ) {
        $the_query->the_post();

        $posts[ get_the_ID() ] = get_the_title();
    }
} 

wp_reset_postdata(); // Restore original Post Data

$this->sections[] = array(
    'title'     => __('Breaking News', 'warta'),
    'icon'      => 'el-icon-reverse-alt',
    'fields'    => array(
        array(
            'id'       => 'breaking_news_title',
            'type'     => 'text',
            'title'    => __('Title', 'warta'),
            'validate' => 'no_html',
            'default'  => __('Breaking News', 'warta')
        ),
        array(
            'id'       => 'breaking_news',
            'type'     => 'checkbox',
            'title'    => __('Enable on', 'warta'), 
            'subtitle' => __('Where do you want to enable the breaking news?', 'warta'),
            'options'  => array(
                'home'      => 'Home Page',
                'archive'   => 'Archive Page',
                'singular'  => 'Single Post Page',
                'page'      => 'Static Page'
            ),
            'validate' => 'no_html',
        ),
        array(
            'id'       => 'breaking_news_data',
            'type'     => 'radio',
            'title'    => __('Data', 'warta'), 
            'options'  => array(
                'latest'        => 'Latest posts', 
                'cats'          => 'Categories',
                'tags'          => 'Tags',
                'posts'         => 'Selected posts'
            ),
            'default'  => 'latest',
            'validate' => 'no_html'
        ),
        array(
            'id'        => 'breaking_news_categories',
            'type'      => 'select',
            'data'      => 'categories',
            'multi'     => true,
            'title'     => __('Categories', 'warta'), 
            'validate_callback' => 'warta_validate_integer',
            'required'  => array('breaking_news_data','equals','cats'),
        ),
        array(
            'id'        => 'breaking_news_tags',
            'type'      => 'select',
            'data'      => 'tags',
            'multi'     => true,
            'title'     => __('Tags', 'warta'), 
            'validate_callback' => 'warta_validate_integer',
            'required'  => array('breaking_news_data','equals','tags'),
        ),
        array(
            'id'        => 'breaking_news_posts',
            'desc'      => __('Only last 100 posts that are displayed.', 'warta'),
            'type'      => 'select',
            'multi'     => true,
            'title'     => __('Posts', 'warta'), 
            'validate_callback' => 'warta_validate_integer',
            'options'   => $posts,
            'required'  => array('breaking_news_data','equals','posts'),
        ),
        array(
            'id'       => 'breaking_news_ignore_sticky',
            'type'     => 'switch', 
            'title'    => __('Ignore Sticky Posts', 'warta'),
            'default'  => true,
            'validate_callback' => 'warta_validate_integer',
        ),
        array(
            'id'       => 'breaking_news_icon',
            'type'     => 'warta_font_awesome_select',
            'title'    => __('Icon', 'warta'),
            'validate' => 'no_html',
            'default'  => 'fa fa-arrow-circle-o-right'
        ),
        array(
            'id'       => 'breaking_news_count',
            'type'     => 'spinner', 
            'title'    => __('Number of posts', 'warta'),
            'subtitle' => __('How many posts do you want to display?','warta'),
            'default'  => 5,
            'min'      => 1,
            'step'     => 1,
            'max'      => 100,
            'validate_callback' => 'warta_validate_integer',
        ),
        array(
            'id'       => 'breaking_news_duration',
            'type'     => 'slider', 
            'title'    => __('Duration', 'warta'),
            'subtitle' => __('Duration in milliseconds','warta'),
            'default'  => 20000,
            'min'      => 1000,
            'step'     => 1000,
            'max'      => 100000,
            'validate_callback' => 'warta_validate_integer',
        ),
        array(
            'id'       => 'breaking_news_direction',
            'type'     => 'radio',
            'title'    => __('Direction', 'warta'), 
            'options'  => array(
                'left'  => _x('Left', 'Breaking news direction', 'warta'),
                'right' => _x('Right', 'Breaking news direction', 'warta')
            ),
            'default' => 'left'
        )
    )
);