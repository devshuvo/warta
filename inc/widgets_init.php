<?php
/**
 * Register widgetized area
 * 
 * @package Warta
 */

if( !function_exists('warta_widgets_init')) : 
/**
 * Register widgetized area and update sidebar with default widgets.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
function warta_widgets_init() {   
    
    $friskamax_warta      = get_option('friskamax_warta');
        
    /**
     * Top Sections
     * -------------------------------------------------------------------------
     */
    register_sidebar( array(
        'name'          => __( 'Top Section - Homepage', 'warta' ),
        'description'   => __( 'Appears above main section of homepage.', 'warta' ),
        'id'            => 'home-top-section',
        'before_widget' => '<section id="%1$s" class="col-md-12 widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<header class="clearfix"><h4 class="widget-title">',
        'after_title'   => '</h4></header>',
    ) );
    
        
    /**
     * Main Section
     * -------------------------------------------------------------------------
     */
    register_sidebar( array(
        'name'          => __( 'Main Section - Homepage', 'warta' ),
        'description'   => __( 'Appears in the main section of homepage (below breaking news and carousel small if any).', 'warta' ),
        'id'            => 'home-main-section',
        'before_widget' => '<section id="%1$s" class="col-md-12 widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<header class="clearfix"><h4 class="widget-title">',
        'after_title'   => '</h4></header>',
    ) );
    
    /**
     * Bottom Sections
     * -------------------------------------------------------------------------
     */
    register_sidebar( array(
        'name'          => __( 'Bottom Section - Homepage', 'warta' ),
        'description'   => __( 'Appears below main section and sidebar section of homepage.', 'warta' ),
        'id'            => 'home-bottom-section',
        'before_widget' => '<section id="%1$s" class="col-md-12 widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<header class="clearfix"><h4 class="widget-title">',
        'after_title'   => '</h4></header>',
    ) );
    
    /**
     * Before Content
     * -------------------------------------------------------------------------
     */    
    register_sidebar( array(
        'name'          => __( 'Before Content - Archive Page', 'warta' ),
        'description'   => __( 'Appears on top main section of archive page (below breaking news if any).', 'warta' ),
        'id'            => 'archive-before-content-section',
        'before_widget' => '<section id="%1$s" class="col-md-12 widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<header class="clearfix"><h4 class="widget-title">',
        'after_title'   => '</h4></header>',
    ) );
    
    register_sidebar( array(
        'name'          => __( 'Before Content - Single Post Page', 'warta' ),
        'description'   => __( 'Appears on top main section of single post page (below breaking news if any).', 'warta' ),
        'id'            => 'singular-before-content-section',
        'before_widget' => '<section id="%1$s" class="col-md-12 widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<header class="clearfix"><h4 class="widget-title">',
        'after_title'   => '</h4></header>',
    ) );
    
    /**
     * After Content
     * -------------------------------------------------------------------------
     */    
    register_sidebar( array(
        'name'          => __( 'After Content - Archive Page', 'warta' ),
        'description'   => __( 'Appears on bottom main section of archive page (above pagination).', 'warta' ),
        'id'            => 'archive-after-content-section',
        'before_widget' => '<section id="%1$s" class="col-md-12 widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<header class="clearfix"><h4 class="widget-title">',
        'after_title'   => '</h4></header>',
    ) );
    
    register_sidebar( array(
        'name'          => __( 'After Content - Single Post Page', 'warta' ),
        'description'   => __( 'Appears on main section of single post page below post content (or author info if any).', 'warta' ),
        'id'            => 'singular-after-content-section',
        'before_widget' => '<section id="%1$s" class="col-md-12 widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<header class="clearfix"><h4 class="widget-title">',
        'after_title'   => '</h4></header>',
    ) );
    
            
    /**
     * Sidebar Sections
     * -------------------------------------------------------------------------
     */
    register_sidebar( array(
        'name'          => __( 'Sidebar Section - Default', 'warta' ),
        'description'   => __( 'Appears in the sidebar section (next to main section) of all pages. Can be replaced with custom widgets area.', 'warta' ),
        'id'            => 'default-sidebar-section',
        'before_widget' => '<section id="%1$s" class="col-sm-6 col-md-12 widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<header class="clearfix"><h4 class="widget-title">',
        'after_title'   => '</h4></header>',
    ) );
    
    $sidebar = array(
        'homepage_specific_widget'  => array( 
                                        'id'    => 'home-sidebar-section',
                                        'name'  => 'Homepage',
                                        'desc'  => __( 'Appears in the sidebar section (next to main section) of homepage. Replaces "Sidebar Section - Default".', 'warta' )
                                    ),
        'archive_specific_widget'   => array( 
                                        'id'    => 'archive-sidebar-section',
                                        'name'  => 'Archive Page',
                                        'desc'  => __( 'Appears in the sidebar section (next to main section) of archive page. Replaces "Sidebar Section - Default".', 'warta' )
                                    ),
        'singular_specific_widget'  => array( 
                                        'id'    => 'singular-sidebar-section',
                                        'name'  => 'Single Post Page',
                                        'desc'  => __( 'Appears in the sidebar section (next to main section) of single post page. Replaces "Sidebar Section - Default".', 'warta' )
                                    ),
        'page_specific_widget'      => array( 
                                        'id'    => 'page-sidebar-section',
                                        'name'  => 'Static Page',
                                        'desc'  => __( 'Appears in the sidebar section (next to main section) of static page. Replaces "Sidebar Section - Default".', 'warta' )
                                    ),
    );
    
    foreach ($sidebar as $key => $value) {
        if( isset( $friskamax_warta[$key]['sidebar'] ) && $friskamax_warta[$key]['sidebar'] == 1 ) {
            register_sidebar( array(
                'name'          => sprintf( __( "Sidebar Section - %s", 'warta' ), $value['name'] ),
                'id'            => $value['id'],
                'description'   => $value['desc'],
                'before_widget' => '<section id="%1$s" class="col-sm-6 col-md-12 widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<header class="clearfix"><h4 class="widget-title">',
                'after_title'   => '</h4></header>',
            ) );
        }
    }    
            
    
    /**
     * Footer Sections
     * -------------------------------------------------------------------------
     */    
    register_sidebar( array(
        'name'          => __( 'Footer Section - Default', 'warta' ),
        'id'            => 'default-footer-section',
        'description'   => __( 'Appears in the footer section of all pages. Can be replaced with custom widgets area.', 'warta' ),
        'before_widget' => $friskamax_warta['footer_layout'] == 1 
                        ? '<section id="%1$s" class="col-md-2 col-sm-4 %2$s">'
                        : '<section id="%1$s" class="col-md-3 col-sm-6 %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<div class="title"><h4 class="widget-title">',
        'after_title'   => '</h4></div>',
    ) );
    
    $footer = array(
        'homepage_specific_widget'  => array( 
                                        'id'            => 'home-footer-section',
                                        'name'          => 'Homepage',
                                        'before_widget' => $friskamax_warta['homepage_footer_layout'] == 1 
                                                        ? '<section id="%1$s" class="col-md-2 col-sm-4 %2$s">'
                                                        : '<section id="%1$s" class="col-md-3 col-sm-6 %2$s">',
                                        'desc'          => __( 'Appears in the footer section of homepage. Replaces "Footer Section - Default".', 'warta' )
                                    ),
        'archive_specific_widget'   => array( 
                                        'id'            => 'archive-footer-section',
                                        'name'          => 'Archive Page',
                                        'before_widget' => $friskamax_warta['archive_footer_layout'] == 1 
                                                        ? '<section id="%1$s" class="col-md-2 col-sm-4 %2$s">'
                                                        : '<section id="%1$s" class="col-md-3 col-sm-6 %2$s">',
                                        'desc'          => __( 'Appears in the footer section of archive page. Replaces "Footer Section - Default".', 'warta' )
                                    ),
        'singular_specific_widget'  => array( 
                                        'id'            => 'singular-footer-section',
                                        'name'          => 'Single Post Page',
                                        'before_widget' => $friskamax_warta['singular_footer_layout'] == 1 
                                                        ? '<section id="%1$s" class="col-md-2 col-sm-4 %2$s">'
                                                        : '<section id="%1$s" class="col-md-3 col-sm-6 %2$s">',
                                        'desc'          => __( 'Appears in the footer section of single post page. Replaces "Footer Section - Default".', 'warta' )
                                    ),
        'page_specific_widget'      => array( 
                                        'id'            => 'page-footer-section',
                                        'name'          => 'Static Page',
                                        'before_widget' => $friskamax_warta['page_footer_layout'] == 1 
                                                        ? '<section id="%1$s" class="col-md-2 col-sm-4 %2$s">'
                                                        : '<section id="%1$s" class="col-md-3 col-sm-6 %2$s">',
                                        'desc'          => __( 'Appears in the footer section of static page. Replaces "Footer Section - Default".', 'warta' )
                                    ),
    );
    
    foreach ($footer as $key => $value) {
        if( isset( $friskamax_warta[$key]['footer'] ) && $friskamax_warta[$key]['footer'] == 1 ) {
            register_sidebar( array(
                'name'          => sprintf( __( "Footer Section - %s", 'warta' ), $value['name'] ),
                'id'            => $value['id'],
                'before_widget' => $value['before_widget'],
                'description'   => $value['desc'],
                'after_widget'  => '</section>',
                'before_title'  => '<div class="title"><h4 class="widget-title">',
                'after_title'   => '</h4></div>',
            ) );
        }
    }
    
}
endif; // warta_widgets_init
add_action( 'widgets_init', 'warta_widgets_init' );