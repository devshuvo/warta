<?php
/**
 * Warta Post Views Counter
 *
 * @package Warta
 */

if(!function_exists('warta_set_post_views')) :
       /**
        * Add/update post meta views count
        * @param int $postID
        */
        function warta_set_post_views() {
                global $friskamax_warta;
                
                $is_ajax = isset($_POST['id']); 
                
                if( $is_ajax ) {
                        $postID = (int) $_POST['id']; 
                } elseif( is_single() && isset($friskamax_warta['ajax_post_views']) && !$friskamax_warta['ajax_post_views'] ) {
                        $postID = get_the_ID();                              
                } else {
                        return;
                }
                                
                $count = get_post_meta($postID, 'warta_post_views_count', true);
                if($count == '') {
                        $count = 0;
                } else {
                        $count++;
                }

                update_post_meta($postID, 'warta_post_views_count', $count);

                if($is_ajax) {
                        die();
                }
        }
endif; 
add_action( 'before', 'warta_set_post_views' );
add_action( 'wp_ajax_warta_set_post_views', 'warta_set_post_views' );
add_action( 'wp_ajax_nopriv_warta_set_post_views', 'warta_set_post_views' );