<?php
/**
 * Translate TinyMCE custom plugins
 * 
 * @package Warta
 */

if( !function_exists('warta_mce_translation') ) :
function warta_mce_translation() {
        
    $array = array(
        'general'   => array (
                        'title'         => __('Title', 'warta'),
                        'content'       => __('Content', 'warta'),
                        'insert'        => __('Insert', 'warta'),
                        'cancel'        => __('Cancel', 'warta'),
                        'url'           => _x('URL', 'Uniform Resource Locator', 'warta'),
                        'target'        => _x('Target', 'Link target, open link in current tab/new tab', 'warta'),
                        'sameWindow'    => _x('Same window', 'Link target, open in same window', 'warta'),
                        'newWindow'     => _x('New window', 'Link target, open in new window', 'warta'),
                        'maxWidth'      => __('Max width', 'warta'),
                        'maxHeight'     => __('Max height', 'warta'),
                        'style'         => __('Display style', 'warta'),
                        'reverse'       => __('Reverse', 'warta'),
                        'align'         => __('Alignment', 'warta'),
                        'alignLeft'     => __('Align left', 'warta'),
                        'alignCenter'   => __('Align center', 'warta'),
                        'alignRight'    => __('Align right', 'warta'),
                        'alignFull'     => __('Align full', 'warta'),
                    ),
        'columns'   => array(
                        'title'     => __('Columns', 'warta'),
                        'numberCol' => __('Number of columns', 'warta'),
                        'col2'      => __('2 columns', 'warta'),
                        'col3'      => __('3 columns', 'warta'),
                        'col4'      => __('4 columns', 'warta'),
                    ),
        'dropcaps'  => array(
                        'title' => __('Dropcaps', 'warta')
                    ),
        'smallFont' => array(
                        'title' => __('Small Font', 'warta')
                    ),
        'button'    => array (
                        'title'     => __('Button', 'warta'),
                        'style'     => __('Button style', 'warta'),
                        'default'   => __('Default button', 'warta'),
                        'primary'   => __('Primary button', 'warta'),
                        'success'   => __('Success button', 'warta'),
                        'info'      => __('Info button', 'warta'),
                        'warning'   => __('Warning button', 'warta'),
                        'danger'    => __('Danger button', 'warta'),
                        'size'      => __('Button Size', 'warta'),
                        'large'     => __('Large button', 'warta'),
                        'small'     => __('Small button', 'warta'),
                        'extraSmall'=> __('Extra Small button', 'warta'),
                    ),
        'label'     => array(
                        'title'     => __('Label', 'warta'),
                        'style'     => __('Label style', 'warta'),
                        'default'   => __('Default label', 'warta'),
                        'primary'   => __('Primary label', 'warta'),
                        'success'   => __('Success label', 'warta'),
                        'info'      => __('Info label', 'warta'),
                        'warning'   => __('Warning label', 'warta'),
                        'danger'    => __('Danger label', 'warta'),
                    ),
        'alert'     => array(
                        'title'     => __('Alert', 'warta'),
                        'style'     => __('Alert style', 'warta'),
                        'success'   => __('Success alert', 'warta'),
                        'info'      => __('Info alert', 'warta'),
                        'warning'   => __('Warning alert', 'warta'),
                        'danger'    => __('Danger alert', 'warta'),
                    ),
        'quote'     => array(
                        'title'         => __('Custom blockquote', 'warta'),
                        'source'        => __('Source title', 'warta'),
                        'sourceURL'     => __('Source URL', 'warta'),
                    ),
        'embed'     => array(
                        'title'     => __('Embed', 'warta'),
                        'sites'     => __('Click here to see all sites that can be embedded.', 'warta')
                    ),
        'icons'     => array(
                        'title'     => __('Font Awesome Icons', 'warta'),
                    ),
        'tabs'      => array(
                        'title'     => __('Tabs', 'warta'),
                        'add'       => __('Add tab', 'warta'),
                        'remove'    => __('Remove tab', 'warta')
                    ),
        'accordion' => array(
                        'title'     => __('Accordion', 'warta'),
                        'remove'    => __('Remove section', 'warta'),
                        'add'       => __('Add section', 'warta')
                    ),
        'carousel'  => array(
                        'title'     => __('Carousel', 'warta'),
                        'create'    => __('Create Carousel', 'warta')
                    ),
        'divider'   => array(
                        'title'     => __('Divider Line', 'warta')
                    ),
        'nextPage'  => array(
                        'title'     => __('Next Page', 'warta')
                    )
    );
    
    $table_dlg = array(
        "rules_border" => __("border", "warta"), 
        "rules_box" => __("box", "warta"), 
        "rules_vsides" => __("vsides", "warta"), 
        "rules_rhs" => __("rhs", "warta"), 
        "rules_lhs" => __("lhs", "warta"), 
        "rules_hsides" => __("hsides", "warta"), 
        "rules_below" => __("below", "warta"), 
        "rules_above" => __("above", "warta"), 
        "rules_void" => __("void", "warta"), 
        "rules" => __("Rules", "warta"), 
        "frame_all" => __("all", "warta"), 
        "frame_cols" => __("cols", "warta"), 
        "frame_rows" => __("rows", "warta"), 
        "frame_groups" => __("groups", "warta"), 
        "frame_none" => __("none", "warta"), 
        "frame" => __("Frame", "warta"), 
        "caption" => __("Table Caption", "warta"), 
        "missing_scope" => __("Are you sure you want to continue without specifying a scope for this table header cell. "
                . "Without it, it may be difficult for some users with disabilities to understand the content or data displayed of the table.", "warta"), 
        "cell_limit" => __('You\'ve exceeded the maximum number of cells of {$cells}.', "warta"), 
        "row_limit" => __('You\'ve exceeded the maximum number of rows of {$rows}.', "warta"), 
        "col_limit" => __('You\'ve exceeded the maximum number of columns of {$cols}.', "warta"), 
        "colgroup" => __("Col Group", "warta"), 
        "rowgroup" => __("Row Group", "warta"), 
        "scope" => __("Scope", "warta"), 
        "tfoot" => __("Footer", "warta"), 
        "tbody" => __("Body", "warta"), 
        "thead" => __("Header", "warta"), 
        "row_all" => __("Update All Rows in Table", "warta"), 
        "row_even" => __("Update Even Rows in Table", "warta"), 
        "row_odd" => __("Update Odd Rows in Table", "warta"), 
        "row_row" => __("Update Current Row", "warta"), 
        "cell_all" => __("Update All Cells in Table", "warta"), 
        "cell_row" => __("Update All Cells in Row", "warta"), 
        "cell_cell" => __("Update Current Cell", "warta"), 
        "th" => __("Header", "warta"),
        "td" => __("Data", "warta"), 
        "summary" => __("Summary", "warta"), 
        "bgimage" => __("Background Image", "warta"),
        "rtl" => __("Right to Left", "warta"), 
        "ltr" => __("Left to Right", "warta"), 
        "mime" => __("Target MIME Type", "warta"), 
        "langcode" => __("Language Code", "warta"), 
        "langdir" => __("Language Direction", "warta"), 
        "style" => __("Style", "warta"), 
        "id" => __("ID", "warta"),
        "merge_cells_title" => __("Merge Table Cells", "warta"), 
        "bgcolor" => __("Background Color", "warta"), "bordercolor"
        => __("Border Color", "warta"), "align_bottom" => __("Bottom", "warta"), "align_top" => __("Top", "warta"), 
        "valign" => __("Vertical Alignment", "warta"), 
        "cell_type" => __("Cell Type", "warta"), 
        "cell_title" => __("Table Cell Properties", "warta"), 
        "row_title" => __("Table Row Properties", "warta"), 
        "align_middle" => __("Center", "warta"), 
        "align_right" => __("Right", "warta"), 
        "align_left" => __("Left", "warta"), 
        "align_default" => __("Default", "warta"), 
        "align" => __("Alignment", "warta"),
        "border" => __("Border", "warta"), 
        "cellpadding" => __("Cell Padding", "warta"),
        "cellspacing" => __("Cell Spacing", "warta"),
        "rows" => __("Rows", "warta"), 
        "cols" => __("Columns", "warta"), 
        "height" => __("Height", "warta"), 
        "width" => __("Width", "warta"), 
        "title" => __("Insert/Edit Table", "warta"), 
        "rowtype" => __("Row Type", "warta"), 
        "advanced_props" => __("Advanced Properties", "warta"), 
        "general_props" => __("General Properties", "warta"), 
        "advanced_tab" => __("Advanced", "warta"), 
        "general_tab" => __("General", "warta"), 
        "cell_col" => __("Update all cells in column", "warta")
    );
        
    $table = array(        
        "cell" => __("Cell", "warta"), 
        "cell_desc" => __("Table Cell Properties", "warta"), 
        "cellprops_delta_height" => "",
        "cellprops_delta_width" => "", 
        "col" => __("Column", "warta"), 
        "col_after_desc" => __("Insert Column After", "warta"), 
        "col_before_desc" => __("Insert Column Before", "warta"), 
        "copy_row_desc" => __("Copy Table Row", "warta"), 
        "cut_row_desc" => __("Cut Table Row", "warta"), 
        "del" => __("Delete Table", "warta"), 
        "delete_col_desc" => __("Delete Column", "warta"), 
        "delete_row_desc" => __("Delete Row", "warta"), 
        "desc" => __("Insert/Edit Table", "warta"), 
        "merge_cells_delta_height" => "", 
        "merge_cells_delta_width" => "", 
        "merge_cells_desc" => __("Merge Table Cells", "warta"), 
        "paste_row_after_desc" => __("Paste Table Row After", "warta"), 
        "paste_row_before_desc" => __("Paste Table Row Before", "warta"), 
        "props_desc" => __("Table Properties", "warta"), 
        "row" => __("Row", "warta"), 
        "row_after_desc" => __("Insert Row After", "warta"), 
        "row_before_desc" => __("Insert Row Before", "warta"), 
        "row_desc" => __("Table Row Properties", "warta"), 
        "rowprops_delta_height" => "", 
        "rowprops_delta_width" => "",
        "split_cells_desc" => __("Split Merged Table Cells", "warta"), 
        "table_delta_height" => "",
        "table_delta_width" => "",
    );
    
    $locale     = _WP_Editors::$mce_locale;    
    $translated = 'tinyMCE.addI18n( "' . $locale . '.table_dlg", ' . json_encode( $table_dlg ) . ' );';
    $translated .= 'tinyMCE.addI18n( "' . $locale . '.table", ' . json_encode( $table ) . ' );';
        
    foreach ( $array as $key => $value ) {        
        $translated .= 'tinyMCE.addI18n( "' . $locale . '.warta' . ucfirst($key) . '", ' . json_encode( $value ) . ' );';
    }
    
    

    return $translated;
}
endif; // warta_mce_translation

$strings = warta_mce_translation();

