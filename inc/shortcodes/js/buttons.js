/**
 * Adds TinyMCE buttons.
 *
 * @package Warta
 */

+function( $ ) { 'use strict';    
 
    // Register plugin
    tinymce.PluginManager.add( 'warta_shortcodes', function(ed, url) {
        
       /**
        * Columns shortcode
        * ----------------------------------------------------------------------
        */
        ed.addButton( 'warta_columns', {
            title    : ed.getLang('wartaColumns.title'),
            image    : url + '/../img/columns.gif',
            onclick  : function() {
                 ed.windowManager.open({
                     file    : url + '/../modal/modal.php?shortcode=columns',
                     inline  : 1,
                     width   : 250,
                     height  : 263
                 });
            }
        } );
        
       /**
        * Dropcaps shortcode
        * ----------------------------------------------------------------------
        */
        ed.addButton( 'warta_dropcaps', {
            title    : ed.getLang('wartaDropcaps.title'),
            image    : url + '/../img/dropcaps.gif',
            onclick  : function() {
                var content = ed.selection.getContent().replace( new RegExp(/(\[dropcaps\])|(\[\/dropcaps\])/gi), '' ),
                    output  = '[dropcaps]' + content + '[/dropcaps]';
                
                ed.execCommand('mceInsertContent', 0, output);
            }
        } );    
        
       /**
        * Small font shortcode
        * ----------------------------------------------------------------------
        */
        ed.addButton( 'warta_small', {
            title    : ed.getLang('wartaSmallFont.title'),
            image    : url + '/../img/small.png',
            onclick  : function() {
                var content = ed.selection.getContent().replace( new RegExp(/(\<small\>)|(\<\/small\>)/gi), '' ),
                    output  = '<small>' + content + '</small>';
                
                ed.execCommand('mceInsertContent', 0, output);
            }
        } );        
        
       /**
        * Button shortcode
        * ----------------------------------------------------------------------
        */
        ed.addButton( 'warta_button', {
            title    : ed.getLang('wartaButton.title'),
            image    : url + '/../img/button.png',
            onclick  : function() {
                ed.windowManager.open({
                     file   : url + '/../modal/modal.php?shortcode=button',
                     inline : 1,
                     width  : 480,
                     height : 534
                });
            }
        } );
        
       /**
        * Label shortcode
        * ----------------------------------------------------------------------
        */
        ed.addButton( 'warta_label', {
            title    : ed.getLang('wartaLabel.title'),
            image    : url + '/../img/label.png',
            onclick  : function() {
                ed.windowManager.open({
                     file   : url + '/../modal/modal.php?shortcode=label',
                     inline : 1,
                     width  : 260,
                     height : 295
                });
            }
        } );
        
       /**
        * Alert shortcode
        * ----------------------------------------------------------------------
        */
        ed.addButton( 'warta_alert', {
            title    : ed.getLang('wartaAlert.title'),
            image    : url + '/../img/alert.png',
            onclick  : function() {
                ed.windowManager.open({
                     file   : url + '/../modal/modal.php?shortcode=alert',
                     inline : 1,
                     width  : 230,
                     height : 308
                });
            }
        } );
        
       /**
        * Quote shortcode
        * ----------------------------------------------------------------------
        */
        ed.addButton( 'warta_quote', {
            title    : ed.getLang('wartaQuote.title'),
            image    : url + '/../img/quote.png',
            onclick  : function() {
                ed.windowManager.open({
                     file   : url + '/../modal/modal.php?shortcode=quote',
                     inline : 1,
                     width  : 600,
                     height : 320
                });
            }
        } );
        
       /**
        * Embed
        * ----------------------------------------------------------------------
        */
        ed.addButton( 'warta_embed', {
            title    : ed.getLang('wartaEmbed.title'),
            image    : url + '/../img/embed.png',
            onclick  : function() {
                ed.windowManager.open({
                     file   : url + '/../modal/modal.php?shortcode=embed',
                     inline : 1,
                     width  : 530,
                     height : 165
                });
            }
        } );
        
       /**
        * Icons
        * ----------------------------------------------------------------------
        */
        ed.addButton( 'warta_icons', {
            title    : ed.getLang('wartaIcons.title'),
            image    : url + '/../img/icons.png',
            onclick  : function() {
                ed.windowManager.open({
                     file   : url + '/../modal/modal.php?shortcode=icons',
                     inline : 1,
                     width  : 960,
                     height : 455
                });
            }
        } );
        
       /**
        * Tabs shortcode
        * ----------------------------------------------------------------------
        */
        ed.addButton( 'warta_tabs', {
            title    : ed.getLang('wartaTabs.title'),
            image    : url + '/../img/tabs.gif',
            onclick  : function() {
                 ed.windowManager.open({
                     file   : url + '/../modal/modal.php?shortcode=tabs',
                     inline : 1,
                     width  : 960,
                     height : 540
                 });
            }
        } );
        
       /**
        * Accordion shortcode
        * ----------------------------------------------------------------------
        */
        ed.addButton( 'warta_accordion', {
            title    : ed.getLang('wartaAccordion.title'),
            image    : url + '/../img/accordion.gif',
            onclick  : function() {
                 ed.windowManager.open({
                     file   : url + '/../modal/modal.php?shortcode=accordion',
                     inline : 1,
                     width  : 960,
                     height : 540
                 });
            }
        } );
        
       /**
        * Divider
        * ----------------------------------------------------------------------
        */
        ed.addButton( 'warta_divider', {
            title    : ed.getLang('wartaDivider.title'),
            image    : url + '/../img/divider.gif',
            onclick  : function() {
                ed.execCommand('mceInsertContent', 0, '<hr>');
            }
        } );
        
       /**
        * Next Page
        * ----------------------------------------------------------------------
        */
        ed.addButton( 'warta_next_page', {
            title    : ed.getLang('wartaNextPage.title'),
            image    : url + '../../../../../../../wp-includes/js/tinymce/plugins/wordpress/img/page.gif',
            cmd     : "WP_Page"
        } );
        
        
        ed.addButton( 'warta_carousel', {
                title    : ed.getLang('wartaCarousel.title'),
                image    : url + '/../img/carousel.png',
                onclick  : function() {
                        var file_frame;

                        // If the media frame already exists, reopen it.
                        if ( file_frame ) {
                                file_frame.open();
                                return;
                        }

                        // Create the media frame.
                        file_frame = wp.media.frames.file_frame = wp.media({
                                title: ed.getLang('wartaCarousel.title'),
                                button: {
                                        text: ed.getLang('wartaCarousel.create'),
                                },
                                library: {
                                        type: "image"
                                },
                                multiple: true // Set to true to allow multiple files to be selected
                        });

                        // When an image is selected, run a callback.
                        file_frame.on( 'select', function() {                                
                                var attachment  = file_frame.state().get('selection').toJSON(), //  Get selection from the uploader
                                    output      = '';

                                // Do something with attachment.id and/or attachment.url here
                                $.each( attachment, function() {
                                        output += this.id + ',';
                                });
                                
                                ed.execCommand('mceInsertContent', 0, '[carousel ids="' + output.replace(/,$/, '') + '"]');
                        });

                        // Finally, open the modal
                        file_frame.open();                         
                }
        } );
        
        /**
         * Disable buttons
         * =====================================================================
         */
        [
            'warta_columns',
            'warta_dropcaps',
            'warta_small',
            'warta_button',
            'warta_label',
            'warta_alert'
        ].forEach( function(element, index, array) {            
            ed.onNodeChange.add(function(ed, cm, n) {
                cm.get( element ).setDisabled( ed.selection.isCollapsed() );
            });
        } );
        
        /**
         * Show/hide kitchen sink on row 3 and 4
         * =====================================================================
         */
        ed.onInit.add(function( ed ) {
                if ( getUserSetting( 'hidetb', '0' ) == '0' ) {
                        $( '.mceToolbarRow3, .mceToolbarRow4' ).hide();
                }

                $( '.mceToolbarRow1' ).click(function() {
                        var $this = $( this );
                        if ( $this.siblings( '.mceToolbarRow2' ).is( ':visible' ) ) {
                                $this.siblings( '.mceToolbarRow3, .mceToolbarRow4' ).show();
                        } else {
                                $this.siblings( '.mceToolbarRow3, .mceToolbarRow4' ).hide();
                        }
                });
        });
       
    });
    
} ( jQuery );