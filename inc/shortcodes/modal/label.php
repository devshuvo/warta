<?php 
/**
 * TinyMCE modal content: columns shortcode.
 *
 * @package Warta
 */
?>

<!--Input
---------------------------------------------------------------------------- -->
<fieldset>
    <table>
        <tr>
            <td>{#wartaLabel.style}</td>
            <td>
                <label>
                    <input type="radio" name="option" value="default" checked> 
                    <span class="label label-default">{#wartaLabel.default}</span>
                </label><br><br>
                <label>
                    <input type="radio" name="option" value="primary"> 
                    <span class="label label-primary">{#wartaLabel.primary}</span>
                </label><br><br>
                <label>
                    <input type="radio" name="option" value="success"> 
                    <span class="label label-success">{#wartaLabel.success}</span>
                </label><br><br>
                <label>
                    <input type="radio" name="option" value="info"> 
                    <span class="label label-info">{#wartaLabel.info}</span>
                </label><br><br>
                <label>
                    <input type="radio" name="option" value="warning"> 
                    <span class="label label-warning">{#wartaLabel.warning}</span>
                </label><br> <br>               
                <label>
                    <input type="radio" name="option" value="danger"> 
                    <span class="label label-danger">{#wartaLabel.danger}</span>
                </label>
            </td>
        </tr>
    </table>
</fieldset>

<!--Script
---------------------------------------------------------------------------- -->
<script>
    
+function( $ ) { 'use strict';
    var Label = {

        ed: '',

        init: function(ed) {
            Label.ed = ed;
            tinyMCEPopup.resizeToInnerSize();
        },

        insert: function(ed) {
            var option      = $('[name=option]:checked').val(),
                content     = ed.selection.getContent().replace( new RegExp(/(\[label.*?\])|(\[\/label\])/gi), '' ),
                output      = '[label style="' + option + '"]' + content + '[/label]';

            tinyMCEPopup.execCommand( 'mceReplaceContent', false, output);
            tinyMCEPopup.close();
        }
    };

    tinyMCEPopup.onInit.add(Label.init, Label);

    $('body').on('submit', '#shortcode_form', function() {
            Label.insert(Label.ed);
    });
    
} (jQuery);

</script>