<?php 
/**
 * TinyMCE modal content: alert shortcode.
 *
 * @package Warta
 */
?>

<!--Input
---------------------------------------------------------------------------- -->
<fieldset>
    <table>
        <tr>
            <td>
                <label>
                    <input type="radio" name="option" value="success"> 
                    <div class="alert alert-success">{#wartaAlert.success}</div>
                </label><br>
                <label>
                    <input type="radio" name="option" value="info"> 
                    <div class="alert alert-info">{#wartaAlert.info}</div>
                </label><br>
                <label>
                    <input type="radio" name="option" value="warning"> 
                    <div class="alert alert-warning">{#wartaAlert.warning}</div>
                </label><br>
                <label>
                    <input type="radio" name="option" value="danger"> 
                    <div class="alert alert-danger">{#wartaAlert.danger}</div>
                </label>
            </td>
        </tr>
    </table>
</fieldset>

<!--Script
---------------------------------------------------------------------------- -->
<script>
    
+function( $ ) { 'use strict';
    var Alert = {

        ed: '',

        init: function(ed) {
            Alert.ed = ed;
            tinyMCEPopup.resizeToInnerSize();
        },

        insert: function(ed) {
            var option      = $('[name=option]:checked').val(),
                content     = ed.selection.getContent().replace( new RegExp(/(\[alert.*?\])|(\[\/alert\])/gi), '' ),
                output      = '[alert style="' + option + '"]' + content + '[/alert]';

            tinyMCEPopup.execCommand( 'mceReplaceContent', false, output);
            tinyMCEPopup.close();
        }
    };

    tinyMCEPopup.onInit.add(Alert.init, Alert);

    $('body').on('submit', '#shortcode_form', function() {
            Alert.insert(Alert.ed);
    });
    
} (jQuery);

</script>