<?php 
/**
 * TinyMCE modal content: button shortcode.
 *
 * @package Warta
 */
?>

<!--Input
---------------------------------------------------------------------------- -->
<fieldset>
    <table>
        <tr>
            <td>{#wartaButton.style}</td>
            <td>
                <label>
                    <input type="radio" name="option" value="default" checked> 
                    <span class="btn btn-default btn-xs">{#wartaButton.default}</span>
                </label><br>
                <label>
                    <input type="radio" name="option" value="primary"> 
                    <span class="btn btn-primary btn-xs">{#wartaButton.primary}</span>
                </label><br>
                <label>
                    <input type="radio" name="option" value="success"> 
                    <span class="btn btn-success btn-xs">{#wartaButton.success}</span>
                </label><br>
                <label>
                    <input type="radio" name="option" value="info"> 
                    <span class="btn btn-info btn-xs">{#wartaButton.info}</span>
                </label><br>
                <label>
                    <input type="radio" name="option" value="warning"> 
                    <span class="btn btn-warning btn-xs">{#wartaButton.warning}</span>
                </label><br>                
                <label>
                    <input type="radio" name="option" value="danger"> 
                    <span class="btn btn-danger btn-xs">{#wartaButton.danger}</span>
                </label>
            </td>
        </tr>
        <tr><td colspan="2">&nbsp;</td></tr>
        <tr>
            <td>{#wartaButton.size}</td>
            <td>
                <label>
                    <input type="radio" name="size" value="lg"> 
                    <span class="btn btn-primary btn-lg">{#wartaButton.large}</span>
                </label><br>
                <label>
                    <input type="radio" name="size" value="" checked> 
                    <span class="btn btn-primary">{#wartaButton.default}</span>
                </label><br>               
                <label>
                    <input type="radio" name="size" value="sm"> 
                    <span class="btn btn-primary btn-sm">{#wartaButton.small}</span>
                </label><br>                
                <label>
                    <input type="radio" name="size" value="xs"> 
                    <span class="btn btn-primary btn-xs">{#wartaButton.extraSmall}</span>
                </label>
            </td>
        </tr>
        <tr><td colspan="2">&nbsp;</td></tr>
        <tr>
            <td>{#wartaGeneral.url}</td>
            <td><input type="url" name="url" class="input-light" placeholder="http://"></td>
        </tr>
        <tr><td colspan="2">&nbsp;</td></tr>
        <tr>
            <td>{#wartaGeneral.target}</td>
            <td>
                <label>
                    <input type="radio" name="target" value="" checked> {#wartaGeneral.sameWindow}
                </label><br>
                <label>
                    <input type="radio" name="target" value="_blank"> {#wartaGeneral.newWindow}
                </label>
            </td>
        </tr>
    </table>
</fieldset>

<!--Script
---------------------------------------------------------------------------- -->
<script>
    
+function( $ ) { 'use strict';
    var Buttons = {

        ed: '',

        init: function(ed) {
            Buttons.ed = ed;
            tinyMCEPopup.resizeToInnerSize();
        },

        insert: function(ed) {
            var option      = $('[name=option]:checked').val(),
                size        = $('[name=size]:checked').val(),
                size_op     = size ? ' size="' + size + '"' : '',
                url         = $('[name=url]').val(),
                target      = $('[name=target]:checked').val(),
                target_op   = target ? ' target="' + target + '"' : '',
                content     = ed.selection.getContent().replace( new RegExp(/(\[button.*?\])|(\[\/button\])/gi), '' ),
                output      = '[button style="' + option + '"' + size_op + ' url="' + url + '"' + target_op + ']' + content + '[/button]';

            tinyMCEPopup.execCommand( 'mceReplaceContent', false, output);
            tinyMCEPopup.close();
        }
    };

    tinyMCEPopup.onInit.add(Buttons.init, Buttons);

    $('body').on('submit', '#shortcode_form', function() {
            Buttons.insert(Buttons.ed);
    });
        
} (jQuery);

</script>