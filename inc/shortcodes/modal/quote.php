<?php 
/**
 * TinyMCE modal content: quote shortcode.
 *
 * @package Warta
 */
?>

<!--Input
---------------------------------------------------------------------------- -->
<fieldset>
    <table>
        <tr>
            <td>{#wartaGeneral.content}</td>
            <td><textarea name="content" class="input-light" rows="5"></textarea></td>
        </tr>
        <tr>
            <td>{#wartaQuote.source}</td>
            <td><input type="text" name="source_title" class="input-light"></td>
        </tr>
        <tr>
            <td>{#wartaQuote.sourceURL}</td>
            <td><input type="url" name="source_url" class="input-light" placeholder="http://"></td>
        </tr>
        <tr>
            <td>{#wartaGeneral.style}</td>
            <td>
                <label class="checkbox">
                    <input type="checkbox" name="style" value="reverse"> {#wartaGeneral.reverse}
                </label>
            </td>
        </tr>
    </table>
</fieldset>

<!--Script
---------------------------------------------------------------------------- -->
<script>
    
+function( $ ) { 'use strict';
    var Quote = {

        ed: '',

        init: function(ed) {
            Quote.ed = ed;
            tinyMCEPopup.resizeToInnerSize();
        },

        insert: function(ed) {
            var content         = $('[name=content]').val(),
                sourceTitle     = $('[name=source_title]').val(),
                sourceTitle_op  = sourceTitle ? ' source_title="' + sourceTitle + '"' : '',
                sourceURL       = $('[name=source_url]').val(),
                sourceURL_op    = sourceURL && ( sourceURL !== 'http://' ) ? ' source_url="' + sourceURL + '"' : '',
                style           = $('[name=style]:checked').val(),
                style_op        = style ? ' style="reverse"' : '',
                output          = '[blockquote' + sourceTitle_op + sourceURL_op + style_op + ']' + content + '[/blockquote]';

            tinyMCEPopup.execCommand( 'mceReplaceContent', false, output);
            tinyMCEPopup.close();
        }
    };

    tinyMCEPopup.onInit.add(Quote.init, Quote);

    $('body').on('submit', '#shortcode_form', function() {
            Quote.insert(Quote.ed);
    });
        
} (jQuery);

</script>