<?php 
/**
 * TinyMCE modal content: columns shortcode.
 *
 * @package Warta
 */
?>

<!--Input
---------------------------------------------------------------------------- -->
<fieldset>
    <table>
        <tr>
            <td>{#wartaColumns.numberCol}</td>
            <td>
                <label><input type="radio" name="columns" value="2" checked> {#wartaColumns.col2}</label><br>
                <label><input type="radio" name="columns" value="3"> {#wartaColumns.col3}</label><br>
                <label><input type="radio" name="columns" value="4"> {#wartaColumns.col4}</label>
            </td>
        </tr>
        <tr><td colspan="2">&nbsp;</td></tr>
        <tr>
            <td>{#wartaGeneral.align}</td>
            <td>
                <label><input type="radio" name="align" value="left" checked> {#wartaGeneral.alignLeft}</label><br>
                <label><input type="radio" name="align" value="center"> {#wartaGeneral.alignCenter}</label><br>
                <label><input type="radio" name="align" value="right"> {#wartaGeneral.alignRight}</label>
                <label><input type="radio" name="align" value="full"> {#wartaGeneral.alignFull}</label>
            </td>
        </tr>
    </table>
</fieldset>

<!--Script
---------------------------------------------------------------------------- -->
<script>
    
+function( $ ) { 'use strict';
    var Columns = {

        ed: '',

        init: function(ed) {
            Columns.ed = ed;
            tinyMCEPopup.resizeToInnerSize();
        },

        insert: function(ed) {
            var columns = $('[name=columns]:checked').val(),
                align   = $('[name=align]:checked').val(),
                content = ed.selection.getContent().replace( new RegExp(/(\[columns.*?\])|(\[\/columns\])/gi), '' ),
                output  = '[columns col="' + columns + '" align="' + align + '"]' + content + '[/columns]';

            tinyMCEPopup.execCommand('mceReplaceContent', false, output);
            tinyMCEPopup.close();
        }
    };

    tinyMCEPopup.onInit.add(Columns.init, Columns);

    $('body').on('submit', '#shortcode_form', function() {
            Columns.insert(Columns.ed);
    });
    
} (jQuery);

</script>