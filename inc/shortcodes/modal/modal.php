<?php
/**
 * TinyMCE modal window.
 *
 * @package Warta
 */
?><!DOCTYPE html>
<html>
    
<head>
<meta charset="UTF-8">
<title>
    <?php
        $shortcode = filter_input( INPUT_GET, 'shortcode');
        if ($shortcode) {
            echo ucfirst($shortcode);
        }
    ?>
</title>
<link rel="stylesheet" href="../../../css/style.css">
<style>
        fieldset { 
            margin-top  : 20px!important;
            padding     : 15px!important; 
        }
        fieldset:first-of-type .remove_tab { display: none }
        fieldset:first-of-type { padding: 15px 15px 0!important;  }

        input[type="radio"] {
            margin-top      : -1px;
            vertical-align  : middle;
        }

        input[type="checkbox"] { margin: 0 }

        input, select, textarea {
            color       : #606068!important;
            font-size   : 10px!important;
            font-weight : normal!important;
        }

        input[type="color"], 
        input[type="email"], 
        input[type="number"], 
        input[type="password"], 
        input[type="tel"], 
        input[type="url"], 
        input[type="search"], 
        input[type="text"] {
            height: 20px;
        }

        label { font-weight: normal; }
        label:last-child { margin-bottom: 15px }

        table { width: 100% }

        td { vertical-align: top }

        textarea { 
            -webkit-box-sizing  : border-box;
            -moz-box-sizing     : border-box;
            box-sizing          : border-box; 
            margin-bottom       : 15px; 
        }

        a {
            color       : #3C2BB6!important;
            font-family : Verdana, Arial;
            font-size   : 10px;
        }

        .alert { 
                display : inline-block;
                margin  : 0;
        }
        
        .checkbox { margin-top: 0 }
        
        .fa {
                cursor: pointer;
                font-size: 18px;
                margin: 8px;
        }
        
        .label          { font-size: 1em; }
        .remove_tab     { margin-bottom: 15px; }
</style>
<script src="../../../../../../wp-includes/js/jquery/jquery.js"></script>
<script src="../../../../../../wp-includes/js/tinymce/tiny_mce_popup.js"></script>
</head>

<body>

<form id="shortcode_form">
        <?php     
                if( $shortcode ) {
                        $file = __DIR__ . '/' . $shortcode . '.php'; 

                        if( file_exists($file) ) { 
                                require $file;
                        }
                }
        ?>

        <p>
                <button id="insert" type="submit">{#wartaGeneral.insert}</button>
                <button id="cancel">{#wartaGeneral.cancel}</button>
        </p>
</form>
        
<script>
    
+function( $ ) { 'use strict';
    
    var $body       = $('body'),
        fixChrome   = function() {
                            var spanWrapper = window.parent.jQuery('.clearlooks2 .mceMiddle span').css('top', 0);
                            setTimeout( function (){
                                    spanWrapper.css('top', '23px');            
                                    $(window).scrollTop( $body.height() );
                            }, 10 );
                    };

   /**
    * Add tab
    * -------------------------------------------------------------------------
    */
   $body.on('click', 'p:has(.add_tab)', function() {
       $(this).prev().clone().insertBefore(this).find('input, textarea').each(function() {
                $(this).val('');
       });
       
       fixChrome();       
   });

   /**
    * Remove tab
    * -------------------------------------------------------------------------
    */
   $body.on('click', '.remove_tab', function() {
       $(this).closest('fieldset').remove();
       fixChrome();
   });

   /**
    * Close
    * -------------------------------------------------------------------------
    */
   $('body').on('click', '#cancel', function() {
       tinyMCEPopup.close();
   });

} ( jQuery );

</script>
    
</body>    
</html>