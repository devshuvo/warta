<?php 
/**
 * TinyMCE modal content: tabs shortcode.
 *
 * @package Warta
 */
?>

<!--Input
---------------------------------------------------------------------------- -->
<fieldset>
    <table>
        <tr>
            <td>{#wartaGeneral.title}</td>
            <td>
                <input type="text" class="title input-light" required>
            </td>
        </tr>
        <tr>
            <td>{#wartaGeneral.content}</td>
            <td>
                <textarea class="content input-light" rows="10"></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="2"><a href="#" class="remove_tab">{#wartaTabs.remove}</a></td>
        </tr>
    </table>
</fieldset>
<p><a href="#" class="add_tab">{#wartaTabs.add}</a></p>

<!--Script
---------------------------------------------------------------------------- -->
<script>
    
+function( $ ) { 'use strict';
    var Tabs = {

        ed: '',

        init: function(ed) {
            Tabs.ed = ed;
            tinyMCEPopup.resizeToInnerSize();
        },

        insert: function(ed) {
            var contents    = $('.content'),
                head        = '',
                body        = '';
            
            $('.title').each(function( i ) {
                var title   = $(this).val(),
                    active  = '';
                
                if( i === 0 ) {
                    active = ' active="true"';
                }
                
                head += '[tab_title' + active + ']' + title + '[/tab_title]';
                body += '[tab_content for="' + title + '"' + active + ']' + contents.eq(i).val() + '[/tab_content]';
            });
            
            var output = '[tabs][tab_head]' + head + '[/tab_head][tab_body]' + body + '[/tab_body][/tabs]';  

            tinyMCEPopup.execCommand('mceReplaceContent', false, output);
            tinyMCEPopup.close();
        }
    };

    tinyMCEPopup.onInit.add(Tabs.init, Tabs);
    
    /**
     * Insert
     * -------------------------------------------------------------------------
     */
    $('body').on('submit', '#shortcode_form', function() {
            Tabs.insert(Tabs.ed);
    });
    
} (jQuery);

</script>