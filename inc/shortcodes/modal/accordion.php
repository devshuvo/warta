<?php 
/**
 * TinyMCE modal content: accordion shortcode.
 *
 * @package Warta
 */
?>

<!--Input
---------------------------------------------------------------------------- -->
<fieldset>
    <table>
        <tr>
            <td>{#wartaGeneral.title}</td>
            <td>
                <input type="text" class="title input-light" required>
            </td>
        </tr>
        <tr>
            <td>{#wartaGeneral.content}</td>
            <td>
                <textarea class="content input-light" rows="10"></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="2"><a href="#" class="remove_tab">{#wartaAccordion.remove}</a></td>
        </tr>
    </table>
</fieldset>
<p><a href="#" class="add_tab">{#wartaAccordion.add}</a></p>

<!--Script
---------------------------------------------------------------------------- -->
<script>
    
+function( $ ) { 'use strict';
    var Accordion = {

        ed: '',

        init: function(ed) {
            Accordion.ed = ed;
            tinyMCEPopup.resizeToInnerSize();
        },

        insert: function(ed) {
            var contents    = $('.content'),
                output      = '[accordion]';
            
            $('.title').each(function( i ) {                
                output += '[accordion_section title="' + $(this).val() + '"]' + contents.eq(i).val() + '[/accordion_section]';                
            });
            
            output += '[/accordion]';
            
            tinyMCEPopup.execCommand('mceReplaceContent', false, output);
            tinyMCEPopup.close();
        }
    };

    tinyMCEPopup.onInit.add(Accordion.init, Accordion);
    
   /**
    * Insert
    * -------------------------------------------------------------------------
    */
    $('body').on('submit', '#shortcode_form', function() {
            Accordion.insert(Accordion.ed);
    });
    
} (jQuery);

</script>