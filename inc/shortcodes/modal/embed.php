<?php 
/**
 * TinyMCE modal content: embed shortcode.
 *
 * @package Warta
 */
?>

<!--Input
---------------------------------------------------------------------------- -->
<fieldset>
    <table>
        <tr>
            <td>{#wartaGeneral.url}</td>
            <td><input type="url" name="url" class="input-light" placeholder="http://"></td>
        </tr>
<!--        <tr>
            <td>{#wartaGeneral.maxWidth}</td>
            <td><input type="number" name="max_width" class="input-light"></td>
        </tr>
        <tr>
            <td>{#wartaGeneral.maxHeight}</td>
            <td><input type="number" name="max_height" class="input-light"></td>
        </tr>-->
    </table>
</fieldset>
<p>
    <a href="http://codex.wordpress.org/Embeds#Okay.2C_So_What_Sites_Can_I_Embed_From.3F" target="_blank">{#wartaEmbed.sites}</a>
</p>

<!--Script
---------------------------------------------------------------------------- -->
<script>
    
+function( $ ) { 'use strict';
    var Embed = {

        ed: '',

        init: function(ed) {
            Embed.ed = ed;
            tinyMCEPopup.resizeToInnerSize();
        },

        insert: function(ed) {
            var url             = $('[name=url]').val(),
                maxWidth        = $('[name=max_width]').val(),
                maxWidth_op     = maxWidth ? ' width="' + maxWidth + '"' : '',
                maxHeight       = $('[name=max_height]').val(),
                maxHeight_op    = maxHeight ? ' height="' + maxHeight + '"' : '',
                output          = '[embed' + maxWidth_op + maxHeight_op + ']' + url + '[/embed]';

            tinyMCEPopup.execCommand( 'mceReplaceContent', false, output);
            tinyMCEPopup.close();
        }
    };

    tinyMCEPopup.onInit.add(Embed.init, Embed);

    $('body').on('submit', '#shortcode_form', function() {
            Embed.insert(Embed.ed);
    });
        
} (jQuery);

</script>