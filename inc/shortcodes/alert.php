<?php
/**
 * Alert Shortcode.
 *
 * @package Warta
 */

if( !function_exists('warta_alert_shortcode') ) :
function warta_alert_shortcode( $atts, $content = '' ) {    
    extract( shortcode_atts( 
        array( 'style' => 'default' ), 
        $atts 
    ) );
    
    $content    = apply_filters('the_content', $content);
    $content    = str_replace(']]>', ']]&gt;', $content);
    
    return '<div class="alert alert-' . esc_attr($style) . '">' . do_shortcode( $content ) . '</div>';
}
endif; // warta_alert_shortcode
add_shortcode( 'alert', 'warta_alert_shortcode' );