<?php
/**
 * Initialize Shortcodes.
 *
 * @package Warta
 */

/**
 * Load Shorcodes
 * =============================================================================
 */
require __DIR__ . '/columns.php';
require __DIR__ . '/dropcaps.php';
require __DIR__ . '/tabs.php';
require __DIR__ . '/accordion.php';
require __DIR__ . '/button.php';
require __DIR__ . '/label.php';
require __DIR__ . '/alert.php';
require __DIR__ . '/quote.php';
require __DIR__ . '/icons.php';
require __DIR__ . '/carousel.php';

// replace core shortcodes
require __DIR__ . '/gallery.php';



if( !function_exists('warta_register_tinymce_plugin') ) :
/**
 * Registering TinyMCE Plugin
 * =============================================================================
 */
function warta_register_tinymce_plugin() {
    add_filter( "mce_external_plugins", "warta_add_buttons" );
    add_filter( 'mce_buttons_3', 'warta_register_buttons' );
    add_filter( 'mce_buttons_4', 'warta_register_buttons_table' );
}
endif; // warta_register_tinymce_plugin
add_action( 'init', 'warta_register_tinymce_plugin' );



/**
 * Add TinyMCE buttons
 * -----------------------------------------------------------------------------
 * @param array $plugins Default plugins
 * @return array $plugins Default and custom plugins
 */
function warta_add_buttons( $plugins ) {
    $plugins['warta_shortcodes'] = get_template_directory_uri()  . '/inc/shortcodes/js/buttons.js';
    $plugins['table'] = get_template_directory_uri()  . '/inc/shortcodes/js/plugin/table/editor_plugin_src.js';
    return $plugins;
}



if( !function_exists('warta_register_buttons') ) :
/**
 * Register TinyMCE buttons
 * -----------------------------------------------------------------------------
 * @param array $buttons Default buttons
 * @return array $buttons Default and custom buttons
 */
function warta_register_buttons( $buttons ) {
        array_push( 
                $buttons, 
                'warta_columns',
                'warta_dropcaps',
                'warta_small',
                'warta_button',
                'warta_label',
                'warta_alert',
                'warta_quote',
                'warta_embed',
                'warta_icons',
                'warta_tabs',
                'warta_accordion',
                'warta_carousel',
                'warta_divider',
                'warta_next_page'
        );
        return $buttons;
}
endif; // warta_register_buttons




if( !function_exists('warta_register_buttons_table') ) :
/**
 * Register TinyMCE table buttons
 */
function warta_register_buttons_table($buttons) {
        array_push( 
                $buttons, 
                'table',
                'tablecontrols'
        );
        return $buttons;
}
endif; // warta_register_table



/**
 * Register custom classes
 * -----------------------------------------------------------------------------
 * @param array $arr_options
 * @return array $arr_options
 */
function add_custom_classes($arr_options) {    
    $arr_options['theme_advanced_styles'] = ""
            . "Default=table table-hover;"
            . "Striped rows=table table-hover table-striped;"
            . "Bordered table=table table-hover table-bordered;"
            . "Bordered and striped=table table-hover table-striped table-bordered";
    return $arr_options;
}
add_filter('tiny_mce_before_init', 'add_custom_classes');



if( !function_exists('warta_mce_translate') ) :
/**
 * Get TinyMCE translation
 * -----------------------------------------------------------------------------
 * @param array $arr
 * @return array $arr
 */
function warta_mce_translate( $arr )
{
    $arr[] = __DIR__ . '/lang/lang.php';
    return $arr;
}
endif; // warta_mce_translate
add_filter( 'mce_external_languages', 'warta_mce_translate', 10, 1 );