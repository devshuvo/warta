<?php

/**
 * Gallery functions
 * 
 * @package Warta
 */

if ( ! function_exists( 'warta_match_gallery' ) ) :
/**
 * Get information for featured image use
 * 
 * @param string $content Text to search for
 * @param int $id Post ID
 * @return array
 */
function warta_match_gallery( $content = '', $id = 0 ) {
    
        $content        = $content ? $content : get_the_content();
        $output         = array();

        preg_match_all('/\[(?:gallery|carousel).+?ids="([0-9 ,]+?)".*?\]/is', $content, $matches_gallery);
                        
        if( $matches_gallery[0] ) {
                $output['image_ids'] = array();
                
                foreach ( $matches_gallery[1] as $value) {
                        $output['image_ids'] = array_merge( 
                                $output['image_ids'],
                                explode( ',', $value )
                        );
                }        
                
        } else {
                $attachments = get_children( array(
                        'post_parent'       => !!$id ? $id : get_the_ID(), 
                        'post_status'       => 'inherit', 
                        'post_type'         => 'attachment', 
                        'post_mime_type'    => 'image', 
                ) );
                
                if( $attachments ) {
                        $output['image_ids'] = array_keys( $attachments );                        
                }
        }
                
        return $output;
    
} 
endif; // ! function_exists( 'warta_match_gallery' )

/**
 * Modify gallery shortcode attributes.
 * 
 * @param array $out    The output array of shortcode attributes.
 * @param array $pairs  The supported attributes and their defaults.
 * @param array $atts   The user defined shortcode attributes.
 * 
 * @return array        New attributes.
 */
function warta_shortcode_atts_gallery( $out, $pairs, $atts ) {
        $o = friskamax_get_options( array( 
                'gallery_page_columns'  => ''
        ), 'warta_shortcode_atts_gallery_o' );
                        
        $out['columns'] = $o->gallery_page_columns ? $o->gallery_page_columns : $out['columns']; 
        
        return $out;
}
add_filter( 'shortcode_atts_gallery', 'warta_shortcode_atts_gallery', 10, 3 );