<?php
/**
 * Archive custom functions.
 * 
 * @since 1.7.0
 * @package warta\archive
 */

/* -------------------------------------------------------------------------- *
 * Category
 * -------------------------------------------------------------------------- */

// Custom Fields
// -----------------------------------------------------------------------------

if ( ! function_exists( 'warta_category_form_fields' ) ) :
/**
 * Add extra fields to category edit form callback function.
 * 
 * @param object $tag Current category term object.
 */
function warta_category_form_fields( $tag ) { 
        $cat_meta       = friskamax_parse_args( 
                                get_option( "warta_cat_meta_" . $tag->term_id, array() ), 
                                array( 'page_title_bg' => '' ), 
                                'warta_category_form_fields_cat_meta' 
                        ); 
        
        wp_enqueue_media();
        
        ?>

        <tr class="form-field">
                <th scope="row" valign="top"><label for="cat_Image_url"><?php _e( 'Header Background', 'warta' ); ?></label></th>
                <td>
                        <!--  Preview
                        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                        <div id="warta_page_title_bg_holder">
                                <?php if( $cat_meta->page_title_bg ) {
                                        echo wp_get_attachment_image( $cat_meta->page_title_bg, 'medium' );
                                } ?>
                        </div>
                        
                        <!-- Buttons
                        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->      
                        <a class="button" id="warta_page_title_bg_upload" href="#"><?php _e( 'Upload', 'warta' ) ?></a>
                        <a class="button <?php if ( ! $cat_meta->page_title_bg ) echo 'hidden' ?>" id="warta_page_title_bg_remove" href="#"><?php _e( 'Remove', 'warta' ) ?></a>

                        <!-- Script
                        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                        <script>
                                jQuery( document ).ready( function ( $ ) {
                                        var     $btnUpload      = $( "#warta_page_title_bg_upload" ),
                                                $btnRemove      = $( '#warta_page_title_bg_remove' ),
                                                $field          = $( "#warta_page_title_bg" ),
                                                $holder         = $( "#warta_page_title_bg_holder" );
                                        
                                        $btnUpload.click( function ( event ) {
                                                var UploadFrame = false;

                                                event.preventDefault();

                                                if ( UploadFrame ) {
                                                        UploadFrame.open();
                                                        return;
                                                }

                                                UploadFrame = wp.media.frames.my_upload_frame = wp.media( {
                                                        frame   : "select",
                                                        title   : "<?php _e( 'Header Background', 'warta' ) ?>",
                                                        library : { type: "image" },
                                                        button  : { text: "<?php _e( 'Set as Header Background', 'warta' ) ?>" },
                                                        multiple: false
                                                } );

                                                UploadFrame.on( "select", function () {
                                                        var selection = UploadFrame.state().get( "selection" );

                                                        selection.map( function (attachment) {
                                                                attachment = attachment.toJSON();

                                                                if ( attachment.id ) {
                                                                        var     imgUrl          = attachment.sizes.medium ? attachment.sizes.medium.url : attachment.url,
                                                                                $newImage       = $( "<img>" ).attr( { src: imgUrl } );;

                                                                        $field.val( attachment.id );

                                                                        $holder.empty().append( $newImage );
                                                                        $btnRemove.removeClass( 'hidden' ).show();
                                                                }
                                                        } );
                                                } );

                                                UploadFrame.open();
                                        } );

                                        $btnRemove.click( function(event) {
                                                $( '#warta_page_title_bg' ).val( '' );
                                                $holder.empty();
                                                $( this ).hide();

                                                event.preventDefault();
                                        } );
                                } );
                        </script>

                        <!-- The field
                        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                        <input type="hidden" name="warta_cat_meta[page_title_bg]" id="warta_page_title_bg" value="<?php echo $cat_meta->page_title_bg; ?>">
                </td>
        </tr>
        
        <?php
}
endif;
add_action ( 'edit_category_form_fields', 'warta_category_form_fields' );

if ( ! function_exists( 'warta_save_category_form_fields' ) ) :
/**
 * Save extra category extra fields callback function.
 * 
 * @param int $term_id
 */
function warta_save_category_form_fields( $term_id ) {
        if ( ! isset( $_POST['warta_cat_meta'] ) ) return;
        
        $cat_meta = get_option( "warta_cat_meta_$term_id", array() );

        foreach ( $_POST['warta_cat_meta'] as $key => $value ) {
                $cat_meta[$key] = sanitize_text_field( $value );
        }

        //save the option array
        update_option( "warta_cat_meta_" . $term_id, $cat_meta );
}
endif;
add_action ( 'edited_category', 'warta_save_category_form_fields' );

// Walker
// -----------------------------------------------------------------------------

if ( ! class_exists( 'Warta_Walker_CategoryDropdown_SlugValue' ) ) : 
/**
 * Create HTML dropdown list of Categories.
 * Change option value from $category->id to $category->slug
 *
 * @package warta
 * @since 1.3.4
 * @uses Walker
 */
class Warta_Walker_CategoryDropdown_SlugValue extends Walker_CategoryDropdown {
        /**
         * Start the element output.
         *
         * @see Walker::start_el()
         * @since 2.1.0
         *
         * @param string $output   Passed by reference. Used to append additional content.
         * @param object $category Category data object.
         * @param int    $depth    Depth of category. Used for padding.
         * @param array  $args     Uses 'selected' and 'show_count' keys, if they exist. @see wp_dropdown_categories()
         */
        function start_el( &$output, $category, $depth = 0, $args = array(), $id = 0 ) {
            $pad = str_repeat('&nbsp;', $depth * 3);

            /** This filter is documented in wp-includes/category-template.php */
            $cat_name = apply_filters( 'list_cats', $category->name, $category );

            $output .= "\t<option class=\"level-$depth\" value=\"".$category->slug."\"";
            if ( $category->slug == $args['selected'] )
                $output .= ' selected="selected"';
            $output .= '>';
            $output .= $pad.$cat_name;
            if ( $args['show_count'] )
                $output .= '&nbsp;&nbsp;('. number_format_i18n( $category->count ) .')';
            $output .= "</option>\n";
        }
}
endif;

/* -------------------------------------------------------------------------- *
 * Deprecated
 * -------------------------------------------------------------------------- */

/**
 * Sets the authordata global when viewing an author archive.
 *
 * This provides backwards compatibility with
 * http://core.trac.wordpress.org/changeset/25574
 *
 * It removes the need to call the_post() and rewind_posts() in an author
 * template to print information about the author.
 * 
 * @since 1.0.0
 * @deprecated since version 1.7.5 https://github.com/Automattic/_s/commit/2580a0a69f091b98272bc74e35c347b34a20a52d
 *
 * @global WP_Query $wp_query WordPress Query object.
 * @return void
 */
function warta_setup_author() {
        _deprecated_function( __FUNCTION__, '1.7.5' );
        
        global $wp_query;

        if ( $wp_query->is_author() && isset( $wp_query->post ) ) {
                $GLOBALS['authordata'] = get_userdata( $wp_query->post->post_author );
        }
}