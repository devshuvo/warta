<?php

/* -------------------------------------------------------------------------- *
 * Actions
 * -------------------------------------------------------------------------- */

if ( ! function_exists( 'warta_bbp_template_after_user_profile' ) ) :
/**
 * Add social media icons on bbp_template_after_user_profile hook.
 */
function warta_bbp_template_after_user_profile() {
        warta_social_media_author( array( 
                'user_id' => bbp_get_user_id() 
        ) );
}
endif;
add_action( 'bbp_template_after_user_profile', 'warta_bbp_template_after_user_profile' );

/* -------------------------------------------------------------------------- *
 * Filters
 * -------------------------------------------------------------------------- */

if ( ! function_exists( 'warta_bbp_before_list_forums_parse_args' ) ) :
/**
 * Modify bbp_list_forums() arguments.
 * @param array $args
 * @return array
 */
function warta_bbp_before_list_forums_parse_args( $args ) {      
        $args['before']                 = '<ul class="bbp-forums-list tags clearfix">';
        $args['separator']              = '';
        $args['show_topic_count']       = false;
        $args['show_reply_count']       = false;

        return $args;
}
endif; 
add_filter( 'bbp_before_list_forums_parse_args', 'warta_bbp_before_list_forums_parse_args' );

if ( ! function_exists( 'warta_bbp_before_get_topic_pagination_parse_args' ) ) : 
/**
 * Modify bbp_get_topic_pagination(0 arguments.
 * @param array $args
 * @return array
 */
function warta_bbp_before_get_topic_pagination_parse_args($args) {
        $args['before'] = '<span class="bbp-topic-pagination page-links">';

        return $args;
}
endif;
add_filter( 'bbp_before_get_topic_pagination_parse_args', 'warta_bbp_before_get_topic_pagination_parse_args' );