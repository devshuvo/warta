<?php

/**
 * Header custom functions.
 * 
 * @since 1.7.0
 * @package warta\extras
 */

/* -------------------------------------------------------------------------- *
 * Title
 * -------------------------------------------------------------------------- */

if ( version_compare( $GLOBALS['wp_version'], '4.1', '<' ) ) :
        /**
         * Filters wp_title to print a neat <title> tag based on what is being viewed.
         * 
         * @since 1.0.0
         *
         * @param string $title Default title text for current view.
         * @param string $sep Optional separator.
         * @return string The filtered title.
         */
        function warta_wp_title( $title, $sep ) {
                if ( is_feed() ) {
                    return $title;
                }

                global $page, $paged;

                // Add the blog name
                $title .= get_bloginfo( 'name', 'display' );

                // Add the blog description for the home/front page.
                $site_description = get_bloginfo( 'description', 'display' );
                if ( $site_description && ( is_home() || is_front_page() ) ) {
                    $title .= " $sep $site_description";
                }

                // Add a page number if necessary:
                if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
                    $title .= " $sep " . sprintf( __( 'Page %s', 'warta' ), max( $paged, $page ) );
                }

                return $title;
        }
        add_filter( 'wp_title', 'warta_wp_title', 10, 2 );

        /**
         * Title shim for sites older than WordPress 4.1.
         *
         * @link https://make.wordpress.org/core/2014/10/29/title-tags-in-4-1/
         * @todo Remove this function when WordPress 4.3 is released.
         * 
         * @since 1.7.5
         */
        function warta_render_title() {
                ?>

                <title><?php wp_title( '|', true, 'right' ); ?></title>

                <?php
        }
        add_action( 'wp_head', 'warta_render_title' );
endif; // version_compare( $GLOBALS['wp_version'], '4.1', '<' )

/* -------------------------------------------------------------------------- *
 * Page title
 * -------------------------------------------------------------------------- */

/**
 * Filter the page title arguments.
 * 
 * @since 1.7.0
 * 
 * @param array $d      The default arguments of warta_get_page_title() 
 * @return array        Filtered arguments of warta_get_page_title()
 */
function warta_get_page_title_r_default( $d ) {
        if ( warta_page_title_is_site_title() ) return $d;
        
        if ( function_exists( 'is_woocommerce' ) && is_woocommerce() ) {
                $d['primary']   = woocommerce_page_title( false );
                $d['secondary'] = get_post_meta( warta_get_page_id(), 'friskamax_page_subtitle', true );
                
                return $d;
        }
        
        if ( function_exists( 'is_bbpress' ) && is_bbpress() ) {
                $d['primary']   = get_the_title();
                $d['secondary'] = get_post_meta( get_the_ID(), 'friskamax_page_subtitle', true );
                
                return $d;
                
        }
        
        if ( is_single() || is_page() ) {        
                $is_aside       = get_post_format() == 'aside';
                $subtitle       = get_post_meta( get_the_ID(), 'friskamax_page_subtitle', true ); 

                if ( is_single() ) {
                        $d['primary']   = $is_aside ? ''        : get_the_title();
                        $d['secondary'] = $subtitle ? $subtitle : get_the_category_list( _x( ' / ', 'Used between category list items.', 'warta' ) );
                }

                if ( is_page() ) {
                        $d['primary']   = get_the_title();
                        $d['secondary'] = $subtitle;
                }
        }
        
        if ( is_archive() ) {
                $archive_title  = warta_get_archive_page_title();
                $d['primary']   = $archive_title['primary'];
                $d['secondary'] = $archive_title['secondary'];
                $d['one_line']  = term_description() ? false : true;
        }
        
        if ( is_home() ) {
                $d['primary']   = get_the_title( get_queried_object_id() );
                $d['secondary'] = get_post_meta( get_queried_object_id(), 'friskamax_page_subtitle', true );
        }
        
        if ( is_search() ) {
                $d['primary']   = get_search_query();
                $d['secondary'] = __( 'Search Results for:', 'warta' );
                $d['one_line']  = true;
        }
        
        return $d;
}
add_filter( 'warta_get_page_title_r_default', 'warta_get_page_title_r_default', 9 ); 

// gege31415 already add this from his child theme
if ( ! function_exists( 'gege31415_add_yoast_breadcrumb_after_page_title' ) ) :
       /**
        * Add Yoast Breadcrumbs after page title.
        * 
        * @since 1.7.5
        * 
        * @param string $page_title    HTML page title.
        * @return string               HTML page title and Yoast Breadcrumbs.
        */
       function warta_add_yoast_breadcrumb_after_page_title( $page_title ) {
               if ( function_exists('yoast_breadcrumb') ) {
                       $breadcrumb = yoast_breadcrumb( '<div class="breadcrumb-wrapper wordpress-seo-breadcrumb-wrapper"><div class="container">', "</div></div>", false );
                       $breadcrumb = trim( $breadcrumb );
                       
                       if ( ! empty( $breadcrumb ) ) {
                               $page_title .= $breadcrumb;
                       }
               } else if (function_exists('woocommerce_breadcrumb')) {
                    ob_start();
                    woocommerce_breadcrumb(array(
			'wrap_before' => '<nav class="breadcrumb-wrapper woocommerce-breadcrumb"><div class="container">',
			'wrap_after'  => '</div></nav>',
                    ));
                    
                    $page_title .= ob_get_clean();
               }

               return $page_title;
       }
       add_filter( 'warta_get_page_title', 'warta_add_yoast_breadcrumb_after_page_title' );
endif; 

/* -------------------------------------------------------------------------- *
 * Entry header
 * -------------------------------------------------------------------------- */

/**
 * Append <hr> to entry-header. 
 * 
 * @since 1.7.0
 * 
 * @param string $entry_header HTML entry-header.
 * @return string
 */
function warta_add_hr_to_entry_header( $entry_header ) {   
        // Return if Page Title content is not Site Title and Tagline.
        if ( ! warta_page_title_is_site_title() ) return $entry_header;
        
        return  is_page() ||
                ( is_single() && warta_template_single_is_featured_image_included() ) ||
                ( function_exists( 'is_bbpress' ) && is_bbpress() )
                ? $entry_header . '<hr>'
                : $entry_header;
} 
add_filter( 'warta_get_entry_header', 'warta_add_hr_to_entry_header', 9 );

/* -------------------------------------------------------------------------- *
 * Page header
 * -------------------------------------------------------------------------- */

/**
 * Filter warta_get_page_header()'s default arguments.
 * 
 * @since 1.7.0
 * 
 * @param array $d      The default arguments.
 * @return array        The default filtered arguments.
 */
function warta_get_page_header_r_default( $d ) {        
        if ( is_archive() ) {
                $title = warta_get_archive_page_title( array( 'concat' => true ) );
                $d['title']     = $title['primary'];
                $d['subtitle']  = $title['secondary'];
        }
        
        if ( is_search() ) {
                $d['title']     = sprintf( __( 'Search Results for: %s', 'warta' ), '<span>' . get_search_query() . '</span>' );
        }
        
        return $d;
}
add_filter( 'warta_get_page_header_r_default', 'warta_get_page_header_r_default', 9 );

/* -------------------------------------------------------------------------- *
 * Global Variables
 * -------------------------------------------------------------------------- */

/**
 * Setup global variables.
 * 
 * @since 1.7.0
 * @access private
 * 
 * @global type $friskamax_warta_var
 * @return type
 */
function warta_setup_global_variables() {
        global $friskamax_warta_var;
        
        if( 
                ( function_exists( 'is_woocommerce' )   && is_woocommerce() )   ||
                ( function_exists( 'is_bbpress' )       && is_bbpress() ) 
        ) {
                $friskamax_warta_var['page']      = 'page';
                $friskamax_warta_var['html_id']   = 'blog-detail';
                
                return;
        }
        
        if ( is_page() ) {
                switch( get_page_template_slug() ) {
                        case 'template-archive-1.php':
                                warta_set_archive_page_vars( array( 'archive_layout' => 1 ) ); 
                                break;
                        case 'template-archive-2.php':
                                warta_set_archive_page_vars( array( 'archive_layout' => 2 ) ); 
                                break;
                        case 'template-archive-3.php':
                                warta_set_archive_page_vars( array( 'archive_layout' => 3 ) ); 
                                break;
                        case 'template-page-full-width.php':
                                $friskamax_warta_var['page']      = 'page';
                                $friskamax_warta_var['html_id']   = 'blog-detail';
                                $friskamax_warta_var['full-width']= true;
                                break;
                        default:
                                $friskamax_warta_var['page']      = 'page';
                                $friskamax_warta_var['html_id']   = 'blog-detail';
                                break;
                }
        }
        
        if ( is_archive() || is_home() || is_search() ) {
                warta_set_archive_page_vars();
        }
        
        if ( is_single() ) {                        
                $friskamax_warta_var['page']      = 'singular';
                $friskamax_warta_var['html_id']   = 'blog-detail';
        }
        
        if ( is_404() ) {
                $friskamax_warta_var['html_id'] = 'page-404';
                $friskamax_warta_var['page']    = 'page';
        }
}
add_action( 'get_header', 'warta_setup_global_variables' );