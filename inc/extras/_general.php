<?php

if ( ! function_exists( 'warta_get_html_class' ) ) :
/**
 * Returns html classes.
 * 
 * @return string
 */
function warta_get_html_class() {
        $o              = friskamax_get_options( array(
                                'boxed_style'           => false,
                                'flat_style'            => false,
                                'image_light'           => true,
                                'zoom_effect'           => true,
                                'ajax_post_views'       => false,
                        ), 'warta_get_html_class_o' );
        $html_class     = '';

        if ( $o->boxed_style )          $html_class .= ' boxed-style';
        if ( $o->flat_style )           $html_class .= ' flat-style';
        if ( ! $o->image_light )        $html_class .= ' no-image-light';
        if ( ! $o->zoom_effect )        $html_class .= ' no-zoom-effect';
        if ( $o->ajax_post_views )      $html_class .= ' ajax-post-views';

        return apply_filters( 'warta_get_html_class', $html_class, $o );
}
endif; 

if ( ! function_exists( 'warta_html_class' ) ) : 
/**
 * Print html classes.
 */
function warta_html_class() {        
        echo warta_get_html_class();
}
endif;

/* -------------------------------------------------------------------------- *
 * Filters
 * -------------------------------------------------------------------------- */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function warta_body_classes( $classes ) {
        // Adds a class of group-blog to blogs with more than 1 published author.
        if ( is_multi_author() ) {
            $classes[] = 'group-blog';
        }

        return $classes;
}
add_filter( 'body_class', 'warta_body_classes' );