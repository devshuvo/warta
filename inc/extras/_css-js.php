<?php

/**
 * Add custom css code to head .
 */
function warta_css_code() {        
        $o = friskamax_get_options( array( 'css_code' => '' ), 'warta_css_code_var_o' );

        ?>

        <!--Warta - IE8 support-->
        <!--[if lt IE 9]>
                <link href="<?php echo get_template_directory_uri() ?>/css/ie8.css" rel="stylesheet">        
                <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
                <script src="<?php echo get_template_directory_uri() ?>/js/ie8.js"></script>
        <![endif]-->

        <?php if( $o->css_code ) :  ?>
                <!--Warta - Custom CSS-->
                <style><?php echo $o->css_code ?></style>
        <?php endif; ?>

        <?php
}
add_action( 'wp_head', 'warta_css_code', 20 );

if ( ! function_exists( 'warta_js_code' ) ):
/**
 * Add custom js code to footer.
 */
function warta_js_code() {
        $o = friskamax_get_options( array( 
                'js_code'       => '',
                'tracking_code' => ''
        ), 'warta_js_code_o' );
        
        ?>

        <?php if( $o->js_code ) :  ?>
                <!--Warta - Custom JavaScript-->
                <script><?php echo $o->js_code ?></script>
        <?php endif; ?>

        <?php if( $o->tracking_code ) :  ?>
                <!--Warta - Tracking Code-->
                <?php echo $o->tracking_code; ?>
        <?php endif; ?>

        <?php
}
endif; //! function_exists( 'warta_js_code' )
add_action( 'wp_footer', 'warta_js_code', 20 );
