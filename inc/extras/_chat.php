<?php

/**
 * Post format chat functions
 * 
 * @package Warta
 */

if( !function_exists('warta_match_chat') ) :
/**
 * Get chat excerpt from a post
 * 
 * @param array $args
 * @return $string chat html code
 */
function warta_match_chat( $args = array() ) {
        $args           = apply_filters('warta_match_chat_args', $args);
        $r              = wp_parse_args( $args, array( 'chat_count' => 5 ) );
        $content        = wpautop( get_the_content() );
        $output         = '';
        
        if( preg_match("/(<p>.+?<\/p>.{0,1}){1,{$r['chat_count']}}/is", $content, $matches_chat) ) {
                $output = $matches_chat[0];
        } 
                
        return apply_filters('warta_match_chat', $output);
} 
endif; // warta_match_chat
