<?php

/* -------------------------------------------------------------------------- *
 * Page Title
 * -------------------------------------------------------------------------- */

// Disable page title.
add_filter( 'woocommerce_show_page_title', '__return_false' );

/**
 * Adds page header on archive product.
 */
function warta_woocommerce_archive_description() {
        warta_page_header( array(
                'title'     => woocommerce_page_title( false ),
                'subtitle'  => get_post_meta( warta_get_page_id(), 'friskamax_page_subtitle', true )
        ) );      
}
add_action( 'woocommerce_archive_description', 'warta_woocommerce_archive_description' );

/* -------------------------------------------------------------------------- *
 * Archive
 * -------------------------------------------------------------------------- */

// Remove the opening anchor tag for products in the loop.
remove_action('woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10);
// Remove the closing anchor tag for products in the loop
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5);
// Move the average rating in the loop.
remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
add_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_rating', 20);
        
/**
 * Change number of products displayed per page.
 * @param int $post_per_page
 * @return int
 */
function warta_woocommerce_loop_shop_per_page( $post_per_page ) {
        return 12;
}
add_filter( 'loop_shop_per_page', 'warta_woocommerce_loop_shop_per_page' );

/**
 * Modify the CSS classes from the add to cart template for the loop.
 * 
 * Remove `button` class.
 * 
 * @param array $args
 * @param WC_Product $product
 * @return array
 */
function warta_woocommerce_loop_add_to_cart_args($args, $product) {
    if (isset($args['class'])) {
        // Remove `button` class.
        $class          = " {$args['class']} ";
        $args['class']  = trim(str_replace(' button ', '', $class));
    }
    
    return $args;
}
add_filter('woocommerce_loop_add_to_cart_args', 'warta_woocommerce_loop_add_to_cart_args', 10, 2);

/* -------------------------------------------------------------------------- *
 * Single
 * -------------------------------------------------------------------------- */

/**
 * Related products args.
 * @return array
 */
function warta_woocommerce_output_related_products_args() {
		return array(
                'posts_per_page'        => 4,
                'columns'               => 4,
                'orderby'               => 'rand'
		);
}
add_filter( 'woocommerce_output_related_products_args', 'warta_woocommerce_output_related_products_args' );

// Reorder product summary. {content-single-product.php}.
// -----------------------------------------------------------------------------
remove_action   ( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
add_action      ( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 11 );
remove_action   ( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
add_action      ( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 12 );