<?php

if( !function_exists('warta_match_featured_media') ) :         
/**
 * Get featured media from a post
 * 
 * @return string Featured media html code
 */
function warta_match_featured_media( $args = array() ) {       
        global $friskamax_warta;
        
        $args                   = apply_filters('warta_match_featured_media_args', $args);
        $r                      = wp_parse_args( $args, array(
                                        'chat_count'                    => 5,           // number of paragraphs
                                        'gallery_convert_to_carousel'   => FALSE,       // conver [gallery] to [carousel]
                                ) );        
        $o                      = wp_parse_args( (array) $friskamax_warta, array(
                                        'featured_media_convert_gallery_to_carousel' => TRUE
                                ) );
        $convert_to_carousel    = $r['gallery_convert_to_carousel'] 
                                ? TRUE 
                                : $o['featured_media_convert_gallery_to_carousel'];
        $format                 = get_post_format();
        $content                = trim( get_the_content() );
        $pattern                = '';
        $return                 = '';
        
        switch ( $format ) {
                case 'video':
                case 'audio':
                case 'gallery':                        
                        if( isset( $o[ "featured_media_$format" ] ) ) {        
                                foreach ( $o[ "featured_media_$format" ] as $shortcode ) {
                                        $shortcode      = preg_replace('/\[|\]/', '', $shortcode);
                                        $pattern        .= "(\[$shortcode.*?\[\/$shortcode\])|(\[$shortcode.*?\])|";
                                }
                                $pattern = trim($pattern, '|');
                                
                                if( !!$pattern && preg_match("/$pattern/is", $content, $matches) ) {
                                        // convert [gallery] to [carousel]
                                        if( $format == 'gallery' && $convert_to_carousel ) {       
                                                $matches[0] = preg_replace('/^(\[gallery)(.+?\])$/is', '[carousel$2', $matches[0] );
                                        }

                                        $return = preg_match('/^\[embed/i', $matches[0])
                                                ? $GLOBALS['wp_embed']->run_shortcode($matches[0]) // [embed] needs $GLOBALS['wp_embed']
                                                : do_shortcode($matches[0]);
                                }
                        }
                        break;
                        
                case 'quote':
                        $return = warta_match_quote();
                        
                        if( !$return && $content ) {
                                $return = "<blockquote>$content</blockquote>";
                        }
                        
                        break;
                        
                case 'link':
                        $link = warta_match_link();
                        
                        if( $link ) {
                                $return = "<div class='link-content'>$link</div>";
                        }
                        
                        break;
                        
                case 'aside':                 
                case 'status':                 
                case 'chat':                        
                        if($content) {
                                switch ($format) {
                                        case 'aside':                 
                                        case 'status': 
                                                $format_content = wpautop($content);
                                                break;
                                        case 'chat':
                                                $format_content = warta_match_chat($r);
                                                break;
                                }
                                
                                $return = "<div class='$format-content'>$format_content</div>";
                        }      
                        
                        break;
        }
        
        return apply_filters('warta_match_featured_media', $return);
                        
}
endif; // warta_match_featured_media









if( !function_exists( 'warta_match_video' ) ) :
/**
 * Get video content from a post
 * 
 * @return string Video html code
 * @deprecated since version 1.6.4
 */
function warta_match_video( $start = '', &$rest_content = '' ) {
    
        $content    = get_the_content();
        $output     = '';
        
        preg_match( "/{$start}(?:<p.*?>|)(\[video.*?\[\/video\])/is", $content, $matches_video );
        preg_match( "/{$start}(?:<p.*?>|)(\[wpvideo.*?\])/is", $content, $matches_wp_video );

        if( $matches_video ) {        
                $rest_content   = str_replace( $matches_video[1], '', $content);
                $output         = do_shortcode( $matches_video[1] );
        } else if( $matches_wp_video ) {
                $rest_content   = str_replace( $matches_wp_video[1], '', $content);
                $output         = do_shortcode( $matches_wp_video[1] );
        } else {
                $content = apply_filters( 'the_content', $content );
                
                preg_match( "/{$start}(?:<p.*?>|)(<iframe.*?<\/iframe>)/is", $content, $matches_iframe );
                
                if( $matches_iframe ) {
                    $output         = $matches_iframe[1];
                    $rest_content   = str_replace( $output, '', $content);
                }
        } 
        
        return $output;
}
endif; // warta_match_video



if( !function_exists( 'warta_match_audio' ) ) :
/**
 * Get audio content from a post
 * \
 * @return string Audio html code
 * @deprecated since version 1.6.4
 */
function warta_match_audio( $start = '', &$rest_content = '' ) {
    
        $content    = get_the_content();
        $output     = '';
        
        preg_match("/{$start}(?:<p.*?>|)(\[audio.*?\[\/audio\])/i", $content, $matches_audio);
        if( !$matches_audio ) {
                preg_match("/{$start}(?:<p.*?>|)(\[audio.*?\]|\[playlist.*?\])/i", $content, $matches_audio);
        }
        
        if( !!$matches_audio ) {
                $output         = do_shortcode( $matches_audio[1] );
                $rest_content   = str_replace( $matches_audio[1], '', $content);                
        } 
        
        return $output;
}
endif; // warta_match_audio