<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features
 *
 * @package Warta
 */

require_once dirname( __FILE__ ) . '/_archive.php';
require_once dirname( __FILE__ ) . '/_excerpt.php';
require_once dirname( __FILE__ ) . '/_general.php';
require_once dirname( __FILE__ ) . '/_helper.php';
require_once dirname( __FILE__ ) . '/_nav.php';
require_once dirname( __FILE__ ) . '/_profile.php';
require_once dirname( __FILE__ ) . '/_widget.php';
require_once dirname( __FILE__ ) . '/_featured-media.php';
require_once dirname( __FILE__ ) . '/_gallery.php';
require_once dirname( __FILE__ ) . '/_quote.php';
require_once dirname( __FILE__ ) . '/_image.php';
require_once dirname( __FILE__ ) . '/_chat.php';
require_once dirname( __FILE__ ) . '/_jetpack.php';
require_once dirname( __FILE__ ) . '/_link.php';
require_once dirname( __FILE__ ) . '/_counts.php';
require_once dirname( __FILE__ ) . '/_css-js.php';
require_once dirname( __FILE__ ) . '/_woocommerce.php';
require_once dirname( __FILE__ ) . '/_header.php';
require_once dirname( __FILE__ ) . '/_bbpress.php';
require_once dirname( __FILE__ ) . '/_page-builder.php';