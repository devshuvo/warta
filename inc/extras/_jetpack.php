<?php

/**
* Add theme support for Responsive Videos.
 *
 * @since 1.8.1.2
*/
function warta_responsive_videos_setup() {
    add_theme_support( 'jetpack-responsive-videos' );
}
add_action( 'after_setup_theme', 'warta_responsive_videos_setup' );

/**
 * Retrieve an image to represent an attachment without Jetpack Photon.
 *
 * Disable Jetpack Photon.
 * Call wp_get_attachment_image_src().
 * Re-enable Jetpack Photon.
 *
 * @since 1.8.1.2
 *
 * @param int          $attachment_id Image attachment ID.
 * @param string|array $size          Optional. Image size. Accepts any valid image size, or an array of width
 *                                    and height values in pixels (in that order). Default 'thumbnail'.
 * @param bool         $icon          Optional. Whether the image should be treated as an icon. Default false.
 * @return false|array Returns an array (url, width, height, is_intermediate), or false, if no image is available.
 */
function warta_get_attachment_image_src_without_jetpack_photon($attachment_id, $size = 'thumbnail', $icon = false) {
    $photon_removed = false;
    
    if ( class_exists( 'Jetpack' ) && Jetpack::is_module_active( 'photon' ) ) {
        $photon_removed = remove_filter( 'image_downsize', array( Jetpack_Photon::instance(), 'filter_image_downsize' ) );
    }
    
    $src = wp_get_attachment_image_src($attachment_id, $size, $icon);
    
    if ( $photon_removed ) {
        add_filter( 'image_downsize', array( Jetpack_Photon::instance(), 'filter_image_downsize' ), 10, 3 );
    }
    
    return $src;
}

/**
 * Modify the Photon Arguments added to an image when going through Photon, when that image size is a string.
 * 
 * Change fit to resize for medium and large images.
 *
 * @since 1.8.1.2
 *
 * @param array $photon_args Array of Photon arguments.
 * @param array $args {
 * 	 Array of image details.
 *
 * 	 @type $image_args Array of Image arguments (width, height, crop).
 * 	 @type $image_url Image URL.
 * 	 @type $attachment_id Attachment ID of the image.
 * 	 @type $size Image size. Can be a string (name of the image size, e.g. full) or an integer.
 * 	 @type $transform Value can be resize or fit.
 *                    @see https://developer.wordpress.com/docs/photon/api
 * }
 */
function warta_jetpack_photon_image_downsize_string($photon_args, $args) {
    if ( ! isset( $photon_args['fit'] ) ) {        
        return $photon_args;
    }
    
    switch ($args['size']) {
        case 'medium':
            $ratio = 185 / 350;
            break;
        case 'large':
            $ratio = 370 / 730;
            break;
    }
    
    if ( isset($ratio) ) {
        if ( $args['image_args']['height'] === 0 ) {
            $args['image_args']['height']   = $args['image_args']['width'] * $ratio;
            $photon_args['fit']             = $args['image_args']['width'] . ',' . $args['image_args']['height'];
        }

        $photon_args['resize'] = $photon_args['fit'];
        unset( $photon_args['fit'] );
    }
    
    return $photon_args;
}
add_filter('jetpack_photon_image_downsize_string', 'warta_jetpack_photon_image_downsize_string', 10, 2);