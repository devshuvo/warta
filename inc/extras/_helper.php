<?php
/**
 * Helper functions.
 * 
 * @since 1.1.0
 * @package warta
 */

/* -------------------------------------------------------------------------- *
 * Twitter
 * -------------------------------------------------------------------------- */

/**
 * Check if the user has input the twitter API keys in options page.
 * 
 * @since 1.6.2
 * 
 * @return boolean
 */
function warta_isset_twitter_api() {
        $options = get_option( 'friskamax_warta' );

        if ( $options ) {
                $consumer_key       = trim( $options['twitter_consumer_key'] ); 
                $consumer_secret    = trim( $options['twitter_consumer_secret'] ); 
                $access_token       = trim( $options['twitter_access_token'] ); 
                $access_secret      = trim( $options['twitter_access_token_secret'] );  

                return $consumer_key && $consumer_secret && $access_token && $access_secret;
        }
}

/* -------------------------------------------------------------------------- *
 * Pages
 * -------------------------------------------------------------------------- */

/**
 * Get the page ID.
 *  
 * It works like get_the_ID(), except it combines all WooCommerce pages into 
 * one, shop page and combines all bbPress pages into one, forums page.
 * 
 * @since 1.6.2
 * 
 * @return int
 */
function warta_get_page_id() {
        $page_id = 0;
        
        if ( function_exists( 'is_woocommerce' ) && is_woocommerce() && function_exists( 'wc_get_page_id' ) ) {
                $page_id = wc_get_page_id( 'shop' );
        } else if ( function_exists( 'is_bbpress' ) && is_bbpress() && function_exists( 'bbp_get_root_slug' ) ) {
                $page_id = get_page_by_path( bbp_get_root_slug() )->ID;
        } else if ( is_page() || is_single() ) {
                $page_id = get_the_ID(); 
        }

        return $page_id;
}

/**
 * Setup global variables for archive pages.
 */
function warta_set_archive_page_vars( $args = array() ) {
        global  $friskamax_warta,
                $friskamax_warta_var, 
                $content_width;

        $o      = friskamax_get_options( array(
                        'archive_layout' => 3
                ), 'warta_set_archive_page_vars_o' );
        $r      = friskamax_parse_args($args, array(
                        'archive_layout' => $o->archive_layout
                ), 'warta_set_archive_page_vars_r' ); 
        
        $friskamax_warta_var['page']            = 'archive';
        $friskamax_warta['archive_layout']      = $r->archive_layout;

        switch ( $r->archive_layout ) {
                case 1: 
                        $friskamax_warta_var['html_id'] = 'blog-version-1';
                        $content_width = 717;
                        break;
                case 2: 
                        $friskamax_warta_var['html_id'] = 'blog-version-2';
                        $content_width = 360;
                        break;
                default:
                        $friskamax_warta_var['html_id'] = 'blog-version-3';
                        $content_width = 645;
        }
}

/**
 * Return true if full width carousel enabled for the current page.
 * 
 * @return boolean
 */
function warta_is_full_width_carousel_enabled() {
        return ( boolean ) get_post_meta( get_the_ID(), 'warta_full_width_carousel_enable', true );
}

/* -------------------------------------------------------------------------- *
 * Header
 * -------------------------------------------------------------------------- */

/**
 * Return archive page title and subtitle.
 * 
 * @param array $args           Optional. Arguments.
 * @param boolean $args[contat] Optional. Whether to concatenate or not.
 *                              $primary    = $secondary . $primary;
                                $secondary  = '';
 * @return type
 */
function warta_get_archive_page_title( $args = array() ) {
        $r                      = friskamax_parse_args( $args, array( 'concat' => false ), 'warta_get_archive_page_title_r' );
        $primary                = ''; // Title
        $secondary              = ''; // Subtitle
        $term_description       = term_description(); // Show an optional term description.

        if ( is_category() ) :
                $primary    = single_cat_title('', 0);
                $secondary  = __('Category: ', 'warta');
        elseif ( is_tag() ) :
                $primary    = single_tag_title('', 0);
                $secondary  = __('Tag: ', 'warta');
        elseif ( is_author() ) :
                $secondary  = __( 'Author: ', 'warta' );
                $primary    = get_the_author();
        elseif ( is_day() ) :
                $secondary  = __( 'Day: ', 'warta' );
                $primary    = get_the_date();
        elseif ( is_month() ) :
                $secondary  = __( 'Month: ', 'warta' );
                $primary    = get_the_date( _x( 'F Y', 'monthly archives date format', 'warta' ) );
        elseif ( is_year() ) :
                $secondary  = __( 'Year: ', 'warta' );
                $primary    = get_the_date( _x( 'Y', 'yearly archives date format', 'warta' ) );
        elseif ( is_tax( 'post_format' ) ) :
                if ( is_tax( 'post_format', 'post-format-aside' ) ) :
                        $primary = __( 'Asides', 'warta' );
                elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) :
                        $primary = __( 'Galleries', 'warta');
                elseif ( is_tax( 'post_format', 'post-format-image' ) ) :
                        $primary = __( 'Images', 'warta');
                elseif ( is_tax( 'post_format', 'post-format-video' ) ) :
                        $primary = __( 'Videos', 'warta' );
                elseif ( is_tax( 'post_format', 'post-format-quote' ) ) :
                        $primary = __( 'Quotes', 'warta' );
                elseif ( is_tax( 'post_format', 'post-format-link' ) ) :
                        $primary = __( 'Links', 'warta' );
                elseif ( is_tax( 'post_format', 'post-format-status' ) ) :
                        $primary = __( 'Statuses', 'warta' );
                elseif ( is_tax( 'post_format', 'post-format-audio' ) ) :
                        $primary = __( 'Audios', 'warta' );
                elseif ( is_tax( 'post_format', 'post-format-chat' ) ) :
                        $primary = __( 'Chats', 'warta' );
                endif;
        else :
                $primary = __( 'Archives', 'warta' );
        endif;
        
        if ( $term_description ) {
                $primary    = $secondary . $primary;
                $secondary  = strip_tags( $term_description ); 
        } elseif ( $r->concat ) {
                $primary    = $secondary . $primary;
                $secondary  = '';
        }
        
        return apply_filters( 'warta_get_archive_page_title', array(
                'primary'       => $primary,
                'secondary'     => $secondary
        ) );
}

/**
 * Return true is the Page Title content is Site Title and Tagline.
 * @return boolean
 */
function warta_page_title_is_site_title() {
        $o = friskamax_get_options( array( 
                'title_content'                 => 'post_title', 
                'title_content_front_page'      => 'site_title'
        ), 'warta_get_page_title_r_o' );
        
        return  $o->title_content == 'site_title' || 
                ( is_front_page() && $o->title_content_front_page == 'site_title' );
}

/* -------------------------------------------------------------------------- *
 * Layout
 * -------------------------------------------------------------------------- */

/**
 * Return archive layout ID.
 * 
 * @return int
 */
function warta_get_archive_layout_id() {
        $o = friskamax_get_options( array(
                'archive_layout'        => 3
        ), 'warta_get_archive_layout_id_o' );
                
        return $o->archive_layout;
}

/* -------------------------------------------------------------------------- *
 * Deprecated
 * -------------------------------------------------------------------------- */

// Options
// -----------------------------------------------------------------------------

if( ! function_exists( 'warta_get_options' ) ) :
/**
 * Merge theme options into defaults array and filter it. 
 * @deprecated since version 1.6.9.1
 */
function warta_get_options( $defaults = array(), $filter_tag = '' ) {
        _deprecated_function( __FUNCTION__, '1.6.9.1', 'friskamax_get_options' );
        return friskamax_get_options( $defaults, $filter_tag );
}
endif;

// Array
// -----------------------------------------------------------------------------

if( ! function_exists( 'warta_parse_args' ) ) :
/**
 * Filter user defined arguments and merge it into defaults array.
 * @deprecated since version 1.6.9.1
 */
function warta_parse_args( $args = array(), $defaults = array(), $filter_tag = '' ) {
        _deprecated_function( __FUNCTION__, '1.6.9.1', 'friskamax_parse_args' );
        return friskamax_parse_args( $args, $defaults, $filter_tag );
}
endif;

// Image
// -----------------------------------------------------------------------------

if ( ! function_exists( 'warta_get_img_url_for_all_sizes' ) ) :
/**
 * Return image URLs.
 * @deprecated since version 1.7.2
 */
function warta_get_img_url_for_all_sizes() {
        _deprecated_function( __FILE__, '1.7.2' );
}
endif;

if ( ! function_exists( 'warta_get_img_data_attr_for_all_sizes' ) ) :         
/**
 * Return image URLs data attribute.
 * @deprecated since version 1.7.2
 */
function warta_get_img_data_attr_for_all_sizes() {
        _deprecated_function( __FUNCTION__, '1.7.2' );
}
endif;

if ( ! function_exists( 'warta_img_data_attr_for_all_sizes' ) ) :
/**
 * Print image URLs data attribute.
 * @deprecated since version 1.7.2
 */
function warta_img_data_attr_for_all_sizes() {
        _deprecated_function( __FUNCTION__, '1.7.2' );
}
endif;

if ( ! class_exists( 'Warta_Helper' ) ) :
/** 
 * Helper class.
 * @deprecated since version 1.6.2 
 */
class Warta_Helper {            
        /** 
         * Returns image URLs data attribute.
         * @deprecated since version 1.6.2 
         */
        public static function get_image_sizes_data_attr() {    
                _deprecated_function( __FUNCTION__, '1.6.2' );
        }
}
endif; 