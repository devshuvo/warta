<?php
/**
 * Load custom functions that act independently of the theme templates.
 * 
 * @package Warta
 */

require __DIR__ . '/default-option-values.php';
require __DIR__ . '/excerpt.php';
require __DIR__ . '/widget.php';
require __DIR__ . '/featured-media.php';
require __DIR__ . '/gallery.php';
require __DIR__ . '/quote.php';
require __DIR__ . '/image.php';
require __DIR__ . '/counts.php';