<?php

if ( ! function_exists( 'warta_the_excerpt_max_charlength' ) ) :
/**
 * Limit the excerpt maximum number of characters
 * 
 * @param int $charlength maximum number characters of the excerpt
 * @param string $excerpt custom excerpt
 * @return string The excerpt
 */
function warta_the_excerpt_max_charlength($charlength, $excerpt = '') {
    $charlength     = apply_filters( 'warta_the_excerpt_max_charlength_arg_charlength', $charlength );
    $excerpt        = $excerpt ? $excerpt : strip_tags(get_the_excerpt());
    $excerpt        = apply_filters( 'warta_the_excerpt_max_charlength_arg_excerpt', $excerpt );
    $output         = '';     
    
    if ($charlength <= 0) {
        // Do nothing. Return an empty string. Basically disables the excerpt.
    } else if (strlen($excerpt) > $charlength) {
        // Split the excerpt into 2 parts.
        // Begining of the excerpt,
        $begin_excerpt  = substr($excerpt, 0, $charlength);
        // and the end of the excerpt.
        $end_excerpt    = substr($excerpt, strlen($begin_excerpt));
        
        $words_array    = preg_split("/[\n\r\t ]+/", $begin_excerpt);
        $last_word      = end($words_array);
                
        if (strlen($last_word) > 0) {
            // Whether the last word got cut or not.
            // If the word begin with a white space, then the last word doesn't get cut.
            $is_last_word_cut = !preg_match("/^[\n\r\t ]+/", $end_excerpt);

            // If the last word got cut, remove it.
            $output .= $is_last_word_cut 
                    ? substr($begin_excerpt, 0, -1 * strlen($last_word)) 
                    : $begin_excerpt;
        } else {
            $output .= $begin_excerpt;
        }

        $output = trim($output);
        // Append &hellip; only if there is an output.
        // Remember, the last word may be removed if got cut, so the output can be empty.
        $output .= $output ? '&hellip;' : '';
    } else {
        $output .= $excerpt;
    }

    return apply_filters( 'warta_the_excerpt_max_charlength', $output );
}
endif;

/* -------------------------------------------------------------------------- *
 * Filters
 * -------------------------------------------------------------------------- */

if ( ! function_exists( 'warta_excerpt_length' ) ) :
/**
 * Change default excerpt length that are 55 words to 100 words.
 * 
 * @param int $length
 * @return int
 */
function warta_excerpt_length($length) {
        return 100;
}
endif; // ! function_exists( 'warta_excerpt_length' )
add_filter( 'excerpt_length', 'warta_excerpt_length' );



if ( !function_exists('warta_excerpt_more') ) :
/**
 * Change default excerpt more [...] to ...
 * 
 * @param string $more Default excerpt more
 * @return string
 */
function warta_excerpt_more( $more ) {
        return ' &hellip;';
}
endif; // warta_excerpt_more
add_filter( 'excerpt_more', 'warta_excerpt_more' );