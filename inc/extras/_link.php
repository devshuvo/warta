<?php

/**
 * Post format link functions
 * 
 * @package Warta
 */

if( !function_exists('warta_match_link') ) :
/**
 * Get link from a post
 * 
 * @param array $args
 * @return $string link html code
 */
function warta_match_link( $args = array() ) {
        $content        = get_the_content();
        $output         = '';
        
        if( preg_match("/<a.*?>.+?<\/a>/is", $content, $matches_link) ) {
                $output = $matches_link[0];
        } 
                
        return apply_filters('warta_match_link', $output);
} 
endif; // warta_match_link
