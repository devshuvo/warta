<?php

/**
 * Update page template if the data is old.
 * 
 * @param string $template
 * @return string
 */
function warta_fsmpb_update_page_template( $template ) {        
        if ( ! is_page() )                                              return $template;
        if ( $template != get_template_directory() . '/page.php' )      return $template; 
        if ( ! fsmpb_is_page_builder() )                                return $template;
        if ( ! fsmpb_is_old_data() )                                    return $template;
        
        $new_template_slug      = 'template-page-full-width.php';
        $new_template           = locate_template( $new_template_slug );
        
		if ( '' != $new_template ) {
                update_post_meta( get_the_ID(), '_wp_page_template', $new_template_slug );
                return $new_template ;
		}

        return $template;
        
}
add_filter( 'template_include', 'warta_fsmpb_update_page_template' );

/**
 * Update page template from admin page if the data is old.
 */
function warta_fsmpb_update_page_template_from_admin() { 
        if ( get_post_type() != 'page' )        return;
        if ( get_page_template_slug() != '' )   return;
        if ( ! fsmpb_is_page_builder() )        return;
        if ( ! fsmpb_is_old_data()  )           return;
                
        update_post_meta( get_the_ID(), '_wp_page_template', 'template-page-full-width.php' );
}
add_action( 'admin_head', 'warta_fsmpb_update_page_template_from_admin' );