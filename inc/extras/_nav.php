<?php

/**
 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
 * 
 * @since 1.0.0
 * @deprecated since version 1.7.5 https://github.com/Automattic/_s/pull/673
 *
 * @param array $args Configuration arguments.
 * @return array
 */
function warta_page_menu_args( $args ) {
        _deprecated_function( __FUNCTION__, '1.7.5' );
        
        $args['show_home'] = true;
        return $args;
}