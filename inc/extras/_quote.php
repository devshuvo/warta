<?php

/**
 * Post format quote functions
 * 
 * @package Warta
 */

if ( ! function_exists( 'warta_match_quote' ) ) :
/**
 * Get blockquote from a post
 * 
 * @return $string Blockquote html code
 */
function warta_match_quote() {
        $content        = get_the_content();
        $output         = '';
        
        if( preg_match( '/<blockquote.*?<\/blockquote>/is', $content, $matches_blockquote ) ) {
                $output = $matches_blockquote[0];
        } else if( preg_match( '/\[blockquote.*?\[\/blockquote\]/is', $content, $matches_blockquote ) ) {
                $output = do_shortcode( $matches_blockquote[0] );
        } 
                
        return apply_filters( 'warta_match_quote', $output );
} 
endif; // ! function_exists( 'warta_match_quote' )

/* -------------------------------------------------------------------------- *
 * Filters
 * -------------------------------------------------------------------------- */

if ( ! function_exists( 'warta_quote_content' ) ) :
/**
 * Adds blockquote html tag if there isn't any.
 * 
 * @param string $content Post content
 * @return string Filtered post content
 */
function warta_quote_content( $content ) {
        if ( has_post_format( 'quote' ) ) {
                $match_quote = warta_match_quote();

                if ( ! $match_quote ) {
                        $content = "<blockquote>$content</blockquote>";
                }
        }

        return $content;
}
endif; // ! function_exists( 'warta_quote_content' )
add_filter( 'the_content', 'warta_quote_content', 1 );