<?php

if( !function_exists( 'warta_set_default_option_values' ) ):
function warta_set_default_option_values() {
        global  $friskamax_warta,
                $friskamax_warta_var;
        
        $page                                   = $friskamax_warta_var['page'];
        
        /**
         * Post Meta
         * ---------------------------------------------------------------------
         */
        $friskamax_warta["{$page}_date_format"] = isset($friskamax_warta["{$page}_date_format"]) 
                                                ? $friskamax_warta["{$page}_date_format"] 
                                                : 'l, F j, Y g:i a';
        $friskamax_warta["{$page}_icons"]       = wp_parse_args(
                                                        isset( $friskamax_warta["{$page}_icons"] )
                                                                ? $friskamax_warta["{$page}_icons"] 
                                                                : array(
                                                                        'author'    => 1,
                                                                        'comments'  => 1,
                                                                        'format'    => 1,
                                                                ),
                                                        array(
                                                                'author'    => 0,
                                                                'comments'  => 0,
                                                                'format'    => 0,
                                                        )
                                                ); // icons        
        $friskamax_warta["{$page}_post_meta"]   = wp_parse_args(
                                                        isset( $friskamax_warta["{$page}_post_meta"] ) 
                                                                ? $friskamax_warta["{$page}_post_meta"] 
                                                                : array(
                                                                        'date'          => 1,
                                                                        'format'        => 0,
                                                                        'category'      => 0,
                                                                        'categories'    => 1,
                                                                        'tags'          => 1,
                                                                        'author'        => 1,
                                                                        'comments'      => 1,
                                                                        'views'         => 1, 
                                                                        'review_score'  => 0,
                                                                ),
                                                        array(
                                                                'date'          => 0,
                                                                'format'        => 0,
                                                                'category'      => 0,
                                                                'categories'    => 0,
                                                                'tags'          => 0,
                                                                'author'        => 0,
                                                                'comments'      => 0,
                                                                'views'         => 0, 
                                                                'review_score'  => 0,
                                                        )
                                                ); // post meta  
        
        /**
         * Archive Page
         * ---------------------------------------------------------------------
         */
        $friskamax_warta['archive_layout']      = isset( $friskamax_warta['archive_layout'] )
                                                ? $friskamax_warta['archive_layout']
                                                : 1;
        $friskamax_warta['archive_excerpt_length']      = isset( $friskamax_warta['archive_excerpt_length'] ) 
                                                ? $friskamax_warta['archive_excerpt_length']
                                                : 320;
        
        
        
              
        $friskamax_warta['breaking_news']       = isset( $friskamax_warta['breaking_news'] ) 
                                                ? $friskamax_warta['breaking_news']
                                                : array(
                                                        'home'      => 0,
                                                        'archive'   => 0,
                                                        'singular'  => 0,
                                                        'page'      => 0
                                                );
}
endif; // warta_set_default_option_values

