<?php
/**
 * Widget functions
 * 
 * @package Warta
 */

if( !function_exists( 'warta_is_sidebar' ) ) :
/**
 * Check whether the current widget location is in sidebar or not.
 * 
 * @param string $id Sidebar ID
 * @return boolean
 */
function warta_is_sidebar($id) {
        $fsmpb_widget_areas = get_option('fsmpb_widget_areas', array()); 
        return ( 
                $id === 'home-sidebar-section' || 
                $id === 'archive-sidebar-section' || 
                $id === 'singular-sidebar-section' || 
                $id === 'page-sidebar-section' || 
                $id === 'default-sidebar-section' ||
                isset($fsmpb_widget_areas[$id])
        ) ? TRUE : FALSE;
}
endif;



if( !function_exists( 'warta_is_footer' ) ) :
/**
 * Check whether the current widget location is in footer or not.
 * 
 * @param string $id Sidebar ID
 * @return boolean
 */
function warta_is_footer($id) {
    return ( 
        $id === 'home-footer-section' || 
        $id === 'archive-footer-section' || 
        $id === 'singular-footer-section' || 
        $id === 'page-footer-section' || 
        $id === 'default-footer-section' 
    ) ? TRUE : FALSE;
}
endif;



if( !function_exists( 'warta_is_main' ) ) :
/**
 * Check whether the current widget location is in #main-content or not.
 * 
 * @param string $id Sidebar ID
 * @return boolean
 */
function warta_is_main($id) {
    return ( 
        $id === 'home-main-section' ||
        $id === 'archive-before-content-section' ||
        $id === 'archive-after-content-section' ||
        $id === 'singular-before-content-section' ||
        $id === 'singular-after-content-section'
    ) ? TRUE : FALSE;
}
endif;



if( !function_exists( 'warta_is_full' ) ) :
/**
 * Check whether the current widget location is in top and bottom section or not.
 * 
 * @param string $id Sidebar ID
 * @return boolean
 */
function warta_is_full($id) {
    return ( 
        $id === 'home-top-section' || 
        $id === 'home-bottom-section' 
    ) ? TRUE : FALSE;
}
endif;

if (!function_exists('warta_get_footer_sidebar_id')) :
/**
 * Get current footer sidebar ID.
 * 
 * @global array $friskamax_warta_var
 * @return string
 */
function warta_get_footer_sidebar_id() {
    global $friskamax_warta_var;
    
    switch($friskamax_warta_var['page']) {
        case 'home':
            if (is_active_sidebar('home-footer-section')) {
                return 'home-footer-section';
            }
        case 'archive':
            if (is_active_sidebar('archive-footer-section')) {
                return 'archive-footer-section';
            }
        case 'singular':
            if (is_active_sidebar('singular-footer-section')) {
                return 'singular-footer-section';
            }
        case 'page':
            if (is_active_sidebar('page-footer-section')) {
                return 'page-footer-section';
            }
        default:
            return 'default-footer-section';
    }
}
endif;

/* ========================================================================== *
 * Deprecated
 * ========================================================================== */

if( !function_exists( 'warta_add_clearfix' ) ) :
/**
 * Add clearfix in every row
 * 
 * @param int $sidebar_id Sidebar ID
 * @param int $col Current widget column 
 * 
 * @deprecated since version 1.6.3 
 */
function warta_add_clearfix( $sidebar_id, $col = 0 ) {
    _deprecated_function(__FUNCTION__, '1.6.3');
}
endif;

if( !function_exists( 'warta_widget_class' ) ) :
/**
 * Generate widget classes
 * 
 * @param int $sidebar_id               Current sidebar ID
 * @param int $col                      Current widget columns
 * @param boolean $echo                 Echo the output?
 * @param boolean $is_pb                Is using page builder?
 * @return string 
 * 
 * @deprecated since version 1.6.3
 */
function warta_widget_class( $sidebar_id, $col = 6, $echo = TRUE, $is_pb = FALSE ) {  
    _deprecated_function(__FUNCTION__, '1.6.3');
    if( !$echo ) return '';
}
endif;