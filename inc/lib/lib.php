<?php

/**
 * Load library
 */

require_once dirname( __FILE__ ) . '/friskamax/friskamax.php';
require_once dirname( __FILE__ ) . '/tgm-plugin-activation/class-tgm-plugin-activation.php';

if ( ! class_exists( 'wp_bootstrap_navwalker' ) ) { 
        require_once dirname( __FILE__ ) . '/wp-bootstrap-navwalker/wp_bootstrap_navwalker.php';
}