<?php

/**
 * Helper functions.
 * 
 * @since 1.0.0
 * @package friskaMax-library\helpers
 */

/* -------------------------------------------------------------------------- *
 * Arguments
 * -------------------------------------------------------------------------- */

/**
 * Filter the user defined arguments and the defaults array. Then merge user 
 * defined arguments into defaults array.
 * 
 * @since 1.0.0
 * 
 * @param array $args                   Value to merge with $defaults.
 * @param array $defaults               Value that serves as the defaults.
 * @param array $filter_tag             Filter tag string.
 * @param boolean $return_object        Return the value as an object (true) or array (false).
 * @return object|array                 Merged value user defined values with defaults.
 */
function friskamax_parse_args( $args = array(), $defaults = array(), $filter_tag = '', $return_object = true ) {
        /**
         * Filter user defined arguments.
         * 
         * @since 1.0.0
         * 
         * @param array $args User defined arguments.
         */
        $r      = apply_filters( $filter_tag, $args );
        
        /**
         * Filter the defaults array.
         * 
         * @since 1.0.0
         * 
         * @param array $defaults User defined arguments.
         */
        $d      = apply_filters( $filter_tag . '_default', $defaults );
        $return = wp_parse_args( $r, $d );
        
        return  $return_object ? ( object ) $return : $return;
}

/* -------------------------------------------------------------------------- *
 * options
 * -------------------------------------------------------------------------- */

/**
 * Merge theme options into defaults array and filter it. 
 * 
 * @since 1.0.0
 * 
 * @param array $defaults               Default options, will be used when redux is not installed.
 * @param string $filter_tag            Filter tag.
 * @param boolean $return_object        Return the value as an object (true) or array (false).
 * @return object|array                 Theme options
 */
function friskamax_get_options( $defaults = array(), $filter_tag = '', $return_object = true ) { 
        
        $theme_options  = isset( $GLOBALS[ FRISKAMAX_THEME_OPTION_NAME ] ) 
                        ? $GLOBALS[ FRISKAMAX_THEME_OPTION_NAME ] 
                        : get_option( FRISKAMAX_THEME_OPTION_NAME, array() );
        $return         = wp_parse_args( $theme_options, $defaults );
        
        if ( $filter_tag ) {
                /**
                 * Filter the theme options.
                 * 
                 * @since 1.0.0
                 * 
                 * @param array $return         Theme options.
                 * @param array $defaults       Default values.
                 */
                $return = apply_filters( $filter_tag, $return, $defaults );
        }
        
        return  $return_object ? ( object ) $return : $return;
        
}

/**
 * Backup a set of options, then update the options with the new values.
 * 
 * @since 1.0.1
 * 
 * @param array $options Key value pairs of the options. array( option_name => new_option_value ).
 */
function friskamax_backup_and_update_options( $options ) {
        $backups = get_option( FRISKAMAX_THEME_SLUG . '_backup_options', array() );
        
        foreach ( $options as $key => $value ) {
                $backups[ $key ] = get_option( $key );
                update_option( $key, $value );
        }
        
        update_option( FRISKAMAX_THEME_SLUG . '_backup_options', $backups );
}

/**
 * Restore a set of options from previously backed up options.
 * 
 * @since 1.0.1
 * 
 * @param array $option_names The option names.
 */
function friskamax_restore_options( $option_names ) {
        $backups = get_option( FRISKAMAX_THEME_SLUG . '_backup_options', array() );
        
        foreach ( $option_names as $key ) {
                if ( isset( $backups[ $key ] ) ) {
                        update_option( $key, $backups[ $key ] );
                        unset( $backups[ $key ] );
                }                
        }
        
        update_option( FRISKAMAX_THEME_SLUG . '_backup_options', $backups );
}

/* -------------------------------------------------------------------------- *
 * Variables
 * -------------------------------------------------------------------------- */

/**
 * Determine whether a variable is not empty.
 * 
 * @since 1.0.0
 * 
 * @param mixed $var    Variable to be checked.
 * @return boolean      Returns true if var exists and has a non-empty, non-zero value. Otherwise returns false. 
 */
function friskamax_is_not_empty( $var ) {
        return ! empty( $var );
}

/**
 * Determine whether a variable is not empty string.
 * 
 * @since 1.0.2
 * 
 * @param mixed $var    Variable to be checked.
 * @return boolean      Returns true if var has a non-empty string value. Otherwise returns false. 
 */
function friskamax_is_not_empty_string( $var ) {
        return $var !== '';
}

/**
 * Set a new Content Width.
 * 
 * Content Width is the maximum allowed width for any content in the theme, 
 * like oEmbeds and images added to posts.
 * The previous excerpt length will be stored in $friskamax_content_width_old.
 * 
 * @link http://codex.wordpress.org/Content_Width
 * @since 1.0.2
 * 
 * @global int $content_width                   Current Content Width.
 * @global int $friskamax_content_width_old     Previous excerpt length.
 * @param int $number                           The new Content Width.
 */
function friskamax_set_content_width( $number ) {
        global $content_width, $friskamax_content_width_old;
        
        $friskamax_content_width_old    = $content_width;
        $content_width                  = $number;
}

/**
 * Reset the Content Width to previous value.
 * 
 * @link http://codex.wordpress.org/Content_Width
 * @since 1.0.2
 * 
 * @global int $content_width                   Current Content Width.
 * @global int $friskamax_content_width_old     Previous excerpt length.
 */
function friskamax_reset_content_width() {
        global $content_width, $friskamax_content_width_old;
        
        if ( isset( $friskamax_content_width_old ) ) {
                $content_width = $friskamax_content_width_old;
                unset( $friskamax_content_width_old );
        }
}

/* -------------------------------------------------------------------------- *
 * Arrays
 * -------------------------------------------------------------------------- */

/**
 * Join array elements with a string.
 * Works like implode(), except it removes items that have an empty value.
 * 
 * @since 1.0.0
 * 
 * @param string $glue  Defaults to an empty string. 
 * @param array $pieces The array of strings to implode.
 * @return string       Returns a string containing a string representation of all the array elements in the same order, with the glue string between each element.
 */
function friskamax_implode( $glue, $pieces ) {
        $glue   = apply_filters( 'friskamax_implode_glue', $glue );
        $pieces = apply_filters( 'friskamax_implode_pieces', array_filter( $pieces, 'friskamax_is_not_empty' ) );
        
        return apply_filters( 'friskamax_implode', implode( $glue, $pieces ), $glue, $pieces );
}

/* -------------------------------------------------------------------------- *
 * Modals
 * -------------------------------------------------------------------------- */

/**
 * Display Bootstrap modal.
 * 
 * @since 1.0.2
 * @access private
 * 
 * @param array $args {
 *      An array of arguments.
 * 
 *      @type string $wrapper   Wrapper tag.
 *      @type string $id        Modal ID.
 *      @type string $size      Modal size class.
 *      @type string $title     Modal title.
 *      @type string $content   Modal content.
 *      @type string $cancel    Cancel button text.
 *      @type string $submit    Submit button text.
 * }
 */
function friskamax_modal( $args ) {
        $r = wp_parse_args( $args, array(
                'wrapper'       => 'div',
                'id'            => '',
                'size'          => '',
                'title'         => '',
                'content'       => '',
                'cancel'        => __( 'Cancel', FRISKAMAX_THEME_SLUG ),
                'submit'        => __( 'Insert', FRISKAMAX_THEME_SLUG ),
                'submit_type'   => 'button',
        ) );
        
        ?>     

        <div class="fsmh-container">
                <<?php echo $r[ 'wrapper' ]; ?> id="<?php echo $r[ 'id' ]; ?>"  class="modal modal-fsmh hidden">
                        <div class="modal-dialog <?php echo $r[ 'size' ]; ?>">
                                <div class="modal-content">
                                        
                                        <?php if ( $r[ 'title' ] ) : ?>
                                                <div class="modal-header">
                                                        <h4 class="modal-title"><?php echo $r[ 'title' ]; ?></h4>
                                                </div>
                                        <?php endif; ?>
                                        
                                        <div class="modal-body">
                                                <?php echo $r[ 'content' ]; ?>
                                        </div>
                                        
                                        <div class="modal-footer">
                                                <button type="button" class="button" data-dismiss="modal"><?php echo $r[ 'cancel' ]; ?></button>
                                                        
                                                <?php if ( $r[ 'submit' ] ) : ?>
                                                        <button type="<?php echo $r[ 'submit_type' ]; ?>" class="button-primary button-submit"><?php echo $r[ 'submit' ]; ?></button>
                                                <?php endif; ?>
                                        </div>
                                        
                                </div>
                        </div>
                </<?php echo $r[ 'wrapper' ]; ?>>
        </div>

        <?php
}

/**
 * Display modal Font Awesome.
 * 
 * @since 1.0.2
 * @access private
 * 
 * @param array $icon_classes   An array of Font Awesome icon classes.
 */
function friskamax_modal_font_awesome( $icon_classes = array() ) {
        if ( ! $icon_classes ) {
                // Font Awesome 4.2.0
                $icon_classes = array(
                        'fa-adjust',
                        'fa-adn',
                        'fa-align-center',
                        'fa-align-justify',
                        'fa-align-left',
                        'fa-align-right',
                        'fa-ambulance',
                        'fa-anchor',
                        'fa-android',
                        'fa-angellist',
                        'fa-angle-double-down',
                        'fa-angle-double-left',
                        'fa-angle-double-right',
                        'fa-angle-double-up',
                        'fa-angle-down',
                        'fa-angle-left',
                        'fa-angle-right',
                        'fa-angle-up',
                        'fa-apple',
                        'fa-archive',
                        'fa-area-chart',
                        'fa-arrow-circle-down',
                        'fa-arrow-circle-left',
                        'fa-arrow-circle-o-down',
                        'fa-arrow-circle-o-left',
                        'fa-arrow-circle-o-right',
                        'fa-arrow-circle-o-up',
                        'fa-arrow-circle-right',
                        'fa-arrow-circle-up',
                        'fa-arrow-down',
                        'fa-arrow-left',
                        'fa-arrow-right',
                        'fa-arrow-up',
                        'fa-arrows',
                        'fa-arrows-alt',
                        'fa-arrows-h',
                        'fa-arrows-v',
                        'fa-asterisk',
                        'fa-at',
                        'fa-automobile',
                        'fa-backward',
                        'fa-ban',
                        'fa-bank',
                        'fa-bar-chart',
                        'fa-bar-chart-o',
                        'fa-barcode',
                        'fa-bars',
                        'fa-beer',
                        'fa-behance',
                        'fa-behance-square',
                        'fa-bell',
                        'fa-bell-o',
                        'fa-bell-slash',
                        'fa-bell-slash-o',
                        'fa-bicycle',
                        'fa-binoculars',
                        'fa-birthday-cake',
                        'fa-bitbucket',
                        'fa-bitbucket-square',
                        'fa-bitcoin',
                        'fa-bold',
                        'fa-bolt',
                        'fa-bomb',
                        'fa-book',
                        'fa-bookmark',
                        'fa-bookmark-o',
                        'fa-briefcase',
                        'fa-btc',
                        'fa-bug',
                        'fa-building',
                        'fa-building-o',
                        'fa-bullhorn',
                        'fa-bullseye',
                        'fa-bus',
                        'fa-cab',
                        'fa-calculator',
                        'fa-calendar',
                        'fa-calendar-o',
                        'fa-camera',
                        'fa-camera-retro',
                        'fa-car',
                        'fa-caret-down',
                        'fa-caret-left',
                        'fa-caret-right',
                        'fa-caret-square-o-down',
                        'fa-caret-square-o-left',
                        'fa-caret-square-o-right',
                        'fa-caret-square-o-up',
                        'fa-caret-up',
                        'fa-cc',
                        'fa-cc-amex',
                        'fa-cc-discover',
                        'fa-cc-mastercard',
                        'fa-cc-paypal',
                        'fa-cc-stripe',
                        'fa-cc-visa',
                        'fa-certificate',
                        'fa-chain',
                        'fa-chain-broken',
                        'fa-check',
                        'fa-check-circle',
                        'fa-check-circle-o',
                        'fa-check-square',
                        'fa-check-square-o',
                        'fa-chevron-circle-down',
                        'fa-chevron-circle-left',
                        'fa-chevron-circle-right',
                        'fa-chevron-circle-up',
                        'fa-chevron-down',
                        'fa-chevron-left',
                        'fa-chevron-right',
                        'fa-chevron-up',
                        'fa-child',
                        'fa-circle',
                        'fa-circle-o',
                        'fa-circle-o-notch',
                        'fa-circle-thin',
                        'fa-clipboard',
                        'fa-clock-o',
                        'fa-close',
                        'fa-cloud',
                        'fa-cloud-download',
                        'fa-cloud-upload',
                        'fa-cny',
                        'fa-code',
                        'fa-code-fork',
                        'fa-codepen',
                        'fa-coffee',
                        'fa-cog',
                        'fa-cogs',
                        'fa-columns',
                        'fa-comment',
                        'fa-comment-o',
                        'fa-comments',
                        'fa-comments-o',
                        'fa-compass',
                        'fa-compress',
                        'fa-copy',
                        'fa-copyright',
                        'fa-credit-card',
                        'fa-crop',
                        'fa-crosshairs',
                        'fa-css3',
                        'fa-cube',
                        'fa-cubes',
                        'fa-cut',
                        'fa-cutlery',
                        'fa-dashboard',
                        'fa-database',
                        'fa-dedent',
                        'fa-delicious',
                        'fa-desktop',
                        'fa-deviantart',
                        'fa-digg',
                        'fa-dollar',
                        'fa-dot-circle-o',
                        'fa-download',
                        'fa-dribbble',
                        'fa-dropbox',
                        'fa-drupal',
                        'fa-edit',
                        'fa-eject',
                        'fa-ellipsis-h',
                        'fa-ellipsis-v',
                        'fa-empire',
                        'fa-envelope',
                        'fa-envelope-o',
                        'fa-envelope-square',
                        'fa-eraser',
                        'fa-eur',
                        'fa-euro',
                        'fa-exchange',
                        'fa-exclamation',
                        'fa-exclamation-circle',
                        'fa-exclamation-triangle',
                        'fa-expand',
                        'fa-external-link',
                        'fa-external-link-square',
                        'fa-eye',
                        'fa-eye-slash',
                        'fa-eyedropper',
                        'fa-facebook',
                        'fa-facebook-square',
                        'fa-fast-backward',
                        'fa-fast-forward',
                        'fa-fax',
                        'fa-female',
                        'fa-fighter-jet',
                        'fa-file',
                        'fa-file-archive-o',
                        'fa-file-audio-o',
                        'fa-file-code-o',
                        'fa-file-excel-o',
                        'fa-file-image-o',
                        'fa-file-movie-o',
                        'fa-file-o',
                        'fa-file-pdf-o',
                        'fa-file-photo-o',
                        'fa-file-picture-o',
                        'fa-file-powerpoint-o',
                        'fa-file-sound-o',
                        'fa-file-text',
                        'fa-file-text-o',
                        'fa-file-video-o',
                        'fa-file-word-o',
                        'fa-file-zip-o',
                        'fa-files-o',
                        'fa-film',
                        'fa-filter',
                        'fa-fire',
                        'fa-fire-extinguisher',
                        'fa-flag',
                        'fa-flag-checkered',
                        'fa-flag-o',
                        'fa-flash',
                        'fa-flask',
                        'fa-flickr',
                        'fa-floppy-o',
                        'fa-folder',
                        'fa-folder-o',
                        'fa-folder-open',
                        'fa-folder-open-o',
                        'fa-font',
                        'fa-forward',
                        'fa-foursquare',
                        'fa-frown-o',
                        'fa-futbol-o',
                        'fa-gamepad',
                        'fa-gavel',
                        'fa-gbp',
                        'fa-ge',
                        'fa-gear',
                        'fa-gears',
                        'fa-gift',
                        'fa-git',
                        'fa-git-square',
                        'fa-github',
                        'fa-github-alt',
                        'fa-github-square',
                        'fa-gittip',
                        'fa-glass',
                        'fa-globe',
                        'fa-google',
                        'fa-google-plus',
                        'fa-google-plus-square',
                        'fa-google-wallet',
                        'fa-graduation-cap',
                        'fa-group',
                        'fa-h-square',
                        'fa-hacker-news',
                        'fa-hand-o-down',
                        'fa-hand-o-left',
                        'fa-hand-o-right',
                        'fa-hand-o-up',
                        'fa-hdd-o',
                        'fa-header',
                        'fa-headphones',
                        'fa-heart',
                        'fa-heart-o',
                        'fa-history',
                        'fa-home',
                        'fa-hospital-o',
                        'fa-html5',
                        'fa-ils',
                        'fa-image',
                        'fa-inbox',
                        'fa-indent',
                        'fa-info',
                        'fa-info-circle',
                        'fa-inr',
                        'fa-instagram',
                        'fa-institution',
                        'fa-ioxhost',
                        'fa-italic',
                        'fa-joomla',
                        'fa-jpy',
                        'fa-jsfiddle',
                        'fa-key',
                        'fa-keyboard-o',
                        'fa-krw',
                        'fa-language',
                        'fa-laptop',
                        'fa-lastfm',
                        'fa-lastfm-square',
                        'fa-leaf',
                        'fa-legal',
                        'fa-lemon-o',
                        'fa-level-down',
                        'fa-level-up',
                        'fa-life-bouy',
                        'fa-life-buoy',
                        'fa-life-ring',
                        'fa-life-saver',
                        'fa-lightbulb-o',
                        'fa-line-chart',
                        'fa-link',
                        'fa-linkedin',
                        'fa-linkedin-square',
                        'fa-linux',
                        'fa-list',
                        'fa-list-alt',
                        'fa-list-ol',
                        'fa-list-ul',
                        'fa-location-arrow',
                        'fa-lock',
                        'fa-long-arrow-down',
                        'fa-long-arrow-left',
                        'fa-long-arrow-right',
                        'fa-long-arrow-up',
                        'fa-magic',
                        'fa-magnet',
                        'fa-mail-forward',
                        'fa-mail-reply',
                        'fa-mail-reply-all',
                        'fa-male',
                        'fa-map-marker',
                        'fa-maxcdn',
                        'fa-meanpath',
                        'fa-medkit',
                        'fa-meh-o',
                        'fa-microphone',
                        'fa-microphone-slash',
                        'fa-minus',
                        'fa-minus-circle',
                        'fa-minus-square',
                        'fa-minus-square-o',
                        'fa-mobile',
                        'fa-mobile-phone',
                        'fa-money',
                        'fa-moon-o',
                        'fa-mortar-board',
                        'fa-music',
                        'fa-navicon',
                        'fa-newspaper-o',
                        'fa-openid',
                        'fa-outdent',
                        'fa-pagelines',
                        'fa-paint-brush',
                        'fa-paper-plane',
                        'fa-paper-plane-o',
                        'fa-paperclip',
                        'fa-paragraph',
                        'fa-paste',
                        'fa-pause',
                        'fa-paw',
                        'fa-paypal',
                        'fa-pencil',
                        'fa-pencil-square',
                        'fa-pencil-square-o',
                        'fa-phone',
                        'fa-phone-square',
                        'fa-photo',
                        'fa-picture-o',
                        'fa-pie-chart',
                        'fa-pied-piper',
                        'fa-pied-piper-alt',
                        'fa-pinterest',
                        'fa-pinterest-square',
                        'fa-plane',
                        'fa-play',
                        'fa-play-circle',
                        'fa-play-circle-o',
                        'fa-plug',
                        'fa-plus',
                        'fa-plus-circle',
                        'fa-plus-square',
                        'fa-plus-square-o',
                        'fa-power-off',
                        'fa-print',
                        'fa-puzzle-piece',
                        'fa-qq',
                        'fa-qrcode',
                        'fa-question',
                        'fa-question-circle',
                        'fa-quote-left',
                        'fa-quote-right',
                        'fa-ra',
                        'fa-random',
                        'fa-rebel',
                        'fa-recycle',
                        'fa-reddit',
                        'fa-reddit-square',
                        'fa-refresh',
                        'fa-remove',
                        'fa-renren',
                        'fa-reorder',
                        'fa-repeat',
                        'fa-reply',
                        'fa-reply-all',
                        'fa-retweet',
                        'fa-rmb',
                        'fa-road',
                        'fa-rocket',
                        'fa-rotate-left',
                        'fa-rotate-right',
                        'fa-rouble',
                        'fa-rss',
                        'fa-rss-square',
                        'fa-rub',
                        'fa-ruble',
                        'fa-rupee',
                        'fa-save',
                        'fa-scissors',
                        'fa-search',
                        'fa-search-minus',
                        'fa-search-plus',
                        'fa-send',
                        'fa-send-o',
                        'fa-share',
                        'fa-share-alt',
                        'fa-share-alt-square',
                        'fa-share-square',
                        'fa-share-square-o',
                        'fa-shekel',
                        'fa-sheqel',
                        'fa-shield',
                        'fa-shopping-cart',
                        'fa-sign-in',
                        'fa-sign-out',
                        'fa-signal',
                        'fa-sitemap',
                        'fa-skype',
                        'fa-slack',
                        'fa-sliders',
                        'fa-slideshare',
                        'fa-smile-o',
                        'fa-soccer-ball-o',
                        'fa-sort',
                        'fa-sort-alpha-asc',
                        'fa-sort-alpha-desc',
                        'fa-sort-amount-asc',
                        'fa-sort-amount-desc',
                        'fa-sort-asc',
                        'fa-sort-desc',
                        'fa-sort-down',
                        'fa-sort-numeric-asc',
                        'fa-sort-numeric-desc',
                        'fa-sort-up',
                        'fa-soundcloud',
                        'fa-space-shuttle',
                        'fa-spinner',
                        'fa-spoon',
                        'fa-spotify',
                        'fa-square',
                        'fa-square-o',
                        'fa-stack-exchange',
                        'fa-stack-overflow',
                        'fa-star',
                        'fa-star-half',
                        'fa-star-half-empty',
                        'fa-star-half-full',
                        'fa-star-half-o',
                        'fa-star-o',
                        'fa-steam',
                        'fa-steam-square',
                        'fa-step-backward',
                        'fa-step-forward',
                        'fa-stethoscope',
                        'fa-stop',
                        'fa-strikethrough',
                        'fa-stumbleupon',
                        'fa-stumbleupon-circle',
                        'fa-subscript',
                        'fa-suitcase',
                        'fa-sun-o',
                        'fa-superscript',
                        'fa-support',
                        'fa-table',
                        'fa-tablet',
                        'fa-tachometer',
                        'fa-tag',
                        'fa-tags',
                        'fa-tasks',
                        'fa-taxi',
                        'fa-tencent-weibo',
                        'fa-terminal',
                        'fa-text-height',
                        'fa-text-width',
                        'fa-th',
                        'fa-th-large',
                        'fa-th-list',
                        'fa-thumb-tack',
                        'fa-thumbs-down',
                        'fa-thumbs-o-down',
                        'fa-thumbs-o-up',
                        'fa-thumbs-up',
                        'fa-ticket',
                        'fa-times',
                        'fa-times-circle',
                        'fa-times-circle-o',
                        'fa-tint',
                        'fa-toggle-down',
                        'fa-toggle-left',
                        'fa-toggle-off',
                        'fa-toggle-on',
                        'fa-toggle-right',
                        'fa-toggle-up',
                        'fa-trash',
                        'fa-trash-o',
                        'fa-tree',
                        'fa-trello',
                        'fa-trophy',
                        'fa-truck',
                        'fa-try',
                        'fa-tty',
                        'fa-tumblr',
                        'fa-tumblr-square',
                        'fa-turkish-lira',
                        'fa-twitch',
                        'fa-twitter',
                        'fa-twitter-square',
                        'fa-umbrella',
                        'fa-underline',
                        'fa-undo',
                        'fa-university',
                        'fa-unlink',
                        'fa-unlock',
                        'fa-unlock-alt',
                        'fa-unsorted',
                        'fa-upload',
                        'fa-usd',
                        'fa-user',
                        'fa-user-md',
                        'fa-users',
                        'fa-video-camera',
                        'fa-vimeo-square',
                        'fa-vine',
                        'fa-vk',
                        'fa-volume-down',
                        'fa-volume-off',
                        'fa-volume-up',
                        'fa-warning',
                        'fa-wechat',
                        'fa-weibo',
                        'fa-weixin',
                        'fa-wheelchair',
                        'fa-wifi',
                        'fa-windows',
                        'fa-won',
                        'fa-wordpress',
                        'fa-wrench',
                        'fa-xing',
                        'fa-xing-square',
                        'fa-yahoo',
                        'fa-yelp',
                        'fa-yen',
                        'fa-youtube',
                        'fa-youtube-play',
                        'fa-youtube-square',
                );
        }
        
        asort( $icon_classes );        
        ob_start();
        
        fsmf_field_input( array(
                'label' => __( 'Search:', FRISKAMAX_THEME_SLUG ),
                'attr'  => array(
                        'type'  => 'search'
                )
        ) ); 
        
        foreach ( $icon_classes as $_class ) {  
                $title = str_replace( '-', ' ', substr( $_class, 3 ) );
                echo '<i class="fa ' . $_class . '" data-value="' . $_class . '" title="' . $title . '"></i>';
        }

        friskamax_modal( array(
                'id'            => 'fsmh-modal-font-awesome',
                'title'         => __('Icons', 'warta'),
                'size'          => 'modal-lg',
                'content'       => ob_get_clean(),
                'submit'        => ''
        ) );
}

/* -------------------------------------------------------------------------- *
 * Deprecated
 * -------------------------------------------------------------------------- */

/**
 * Returns theme version that saved in database.
 * 
 * @since 1.0.0
 * @deprecated since version 1.0.2
 * 
 * @return string
 */
function friskamax_get_db_theme_version() {
        
        _deprecated_function( __FUNCTION__, '1.0.2', 'get_theme_mod' );
        return get_theme_mod( 'version' );
        
}

/**
 * Update theme version in database.
 * 
 * @since 1.0.0
 * @deprecated since version 1.0.2
 */
function friskamax_update_db_theme_version() {
        
        _deprecated_function( __FUNCTION__, '1.0.2', 'set_theme_mod' );
        set_theme_mod( 'version', wp_get_theme()->get( 'Version' ) );
        
}

/**
 * Returns current theme version.
 * 
 * @since 1.0.0
 * @deprecated since version 1.0.2
 * 
 * @return string
 */
function friskamax_get_theme_version() {   
               
        _deprecated_function( __FUNCTION__, '1.0.2', 'wp_get_theme' );
        return wp_get_theme()->get( 'Version' );
        
}