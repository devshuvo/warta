<?php

/* -----------------------------------------------------------------------------
friskamax_get_quote_content
 ---------------------------------------------------------------------------- */

/**
 * Get blockquote from post content.
 * 
 * @return $string HTML blockquote
 */
function friskamax_get_quote_content() {
        $content        = get_the_content();
        $return         = '';
        
        if( preg_match( '/<blockquote.*?<\/blockquote>/is', $content, $matches ) ) {
                $return = $matches[0];
        }
                
        return apply_filters( 'friskamax_get_quote_content', $return, $matches );
} 

/* -----------------------------------------------------------------------------
friskamax_get_link_content
 ---------------------------------------------------------------------------- */

/**
 * Get link from post content.
 * 
 * @return $string HTML link.
 */
function friskamax_get_link_content() {
        $content        = get_the_content();
        $return         = '';
        
        if( preg_match( '/<a.*?>.+?<\/a>/is', $content, $matches ) ) {
                $return = $matches[0];
        } 
                
        return apply_filters( 'friskamax_get_link_content', $return, $matches );
} 