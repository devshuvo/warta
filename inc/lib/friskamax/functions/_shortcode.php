<?php
/**
 * Shorcode custom functions.
 * 
 * @since   friskaMax-library 1.0.0
 * @package friskaMax-library\shortcode
 */

/* -----------------------------------------------------------------------------
friskamax_get_shortcode_regex
 ---------------------------------------------------------------------------- */

/**
 * Retrieve the shortcode regular expression for searching.
 *
 * @since 1.0.0
 * 
 * @uses get_shortcode_regex()
 * 
 * @param $tagnames     The shortcode tags.
 * @return string       The shortcode search regular expression.
 */
function friskamax_get_shortcode_regex( $tagnames ) {
        global $shortcode_tags;
                
        // Save the current value of $shortcode_tags, because we are going to alter it.
        $shortcode_tags_tmp = $shortcode_tags;
        
        foreach ( $shortcode_tags as $_key => $_value ) {
                if ( ! in_array( $_key, $tagnames ) ) {
                        // Unset the other shortcodes.
                        // 
                        // get_shortcode_regex() uses global variable $shortcode_tags,
                        // so we are going to limit the shortcodes.
                        unset( $shortcode_tags[ $_key ] );
                }
        }
        
        $return         = get_shortcode_regex();
        $shortcode_tags = $shortcode_tags_tmp; // Give $shortcode_tags its original value. 
        
        return $return;
}

/* -----------------------------------------------------------------------------
friskamax_do_shortcode
 ---------------------------------------------------------------------------- */

/**
 * Filter shortcode through its hooks.
 * If it's an [embed] shortcode then use global variable $wp_embed to run the shortcode.
 * Else use do_shortcode() function.
 * 
 * @since 1.0.0
 * 
 * @param string $shortcode     The shortcode.
 * @return string               HTML content with shortcode filtered out.
 */
function friskamax_do_shortcode( $shortcode ) {
        return  preg_match( '/^\[embed/i', $shortcode )
                ? $GLOBALS['wp_embed']->run_shortcode( $shortcode ) // [embed] needs $GLOBALS['wp_embed']
                : do_shortcode( $shortcode );
}

/* -----------------------------------------------------------------------------
friskamax_get_shortcode
 ---------------------------------------------------------------------------- */

/**
 * Helper class for friskamax_get_shortcode() function.
 * 
 * @since 1.0.0
 */
class FriskaMax_Get_Shortcode {
        protected       $shortcode_tags,        // (Array)      Array of shortcode tags.
                        $post_id;               // (Int)        Post ID.
        
        /**
         * Strip bracket from a shortcode tag name.
         * 
         * @since 1.0.0
         * 
         * @param string $string        Shortcode tag name.
         * @return string
         */
        public function strip_bracket( $string ) {
                $return = str_replace('[', '', $string );
                $return = str_replace(']', '', $return );
                
                return $return;
        }
        
        /**
         * Set regex pattern and return it.
         * 
         * @since 1.0.0
         * 
         * @return string The pattern.
         */
        public function get_regex_pattern() {
                $shortcode_tags = array_map( array( $this, 'strip_bracket' ), $this->shortcode_tags );
                $pattern        = friskamax_get_shortcode_regex( $shortcode_tags );

                return $pattern;
        }
        
        /**
         * Returns the shortcode. 
         * 
         * @since 1.0.0
         * 
         * @return string The shortcode.
         */
        public function get() {                
                $content        = get_the_content();
                $pattern        = $this->get_regex_pattern();

                if( $pattern && preg_match( "/$pattern/s", $content, $matches ) ) {
                        return $matches[0];
                }
        }
        
        /**
         * Initialize.
         * 
         * @since 1.0.0
         * 
         * @param array $shortcode_tags An array of shortcode tag names.
         */
        public function __construct( $shortcode_tags ) {
                $this->shortcode_tags   = $shortcode_tags;
                $this->post_id          = get_the_ID();
        } 
}

/**
 * Search shortcode from post content.
 * If there is no shortcode, it will return empty string.
 * 
 * @since 1.0.0
 * 
 * @param array $shortcode_tags         Array of shortcodes to be searched. 
 * @return string                       The shortcode that found from post content.
 */
function friskamax_get_shortcode( $shortcode_tags = array() ) {
        $get_shortcode  = new FriskaMax_Get_Shortcode( $shortcode_tags );
        $return         = $get_shortcode->get();

        return apply_filters( 'friskamax_get_shortcode', $return, $shortcode_tags );
}