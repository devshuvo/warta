<?php
/**
 * General functions.
 * 
 * @package friskamax-library
 * @since 1.0.0
 */

/* -------------------------------------------------------------------------- *
 * Session
 * -------------------------------------------------------------------------- */

/**
 * Start new session and setup some session data.
 * 
 * @since 1.0.0
 */
function friskamax_session_start(){
        // Start session.
        if( ! session_id() ) { 
                session_start();
        }
        
        // Set admin notice data.
        if ( ! isset( $_SESSION[ 'friskamax_admin_notices' ] ) ) { 
                $_SESSION[ 'friskamax_admin_notices' ] = array();
        }
}
add_action( 'init', 'friskamax_session_start' );

/* -------------------------------------------------------------------------- *
 * Notice
 * -------------------------------------------------------------------------- */

/**
 * Display notices displayed near the top of admin pages.
 * 
 * @since 1.0.0
 */
function friskamax_admin_notices() {        
        // Return if there isn't any notice to display.
        if ( ! $_SESSION[ 'friskamax_admin_notices' ] ) { 
                return;
        }
                
        foreach ( $_SESSION[ 'friskamax_admin_notices' ] as $key => $notice ) {
                ?>
                        <div class="<?php echo esc_attr( $notice[ 'class' ] ); ?>">
                                <?php echo $notice[ 'message' ]; ?>
                        </div>
                <?php
                
                if ( $notice['unset'] ) {
                        unset( $_SESSION[ 'friskamax_admin_notices' ][ $key ] );
                }
        }
}
add_action( 'admin_notices', 'friskamax_admin_notices' );

/* -------------------------------------------------------------------------- *
 * Update
 * -------------------------------------------------------------------------- */

/**
 * Fires an action after the theme has been updated.
 * 
 * @since 1.0.1
 */
function friskamax_do_after_update_theme() {
        
        $theme_version = wp_get_theme()->get( 'Version' );
        
        if ( 
                
                ! is_admin() || 
                ! current_user_can( 'manage_options' ) || 
                ( defined( 'DOING_AJAX' ) && DOING_AJAX ) || 
                $theme_version == get_theme_mod( 'version' ) 
                
        ) { 
                return;        
        }
        
        set_theme_mod( 'version', $theme_version );
        
        /**
         * Fires after theme version has been changed.
         * 
         * @since 1.0.1
         */
        do_action( 'friskamax_after_update_theme' );
        
}
add_action( 'admin_init', 'friskamax_do_after_update_theme' );