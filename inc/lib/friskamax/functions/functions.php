<?php
/**
 * General functions.
 * 
 * @since   1.0.2
 * @package friskaMax-library\functions
 */

require_once dirname( __FILE__ ) . '/_helpers.php';
require_once dirname( __FILE__ ) . '/_general.php';
require_once dirname( __FILE__ ) . '/_shortcode.php';
require_once dirname( __FILE__ ) . '/_media.php';