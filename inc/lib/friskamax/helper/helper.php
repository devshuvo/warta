<?php
/**
 * CSS and JS helper
 * 
 * @package friskamax-library\helper
 * @since 1.0.0
 */

/**
 * Enqueue stylesheets and scripts in the WordPress admin pages.
 * 
 * @since 1.0.0
 * 
 * @param string $hook_suffix The current admin page.
 */
function fsmh_admin_enqueue_scripts( $hook_suffix ) {
        $theme_dir_uri          = get_template_directory_uri();
        $theme_version          = wp_get_theme()->get( 'Version' );
        $suffix                 = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
        $suffix_css             = $suffix . '.css';
        $suffix_js              = $suffix . '.js';        
        $pages                  = apply_filters( 'fsmh_admin_enqueue_scripts_pages', array(
                                        'post.php',
                                        'post-new.php',
                                        'widgets.php'
                                ) );
        
        wp_register_style( 'font-awesome', $theme_dir_uri . '/inc/lib/friskamax/assets/css/font-awesome' . $suffix_css, array(), $theme_version );
        wp_register_style( 'fsmh-style', $theme_dir_uri . '/inc/lib/friskamax/helper/css/fsmh' . $suffix_css, array(), $theme_version );
        wp_register_script( 'fsmh-script', $theme_dir_uri . '/inc/lib/friskamax/helper/js/fsmh' . $suffix_js, array( 'jquery' ), $theme_version, true );
        
        if ( in_array( $hook_suffix, $pages ) ) { 
                wp_enqueue_style( 'font-awesome' );
                wp_enqueue_style( 'fsmh-style' );
                wp_enqueue_script( 'fsmh-script' );
        }
}
add_action( 'admin_enqueue_scripts', 'fsmh_admin_enqueue_scripts' );