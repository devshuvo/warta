/*
 * FriskaMax JS Helper
 * 
 * @package friskamax-library\helper
 * @since 1.0.0
 */

; ( function( $, F, undefined ) { 'use strict';
        
        var Helper = function() {
                
                this.$document  = $( document );
                this.$body      = $( 'body' );
                                
                this.listen();
                this.requires();
                this.field_buttons();
                this.field_repeater();
                this.modal();
                this.setCheckboxValue();
                
        };
        
        /**
         * Event listener.
         */
        Helper.prototype.listen = function () {
                var self = this;           
                
                /* -------------------------------------------------------------------------- *
                 * Collapse
                 * -------------------------------------------------------------------------- */
                this.$body.on( 'click.fsmh.slide', '.fsmh-container [data-toggle="fsmh-slide"]', Helper.prototype.slide ); 
        };
        
        /* ========================================================================== *
         * REQUIRES
         * ========================================================================== */
                        
        /**
         * Show hide an element based on the required field values.
         * 
         * @since 1.0.0
         * @private
         */
        Helper.prototype.requires = function() {   
                
                var self = this;
                
                /**
                 * Show/hide option based on required fields.
                 * 
                 * @since 1.0.2
                 */
                function toggle() {
                        
                        var     $field          = $( this ),
                                $wrapper        = $field.closest( '.fsmh-container, .fsmh-field-repeater-wrapper' ),
                                requires        = $field.data( 'requires' ),
                                show            = requires.relation == 'and'; // if the relation is 'and', default show 'true', otherwise 'false'.

                        // Check the required fields.
                        $.each( requires, function() {          
                                
                                // The relation is not an object.
                                if ( typeof this !== 'object' ) {
                                        return;
                                }
                                
                                var     $required_field = $wrapper.find( this.field ),
                                        is_true         = true;

                                switch( this.compare ) {
                                        
                                        case 'check':
                                                is_true = $required_field.prop( 'checked' );                                        
                                                break;
                                                
                                        case 'radio':
                                                is_true = $required_field.filter( ':checked' ).val() == this.value;
                                                break;
                                                
                                        case 'equal':
                                                is_true = $required_field.val() == this.value;
                                                break;
                                                
                                        case 'not-equal':
                                                is_true = $required_field.val() != this.value;
                                                break;
                                                
                                }     

                                if ( requires.relation == 'and' ) {
                                        
                                        if ( ! is_true ) {
                                                show = false;
                                        }
                                        
                                } else if ( is_true ) {
                                        
                                        show = true;
                                        
                                }   
                                
                        } );

                        $field.stop();

                        if ( show ) {
                                $field.slideDown(); 
                        } else if ( ! show ) {
                                $field.slideUp();
                        }
                        
                };
                                
                // On field change, group field duplicated and modal shown.
                this.$body.on( 'change.fsmh fsmh_duplicated_field_repeater shown.bs.modal.fsmh', '.fsmh-container', function() {
                        
                        $( this ).find( '[data-requires]' ).each( toggle );                                                                       
                        
                } );   
                
                // On save widget.
                this.$document.ajaxComplete( function( event, xhr, settings ) {
                        
                        if ( settings.data && settings.data.indexOf( 'action=save-widget' ) >= 0 ) {
                                self.$body.find( '.fsmh-container [data-requires]' ).each( toggle );
                        }
                        
                } );
                
                // Run in the first load.                                                                        
                this.$body.find( '.fsmh-container [data-requires]' ).each( toggle );
                
        };
        
        /* ========================================================================== *
         * BUTTONS FIELD
         * ========================================================================== */
        
        /**
         * Buttons field.
         * 
         * Modified version of Bootstrap Button.
         * 
         * @since 1.0.0
         * @private
         */
        Helper.prototype.field_buttons = function() {
                this.$body.on( 'click.fsmh', '.fsmh-field-buttons .button', function( event ) {
                        var     $this   = $( this ),
                                $parent = $this.closest( '.fsmh-field-buttons' ),
                                $input  = $this.find( 'input' ),
                                changed = true;
                                
                        if ( ! $this.is( event.target ) ) {
                                return;
                        }

                        if ( $input.prop( 'type' ) === 'radio' ) {
                                if ( $input.prop( 'checked' ) && $this.hasClass( 'active' ) ) { 
                                        changed = false;
                                } else {
                                        $parent.find( '.active' ).removeClass( 'active' );
                                }
                        }
                        
                        if ( changed ) { 
                                $input.prop( 'checked', ! $this.hasClass( 'active' ) ).trigger( 'change' );
                                $this.toggleClass( 'active' );
                        }                        
                                
                        event.preventDefault();
                } );
        };
        
        /* ========================================================================== *
         * REPEATER GROUP FIELDS
         * ========================================================================== */
        
        /**
         * Repeater group fields.
         * 
         * @since 1.0.2
         * @private
         */
        Helper.prototype.field_repeater = function() {   
                var wrapper = '.fsmh-field-repeater-wrapper';
                
                // Duplicate group fields.
                this.$body.on( 'click.fsmh', '.fsmh-field-repeater-duplicate', function( event ) {
                        var     $group  = $( this ).closest( wrapper ),
                                $clone  = $group.clone();
                                
                        event.preventDefault();
                        
                        $clone.hide();
                        $clone.insertAfter( $group );
                        
                        /**
                         * Fires before duplicating the fields.
                         * 
                         * @since 1.0.2
                         */
                        $clone.trigger( 'fsmh_duplicate_field_repeater' );
                        
                        $clone.slideDown( 400, function() {
                                /**
                                 * Fires after the fields have been duplicated.
                                 * 
                                 * @since 1.0.2
                                 */
                                $( this ).trigger( 'fsmh_duplicated_field_repeater' );  
                        } );  
                } );
                
                // Remove group fields.
                this.$body.on( 'click.fsmh', '.fsmh-field-repeater-remove', function( event ) {
                        var $group = $( this ).closest( wrapper );                          
                                
                        event.preventDefault();
                        
                        if ( $group.siblings( wrapper ).length === 0 ) {
                                return;
                        }
                        
                        /**
                         * Fires before removing the fields.
                         * 
                         * @since 1.0.2
                         */
                        $group.trigger( 'fsmh_remove_field_repeater' );
                        $group.slideUp( 400, function() {
                                var     $this           = $( this ),
                                        $fsmHelper      = $this.closest( '.fsmh-container' );

                                $this.remove(); 
                                
                                /**
                                 * Fires after the fields have been removed.
                                 * 
                                 * @since 1.0.2
                                 */
                                $fsmHelper.trigger( 'fsmh_removed_field_repeater' );
                        } );  
                } );
        };
        
        /* ========================================================================== *
         * MODAL
         * ========================================================================== */
        
        /**
         * Modals.
         * 
         * @since 1.0.2
         * @private
         */
        Helper.prototype.modal = function() {
                var self = this;
                
                /* -------------------------------------------------------------------------- *
                 * Trigger submit event
                 * -------------------------------------------------------------------------- */
                
                this.$body.on( 'keypress.fsmh', '.fsmh-container .modal :input:not(textarea)', function( event ) {
                        if ( event.which === 13 ) {
                                /**
                                 * Fires before submitting the form.
                                 * 
                                 * @since 1.0.2
                                 */
                                $( this ).trigger( 'fsmh_submit_modal_form' );
                        }
                } );

                this.$body.on( 'click.fsmh', '.fsmh-container .modal .button-submit', function () {
                        /**
                         * Fires before submitting the form.
                         * 
                         * @since 1.0.2
                         */
                        $( this ).closest( '.modal' ).trigger( 'fsmh_submit_modal_form' );
                } );                     
                
                /* -------------------------------------------------------------------------- *
                 * Font Awesome
                 * -------------------------------------------------------------------------- */
                
                // Search                
                this.$body.on( 'keyup.fsmh change.fsmh search.fsmh', '.fsmh-container #fsmh-modal-font-awesome [type="search"]', function() {
                        var     $this   = $( this ),
                                $modal  = $this.closest( '#fsmh-modal-font-awesome' ),
                                value   = $this.val();

                        $modal.find( '.fa' ).each( function() {
                                var $this = $( this );

                                if ( $this.attr( 'title' ).indexOf( value ) >= 0 ) {
                                        $this.show();
                                } else {
                                        $this.hide();
                                }
                        } );
                } );
                
                // Select                
                this.$body.on( 'click.fsmh', '.fsmh-container .fsmh-field-font-awesome', function() {
                        var     $field  = $( this ),
                                $modal  = $( '.fsmh-container #fsmh-modal-font-awesome' ).fsmhBsModal( 'show' );
                                        
                        $modal.one( 'click.fsmh', '.fa', function() {
                                $field.val( $( this ).data( 'value' ) );
                                $modal.fsmhBsModal( 'hide' );
                        } );
                        
                        $modal.one( 'hide.bs.modal.fsmh', function() {
                                $field.trigger( 'change' );
                                $field.trigger( 'focus' );
                        } );
                } );
                
                // Preview                
                this.$body.on( 'keyup.fsmh change.fsmh', '.fsmh-container .fsmh-field-font-awesome', function() {
                        var     $this = $( this ),
                                $icon = $this.next( '.fa' );
                        
                        $icon.attr( 'class', 'fa ' + $this.val() );
                } );
        };  
        
        /* ========================================================================== *
         * CHECKBOX
         * ========================================================================== */
        
        /**
         * Set value input[type=hidden] that next to input[type=checkbox]
         */
        Helper.prototype.setCheckboxValue = function() {
                this.$body.on('change', '.fsmh-container input[type=checkbox]', function() {
                        $(this).next('input[type=hidden]').val(this.checked ? 1 : 0);                        
                });
        };
        
        /* ========================================================================== *
         * SLIDE
         * ========================================================================== */
        
        Helper.prototype.slide = function( event ) {
                $( this ).closest( '[data-fsmh-slide]' ).find( '> .panel-body' ).stop().slideToggle();
        };
        
        /* ========================================================================== *
         * GENERAL
         * ========================================================================== */
        
        /**
         * Checks a string is in JSON format or not.
         * 
         * @param {String} str
         * @returns {Boolean}
         */
        Helper.prototype.isJSON = function( str ) {
                if ( ! str ) {
                        return false;
                }
                
                try {
                        $.parseJSON( str );
                } catch( error ) {                                
                        return false;
                }  

                return true;
        };
        
        F.Helper = new Helper();
            
} ) ( jQuery, window.FriskaMax = window.FriskaMax || {} );