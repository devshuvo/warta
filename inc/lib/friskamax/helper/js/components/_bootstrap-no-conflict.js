/**
 * Implement Bootstrap no conflict mode.
 * 
 * 
 * Because of some WordPress plugins may also include Bootstrap's jQuery 
 * plugins, we cannot initialize Bootstrap DATA-API. We cannot initialize 
 * Bootstrap DATA-API more than once, it will not work. @see {@link
 * http://getbootstrap.com/javascript/#js-data-attrs}.
 * 
 * We cannot use $(document).off('.modal.data-api') because it will remove the
 * functionality of the WordPress plugins that uses Bootstrap's jQuery plugins.
 * 
 * To overcome this, we need to add prefix to the data-toggle with fsmh-. 
 * Example:
 *      data-toggle="modal" to data-toggle="fsmh-modal"
 *      [data-toggle="modal"] to [data-toggle="fsmh-modal"]
 *      
 * 
 * @since 1.0.0
 * @package friskamax-library\js-helper
 * 
 * @param {jQuery} $
 * @param {object} window
 * @param {object} document
 * @param {undefined} undefined
 */
; ( function ( $, window, document, undefined ) { 'use strict';

        /* -------------------------------------------------------------------------- *
         * Modal
         * -------------------------------------------------------------------------- */

        var fsmhBsModal         = $.fn.modal.noConflict();      // return $.fn.modal to previously assigned value
        $.fn.fsmhBsModal        = fsmhBsModal;                  // give $().fsmhBsModal the Bootstrap functionality 
        
        // Replace [data-toggle="modal"] width [data-toggle="fsmh-modal"]
        // Replace Plugin with fsmhBsModal
        $(document).on('click.bs.modal.data-api', '[data-toggle="fsmh-modal"]', function (e) {
          var $this   = $(this);
          var href    = $this.attr('href');
          var $target = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, ''))); // strip for ie7
          var option  = $target.data('bs.modal') ? 'toggle' : $.extend({ remote: !/#/.test(href) && href }, $target.data(), $this.data());

          if ($this.is('a')) e.preventDefault();

          $target.one('show.bs.modal', function (showEvent) {
            if (showEvent.isDefaultPrevented()) return; // only register focus restorer if modal will actually get shown
            $target.one('hidden.bs.modal', function () {
              $this.is(':visible') && $this.trigger('focus');
            });
          });
          fsmhBsModal.call($target, option, this);
        });
        
        /* -------------------------------------------------------------------------- *
         * Tab
         * -------------------------------------------------------------------------- */
        
        var fsmhBsTab           = $.fn.tab.noConflict();        // return $.fn.tab to previously assigned value
        $.fn.fsmhBsTab          = fsmhBsTab;                    // give $().fsmhBsTab the Bootstrap functionality 

        // Replace [data-toggle="tab"], [data-toggle="pill"] with [data-toggle="fsmh-tab"], [data-toggle="fsmh-pill"]
        // Replace Plugin with fsmhBsTab
        $(document).on('click.bs.tab.data-api', '[data-toggle="fsmh-tab"], [data-toggle="fsmh-pill"]', function (e) {
          e.preventDefault();
          fsmhBsTab.call($(this), 'show');
        });
        
} ) ( jQuery, window, document );