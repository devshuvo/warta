/*!========================================================================
 * Bootstrap: modal.js v3.2.0
 * http://getbootstrap.com/javascript/#modals
 * ========================================================================
 * Copyright 2011-2014 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // MODAL CLASS DEFINITION
  // ======================

  var Modal = function (element, options) {
    this.options        = options
    this.$body          = $(document.body)
    this.$element       = $(element)
    this.$backdrop      =
    this.isShown        = null
    this.scrollbarWidth = 0

    if (this.options.remote) {
      this.$element
        .find('.modal-content')
        .load(this.options.remote, $.proxy(function () {
          this.$element.trigger('loaded.bs.modal')
        }, this))
    }
  }

  Modal.VERSION  = '3.2.0'

  Modal.DEFAULTS = {
    backdrop: true,
    keyboard: true,
    show: true
  }

  Modal.prototype.toggle = function (_relatedTarget) {
    return this.isShown ? this.hide() : this.show(_relatedTarget)
  }

  Modal.prototype.show = function (_relatedTarget) {
    var that = this
    var e    = $.Event('show.bs.modal', { relatedTarget: _relatedTarget })

    this.$element.trigger(e)

    if (this.isShown || e.isDefaultPrevented()) return

    this.isShown = true

    this.checkScrollbar()
    this.$body.addClass('modal-open')

    this.setScrollbar()
    this.escape()

    this.$element.on('click.dismiss.bs.modal', '[data-dismiss="modal"]', $.proxy(this.hide, this))

    this.backdrop(function () {
      var transition = $.support.transition && that.$element.hasClass('fade')

      if (!that.$element.parent().length) {
        that.$element.appendTo(that.$body) // don't move modals dom position
      }

      that.$element
        .show()
        .scrollTop(0)

      if (transition) {
        that.$element[0].offsetWidth // force reflow
      }

      that.$element
        .addClass('in')
        .attr('aria-hidden', false)

      that.enforceFocus()

      var e = $.Event('shown.bs.modal', { relatedTarget: _relatedTarget })

      transition ?
        that.$element.find('.modal-dialog') // wait for modal to slide in
          .one('bsTransitionEnd', function () {
            that.$element.trigger('focus').trigger(e)
          })
          .emulateTransitionEnd(300) :
        that.$element.trigger('focus').trigger(e)
    })
  }

  Modal.prototype.hide = function (e) {
    if (e) e.preventDefault()

    e = $.Event('hide.bs.modal')

    this.$element.trigger(e)

    if (!this.isShown || e.isDefaultPrevented()) return

    this.isShown = false

    this.$body.removeClass('modal-open')

    this.resetScrollbar()
    this.escape()

    $(document).off('focusin.bs.modal')

    this.$element
      .removeClass('in')
      .attr('aria-hidden', true)
      .off('click.dismiss.bs.modal')

    $.support.transition && this.$element.hasClass('fade') ?
      this.$element
        .one('bsTransitionEnd', $.proxy(this.hideModal, this))
        .emulateTransitionEnd(300) :
      this.hideModal()
  }

  Modal.prototype.enforceFocus = function () {
    $(document)
      .off('focusin.bs.modal') // guard against infinite focus loop
      .on('focusin.bs.modal', $.proxy(function (e) {
        if (this.$element[0] !== e.target && !this.$element.has(e.target).length) {
          this.$element.trigger('focus')
        }
      }, this))
  }

  Modal.prototype.escape = function () {
    if (this.isShown && this.options.keyboard) {
      this.$element.on('keyup.dismiss.bs.modal', $.proxy(function (e) {
        e.which == 27 && this.hide()
      }, this))
    } else if (!this.isShown) {
      this.$element.off('keyup.dismiss.bs.modal')
    }
  }

  Modal.prototype.hideModal = function () {
    var that = this
    this.$element.hide()
    this.backdrop(function () {
      that.$element.trigger('hidden.bs.modal')
    })
  }

  Modal.prototype.removeBackdrop = function () {
    this.$backdrop && this.$backdrop.remove()
    this.$backdrop = null
  }

  Modal.prototype.backdrop = function (callback) {
    var that = this
    var animate = this.$element.hasClass('fade') ? 'fade' : ''

    if (this.isShown && this.options.backdrop) {
      var doAnimate = $.support.transition && animate

      this.$backdrop = $('<div class="modal-backdrop ' + animate + '" />')
        .appendTo(this.$body)

      this.$element.on('click.dismiss.bs.modal', $.proxy(function (e) {
        if (e.target !== e.currentTarget) return
        this.options.backdrop == 'static'
          ? this.$element[0].focus.call(this.$element[0])
          : this.hide.call(this)
      }, this))

      if (doAnimate) this.$backdrop[0].offsetWidth // force reflow

      this.$backdrop.addClass('in')

      if (!callback) return

      doAnimate ?
        this.$backdrop
          .one('bsTransitionEnd', callback)
          .emulateTransitionEnd(150) :
        callback()

    } else if (!this.isShown && this.$backdrop) {
      this.$backdrop.removeClass('in')

      var callbackRemove = function () {
        that.removeBackdrop()
        callback && callback()
      }
      $.support.transition && this.$element.hasClass('fade') ?
        this.$backdrop
          .one('bsTransitionEnd', callbackRemove)
          .emulateTransitionEnd(150) :
        callbackRemove()

    } else if (callback) {
      callback()
    }
  }

  Modal.prototype.checkScrollbar = function () {
    if (document.body.clientWidth >= window.innerWidth) return
    this.scrollbarWidth = this.scrollbarWidth || this.measureScrollbar()
  }

  Modal.prototype.setScrollbar = function () {
    var bodyPad = parseInt((this.$body.css('padding-right') || 0), 10)
    if (this.scrollbarWidth) this.$body.css('padding-right', bodyPad + this.scrollbarWidth)
  }

  Modal.prototype.resetScrollbar = function () {
    this.$body.css('padding-right', '')
  }

  Modal.prototype.measureScrollbar = function () { // thx walsh
    var scrollDiv = document.createElement('div')
    scrollDiv.className = 'modal-scrollbar-measure'
    this.$body.append(scrollDiv)
    var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth
    this.$body[0].removeChild(scrollDiv)
    return scrollbarWidth
  }


  // MODAL PLUGIN DEFINITION
  // =======================

  function Plugin(option, _relatedTarget) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.modal')
      var options = $.extend({}, Modal.DEFAULTS, $this.data(), typeof option == 'object' && option)

      if (!data) $this.data('bs.modal', (data = new Modal(this, options)))
      if (typeof option == 'string') data[option](_relatedTarget)
      else if (options.show) data.show(_relatedTarget)
    })
  }

  var old = $.fn.modal

  $.fn.modal             = Plugin
  $.fn.modal.Constructor = Modal


  // MODAL NO CONFLICT
  // =================

  $.fn.modal.noConflict = function () {
    $.fn.modal = old
    return this
  }

}(jQuery);

/*!========================================================================
 * Bootstrap: tab.js v3.2.0
 * http://getbootstrap.com/javascript/#tabs
 * ========================================================================
 * Copyright 2011-2014 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // TAB CLASS DEFINITION
  // ====================

  var Tab = function (element) {
    this.element = $(element)
  }

  Tab.VERSION = '3.2.0'

  Tab.prototype.show = function () {
    var $this    = this.element
    var $ul      = $this.closest('ul:not(.dropdown-menu)')
    var selector = $this.data('target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    if ($this.parent('li').hasClass('active')) return

    var previous = $ul.find('.active:last a')[0]
    var e        = $.Event('show.bs.tab', {
      relatedTarget: previous
    })

    $this.trigger(e)

    if (e.isDefaultPrevented()) return

    var $target = $(selector)

    this.activate($this.closest('li'), $ul)
    this.activate($target, $target.parent(), function () {
      $this.trigger({
        type: 'shown.bs.tab',
        relatedTarget: previous
      })
    })
  }

  Tab.prototype.activate = function (element, container, callback) {
    var $active    = container.find('> .active')
    var transition = callback
      && $.support.transition
      && $active.hasClass('fade')

    function next() {
      $active
        .removeClass('active')
        .find('> .dropdown-menu > .active')
        .removeClass('active')

      element.addClass('active')

      if (transition) {
        element[0].offsetWidth // reflow for transition
        element.addClass('in')
      } else {
        element.removeClass('fade')
      }

      if (element.parent('.dropdown-menu')) {
        element.closest('li.dropdown').addClass('active')
      }

      callback && callback()
    }

    transition ?
      $active
        .one('bsTransitionEnd', next)
        .emulateTransitionEnd(150) :
      next()

    $active.removeClass('in')
  }


  // TAB PLUGIN DEFINITION
  // =====================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.tab')

      if (!data) $this.data('bs.tab', (data = new Tab(this)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.tab

  $.fn.tab             = Plugin
  $.fn.tab.Constructor = Tab


  // TAB NO CONFLICT
  // ===============

  $.fn.tab.noConflict = function () {
    $.fn.tab = old
    return this
  }

}(jQuery);

/**
 * Implement Bootstrap no conflict mode.
 * 
 * 
 * Because of some WordPress plugins may also include Bootstrap's jQuery 
 * plugins, we cannot initialize Bootstrap DATA-API. We cannot initialize 
 * Bootstrap DATA-API more than once, it will not work. @see {@link
 * http://getbootstrap.com/javascript/#js-data-attrs}.
 * 
 * We cannot use $(document).off('.modal.data-api') because it will remove the
 * functionality of the WordPress plugins that uses Bootstrap's jQuery plugins.
 * 
 * To overcome this, we need to add prefix to the data-toggle with fsmh-. 
 * Example:
 *      data-toggle="modal" to data-toggle="fsmh-modal"
 *      [data-toggle="modal"] to [data-toggle="fsmh-modal"]
 *      
 * 
 * @since 1.0.0
 * @package friskamax-library\js-helper
 * 
 * @param {jQuery} $
 * @param {object} window
 * @param {object} document
 * @param {undefined} undefined
 */
; ( function ( $, window, document, undefined ) { 'use strict';

        /* -------------------------------------------------------------------------- *
         * Modal
         * -------------------------------------------------------------------------- */

        var fsmhBsModal         = $.fn.modal.noConflict();      // return $.fn.modal to previously assigned value
        $.fn.fsmhBsModal        = fsmhBsModal;                  // give $().fsmhBsModal the Bootstrap functionality 
        
        // Replace [data-toggle="modal"] width [data-toggle="fsmh-modal"]
        // Replace Plugin with fsmhBsModal
        $(document).on('click.bs.modal.data-api', '[data-toggle="fsmh-modal"]', function (e) {
          var $this   = $(this);
          var href    = $this.attr('href');
          var $target = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, ''))); // strip for ie7
          var option  = $target.data('bs.modal') ? 'toggle' : $.extend({ remote: !/#/.test(href) && href }, $target.data(), $this.data());

          if ($this.is('a')) e.preventDefault();

          $target.one('show.bs.modal', function (showEvent) {
            if (showEvent.isDefaultPrevented()) return; // only register focus restorer if modal will actually get shown
            $target.one('hidden.bs.modal', function () {
              $this.is(':visible') && $this.trigger('focus');
            });
          });
          fsmhBsModal.call($target, option, this);
        });
        
        /* -------------------------------------------------------------------------- *
         * Tab
         * -------------------------------------------------------------------------- */
        
        var fsmhBsTab           = $.fn.tab.noConflict();        // return $.fn.tab to previously assigned value
        $.fn.fsmhBsTab          = fsmhBsTab;                    // give $().fsmhBsTab the Bootstrap functionality 

        // Replace [data-toggle="tab"], [data-toggle="pill"] with [data-toggle="fsmh-tab"], [data-toggle="fsmh-pill"]
        // Replace Plugin with fsmhBsTab
        $(document).on('click.bs.tab.data-api', '[data-toggle="fsmh-tab"], [data-toggle="fsmh-pill"]', function (e) {
          e.preventDefault();
          fsmhBsTab.call($(this), 'show');
        });
        
} ) ( jQuery, window, document );
/*
 * FriskaMax JS Helper
 * 
 * @package friskamax-library\helper
 * @since 1.0.0
 */

; ( function( $, F, undefined ) { 'use strict';
        
        var Helper = function() {
                
                this.$document  = $( document );
                this.$body      = $( 'body' );
                                
                this.listen();
                this.requires();
                this.field_buttons();
                this.field_repeater();
                this.modal();
                this.setCheckboxValue();
                
        };
        
        /**
         * Event listener.
         */
        Helper.prototype.listen = function () {
                var self = this;           
                
                /* -------------------------------------------------------------------------- *
                 * Collapse
                 * -------------------------------------------------------------------------- */
                this.$body.on( 'click.fsmh.slide', '.fsmh-container [data-toggle="fsmh-slide"]', Helper.prototype.slide ); 
        };
        
        /* ========================================================================== *
         * REQUIRES
         * ========================================================================== */
                        
        /**
         * Show hide an element based on the required field values.
         * 
         * @since 1.0.0
         * @private
         */
        Helper.prototype.requires = function() {   
                
                var self = this;
                
                /**
                 * Show/hide option based on required fields.
                 * 
                 * @since 1.0.2
                 */
                function toggle() {
                        
                        var     $field          = $( this ),
                                $wrapper        = $field.closest( '.fsmh-container, .fsmh-field-repeater-wrapper' ),
                                requires        = $field.data( 'requires' ),
                                show            = requires.relation == 'and'; // if the relation is 'and', default show 'true', otherwise 'false'.

                        // Check the required fields.
                        $.each( requires, function() {          
                                
                                // The relation is not an object.
                                if ( typeof this !== 'object' ) {
                                        return;
                                }
                                
                                var     $required_field = $wrapper.find( this.field ),
                                        is_true         = true;

                                switch( this.compare ) {
                                        
                                        case 'check':
                                                is_true = $required_field.prop( 'checked' );                                        
                                                break;
                                                
                                        case 'radio':
                                                is_true = $required_field.filter( ':checked' ).val() == this.value;
                                                break;
                                                
                                        case 'equal':
                                                is_true = $required_field.val() == this.value;
                                                break;
                                                
                                        case 'not-equal':
                                                is_true = $required_field.val() != this.value;
                                                break;
                                                
                                }     

                                if ( requires.relation == 'and' ) {
                                        
                                        if ( ! is_true ) {
                                                show = false;
                                        }
                                        
                                } else if ( is_true ) {
                                        
                                        show = true;
                                        
                                }   
                                
                        } );

                        $field.stop();

                        if ( show ) {
                                $field.slideDown(); 
                        } else if ( ! show ) {
                                $field.slideUp();
                        }
                        
                };
                                
                // On field change, group field duplicated and modal shown.
                this.$body.on( 'change.fsmh fsmh_duplicated_field_repeater shown.bs.modal.fsmh', '.fsmh-container', function() {
                        
                        $( this ).find( '[data-requires]' ).each( toggle );                                                                       
                        
                } );   
                
                // On save widget.
                this.$document.ajaxComplete( function( event, xhr, settings ) {
                        
                        if ( settings.data && settings.data.indexOf( 'action=save-widget' ) >= 0 ) {
                                self.$body.find( '.fsmh-container [data-requires]' ).each( toggle );
                        }
                        
                } );
                
                // Run in the first load.                                                                        
                this.$body.find( '.fsmh-container [data-requires]' ).each( toggle );
                
        };
        
        /* ========================================================================== *
         * BUTTONS FIELD
         * ========================================================================== */
        
        /**
         * Buttons field.
         * 
         * Modified version of Bootstrap Button.
         * 
         * @since 1.0.0
         * @private
         */
        Helper.prototype.field_buttons = function() {
                this.$body.on( 'click.fsmh', '.fsmh-field-buttons .button', function( event ) {
                        var     $this   = $( this ),
                                $parent = $this.closest( '.fsmh-field-buttons' ),
                                $input  = $this.find( 'input' ),
                                changed = true;
                                
                        if ( ! $this.is( event.target ) ) {
                                return;
                        }

                        if ( $input.prop( 'type' ) === 'radio' ) {
                                if ( $input.prop( 'checked' ) && $this.hasClass( 'active' ) ) { 
                                        changed = false;
                                } else {
                                        $parent.find( '.active' ).removeClass( 'active' );
                                }
                        }
                        
                        if ( changed ) { 
                                $input.prop( 'checked', ! $this.hasClass( 'active' ) ).trigger( 'change' );
                                $this.toggleClass( 'active' );
                        }                        
                                
                        event.preventDefault();
                } );
        };
        
        /* ========================================================================== *
         * REPEATER GROUP FIELDS
         * ========================================================================== */
        
        /**
         * Repeater group fields.
         * 
         * @since 1.0.2
         * @private
         */
        Helper.prototype.field_repeater = function() {   
                var wrapper = '.fsmh-field-repeater-wrapper';
                
                // Duplicate group fields.
                this.$body.on( 'click.fsmh', '.fsmh-field-repeater-duplicate', function( event ) {
                        var     $group  = $( this ).closest( wrapper ),
                                $clone  = $group.clone();
                                
                        event.preventDefault();
                        
                        $clone.hide();
                        $clone.insertAfter( $group );
                        
                        /**
                         * Fires before duplicating the fields.
                         * 
                         * @since 1.0.2
                         */
                        $clone.trigger( 'fsmh_duplicate_field_repeater' );
                        
                        $clone.slideDown( 400, function() {
                                /**
                                 * Fires after the fields have been duplicated.
                                 * 
                                 * @since 1.0.2
                                 */
                                $( this ).trigger( 'fsmh_duplicated_field_repeater' );  
                        } );  
                } );
                
                // Remove group fields.
                this.$body.on( 'click.fsmh', '.fsmh-field-repeater-remove', function( event ) {
                        var $group = $( this ).closest( wrapper );                          
                                
                        event.preventDefault();
                        
                        if ( $group.siblings( wrapper ).length === 0 ) {
                                return;
                        }
                        
                        /**
                         * Fires before removing the fields.
                         * 
                         * @since 1.0.2
                         */
                        $group.trigger( 'fsmh_remove_field_repeater' );
                        $group.slideUp( 400, function() {
                                var     $this           = $( this ),
                                        $fsmHelper      = $this.closest( '.fsmh-container' );

                                $this.remove(); 
                                
                                /**
                                 * Fires after the fields have been removed.
                                 * 
                                 * @since 1.0.2
                                 */
                                $fsmHelper.trigger( 'fsmh_removed_field_repeater' );
                        } );  
                } );
        };
        
        /* ========================================================================== *
         * MODAL
         * ========================================================================== */
        
        /**
         * Modals.
         * 
         * @since 1.0.2
         * @private
         */
        Helper.prototype.modal = function() {
                var self = this;
                
                /* -------------------------------------------------------------------------- *
                 * Trigger submit event
                 * -------------------------------------------------------------------------- */
                
                this.$body.on( 'keypress.fsmh', '.fsmh-container .modal :input:not(textarea)', function( event ) {
                        if ( event.which === 13 ) {
                                /**
                                 * Fires before submitting the form.
                                 * 
                                 * @since 1.0.2
                                 */
                                $( this ).trigger( 'fsmh_submit_modal_form' );
                        }
                } );

                this.$body.on( 'click.fsmh', '.fsmh-container .modal .button-submit', function () {
                        /**
                         * Fires before submitting the form.
                         * 
                         * @since 1.0.2
                         */
                        $( this ).closest( '.modal' ).trigger( 'fsmh_submit_modal_form' );
                } );                     
                
                /* -------------------------------------------------------------------------- *
                 * Font Awesome
                 * -------------------------------------------------------------------------- */
                
                // Search                
                this.$body.on( 'keyup.fsmh change.fsmh search.fsmh', '.fsmh-container #fsmh-modal-font-awesome [type="search"]', function() {
                        var     $this   = $( this ),
                                $modal  = $this.closest( '#fsmh-modal-font-awesome' ),
                                value   = $this.val();

                        $modal.find( '.fa' ).each( function() {
                                var $this = $( this );

                                if ( $this.attr( 'title' ).indexOf( value ) >= 0 ) {
                                        $this.show();
                                } else {
                                        $this.hide();
                                }
                        } );
                } );
                
                // Select                
                this.$body.on( 'click.fsmh', '.fsmh-container .fsmh-field-font-awesome', function() {
                        var     $field  = $( this ),
                                $modal  = $( '.fsmh-container #fsmh-modal-font-awesome' ).fsmhBsModal( 'show' );
                                        
                        $modal.one( 'click.fsmh', '.fa', function() {
                                $field.val( $( this ).data( 'value' ) );
                                $modal.fsmhBsModal( 'hide' );
                        } );
                        
                        $modal.one( 'hide.bs.modal.fsmh', function() {
                                $field.trigger( 'change' );
                                $field.trigger( 'focus' );
                        } );
                } );
                
                // Preview                
                this.$body.on( 'keyup.fsmh change.fsmh', '.fsmh-container .fsmh-field-font-awesome', function() {
                        var     $this = $( this ),
                                $icon = $this.next( '.fa' );
                        
                        $icon.attr( 'class', 'fa ' + $this.val() );
                } );
        };  
        
        /* ========================================================================== *
         * CHECKBOX
         * ========================================================================== */
        
        /**
         * Set value input[type=hidden] that next to input[type=checkbox]
         */
        Helper.prototype.setCheckboxValue = function() {
                this.$body.on('change', '.fsmh-container input[type=checkbox]', function() {
                        $(this).next('input[type=hidden]').val(this.checked ? 1 : 0);                        
                });
        };
        
        /* ========================================================================== *
         * SLIDE
         * ========================================================================== */
        
        Helper.prototype.slide = function( event ) {
                $( this ).closest( '[data-fsmh-slide]' ).find( '> .panel-body' ).stop().slideToggle();
        };
        
        /* ========================================================================== *
         * GENERAL
         * ========================================================================== */
        
        /**
         * Checks a string is in JSON format or not.
         * 
         * @param {String} str
         * @returns {Boolean}
         */
        Helper.prototype.isJSON = function( str ) {
                if ( ! str ) {
                        return false;
                }
                
                try {
                        $.parseJSON( str );
                } catch( error ) {                                
                        return false;
                }  

                return true;
        };
        
        F.Helper = new Helper();
            
} ) ( jQuery, window.FriskaMax = window.FriskaMax || {} );