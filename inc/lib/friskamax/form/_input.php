<?php

/**
 * Input field.
 * 
 * @since 1.0.1
 * @package friskaMax-library\form
 */

/**
 * Returns HTML input field.
 * 
 * @since 1.0.1
 * 
 * @param array $args {         
 *      An array of Arguments.
 * 
 *      @type string $before    Opening wrapper tag. %s: Additional attributes.
 *      @type string $after     Closing wrapper tag.
 *      @type string $label     Field label.
 *      @type array $attr       Field attributes.
 *      @type array $requires   Requires configuration (field dependencies). 
 * }
 */
function fsmf_get_field_input( $args = array() ) {
        
        $r              = friskamax_parse_args( $args, array(
                                'before'        => '<p %s>',
                                'after'         => '</p>',
                                'label'         => '',
                                'desc'          => '',
                                'attr'          => array(),
                                'requires'      => array()
                        ), 'fsmf_get_field_input_text_r' );
        $r->attr        = wp_parse_args( $r->attr, array(
                                'type'  => 'text',
                                'class' => 'widefat'
                        ) );
                        
        ob_start();
        
        ?>

                <?php echo sprintf( 
                        $r->before, 
                        $r->requires 
                                ? "data-requires='" . json_encode( $r->requires ) . "'"
                                : '' 
                ); ?>

                        <label>
                                <strong><?php echo $r->label ?></strong>
                                <input <?php fsmf_field_attr( $r->attr ) ?>>
                        </label>
                        
                        <?php if( $r->desc ) : ?>
                                <span class="howto"><?php echo $r->desc ?></span>
                        <?php endif ?>
                                
                <?php echo $r->after; ?>
                
        <?php
        
        /**
         * Filter HTML input field.
         * 
         * @since 1.0.1
         * 
         * @param string $input HTML input field.
         * @param object $r     Arguments.
         */
        return apply_filters( 'fsmf_get_field_input', ob_get_clean(), $r );
        
}

/**
 * Prints HTML input field.
 * 
 * @see fsmf_get_field_input() 
 * @since 1.0.0
 * 
 * @param array $args
 */
function fsmf_field_input( $args = array() ) {
        echo fsmf_get_field_input( $args );
}