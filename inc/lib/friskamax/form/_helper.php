<?php

/* -------------------------------------------------------------------------- *
 * General
 * -------------------------------------------------------------------------- */

/**
 * Get form fields.
 * 
 * @since 1.0.2
 * @param array $fields Fields configurations.
 * @return string       HTML form fields.
 */
function fsmf_get_fields( $fields ) {
        $return = '';
        
        foreach ( $fields as $_args ) {
                if ( ! isset( $_args[ 'type' ] ) ) {
                        continue;
                }
                
                switch ( $_args[ 'type' ] ) {
                        case 'input':
                                $return .= fsmf_get_field_input( $_args );
                                break;

                        case 'select':
                                $return .= fsmf_get_field_select( $_args );
                                break;
                }
        }
        
        /**
         * Filter the HTML form fields.
         * 
         * @since 1.0.2
         * 
         * @param string $return        HTML form fields.
         * @param array $fields         Fields configurations.
         */
        return apply_filters( 'fsmf_get_fields', $return, $fields );
}

/**
 * Display form fields.
 * 
 * @see fsmf_get_fields()
 * @since 1.0.2
 * @param array $fields
 */
function fsmf_fields( $fields ) {
        echo fsmf_get_fields( $fields );
}

/* -------------------------------------------------------------------------- *
 * Attributes
 * -------------------------------------------------------------------------- */

/**
 * Returns field attributes.
 * 
 * @param array $attributes The attributes.
 * @return string
 */
function fsmf_get_field_attr( $attributes = array() ) {
        $return = '';
        
        foreach ( $attributes as $key => $value ) {
                $value  = ! is_null( $value ) ? "='" . esc_attr( $value ) . "' " : '';
                $return .= $key . $value;
        }
        
        return apply_filters( 'fsmf_get_field_attr', $return, $attributes );
}

/**
 * Prints field attributes.
 * 
 * @param array $attributes The attributes.
 */
function fsmf_field_attr( $attributes = array() ) {
        echo fsmf_get_field_attr( $attributes );
}

/* -------------------------------------------------------------------------- *
 * Options
 * -------------------------------------------------------------------------- */

/**
 * Returns author options.
 * 
 * @return array
 */
function fsmfo_get_authors() {
        $authors = array();
        
        foreach ( get_users( array( 'fields' => array( 'ID', 'display_name' ), 'who' => 'authors' ) ) as $author ) {
                $authors[ $author->ID ] = $author->display_name;
        }
        
        return apply_filters( 'fsmfo_get_authors', $authors );
}

/**
 * Returns category options.
 * 
 * @return array
 */
function fsmfo_get_categories() {
        $categories = array();
        
        foreach ( get_categories( array( 'hierarchical' => true ) ) as $category ) {
                $categories[ $category->term_id ] = $category->name;
        }
        
        return apply_filters( 'fsmfo_get_categories', $categories );
}

/**
 * Returns post format options. Only post formats that are supported by the theme.
 * 
 * @return array
 */
function fsmfo_get_post_formats() {
        $post_formats           = array();
        $supported_post_formats = get_theme_support( 'post-formats' );
        
        foreach ( $supported_post_formats[0] as $post_format ) {                
                $post_formats[ $post_format ] = get_post_format_string( $post_format );
        }
        
        return apply_filters( 'fsmfo_get_post_formats', $post_formats );
}

/**
 * Returns switch options.
 * 
 * @return array
 */
function fsmfo_get_switch() {
        return apply_filters( 'fsmfo_get_switch', array( 
                __( 'Off', FRISKAMAX_THEME_SLUG ), 
                __( 'On', FRISKAMAX_THEME_SLUG ) 
        ) );
}

/**
 * Returns date options.
 * 
 * @return array
 */
function fsmfo_get_query_date() {
        return apply_filters( 'fsmfo_get_query_date', array(
                'now'                                           => __( 'Now', FRISKAMAX_THEME_SLUG ),                
                'today'                                         => __( 'Today', FRISKAMAX_THEME_SLUG ),
                'yesterday'                                     => __( 'Yesterday', FRISKAMAX_THEME_SLUG ),
                'this week midnight'                            => __( 'This week', FRISKAMAX_THEME_SLUG ),
                'last week midnight'                            => __( 'Last week', FRISKAMAX_THEME_SLUG ),
                'first day of this month midnight'              => __( 'This month', FRISKAMAX_THEME_SLUG ),
                'first day of last month midnight'              => __( 'Last month', FRISKAMAX_THEME_SLUG ),
                'first day of january this year midnight'       => __( 'This year', FRISKAMAX_THEME_SLUG ),
                'first day of january last year midnight'       => __( 'Last year', FRISKAMAX_THEME_SLUG ),
        ) );
}

/**
 * Returns meta options.
 * 
 * @return array
 */
function fsmfo_get_meta() {
        return apply_filters( 'fsmfo_get_meta', array() );
}