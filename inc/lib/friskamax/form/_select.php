<?php

/**
 * Select field.
 * 
 * @since 1.0.1
 * @package friskaMax-library\form
 */

/**
 * Get select field.
 * 
 * @since 1.0.1
 * 
 * @param array $args {
 *      An array of arguments.
 * 
 *      @type string $before            Opening wrapper tag. %s: Additional attributes.
 *      @type string $after             Closing wrapper tag.
 *      @type boolean $multi            Optional. Whether to display multiple select.
 *      @type string $label             Field label.
 *      @type string $desc              Optional. Field description.
 *      @type string $option_none       Optional. Option value to use when no option is selected.
 *      @type array $options            The select options.
 *      @type string|array $selected    Selected options. Accept array if this is a multiple select, otherwise it accept string.
 *      @type array $attr               Field attributes.
 *      @type array $requires           Requires configuration (field dependencies). 
 * }
 * @return string       HTML Select field.
 */
function fsmf_get_field_select( $args = array() ) {
        
        $r              = friskamax_parse_args( $args, array(
                                'before'        => '<p %s>',
                                'after'         => '</p>',
                                'multi'         => false,
                                'label'         => '',
                                'desc'          => '',
                                'option_none'   => '',
                                'options'       => get_post_types(),
                                'selected'      => '',
                                'attr'          => array(),
                                'requires'      => array()
                        ), 'fsmf_get_field_input_text_r' );
        $r->attr        = wp_parse_args( $r->attr, array(
                                'type'  => 'text',
                                'class' => 'widefat'
                        ) );
        
        if ( ! $r->options ) {  
                return;
        }
        
        if ( $r->multi ) {      
                $r->attr[ 'multiple' ] = 'multiple';
        }
        
        ob_start();
        
        ?>

        <?php echo sprintf( 
                $r->before, 
                $r->requires 
                        ? "data-requires='" . json_encode( $r->requires ) . "'"
                        : '' 
        ); ?>

                <label>
                        <strong><?php echo $r->label ?></strong>
                        
                        <select <?php fsmf_field_attr( $r->attr ) ?>>
                                
                                <?php if ( $r->option_none ) : ?>
                                        <option value=""><?php echo $r->option_none ?></option>
                                <?php endif ?>

                                <?php foreach ( $r->options as $key => $value ) : ?>
                                        
                                        <?php 
                                                if ( $r->multi ) {       
                                                        $selected = in_array( $key, ( array ) $r->selected ) ? $key : null;
                                                } else {
                                                        $selected = $r->selected;
                                                }
                                        ?>
                                        
                                        <option value="<?php echo $key ?>" <?php selected( $selected, $key ) ?>>
                                                <?php echo $value ?>
                                        </option>
                                        
                                <?php endforeach; ?>
                                        
                        </select>
                        
                </label>

                <?php if( $r->desc ) : ?>
                        <span class="howto"><?php echo $r->desc ?></span>
                <?php endif ?>
                        
        <?php echo $r->after; ?>
                
        <?php
        
        /**
         * Filter HTML select field.
         * 
         * @since 1.0.1
         * 
         * @param string $select        HTML select field.
         * @param object $r             Arguments.
         */
        return apply_filters( 'fsmf_get_field_select', ob_get_clean(), $r );
        
}

/**
 * Prints HTML select field.
 * 
 * @see fsmf_get_field_select()
 * @since 1.0.1
 * 
 * @param array $args
 */
function fsmf_field_select( $args = array() ) {
        echo fsmf_get_field_select( $args );
}