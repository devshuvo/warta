<?php
/**
 * Forms
 * 
 * @since 1.0.1
 * @package friskaMax-library\forms
 */

require_once dirname( __FILE__ ) . '/_helper.php';
require_once dirname( __FILE__ ) . '/_input.php';
require_once dirname( __FILE__ ) . '/_select.php';