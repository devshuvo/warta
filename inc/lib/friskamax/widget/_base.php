<?php
/**
 * FriskaMax Base Widget.
 * 
 * @since 1.0.1
 * @package friskamax-library\widget
 */

class FriskaMax_Widget extends WP_Widget {
        
        /** 
         * Default query option values.
         * 
         * The query settings is not exactly like WP_Query paramenters. But it will
         * be reconstructed by FriskaMax_Widget::fsm_get_wp_query_args() before we
         * can use it as WP_Query arguments.
         * 
         * @link http://codex.wordpress.org/Class_Reference/WP_Query#Parameters
         * 
         * @since 1.0.1
         * 
         * @var array {
         *      Array of query settings.
         *      
         *      The $post_type and $post_status doesn't have setting fields for the user
         *      to change it. We need to include $post_type and $post_status as WP_Query 
         *      arguments, because:
         *      
         *      1.  The $post_type default value is 'post'. If 'tax_query' is set for a query, 
         *          the default value becomes 'any'.
         *      2.  The $post_status default value is 'publish', but if the user is logged in, 
         *          'private' is added. And if the query is run in an admin context 
         *          (administration area or AJAX call), protected statuses are added too. 
         *          By default protected statuses are 'future', 'draft' and 'pending'.  
         * 
         *      @type string $post_type                 Post type.  
         *      @type string $post_status               Post status.
         *      @type int $author                       Author id.
         *      @type int $cat                          Category id.
         *      @type string $tag                       Tag slugs. Separated by commas.
         *      @type string $post_format               Post format.
         *      @type string $s                         Search keyword.
         *      @type string $post__in                  Post ids. Separated by commas.
         *      @type int $posts_per_page               Number of post to show per page.
         *      @type int $paged                        Number of page.
         *      @type boolean $ignore_sticky_posts      Ignore post stickiness. 
         *      @type string $order                     Designates the ascending or descending order of the 'orderby' parameter.
         *      @type string $orderby                   Sort retrieved posts by parameter.
         *      @type string $date_after                Date to retrieve posts after. Accepts strtotime() compatible string.
         *      @type string $date_before               Date to retrieve posts before. Accepts strtotime() compatible string.
         * }
         */
        public $fsm_query_d = array(
                'post_type'             => 'post',
                'post_status'           => 'publish',
                
                'author'                => '',
                'cat'                   => '',
                'tag'                   => '',
                'post_format'           => '',
                's'                     => '',
                'post__in'              => '',
                'posts_per_page'        => '',
                'paged'                 => 1,
                'ignore_sticky_posts'   => true,
                'order'                 => 'DESC',
                'orderby'               => 'date',
                'date_after'            => '',
                'date_before'           => '',
        );
        
        /**
         * Return query fields.
         * 
         * @since 1.0.1
         * 
         * @param object $i             Previously saved values from database.
         * @param int|null $index       Settings index if the settings is an array.
         * 
         * @return string
         */
        public function fsm_get_query_fields( $i, $index = null ) {                
                $bracket        = $index !== null ? '[]' : '';
                $q              = $this->fsm_parse_query_settings( $i, $index );
                
                /**
                 * Filter the query fields configurations.
                 * 
                 * @since 1.0.2
                 * 
                 * @param array $fields         Field configurations.
                 * @param object $i             Previously saved values from database.
                 * @param int|null $index       Settings index if the settings is an array.
                 */
                $fields_array   = apply_filters( 'FriskaMax_Widget__fsm_get_query_fields_array', array(
                        'author' => array(
                                'type'          => 'select',
                                'label'         => __( 'Author', FRISKAMAX_THEME_SLUG ),
                                'option_none'   => __( '&mdash; Select &mdash;', FRISKAMAX_THEME_SLUG ),
                                'options'       => fsmfo_get_authors(),
                                'selected'      => $q->author,
                                'attr'          => array( 'name'  => $this->get_field_name( 'fsm_query' ) . '[author]' . $bracket ),
                        ),
                        'cat' => array(
                                'type'          => 'select',
                                'label'         => __( 'Category', FRISKAMAX_THEME_SLUG ),
                                'option_none'   => __( '&mdash; Select &mdash;', FRISKAMAX_THEME_SLUG ),
                                'options'       => fsmfo_get_categories(),
                                'selected'      => $q->cat,
                                'attr'          => array( 'name'  => $this->get_field_name( 'fsm_query' ) . '[cat]' . $bracket ),
                        ),
                        'tag' => array(
                                'type'  => 'input',
                                'label' => __( 'Tag slugs', FRISKAMAX_THEME_SLUG ),
                                'desc'  => __( 'Separate tag slugs with commas.', FRISKAMAX_THEME_SLUG ),
                                'attr'  => array(
                                        'name'  => $this->get_field_name( 'fsm_query' ) . '[tag]' . $bracket,
                                        'value' => esc_attr( $q->tag )
                                ),
                        ),
                        'post_format' => array(
                                'type'          => 'select',
                                'label'         => __( 'Post format', FRISKAMAX_THEME_SLUG ),
                                'option_none'   => __( '&mdash; Select &mdash;', FRISKAMAX_THEME_SLUG ),
                                'options'       => fsmfo_get_post_formats(),
                                'selected'      => $q->post_format,
                                'attr'          => array( 'name'  => $this->get_field_name( 'fsm_query' ) . '[post_format]' . $bracket ),
                        ),
                        's' => array(
                                'type'  => 'input',
                                'label' => __( 'Search', FRISKAMAX_THEME_SLUG ),
                                'desc'  => __( 'Enter keywords.', FRISKAMAX_THEME_SLUG ),
                                'attr'  => array(
                                        'name'  => $this->get_field_name( 'fsm_query' ) . '[s]' . $bracket,
                                        'value' => esc_attr( $q->s )
                                ),
                        ),
                        'post__in' => array(
                                'type'  => 'input',
                                'label' => __( 'Post IDs', FRISKAMAX_THEME_SLUG ),
                                'desc'  => __( 'Separate post IDs with commas.', FRISKAMAX_THEME_SLUG ),
                                'attr'  => array(
                                        'name'  => $this->get_field_name( 'fsm_query' ) . '[post__in]' . $bracket,
                                        'value' => esc_attr( $q->post__in )
                                ),
                        ),
                        'posts_per_page' => array(
                                'type'  => 'input',
                                'label' => __( 'Number of posts', FRISKAMAX_THEME_SLUG ),
                                'desc'  => __( 'Enter -1 to show all posts.', FRISKAMAX_THEME_SLUG ),
                                'attr'  => array(
                                        'type'  => 'number',
                                        'min'   => -1,
                                        'name'  => $this->get_field_name( 'fsm_query' ) . '[posts_per_page]' . $bracket,
                                        'value' => esc_attr( $q->posts_per_page )
                                ),
                        ),
                        'ignore_sticky_posts' => array(
                                'type'          => 'select',
                                'label'         => __( 'Ignore sticky posts', FRISKAMAX_THEME_SLUG ),
                                'options'       => fsmfo_get_switch(),
                                'selected'      => $q->ignore_sticky_posts,
                                'attr'          => array( 'name'  => $this->get_field_name( 'fsm_query' ) . '[ignore_sticky_posts]' . $bracket ),
                        ),
                        'order' => array(
                                'type'          => 'select',
                                'label'         => __( 'Order', FRISKAMAX_THEME_SLUG ),
                                'desc'          => sprintf( 
                                                        __( 
                                                                '%1$s - ascending order from lowest to highest values (1, 2, 3; a, b, c).<br>
                                                                %2$s - descending order from highest to lowest values (3, 2, 1; c, b, a).', FRISKAMAX_THEME_SLUG 
                                                        ), 
                                                        '<strong>ASC</strong>',
                                                        '<strong>DESC</strong>' 
                                                ), 
                                'options'       => array( 'ASC' => 'ASC', 'DESC' => 'DESC' ),
                                'selected'      => $q->order,
                                'attr'          => array( 'name'  => $this->get_field_name( 'fsm_query' ) . '[order]' . $bracket ),
                        ),
                        'orderby' => array(
                                'type'          => 'select',
                                'label'         => __( 'Order by', FRISKAMAX_THEME_SLUG ),
                                'options'       => array(
                                                        'none'                  => __( 'No order', FRISKAMAX_THEME_SLUG ),
                                                        'ID'                    => __( 'Post ID', FRISKAMAX_THEME_SLUG ),
                                                        'author'                => __( 'Author', FRISKAMAX_THEME_SLUG ),
                                                        'title'                 => __( 'Title', FRISKAMAX_THEME_SLUG ),
                                                        'name'                  => __( 'Post slug', FRISKAMAX_THEME_SLUG ),
                                                        'date'                  => __( 'Date', FRISKAMAX_THEME_SLUG ),
                                                        'modified'              => __( 'Last modified date', FRISKAMAX_THEME_SLUG ),
                                                        'rand'                  => __( 'Random order', FRISKAMAX_THEME_SLUG ),
                                                        'comment_count'         => __( 'Number of comments', FRISKAMAX_THEME_SLUG ),
                                                        'post__in'              => __( 'Preserve post ID order given in the post IDs option', FRISKAMAX_THEME_SLUG ),
                                                ),
                                'selected'      => $q->orderby,
                                'attr'          => array( 'name'  => $this->get_field_name( 'fsm_query' ) . '[orderby]' . $bracket ),
                        ),
                        'date_after' => array(
                                'type'          => 'select',
                                'label'         => __( 'Date after', FRISKAMAX_THEME_SLUG ),
                                'desc'          => $q->date_after ? date( 'Y-m-d G:i:s T', strtotime( $q->date_after ) ) : '',
                                'option_none'   => __( '&mdash; Select &mdash;', FRISKAMAX_THEME_SLUG ),
                                'options'       => fsmfo_get_query_date(),
                                'selected'      => $q->date_after,
                                'attr'          => array( 'name'  => $this->get_field_name( 'fsm_query' ) . '[date_after]' . $bracket ),
                        ),
                        'date_before' => array(
                                'type'          => 'select',
                                'label'         => __( 'Date before', FRISKAMAX_THEME_SLUG ),
                                'desc'          => $q->date_before ? date( 'Y-m-d G:i:s T', strtotime( $q->date_before ) ) : '',
                                'option_none'   => __( '&mdash; Select &mdash;', FRISKAMAX_THEME_SLUG ),
                                'options'       => fsmfo_get_query_date(),
                                'selected'      => $q->date_before,
                                'attr'          => array( 'name'  => $this->get_field_name( 'fsm_query' ) . '[date_before]' . $bracket ),
                        )        
                ), $i, $index, $this );
                                
                ob_start();
                
                ?>

                <div class="panel panel-success" data-fsmh-slide>
                        <div class="panel-heading" data-toggle="fsmh-slide"><?php _e( 'Query', FRISKAMAX_THEME_SLUG ); ?></div>
                        <div class="panel-body" style="padding-top: 0; padding-bottom: 0; display: none">
                                
                                <?php 
                                        /**
                                         * Filter the query fields HTML.
                                         * 
                                         * @since 1.0.2
                                         * 
                                         * @param array $fields_html    Field configurations.
                                         * @param object $i             Previously saved values from database.
                                         * @param int|null $index       Settings index if the settings is an array.
                                         */
                                        echo apply_filters( 'FriskaMax_Widget__fsm_get_query_fields_html', fsmf_get_fields( $fields_array ), $i, $index, $this ); 
                                ?>
                                
                        </div>
                </div>      
                
                <?php
                
                /**
                 * Filter the full HTML query fields including the panel wrapper.
                 * 
                 * @since 1.0.1
                 * 
                 * @param string $query_fields  Full HTML query fields.
                 * @param object $i             Previously saved values from database.
                 * @param int|null $index       Settings index if the settings is an array.
                 */
                return apply_filters( 'FriskaMax_Widget__fsm_get_query_fields', ob_get_clean(), $i, $index, $this );
        }
        
        /**
         * Display query fields.
         * 
         * @since 1.0.1
         * 
         * @param type $i       Previously saved values from database.
         * @param int $index    Settings index if the settings is an array.
         */
        public function fsm_query_fields( $i, $index = null ) {
                echo $this->fsm_get_query_fields( $i, $index );
        }
        
        /**
         * Parse query settings.
         * 
         * @since 1.0.1
         * 
         * @param object $i     Widget settings.
         * @param int $index    Settings index if the settings is an array.
         * 
         * @return object       Query arguments.
         */
        public function fsm_parse_query_settings( $i, $index = null ) {                
                $query  = isset( $i->fsm_query ) ? $i->fsm_query : array();
                $return = array();
                
                if ( $index !== null ) {                        
                        foreach ( $this->fsm_query_d as $key => $value ) {
                                if ( isset( $query[$key][$index] ) ) {
                                        $return[$key] = $query[$key][$index];
                                }       
                        }
                } else {
                        $return = $query;
                }
                                
                return apply_filters( 'FriskaMax_Widget__fsm_parse_query_settings', (object) wp_parse_args( $return, $this->fsm_query_d ), $i, $index, $this );
        } 
        
        /**
         * Sanitize query options.
         * 
         * @since 1.0.1
         * 
         * @param array $instance       Widget settings just sent to be saved.
         * @return array                Sanitized widget settings.
         */
        public function fsm_sanitize_query( $instance ) {     
                // Make sure fsm_query key exists.
                $s              = wp_parse_args( $instance, array( 'fsm_query' => array() ) );
                // One of the query keys. Only to check whether there is multiple settings or just a single settings.
                $query_key      = key( $this->fsm_query_d );
                                
                
                if ( isset( $s[ 'fsm_query' ][ $query_key ] ) && is_array( $s[ 'fsm_query' ][ $query_key ] ) ) { 
                        // Array settings.                        
                        foreach ( array_keys( $s[ 'fsm_query' ][ $query_key ] ) as $_i ) {
                                foreach ( $this->fsm_query_d as $_key => $_value ) {
                                        // Set the default value if it is not set yet.
                                        $s[ 'fsm_query' ][ $_key ][ $_i ]       = isset( $s[ 'fsm_query' ][ $_key ][ $_i ] ) 
                                                                                ? $s[ 'fsm_query' ][ $_key ][ $_i ] 
                                                                                : $_value;
                                        $s[ 'fsm_query' ][ $_key ][ $_i ]       = $this->fsm_sanitize_query_value( $_key, $s[ 'fsm_query' ][ $_key ][ $_i ] );
                                } 
                        }
                } else { 
                        // Single settings.
                        foreach ( $this->fsm_query_d as $_key => $_value ) {
                                // Set the default value if it is not set yet.
                                $s[ 'fsm_query' ][ $_key ]      = isset( $s[ 'fsm_query' ][ $_key ] ) 
                                                                ? $s[ 'fsm_query' ][ $_key ] 
                                                                : $_value;
                                $s[ 'fsm_query' ][ $_key ]      = $this->fsm_sanitize_query_value( $_key, $s[ 'fsm_query' ][ $_key ] );
                        } 
                }
                
                /**
                 * Filter the query options.
                 * 
                 * @since 1.0.1
                 * 
                 * @param array $s              Widget settings with query options that has been sanitized.
                 * @param array $instance       Original widget settings.
                 * @param object $this          Instance of the current widget.
                 */
                return apply_filters( 'FriskaMax_Widget__fsm_sanitize_query', $s, $instance, $this );
        }
        
        /**
         * Sanitize an query option value.
         * 
         * @since 1.0.1
         * 
         * @param array $query_values   Query options.
         * @return array                Sanitized query options.
         */
        public function fsm_sanitize_query_value( $key, $value ) {                  
                switch ( $key ) {
                        case 'author':
                        case 'cat':
                        case 'posts_per_page':
                        case 'paged':
                        case 'ignore_sticky_posts':
                                // Let the value empty if the user does not enter anything.
                                $return = $value == '' ? '' : (int) $value;  
                                break;
                        default:
                                $return = sanitize_text_field( $value );
                                break;
                }
                
                return apply_filters( 'FriskaMax_Widget__fsm_sanitize_query_value', $return, $key, $value, $this );
        }
        
        /**
         * Returns arguments for WP_Query.
         * 
         * @since 1.0.1
         * 
         * @param object $i     Widget settings.
         * @param int $index    Settings index if the settings is an array.
         * 
         * @return array
         */
        public function fsm_get_wp_query_args( $i, $index = null ) {          
                
                $query  = isset( $i->fsm_query ) 
                        ? $i->fsm_query 
                        : array();
                $return = array( 
                                'date_query'    => array(),
                                'tax_query'     => array(),
                        );
                
                if ( $index !== null ) { 
                        
                        // Array settings.
                        
                        foreach ( $this->fsm_query_d as $_key => $_value ) {
                                
                                $return[ $_key ]        = isset( $query[ $_key ][ $index ] ) 
                                                        ? $query[ $_key ][ $index ] 
                                                        : $_value;
                                
                        }
                        
                } else { 

                        // Single settings.
                        
                        $return = wp_parse_args( $query, $this->fsm_query_d );
                        
                }
                
                // date_query
                // -----------------------------------------------------------------------------
                
                if ( $return[ 'date_after' ] ) {
                        
                        $return[ 'date_query' ][ 'after' ] = $return[ 'date_after' ];
                        unset( $return[ 'date_after' ] );
                        
                } 
                
                if ( $return[ 'date_before' ] ) {
                        
                        $return[ 'date_query' ][ 'before' ] = $return[ 'date_before' ];
                        unset( $return[ 'date_before' ] );
                        
                }
                
                // post_format
                // -----------------------------------------------------------------------------
                
                if ( $return[ 'post_format' ] ) {        
                        
                        $return[ 'tax_query' ][] = array(
                                'taxonomy' => 'post_format',
                                'field'    => 'slug',
                                'terms'    => array( 'post-format-' . $return[ 'post_format' ] ),
                        );
                        unset( $return['post_format'] );
                        
                }
                
                // post__in 
                // -----------------------------------------------------------------------------
                
                $return[ 'post__in' ]   = $return[ 'post__in' ] 
                                        ? explode( ',', $return[ 'post__in' ] ) 
                                        : '';
                
                $return = array_filter( $return, 'friskamax_is_not_empty_string' );
                                
                /**
                 * Filter the WP_Query arguments.
                 * 
                 * @since 1.0.1
                 * 
                 * @param array $return         WP_Query arguments.
                 * @param object $i             Widget settings.
                 * @param int $index            Index of the widget settings if the settings is an array.
                 * @param object $this          Instance of the current widget.
                 */
                return apply_filters( 'FriskaMax_Widget__fsm_get_wp_query_args', $return, $i, $index, $this );
                
        }
        
}