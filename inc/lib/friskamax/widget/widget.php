<?php
/**
 * Widgets
 * 
 * @package friskamax-library\widget
 * @since 1.0.0
 */

/** Base Widget. */
require_once dirname( __FILE__ ) . '/_base.php';