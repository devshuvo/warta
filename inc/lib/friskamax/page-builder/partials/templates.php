<script type="text/html" id="fsmpb-template-element">
        <div class="element-wrapper panel">
                <div class="panel-heading">
                        <h4 class="panel-title">{{title}}</h4>
                        <div class="control">
                                <a href="#" class="panel-title edit" title="<?php esc_attr_e( 'Edit', FRISKAMAX_THEME_SLUG ) ?>"><i class="fa fa-pencil"></i></a>
                                <a href="#" class="panel-title duplicate" title="<?php esc_attr_e( 'Duplicate', FRISKAMAX_THEME_SLUG ) ?>"><i class="fa fa-copy"></i></a>
                                <a href="#" class="panel-title delete" title="<?php esc_attr_e( 'Delete', FRISKAMAX_THEME_SLUG ) ?>"><i class="fa fa-times"></i></a>
                        </div>
                </div>
                <div class="panel-body"><small>{{subtitle}}</small></div>
        </div>
</script>

<script type="text/html" id="fsmpb-template-col">
        <div class="column-wrapper">
                <div class="panel">
                        <div class="panel-heading">
                                <h4 class="panel-title"><?php _e( 'Column', FRISKAMAX_THEME_SLUG ) ?></h4>
                                <div class="control">
                                        <a href="#" class="panel-title decrease" title="<?php esc_attr_e( 'Decrease width', FRISKAMAX_THEME_SLUG ) ?>"><i class="fa fa-chevron-<?php echo is_rtl() ? 'right' : 'left'; ?>"></i></a>
                                        <a href="#" class="panel-title increase" title="<?php esc_attr_e( 'Increase width', FRISKAMAX_THEME_SLUG ) ?>"><i class="fa fa-chevron-<?php echo is_rtl() ? 'left' : 'right'; ?>"></i></a>
                                        <a href="#" class="panel-title edit" title="<?php esc_attr_e( 'Edit', FRISKAMAX_THEME_SLUG ) ?>"><i class="fa fa-pencil"></i></a>
                                        <a href="#" class="panel-title duplicate" title="<?php esc_attr_e( 'Duplicate', FRISKAMAX_THEME_SLUG ) ?>"><i class="fa fa-copy"></i></a>
                                        <a href="#" class="panel-title delete" title="<?php esc_attr_e( 'Delete', FRISKAMAX_THEME_SLUG ) ?>"><i class="fa fa-times"></i></a>
                                </div>
                        </div>
                        <div class="panel-body elements"></div>
                </div>
        </div>
</script>

<script type="text/html" id="fsmpb-template-row">
        <div class="row-wrapper panel">
                <div class="panel-heading">
                        <h4 class="panel-title"><?php _e( 'Row', FRISKAMAX_THEME_SLUG ) ?></h4>
                        <div class="control">
                                <a href="#" class="panel-title duplicate" title="<?php esc_attr_e( 'Duplicate', FRISKAMAX_THEME_SLUG ) ?>"><i class="fa fa-copy"></i></a>
                                <a href="#" class="panel-title delete" title="<?php esc_attr_e( 'Delete', FRISKAMAX_THEME_SLUG ) ?>"><i class="fa fa-times"></i></a>
                        </div>
                </div>
                <div class="panel-body">
                        <div class="row"></div>
                </div>
        </div>                        
</script>