<div class="modal" id="fsmpb-modal-loading" data-backdrop="false">
        <div class="modal-dialog modal-sm">
                <div class="modal-content">
                        <div class="modal-body">
                                <p><span class="spinner is-active"></span> <?php _e( 'Please wait&hellip;', FRISKAMAX_THEME_SLUG ) ?></p>
                        </div>
                </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
</div><!-- /.modal -->