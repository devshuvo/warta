<!--Modal: Insert Element
========================= -->
<div id="fsmpb-modal-insert-element" class="modal">
        <div class="modal-dialog  modal-lg">
                <div class="modal-content">
                        <div class="modal-body">   
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs"><?php $this->tab_menus() ?></ul>
                                
                                <!-- Tab panes -->
                                <div class="tab-content"><?php $this->tab_contents() ?></div>
                        </div>
                        <div class="modal-footer">
                                <button type="button" class="button" data-dismiss="modal"><?php _e('Close', FRISKAMAX_THEME_SLUG) ?></button>
                        </div>
                </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
</div><!-- /.modal -->