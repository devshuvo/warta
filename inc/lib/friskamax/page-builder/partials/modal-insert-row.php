<!--Modal: Insert Row
===================== -->
<div id="fsmpb-modal-insert-row" class="modal" tabindex="-1" role="dialog" aria-labelledby="fsmpb-modal-insert-row-label" aria-hidden="true">
        <div class="modal-dialog modal-sm">
                <div class="modal-content">
                        <div class="modal-header">
                                <h4 class="modal-title" id="fsmpb-modal-insert-row-label" ><?php _e( 'Insert Row', FRISKAMAX_THEME_SLUG ) ?></h4>
                        </div>
                        <div class="modal-body">
                                <?php _e( 'Choose Layout:', FRISKAMAX_THEME_SLUG ) ?>
                                <div class="columns clearfix">
                                        <div class="col-12"             title="12"></div>
                                        <div class="col-6-6"            title="6-6"></div>
                                        <div class="col-4-4-4"          title="4-4-4"></div>
                                        <div class="col-3-3-3-3"        title="3-3-3-3"></div>
                                        <div class="col-2-2-2-2-2-2"    title="2-2-2-2-2-2"></div>
                                        <div class="col-4-8"            title="4-8"></div>
                                        <div class="col-8-4"            title="8-4"></div>
                                        <div class="col-3-6-3"          title="3-6-3"></div>
                                        <div class="col-3-3-6"          title="3-3-6"></div>
                                        <div class="col-6-3-3"          title="6-3-3"></div>
                                </div>

                                <p>
                                        <label><?php _e( 'Custom Layout:', FRISKAMAX_THEME_SLUG ) ?><br> 
                                                <input type="text" name="fsmpb[row]" value="12">
                                        </label><br>
                                        <small><?php _e( 'The sum of the column widths should be 12. Example: 3-6-3', FRISKAMAX_THEME_SLUG ) ?></small>
                                </p>
                        </div>
                        <div class="modal-footer">
                                <button type="button" class="button" data-dismiss="modal"><?php _e( 'Close', FRISKAMAX_THEME_SLUG ) ?></button>
                                <button type="button" class="button button-primary" data-insert><?php _e( 'Insert', FRISKAMAX_THEME_SLUG ) ?></button>
                        </div>
                </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
</div><!-- /.modal -->