<?php
/**
 * Page builder meta box content.
 *
 * @package friskamax-library\page-builder\meta-box\partials
 * @since 1.0.0
 */

$_dir = dirname( __FILE__ );

?>

<div id="fsmpb-container" class="fsmh-container">   
        
        <p>
                <button type="button" class="button" data-toggle="fsmh-modal" data-target="#fsmpb-modal-insert-row"><?php _e( 'Insert Row', FRISKAMAX_THEME_SLUG ) ?></button>
                <button type="button" class="button" data-toggle="fsmh-modal" data-target="#fsmpb-modal-insert-element"><?php _e( 'Insert Element', FRISKAMAX_THEME_SLUG ) ?></button>
                <button type="button" class="button" data-toggle="full-screen"><?php _e( 'Full Screen', FRISKAMAX_THEME_SLUG ) ?></button>
        </p>
        
        <!--Main content
        ================ -->
        <div id="fsmpb-main-content"></div><!-- #fsmpb-main-content -->
        
        <?php   
                require_once $_dir . '/modal-insert-row.php';
                require_once $_dir . '/modal-insert-element.php'; 
                require_once $_dir . '/modal-column.php';
                require_once $_dir . '/modal-loading.php'; 
                require_once $_dir . '/templates.php';
        ?>
        
</div><!-- #fsmpb-container -->

