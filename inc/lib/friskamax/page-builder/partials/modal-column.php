<!-- Modal -->
<div class="modal" id="fsmpb-modal-column" tabindex="-1" role="dialog" aria-labelledby="fsmpb-modal-column-label" aria-hidden="true">
        <div class="modal-dialog modal-sm">
                <div class="modal-content">
                        <div class="modal-header">
                                <h4 class="modal-title" id="fsmpb-modal-column-label"><?php _e( 'Edit Column Width', FRISKAMAX_THEME_SLUG ) ?></h4>
                        </div>
                        <div class="modal-body">
                                <p>
                                        <?php _e( 'Enter the width of the column for each screen size. Min 1 and max 12. 12 equals full width, 6 equals 1/2 width, 4 equals 1/3 width and so on.', FRISKAMAX_THEME_SLUG ); 
                                        ?>
                                </p>
                                <p>
                                        <label>
                                                <strong><?php _e( 'Extra small devices', FRISKAMAX_THEME_SLUG ) ?></strong><br>
                                                <input name="xs" type="number" min="1" max="12" class="widefat column-field">
                                                <small><?php _e( 'Phones', FRISKAMAX_THEME_SLUG ); echo ' (&lt;' . $this->screen_sizes['sm_min'] . 'px)'; ?> </small>
                                        </label>
                                </p>
                                <p>
                                        <label>
                                                <strong><?php _e( 'Small devices', FRISKAMAX_THEME_SLUG ) ?></strong><br>
                                                <input name="sm" type="number" min="1" max="12" class="widefat column-field">
                                                <small><?php _e( 'Tablets', FRISKAMAX_THEME_SLUG ); echo ' (&ge;' . $this->screen_sizes['sm_min'] . 'px)'; ?> </small>
                                        </label>
                                </p>
                                <p>
                                        <label>
                                                <strong><?php _e( 'Medium devices', FRISKAMAX_THEME_SLUG ) ?></strong><br>
                                                <input name="md" type="number" min="1" max="12" class="widefat column-field">
                                                <small><?php _e( 'Desktops', FRISKAMAX_THEME_SLUG ); echo ' (&ge;' . $this->screen_sizes['md_min'] . 'px)'; ?> </small>
                                        </label>
                                </p>
                                <p>
                                        <label>
                                                <strong><?php _e( 'Large devices', FRISKAMAX_THEME_SLUG ) ?></strong><br>
                                                <input name="lg" type="number" min="1" max="12" class="widefat column-field">
                                                <small><?php _e( 'Desktops', FRISKAMAX_THEME_SLUG ); echo ' (&ge;' . $this->screen_sizes['lg_min'] . 'px)'; ?> </small>
                                        </label>
                                </p>
                        </div>
                        <div class="modal-footer">
                                <button type="button" class="button" data-dismiss="modal"><?php _e( 'Close', FRISKAMAX_THEME_SLUG ) ?></button>
                                <button type="button" class="button button-primary" data-save="true"><?php _e( 'Save changes', FRISKAMAX_THEME_SLUG ) ?></button>
                        </div>
                </div>
        </div>
</div>