<?php
/**
 * Widget element.
 * 
 * @since 1.0.0
 * @package friskamax-library\page-builder\element
 */

/**
 * Element type widget.
 * 
 * @since 1.0.0
 * @access private
 */
class Fsmpb_Element_Widget {
        /**
         * Widget option form.
         * 
         * @since 1.0.0
         * @access private
         */
        public function form() {        
                $args           = $_POST['args'];
                $formData       = isset( $_POST['formData'] ) ? stripslashes( $_POST['formData'] ) : '';
                $formData       = ( array ) json_decode( $formData, true );
                $instance       = $GLOBALS['wp_widget_factory']->widgets[ $args['php_class'] ];
                
                if ($args['php_class'] == 'WP_Widget_Text') {
                    // Force using legacy mode.
                    // See WP_Widget_Text::is_legacy_instance()
                    $formData['visual'] = false;
                }
                
                $instance->_set(-1);
                
                /**
                 * Filter the widget settings before displaying the control form.
                 *
                 * Returning false effectively short-circuits display of the control form.
                 *
                 * @since 2.8.0
                 *
                 * @param array     $formData   The current widget instance's settings.
                 * @param WP_Widget $instance   The current widget instance.
                 */
                $formData = apply_filters( 'widget_form_callback', $formData, $instance );
                
                ?>

                <div class="fsmh-container">
                        <form id="fsmpb-modal-element-widget-<?php echo $args['php_class'] ?>" class="modal">
                                <div class="modal-dialog">
                                        <div class="modal-content">
                                                <div class="modal-header">
                                                        <h4 class="modal-title"><?php echo $instance->name ?></h4>
                                                </div>
                                                <div class="modal-body">
                                                        <?php 
                                                                $return = null;
                                                                if ( false !== $formData ) {
                                                                        $return = $instance->form( $formData );

                                                                        /**
                                                                         * Fires at the end of the widget control form.
                                                                         *
                                                                         * Use this hook to add extra fields to the widget form. The hook
                                                                         * is only fired if the value passed to the 'widget_form_callback'
                                                                         * hook is not false.
                                                                         *
                                                                         * Note: If the widget has no form, the text echoed from the default
                                                                         * form method can be hidden using CSS.
                                                                         *
                                                                         * @since 2.8.0
                                                                         *
                                                                         * @param WP_Widget $instance   The widget instance, passed by reference.
                                                                         * @param null      $return     Return null if new fields are added.
                                                                         * @param array     $formData   An array of the widget's settings.
                                                                         */
                                                                        do_action_ref_array( 'in_widget_form', array( &$instance, &$return, $formData ) );
                                                                }                                                                 
                                                        ?>
                                                </div>
                                                <div class="modal-footer">
                                                        <button type="button" class="button" data-dismiss="modal"><?php _e('Close', FRISKAMAX_THEME_SLUG) ?></button>
                                                        <button type="submit" class="button button-primary"><?php _e('Insert', FRISKAMAX_THEME_SLUG) ?></button>
                                                </div>
                                        </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                        </form><!-- /.modal -->
                </div>

                <?php   
                
                die();
        }
                
        /**
         * Sanitize widget options.
         * 
         * @since 1.0.0
         * @access private
         */
        public function sanitize() {            
                if ( ! isset( $_POST['formData'] ) ) { 
                        return;
                }
                            
                $instance       = $GLOBALS['wp_widget_factory']->widgets[ $_POST['args']['php_class'] ];
                $old_formData   = isset( $_POST['oldFormData'] ) ? $_POST['oldFormData'] : array();
                
                // Many plugins does not use WP_Widget::get_field_name( $field_name ) to create the form,
                // and they use global variable $_POST instead of using the parameter that passed via 
                // 'widget_update_callback' filter or WP_Widget::update(). So we need to store the 
                // form data to $_POST.
                //
                // Currently known: WPML Widgets, Jetpack Widget Visibility
                parse_str( $_POST['formData'], $_POST );        
                
                if ( isset( $_POST['widget-' . $instance->id_base] ) && is_array( $_POST['widget-' . $instance->id_base] ) ) {
                        $settings = $_POST[ 'widget-' . $instance->id_base ];
                } else {
                        return;
                }
                
                foreach ( $settings as $number => $new_formData ) {                        
                        $instance->_set( $number );
                        
                        $new_formData           = stripslashes_deep( $new_formData );
                        $sanitized_formData     = $instance->update( $new_formData, $old_formData );

                        /** This filter is documented in wp-includes/widgets.php */
                        $sanitized_formData     = apply_filters( 'widget_update_callback', $sanitized_formData, $new_formData, $old_formData, $instance );
                        
                        // Short-circuit the widget's ability to update settings.
                        if ( false === $sanitized_formData ) { 
                                $sanitized_formData = $old_formData;
                        }
                        
                        break; // run only once
                }

                echo json_encode( $sanitized_formData );

                die();
        }            
}