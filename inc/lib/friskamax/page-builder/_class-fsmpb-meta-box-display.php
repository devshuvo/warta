<?php
/**
 * Page builder form.
 * 
 * @package friskamax-library\page-builder\meta-box\display
 * @since 1.0.0
 */

if ( ! class_exists( 'Fsmpb_Meta_Box_Display' ) ) :

/**
 * Page builder meta box display class.
 * 
 * The class that responsible for displaying the meta box.
 * Requires Fsmpb_Meta_Box_Elements.
 * 
 * @since 1.0.0
 */
class Fsmpb_Meta_Box_Display {
        
        /**
         * Instance of Fsmpb_Meta_Box class.
         * 
         * @since 1.0.0
         * @access private
         * @var object 
         */
        protected $Parent;
        
        /**
         * Media queries breakpoints. 
         * Currently only used as description on column options.
         * 
         * @since 1.0.0
         * @access private
         * @var array
         */
        public $screen_sizes = array(
                'sm_min'        => 768,
                'md_min'        => 992,
                'lg_min'        => 1200
        );
        
        /**
         * Constructor.
         * 
         * @since 1.0.0
         * @access private
         * 
         * @param type $Parent Instance of Fsmpb_Meta_Box class.
         */
        public function __construct( $Parent ) {
                $this->Parent           = $Parent;
                $this->screen_sizes     = apply_filters( 'Fsmpb_Meta_Box_Display__screen_sizes', $this->screen_sizes );
                
                add_action( 'media_buttons', array( $this, 'button' ), 11 );
        }
                
        /**
         * Display page builder button.
         * 
         * @since 1.0.0
         * @access private
         * 
         * @param string $editor_id Unique editor identifier, e.g. 'content'.
         */
        public function button( $editor_id ) {                        
                if ( $editor_id != 'content' || ! in_array( get_current_screen()->post_type, $this->Parent->r->post_types ) ) { 
                        return;
                }
                
                ?>

                        <button type="button" id="fsmpb-switch" class="button">
                                <span class="dashicons dashicons-grid-view"></span> 
                                <?php _e( 'Page Builder', FRISKAMAX_THEME_SLUG ) ?>
                        </button>
                        
                <?php
        }
        
        /**
         * Element type widget.
         * 
         * @global object $wp_widget_factory
         * @param string $widget_class
         */
        protected function element__widget( $widget_class ) {                        
                $info           = $this->Parent->Elements->info[ $widget_class ];
                $widget_name    = preg_replace( '/^\[' . FRISKAMAX_THEME_SLUG . '\] /i', '', $info['name'] );
                
                ?>

                <div    class           = "element"
                        data-type       = "widget"
                        data-args       = '<?php echo json_encode( array( "php_class" => $widget_class ) ) ?>'
                >
                        <h4><strong><?php echo esc_html( $widget_name ) ?></strong></h4>
                        <?php if ( $info['description'] ) : ?>
                                <small><?php echo esc_html( $info['description'] ) ?></small>
                        <?php endif ?>
                </div><!--.element-->
                
                <?php
        }
                
        /**
         * Element type custom.
         * 
         * @param $element Element settings.
         */
        protected function element__custom( $element ) {
                if ( ! class_exists( $element[ 'php_class' ] ) ) {
                        return;
                }
                
                $info = $this->Parent->Elements->info[ $element['php_class'] ];
                
                ?>
                
                <div    class           = "element"
                        data-type       = "custom"
                        data-args       = '<?php echo json_encode( array( "php_class" => $element['php_class'] ) ) ?>'
                >
                        <h4><strong><?php echo strip_tags( $info['name'] ) ?></strong></h4>
                        <?php if ( $info['description'] ) : ?>
                                <small><?php echo wp_kses_post( $info['description'] ) ?></small>
                        <?php endif ?>
                </div>
                
                <?php
        }
                                
        /**
         * Tab contents
         */
        protected function tab_contents() {
                $i = 0; 

                foreach( $this->Parent->Elements->tabs as $id => $args) : ?>
                
                        <div class="tab-pane <?php echo $i++ === 0 ? 'active' : '' ?>" id="<?php echo $id ?>">
                                <div class="row">
                                        <?php foreach( $args['items'] as $element ) : ?> 
                                        
                                                <div class="col-sm-3">
                                                        <?php switch ( $element['type'] ) {
                                                                case 'widget': 
                                                                        $this->element__widget( $element['php_class'] );                                                                        
                                                                        break;
                                                                case 'custom': 
                                                                        $this->element__custom( $element );                                                                        
                                                                        break;
                                                        } ?>                                
                                                </div>
                                        
                                        <?php endforeach; ?>
                                </div><!--.row-->
                        
                                <?php if( isset( $args['note'] ) ) : ?>
                                        <p><small><?php echo $args['note'] ?></small></p>
                                <?php endif ?>
                        </div><!--.tab-pane-->
                        
                <?php endforeach;                         
        }
                
        /**
         * Tab menus
         */
        protected function tab_menus() {                        
                $i = 0;
                
                foreach( $this->Parent->Elements->tabs as $id => $args ) : ?>
                        <li <?php echo $i++ === 0 ? 'class="active"' : '' ?>>
                                <a href="#<?php echo $id ?>" data-toggle="fsmh-tab"><?php echo strip_tags( $args['name'] ) ?></a>
                        </li>
                <?php endforeach; 
        }

        /**
         * Render Meta Box content.
         *
         * @param WP_Post $post The post object.
         */
        public function display( $post ) {
                $fsmpb_main = get_post_meta( $post->ID, 'fsmpb_main', true );   
                
                require dirname( __FILE__ ) . '/partials/main.php';
                
                ?>
                        
                <input type="hidden" name="fsmpb_main" value="<?php echo esc_attr( $fsmpb_main ) ?>">
                <?php wp_nonce_field( 'fsmpb_meta_box', 'fsmpb_meta_box_nonce' ); ?>
                
                <?php
        }    
        
}

endif; // Fsmpb_Meta_Box_Display