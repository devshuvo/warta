<?php
/**
 * FriskaMax Page Builder - Widget Area element.
 * 
 * @since 1.0.0
 * @package friskamax-library\page-builder\elements
 */
        
/**
 * FriskaMax Page Builder - Widget Area element class.
 * 
 * @since 1.0.0
 */
class Fsmpb_Element_Widget_Area extends Fsmpb_Element_Custom {        
        /**
         * An array of arguments.
         * 
         * @since 1.0.2
         * @access private
         * 
         * @var type array {
         *      @type string $before                    Content to prepend to the widget area.
         *      @type string $after                     Content to append to the widget area.
         *      @type array $register_sidebar_args      Arguments for register_sidebar() function.
         * }
         */
        private $args = array(
                'before'                => '',
                'after'                 => '',
                'register_sidebar_args' => array(
                        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
                        'after_widget'  => '</aside>',
                        'before_title'  => '<header class="widget-header"><h1 class="widget-title">',
                        'after_title'   => '</h1></header>',
                )
        );

        /**
         * Initialize.
         * 
         * @since 1.0.0
         * @access private
         * 
         * @param type $Parent  Instance of Fsmpb_Meta_Box class.
         */
        public function __construct( $Parent ) {     
                parent::__construct( $Parent );
                
                $this->args             = friskamax_parse_args( $Parent->r->element_widget_area, $this->args, 'Fsmpb_Element_Widget_Area__args', false );
                $this->name             = __( 'Widget Area', FRISKAMAX_THEME_SLUG );
                $this->description      = __( 'An area that you can add widgets in it from <strong>Appearance > Widgets</strong>', FRISKAMAX_THEME_SLUG );

                // Register widget areas
                add_action( 'widgets_init', array( $this, 'register_widget_areas' ) );
        }

        /**
         * Register widget areas
         * 
         * @since 1.0.0
         * @access private
         */
        public function register_widget_areas() {
                foreach ( get_option( 'fsmpb_widget_areas', array() ) as $args ) {
                        register_sidebar( wp_parse_args( $args, $this->args[ 'register_sidebar_args' ] ) ); 
                }
        }

        /**
         * Widget area form
         * 
         * @since 1.0.0
         * @access private
         * 
         * @param array $settings       Element settings.
         */
        public function form( $settings ) {
                $old_widget_area        = isset( $settings[ 'widget_area' ] )
                                        ? $settings[ 'widget_area' ]
                                        : '';
                $fsmpb_widget_areas     = get_option( 'fsmpb_widget_areas', array() );
                $requires_attr          = array();
                
                foreach ( array_keys( $fsmpb_widget_areas ) as $value ) {
                        $requires_attr[] = array(
                                'field'         => '[name=widget_area]',
                                'compare'       => 'equal',
                                'value'         => $value
                        );
                }
                
                ?>
                
                <p>
                        <label><?php echo strip_tags( $this->name ) ?>
                                <select name="widget_area" class="widefat">
                                        <?php foreach ( $GLOBALS['wp_registered_sidebars'] as $widget_area ) : ?>
                                                <option value="<?php echo $widget_area['id'] ?>" <?php selected( $old_widget_area, $widget_area['id'] ) ?>>
                                                        <?php echo $widget_area['name'] ?>
                                                </option>
                                        <?php endforeach ?>                  

                                        <option value="add_new"><?php _e( '&mdash; Add new &mdash;', FRISKAMAX_THEME_SLUG ) ?></option>
                                </select>
                        </label>
                </p>

                <p data-requires='<?php echo json_encode( $requires_attr ); ?>'>
                        <button data-delete type="button" class="button"><?php _e('Delete', FRISKAMAX_THEME_SLUG) ?></button>
                </p>

                <p data-requires='[{"field":"[name=widget_area]", "compare":"equal", "value":"add_new"}]'>
                        <label><?php _e('Name', 'warta') ?>
                                <input type="text" name="new_name" class="widefat">
                        </label>
                </p>

                <p data-requires='[{"field":"[name=widget_area]", "compare":"equal", "value":"add_new"}]'>
                        <label><?php _e('Description', 'warta') ?>
                                <input type="text" name="new_description" class="widefat">
                        </label>
                </p>
                                                
                <script>
                        ( function( $ ) { 'use strict';                                
                                var     $modal          = $( '#fsmpb-modal-custom-element-<?php echo sanitize_title( $this->name ); ?>' ),
                                        $widgetArea     = $modal.find( '[name=widget_area]' );

                                $modal.find( '[data-delete]' ).click( function() {
                                        if( confirm( '<?php esc_attr_e('Are you sure want to delete this widget area?', FRISKAMAX_THEME_SLUG ); ?>' ) ) {
                                                var widgetAreaId = $widgetArea.val();          

                                                $widgetArea.find( 'option[value="' + widgetAreaId + '"]' ).remove();
                                                $widgetArea.trigger( 'change' );
                                                $modal.append( $( '<input>', {
                                                        type    : 'hidden',
                                                        name    : 'delete[' + widgetAreaId + ']'
                                                } ) );
                                        }
                                } );                   

                                // Get title for page builder
                                $modal.on( 'submit.fsmpb', function() {
                                        var title       = $widgetArea.val() !== 'add_new'
                                                        ? $widgetArea.find( 'option[value=' + $widgetArea.val() + ']' ).text()
                                                        : $modal.find( '[name=new_name]' ).val();

                                        if( title ) {
                                                $modal.append( $('<input>', {
                                                        type    : 'hidden',
                                                        name    : 'title',
                                                        value   : title
                                                } ) );
                                        }
                                } );                                
                        } ) ( jQuery );
                </script>
                
                <?php
        }

        /**
         * Sanitize widget area option
         * 
         * @since 1.0.0
         * @access private
         * 
         * @param array $settings       Element settings.
         * @return array                Sanitized element settings.
         */
        public function sanitize( $settings ) {
                $db_widget_areas                = get_option( 'fsmpb_widget_areas', array() );
                $settings[ 'title' ]            = sanitize_text_field( $settings[ 'title' ] );
                $settings[ 'widget_area' ]      = sanitize_text_field( $settings[ 'widget_area' ] );

                if ( $settings[ 'widget_area' ] == 'add_new' && current_user_can( $this->Parent->r->capability ) ) {
                        $settings[ 'widget_area' ]                          = 'fsmpb-' . strtolower( sanitize_title( $settings[ 'new_name' ] ) );
                        $settings[ 'new_name' ]                             = sanitize_text_field( $settings['new_name'] ); 
                        $settings[ 'new_description' ]                      = sanitize_text_field( $settings['new_description'] ); 
                        $db_widget_areas[ $settings[ 'widget_area' ] ]      = array(
                                                                                'id'            => $settings[ 'widget_area' ],
                                                                                'name'          => $settings[ 'new_name' ],
                                                                                'description'   => $settings[ 'new_description' ],
                                                                        ); 
                        update_option( 'fsmpb_widget_areas', $db_widget_areas );
                } 

                if ( isset( $settings[ 'delete' ] ) && current_user_can( $this->Parent->r->capability ) ) {
                        foreach ( array_keys( $settings[ 'delete' ] ) as $_key ) {
                                unset( $db_widget_areas[ $_key ] );
                        }
                        
                        update_option( 'fsmpb_widget_areas', $db_widget_areas );
                }

                return $settings;
        }

        /**
         * Display the widget area.
         * 
         * @since 1.0.0
         * @access private
         * @param object $settings      Element settings.
         */
        public function display( $settings ) {      
                global $wp_registered_sidebars;
                
                if ( ! isset( $wp_registered_sidebars[ $settings->widget_area ] ) ) {
                        return;
                }
                
                // Save the current widget area parameters.
                $old_widget_area_parameters                             = $wp_registered_sidebars[ $settings->widget_area ];                
                $wp_registered_sidebars[ $settings->widget_area ]       = wp_parse_args( $this->args[ 'register_sidebar_args' ], $wp_registered_sidebars[ $settings->widget_area ] );
                
                echo $this->args[ 'before' ];                
                dynamic_sidebar( $settings->widget_area );
                echo $this->args[ 'after' ];
                
                // Restore the widget area parameters. The widget area may be used again.
                $wp_registered_sidebars[ $settings->widget_area ] = $old_widget_area_parameters;
        }
}