<?php
/**
 * FriskaMax Page Builder Meta Box Element.
 * 
 * @since 1.0.0
 * @package friskamax-library\page-builder
 */

/**
 * FriskaMax Page Builder Meta Box Element.
 * 
 * The class that responsible for managing the elements.
 * 
 * @since 1.0.0
 */
class Fsmpb_Meta_Box_Elements {
        
        /**
         * Instance of Fsmpb_Meta_Box class.
         * 
         * @since 1.0.0
         * @access private
         * @var object
         */
        protected $Parent;
        
        /**
         * Stores element instances.
         * 
         * @since 1.0.0
         * @access private
         * @var array
         */
        public $instances;
        
        /**
         * Stores name and description of the elements.
         * 
         * @since 1.0.0
         * @access private
         * @var array
         */
        public $info = array();

        /**
         * Tabs that contains grouped elements.
         * 
         * @since 1.0.0
         * @access private
         * @var array
         */
        public $tabs = array();
        
        /**
         * Constructor.
         * 
         * @since 1.0.0
         * @access private
         * 
         * @param type $Parent  Instance of Fsmpb_Meta_Box class.
         */
        public function __construct( $Parent ) {
                $this->Parent = $Parent;

                $this->instantiate_and_hook();
                
                // Global variable $wp_registered_widgets isn't available until "widgets_init" fired.
                add_action( 'widgets_init', array( $this, 'setup_info' ) ); 
                add_action( 'widgets_init', array( $this, 'register_tabs' ) ); 
        }
        
        /**
         * Instantiate element classes and hook the ajax functions.
         * 
         * @since 1.0.0
         * @access private
         */
        public function instantiate_and_hook() {
                /* Widget element */
                
                $this->instances[ 'Fsmpb_Element_Widget' ] = new Fsmpb_Element_Widget( $this->Parent );
                
                add_action( 'wp_ajax_fsmpb_widget_form', array( $this->instances['Fsmpb_Element_Widget'], 'form' ) );
                add_action( 'wp_ajax_fsmpb_widget_sanitize', array( $this->instances['Fsmpb_Element_Widget'], 'sanitize' ) );

                /* Custom elements */
                
                foreach ( $this->Parent->r->custom_element_classes as $_php_class ) {
                        if ( class_exists( $_php_class ) ) {
                                $this->instances[ $_php_class ] = new $_php_class( $this->Parent );

                                add_action( 'wp_ajax_fsmpb_custom_form_' . $_php_class, array( $this->instances[ $_php_class ], '_form_callback' ) );
                                add_action( 'wp_ajax_fsmpb_custom_sanitize_' . $_php_class, array( $this->instances[ $_php_class ], '_sanitize_callback' ) );
                        }
                }
        }
        
        /**
         * Stores name and description of the elements in $this->info.
         * 
         * @since 1.0.0
         * @access private
         */
        public function setup_info() {
                foreach ( $this->instances as $php_class => $obj ) {
                        if ( $php_class == 'Fsmpb_Element_Widget' ) {
                                foreach ( $GLOBALS[ 'wp_widget_factory' ]->widgets as $php_class_w => $obj_w ) {
                                        $this->info[ $php_class_w ] = array(
                                                'name'          => $obj_w->name,
                                                'description'   => $obj_w->widget_options[ 'description' ]
                                        );
                                }
                        } else {
                                $this->info[ $php_class ] = array(
                                        'name'          => $obj->name,
                                        'description'   => $obj->description
                                );
                        }
                }
        }
        
        /**
         * Register tabs that contains grouped elements.
         * 
         * @since 1.0.0
         * @access private
         */
        public function register_tabs() {
                if ( has_action( 'fsmpb_register_elements' ) ) {
                        _doing_it_wrong( 
                                __FUNCTION__, 
                                sprintf( 
                                        '"Action hook: <strong>%1$s</strong>" is <strong>deprecated</strong> since version %2$s! Use "filter hook: <strong>%3$s</strong>" instead.', 
                                        'fsmpb_register_elements',  
                                        '1.0.1', 
                                        'Fsmpb_Meta_Box_Elements__tabs' 
                                ),
                                '1.0.1'
                        );
                        
                        do_action( 'fsmpb_register_elements', $this );
                }
                
                /**
                 * Filter the tabs.
                 * 
                 * This also used to register the tabs.
                 * 
                 * @since 1.0.0
                 * @param array $tabs
                 */
                $this->tabs = apply_filters( 'Fsmpb_Meta_Box_Elements__tabs', $this->tabs );
        }
}