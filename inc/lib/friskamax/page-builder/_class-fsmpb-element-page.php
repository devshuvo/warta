<?php
/**
 * FriskaMax Page Builder - Page element.
 * 
 * @since 1.0.0
 * @package friskamax-library\page-builder\elements
 */
        
/**
 * Page element element class.
 * 
 * @since 1.0.0
 */
class Fsmpb_Element_Page extends Fsmpb_Element_Custom {   
        
        /**
         * An array of arguments.
         * 
         * @since   1.0.2
         * @access  private
         * 
         * @var     type    array {
         *      @type string $template  The template for displaying the page. Without .php file extension.
         * }
         */
        private $args = array(
                'template'      => ''
        );
        
        /**
         * Initialize.
         * 
         * @since   1.0.0
         * @access  private
         * 
         * @param   type    $Parent     Instance of Fsmpb_Meta_Box class.
         */
        public function __construct( $Parent ) {     
                
                parent::__construct( $Parent );
                
                $this->args             = friskamax_parse_args( $Parent->r->element_page, $this->args, 'Fsmpb_Element_Page__args', false );
                $this->name             = __( 'Page', FRISKAMAX_THEME_SLUG );
                $this->description      = __( 'A page content.', FRISKAMAX_THEME_SLUG );
                
        }

        /**
         * Page form.
         * 
         * @since   1.0.0
         * @access  private
         * 
         * @param   array   $settings {
         *      Element settings.
         * 
         *      @type   int     $id     Page ID.
         * }
         */
        public function form( $settings ) {
                
                $s = friskamax_parse_args( $settings, array( 'id' => '' ), 'Fsmpb_Element_Page__form_s' );
                
                fsmf_field_input( array(
                        'label'         => __( 'Page ID', FRISKAMAX_THEME_SLUG ),
                        'desc'          => __( 'If empty, the current page content will be displayed', FRISKAMAX_THEME_SLUG ),
                        'attr'          => array(
                                'name'  => 'id',
                                'value' => $s->id
                        )
                ) );
                
        }

        /**
         * Sanitize Page options.
         * 
         * @since   1.0.0
         * @access  private
         * 
         * @param   array   $settings   Element settings.
         * @return  array               Sanitized element settings.
         */
        public function sanitize( $settings ) {
                
                $settings[ 'id' ]       = $settings[ 'id' ] ? ( int ) $settings[ 'id' ] : '';

                return $settings;
                
        }

        /**
         * Display the Page.
         * 
         * @since 1.0.0
         * @access private
         * @param object $settings      Element settings.
         */
        public function display( $settings ) {    
                                
                if ( ! $this->args[ 'template' ] ) {
                        return;
                }
                
                $the_query = new WP_Query( array(
                        'post_type'     => 'page',
                        'page_id'       => $settings->id ? $settings->id : get_the_ID()
                ) );

                if ( $the_query->have_posts() ) {
                        
                        while ( $the_query->have_posts() ) {
                                
                                $the_query->the_post();
                                get_template_part( $this->args[ 'template' ] );
                            
                        }
                        
                } 
                
                wp_reset_postdata();
                
        }
}