<?php
/**
 * Page Builder Base Custom Element.
 * 
 * @since 1.0.2
 * @package friskamax-library\page-builder\elements
 */

if ( class_exists( 'Fsmpb_Element_Custom' ) ) {
        return;
}

/**
 * Page builder base custom element class.
 * 
 * This class must be extended for each custom elements and 
 * Fsmpb_Element_Custom::form(), Fsmpb_Element_Custom::sanitize()
 * and Fsmpb_Element_Custom::display() need to be over-ridden.
 * 
 * @since 1.0.2
 */
class Fsmpb_Element_Custom {        
        /**
         * Name of the element.
         * 
         * @since 1.0.2
         * @var string 
         */
        public $name;
        
        /**
         * Description of the element.
         * 
         * @since 1.0.2
         * @var string
         */
        public $description;

        /**
         * Constructor.
         * 
         * @since 1.0.2
         * 
         * @param type $Parent  Instance of Fsmpb_Meta_Box class.
         */
        public function __construct( $Parent ) {     
                $this->Parent = $Parent;
        }

        /**
         * Form.
         * 
         * @since 1.0.2
         *
         * @param array $settings       Element settings.
         */
        public function form( $settings ) {
                _e( 'There are no options for this element.', FRISKAMAX_THEME_SLUG );
        }

        /**
         * Sanitize element settings.
         * 
         * @since 1.0.2
         * 
         * @param array $settings       Element settings.
         * @return array                Sanitized element settings.
         */
        public function sanitize( $settings ) {
                return $settings;
        }

        /**
         * Display the widget area
         * 
         * @since 1.0.2
         * 
         * @param object $settings      Element settings.
         */
        public function display( $settings ) {      
                wp_die( 'function Fsmpb_Element_Custom::widget() must be over-ridden in a sub-class.' );
        }
        
        /**
         * Generate the form.
         *
         * @since 1.0.2
         * @access private
         */
        public function _form_callback() {
                $settings       = isset( $_POST[ 'formData' ] ) 
                                ? ( array ) json_decode( stripslashes( $_POST[ 'formData' ] ), true )
                                : array();
                
                ob_start();
                
                $this->form( $settings );
                
                friskamax_modal( array(
                        'wrapper'       => 'form',
                        'id'            => 'fsmpb-modal-custom-element-' . sanitize_title( $this->name ),
                        'title'         => $this->name,
                        'content'       => ob_get_clean(),
                        'cancel'        => __( 'Close', FRISKAMAX_THEME_SLUG ),
                        'submit_type'   => 'submit'
                ) );
                
                wp_die();
        }
        
        /**
         * Deal with sanitizing settings.
         * 
         * @since 1.0.0
         * @access private
         */
        public function _sanitize_callback() {
                parse_str( $_POST[ 'formData' ], $settings );
                
                $settings = stripslashes_deep( $settings );
                
                echo json_encode( $this->sanitize( $settings ) );

                wp_die();
        }
}