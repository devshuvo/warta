<?php
/**
 * FriskaMax Page Builder Register.
 * 
 * @since 1.0.0
 * @package friskamax-library\page-builder
 */
        
/**
 * The class that responsible for registering the meta box.
 * 
 * @since 1.0.0
 */
class Fsmpb_Meta_Box_Register {        
        /**
         * Instance of Fsmpb_Meta_Box class.
         * 
         * @since 1.0.0
         * @access private
         * @var object
         */
        protected $Parent;
        
        /**
         * Page templates that can be used by page builder and their default 
         * breakpoint. The breakpoint will be used as default on column element.
         * 
         * @since 1.0.0
         * @access private
         * @var array 
         */
        private $page_templates = array(
                'default'       => array( 'breakpoint' => 'sm' )
        );
        
        /**
         * Constructor.
         * 
         * @since 1.0.0
         * @access private
         * @param type $Parent Instance of Fsmpb_Meta_Box class.
         */
        public function __construct( $Parent ) {
                $this->Parent           = $Parent;
                $this->page_templates   = friskamax_parse_args( $this->Parent->r->page_templates, $this->page_templates, 'Fsmpb_Meta_Box_Register__page_templates', false );

                // Register meta box
                add_action( 'load-post.php', array( $this, 'register' ) );
                add_action( 'load-post-new.php', array( $this, 'register' ) );
        }
        
        /**
         * Register the meta box.
         * 
         * @since 1.0.0
         * @access private
         */
        public function register() {
                add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
                add_action( 'save_post', array( $this, 'save' ) );                    
        }

        /**
         * Adds the meta box container.
         * 
         * @since 1.0.0
         * @access private
         */
        public function add_meta_box( $post_type ) {
                if ( in_array( $post_type, $this->Parent->r->post_types ) ) {  //limit meta box to certain post types
                        add_meta_box( 
                                'fsmpb_meta_box', 
                                __( 'Page Builder', FRISKAMAX_THEME_SLUG ), 
                                array( $this->Parent->Display, 'display' ), 
                                $post_type, 
                                'normal', 
                                'high' 
                        );
                        
                        $this->enqueue_scripts();
                        $this->localize();    
                }
        }

        /**
         * Save the meta when the post is saved.
         * 
         * @since 1.0.0
         * @access private
         *
         * @param int $post_id The ID of the post being saved.
         */
        public function save( $post_id ) {

                /* We need to verify this came from the our screen and with proper authorization,
                 * because save_post can be triggered at other times. */

                // Check if our nonce is set.
                if ( ! isset( $_POST[ 'fsmpb_meta_box_nonce' ] ) ) { 
                        return $post_id;
                }
                
                // Verify that the nonce is valid.
                if ( ! wp_verify_nonce( $_POST[ 'fsmpb_meta_box_nonce' ], 'fsmpb_meta_box' ) ) {
                        return $post_id;
                }
                
                // If this is an autosave, our form has not been submitted, so we don't want to do anything.
                if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
                        return $post_id;
                }
                
                // Check the user's permissions.
                if ( 'page' == $_POST[ 'post_type' ] ) {
                        if ( ! current_user_can( 'edit_page', $post_id ) ) {
                                return $post_id;
                        }
                } else {
                        if ( ! current_user_can( 'edit_post', $post_id ) ) {
                                return $post_id;
                        }
                }
                
                // We cannot do any sanitation here, because users my use WordPress Text 
                // widget and may use HTML code. So all we can do is check the current
                // user capability just like WP_Widget_Text::update().
                if ( ! current_user_can( $this->Parent->r->capability ) ) {
                        return $post_id;
                }

                /* OK, its safe for us to save the data now. */
                
                // Update the meta field.
                update_post_meta( $post_id, 'fsmpb_main', $_POST[ 'fsmpb_main' ] );
        }

        /**
         * Enqueue js and css files.
         * 
         * @since 1.0.0
         * @access private
         */
        protected function enqueue_scripts() {                                
                wp_enqueue_style( 'fsmpb-style', get_template_directory_uri() . '/inc/lib/friskamax/page-builder/css/fsmpb.css', array(), wp_get_theme()->get( 'Version' ) );
                wp_enqueue_script(
                        'fsmpb-script', 
                        get_template_directory_uri() . '/inc/lib/friskamax/page-builder/js/fsmpb.js', 
                        array(
                                'jquery',
                                'jquery-ui-core',
                                'jquery-ui-widget',
                                'jquery-ui-mouse',
                                'jquery-touch-punch',
                                'fsmh-script'
                        ), 
                        wp_get_theme()->get( 'Version' ), 
                        true
                );
        }

        /**
         * Localize translation texts.
         * 
         * @since 1.0.0
         * @access private
         */
        protected function localize() {
                wp_localize_script(
                        'fsmpb-script', 
                        'fsmpbLang', 
                        array(
                                'pageBuilder'           => __( 'Page Builder', FRISKAMAX_THEME_SLUG ),
                                'exitPageBuilder'       => __( 'Exit Page Builder', FRISKAMAX_THEME_SLUG ),
                                'fullScreen'            => __( 'Full Screen', FRISKAMAX_THEME_SLUG ),
                                'exitFullScreen'        => __( 'Exit Full Screen', FRISKAMAX_THEME_SLUG ),
                                'msg'                   => array(
                                                                'delete'        => __( 'Are you sure you want to delete it?', FRISKAMAX_THEME_SLUG ),
                                                                'enterTitle'    => __( 'Please enter the title.', FRISKAMAX_THEME_SLUG ),
                                                                'invalidValue'  => __( 'Invalid value.', FRISKAMAX_THEME_SLUG ),
                                                                'invalidColumn' => __( 'Please enter at least one screen size.', FRISKAMAX_THEME_SLUG )
                                                        )
                        )
                );     
                
                wp_localize_script( 'fsmpb-script', 'fsmpbElementsInfo', $this->Parent->Elements->info );
                wp_localize_script( 'fsmpb-script', 'fsmpbPageTemplates', $this->page_templates );
        }
}