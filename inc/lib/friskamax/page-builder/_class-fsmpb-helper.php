<?php
/**
 * FriskaMax Page Builder Helper.
 * 
 * @since 1.0.0
 * @package friskamax-library\page-builder
 */

/**
 * FriskaMax Page Builder Helper class.
 * 
 * @since 1.0.0
 */
class Fsmpb_Helper {                
        /**
         * Widget PHP classes that have been used.
         * 
         * @since 1.0.0
         * @var array
         */
        public $widget_classes = array();

        /**
         * Get arguments for element with widget type.
         * 
         * @since 1.0.0
         * 
         * @param array $widget_classes Widget PHP classes.
         * @return array                Element arguments.
         */
        public function get_element_args__widget( $widget_classes ) {
                $this->widget_classes   = array_merge( $this->widget_classes, $widget_classes );
                $items                  = array();

                foreach ( $widget_classes as $php_class ) {
                        $items[] = array(
                                'type'          => 'widget',
                                'php_class'     => $php_class
                        );
                } 

                /**
                 * Filter the element arguments.
                 * 
                 * @since 1.0.0
                 * 
                 * @param array $items          Element arguments.
                 * @param array $widget_classes Widget PHP classes.
                 */
                return apply_filters( 'Fsmpb_Helper__get_element_args__widget', $items, $widget_classes );
        }

        /**
         * Get element arguments for WordPress widgets.
         * 
         * @see wp_widgets_init()
         * @since 1.0.0
         * @return array        Element arguments.
         */
        public function get_element_args__wp_widgets() {
                $widget_classes = array(
                        'WP_Widget_Pages',
                        'WP_Widget_Calendar',
                        'WP_Widget_Archives',
                        'WP_Widget_Meta',
                        'WP_Widget_Search',
                        'WP_Widget_Text',
                        'WP_Widget_Categories',
                        'WP_Widget_Recent_Posts',
                        'WP_Widget_Recent_Comments',
                        'WP_Widget_RSS',
                        'WP_Widget_Tag_Cloud',
                        'WP_Nav_Menu_Widget',
                        'WP_Widget_Custom_HTML',
                );
                
                // Since Wordpress 3.5, Link Manager is hidden for new installs and for any 
                // existing installs that have no links (all sites with existing links are 
                // left as is). This can be restored with Link Manager Plugin.
                //
                // http://codex.wordpress.org/Version_3.5#Links
                if ( get_option( 'link_manager_enabled' ) ) {
                        $widget_classes[] = 'WP_Widget_Links';
                }
                        
                asort( $widget_classes );

                return  $this->get_element_args__widget( $widget_classes );
        }

        /**
         * Get element argumnents for plugin widgets.
         * 
         * @since 1.0.0
         * @return array        Element arguments.
         */
        public function get_element_args__plugin_widgets() {                        
                $other_widget_classes = array();
                
                foreach ( array_keys( $GLOBALS[ 'wp_widget_factory' ]->widgets ) as $widget_class ) {  
                    switch ($widget_class) {
                        // Exclude media widgets because they use unsupported form fields.
                        case 'WP_Widget_Media_Audio':
                        case 'WP_Widget_Media_Image':
                        case 'WP_Widget_Media_Video':
                            break;
                        default:
                            if ( ! in_array( $widget_class, $this->widget_classes ) ) {
                                $other_widget_classes[] = $widget_class;
                            }
                            break;
                    }
                }        

                return  $other_widget_classes
                        ? $this->get_element_args__widget( $other_widget_classes )
                        : null;
        }
}