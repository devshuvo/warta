+function( $, F ) { 'use strict';
        
        F.FsmpbColumn = function( Parent ) {
                this.Parent             = Parent;                
                
                this.$modalColumn       = $( '#fsmpb-modal-column' );
                this.$saveButton        = this.$modalColumn.find( '[data-save]' );
                this.$columnFields      = this.$modalColumn.find( '.column-field' );
                
                this.$column            = null;
                
                this.listen();
        };
        
        /**
         * Listen events.
         */
        F.FsmpbColumn.prototype.listen = function() {
                this.$saveButton.on( 'click', { self: this }, F.FsmpbColumn.prototype.save );
        };
        
        /**
         * Show edit column form.
         * @param {jQuery} $column Old element object.
         */
        F.FsmpbColumn.prototype.modal = function( $column ) {
                var     self            = this,  
                        columnData      = this.getColumnData( $column );
                
                this.$columnFields.val( '' );
                
                $.each( columnData, function( index, value ) {
                        self.$modalColumn.find( 'input[name=' + index + ']' ).val( value );
                } );
                
                this.$modalColumn.fsmhBsModal( 'show' );
                this.$column = $column;
        };
        
        /**
         * Save the column data.
         * @param {Event} event
         */
        F.FsmpbColumn.prototype.save = function( event ) {
                var     self    = event.data.self,
                        data    = {},
                        ok      = false;
                
                self.$columnFields.each( function( index, element ) {   
                        var     $element        = $( element ),
                                name            = $element.attr( 'name' ),
                                value           = parseInt( $element.val() );
                        
                        if ( ! isNaN( value ) ) {      
                                if ( value > 12 )       value = 12;
                                else if ( value < 1 )   value = 1;
                                
                                data[ name ]    = value;
                                ok              = true;
                        } else {
                                data[ name ] = '';
                        }
                } );
                
                if ( ! ok ) {
                        alert( fsmpbLang.msg.invalidColumn );
                        return;
                }
                
                self.changeWidth( self.$column, data );
                self.$modalColumn.fsmhBsModal( 'hide' );
        };
        
        /**
         * Increase the column width.
         * 
         * @param {jQuery} $column      The element.
         */
        F.FsmpbColumn.prototype.increaseWidth = function( $column ) {
                var data = this.getSingleColumnData( $column );
                
                $.each( data, function( index, value ) {
                        data[ index ] = value < 12 ? value + 1 : value;
                } );
                
                this.changeWidth( $column, data );
        };
        
        /**
         * Decrease the column width.
         * 
         * @param {jQuery} $column      The element.
         */
        F.FsmpbColumn.prototype.decreaseWidth = function( $column ) {
                var data = this.getSingleColumnData( $column );
                
                $.each( data, function( index, value ) {
                        data[ index ] = value > 1 ? value - 1 : value; 
                } );
                
                this.changeWidth( $column, data );
        };
        
        /**
         * Get the column data.
         * 
         * @param {jQuery} $column      The element.
         * @returns {object}            The column data. 
         * {
         *      lg: #,
         *      md: #,
         *      sm: #,
         *      xs: #
         * }
         */
        F.FsmpbColumn.prototype.getColumnData = function( $column ) {
                var     classname       = $column.attr( 'class' ).replace( /column-wrapper/gi, '' ),
                        columns         = $.trim( classname ).split( ' ' ),
                        data            = {};
                
                $.each( columns, function( index, Element ) {
                        if ( Element.indexOf( 'col-lg-' ) >= 0 ) {
                                data.lg = Element.replace( 'col-lg-', '' );
                                data.lg = parseInt( data.lg );
                        } else if ( Element.indexOf( 'col-md-' ) >= 0 ) {
                                data.md = Element.replace( 'col-md-', '' );
                                data.md = parseInt( data.md );
                        } else if ( Element.indexOf( 'col-sm-' ) >= 0 ) {
                                data.sm = Element.replace( 'col-sm-', '' );
                                data.sm = parseInt( data.sm );
                        } else if ( Element.indexOf( 'col-xs-' ) >= 0 ) {
                                data.xs = Element.replace( 'col-xs-', '' );
                                data.xs = parseInt( data.xs );
                        }
                } ); 
                
                return data;
        };
        
        /**
         * Get a single column data. The largest screen size will be used.
         * 
         * @param {jQuery} $column      The element.
         * @returns {object}            The arguments.
         */
        F.FsmpbColumn.prototype.getSingleColumnData = function( $column ) {
                var     columnData      = this.getColumnData( $column ),
                        data            = {};
                
                if ( columnData.lg )            data.lg = columnData.lg;
                else if ( columnData.md )       data.md = columnData.md;
                else if ( columnData.sm )       data.sm = columnData.sm;
                else if ( columnData.xs )       data.xs = columnData.xs;
                
                return data;
        };
        
        /**
         * Change the column width.
         * 
         * @param {jQuery} $column              The element.
         * @param {object} newColumnData        New column data.
         */
        F.FsmpbColumn.prototype.changeWidth = function( $column, newColumnData ) {
                var columnData = this.getColumnData( $column );
                
                $.each( newColumnData, function( index, value ) {
                        var     oldClass        = 'col-' + index + '-' + columnData[index],
                                newClass        = 'col-' + index + '-' + value;
                        
                        $column.removeClass( oldClass );
                        
                        if ( value ) $column.addClass( newClass );                                
                } );
        };
        
        /**
         * Get screen breakpoint based on page template that being used.
         * 
         * @returns {String}
         */
        F.FsmpbColumn.prototype.getBreakpoint = function() {
                var $pageTemplate = $( '#page_template' );
                
                if ( ! $pageTemplate.length ) return 'sm';
                
                return fsmpbPageTemplates[ $pageTemplate.val() ].breakpoint;
        };
        
} ( jQuery, window.FriskaMax = window.FriskaMax || {} );
+function( $, F ) { 'use strict';
        
        F.FsmpbData = function( Parent ) {
                this.Parent     = Parent;
                this.$fieldMain = $( '[name=fsmpb_main]' );
                
                this.listen(); 
                this.generateLayout(); 
        };
        
        /**
         * Listen events.
         */
        F.FsmpbData.prototype.listen = function() {
                // Submit the Post
                $( '#post' ).on( 'submit.fsmpb', $.proxy( this.submit, this ) );
        };   
        
        /**
         * Submit the data.
         * 
         * @param {Event} event
         */
        F.FsmpbData.prototype.submit = function( event ) {                
                // WordPress required either post title or post content to be filled in order the data can be saved.
                if( this.getData() && ! $( '#title' ).val() ) {
                        alert( fsmpbLang.msg.enterTitle );
                        
                        $( '#title' ).focus();
                        $( '#publish' )
                                .removeClass( 'button-primary-disabled' )       // Reenable publish button
                                .parent().find( '.spinner' ).hide();            // Hide spinner
                        
                        event.preventDefault();
                }
        };     
        
        /* ========================================================================== *
         * Generate layout
         * ========================================================================== */
        
        /**
         * Generate page builder layout from saved data.
         */
        F.FsmpbData.prototype.generateLayout = function() {
                var     self            = this,
                        dataJSON        = this.$fieldMain.val(),
                        dataArray       = F.Helper.isJSON( dataJSON ) ? $.parseJSON( dataJSON ) : [];
                
                // Return if there is no page builder data or the data is broken.
                if ( ! dataJSON || ! dataArray.length ) return;
                
                if ( $.type( dataArray[0].type )  === 'undefined' ) { // In version > 1.0.1, the outermost element is a row and it doesn't have type property.
                        this.old_generateLayout( dataArray );
                } else {
                        $.each( dataArray, function( index, value ) {
                                self.generateElement( value, self.Parent.$mainContent );
                        } );
                }

                this.Parent.Helper.sortable();  
        };
        
        F.FsmpbData.prototype.generateElement = function( elementData, $container ) {
                if ( elementData.type === 'row' ) {
                        this.generateRow( elementData, $container );
                } else {
                        var $element = $( this.Parent.templateElement ).data( elementData ).appendTo( $container );

                        if ( fsmpbElementsInfo[ elementData.args.php_class ] ) { // class exists
                                $element.html(
                                        $element.html()
                                                .replace( /{{title}}/g, fsmpbElementsInfo[ elementData.args.php_class ].name )
                                                .replace( /{{subtitle}}/g, this.Parent.Helper.getElementSubtitle( elementData.formData ) )
                                );
                        } else { // class doesn't exists
                                $element.hide();
                        }
                }
        };
        
        F.FsmpbData.prototype.generateRow = function( rowData, $container ) {
                var     self            = this,
                        $template       = $( this.Parent.templateRow ).appendTo( $container );

                $.each( rowData.children, function( index, value )  {  
                        self.generateColumn( value, $template.find( '> .panel-body > .row' ) );
                } );
        };
        
        F.FsmpbData.prototype.generateColumn = function( columnData, $container ) {
                var     self            = this,
                        $template       = $( this.Parent.templateCol ).appendTo( $container );

                $.each( columnData.formData, function( index, value ) {
                        $template.addClass( 'col-' + index + '-' +value );
                } );

                $.each( columnData.children, function( index, value ) {      
                        self.generateElement( value, $template.find( '> .panel > .panel-body' ) );                               
                });      
        };
        
        /* ========================================================================== *
         * Get data
         * ========================================================================== */
        
        /**
         * Get page builder data from layout and save it to [name=fsmpb_main].
         * 
         * @returns {JSON}      Page builder data.
         */
        F.FsmpbData.prototype.getData = function() {
                var     self            = this,
                        dataArray       = [],
                        dataJSON        = '';
                
                if ( this.Parent.$mainContent.children().length === 0 ) {
                        this.$fieldMain.val( '' );
                        return;
                }
                
                // Remove any sortable data by removing the sortable functionality.
                // The sortable data is unecessary and can cause error when using JSON.stringify (because there's a method in it).
                this.Parent.$mainContent.add( this.Parent.$mainContent.find( '.ui-sortable' ) ).sortable( 'destroy' );
                
                this.Parent.$mainContent.children().each( function( index, element  ) {
                        var $element = $( element );
                        
                        if ( $element.is( '.row-wrapper' ) ) {
                                dataArray.push( self.getRow( $element ) );
                        } else {
                                dataArray.push( self.getElement( $element ) );                                
                        }
                } );
                                
                dataJSON = dataArray.length ? JSON.stringify( dataArray ) : '';
                this.$fieldMain.val( dataJSON );
                
                this.Parent.Helper.sortable();
                
                return dataJSON;
        };
        
        /**
         * Get row data.
         * 
         * @param {jQuery} $row Row element.
         * @returns {object}    Row data.
         */
        F.FsmpbData.prototype.getRow = function( $row ) {
                var     self            = this,
                        children        = [];
                
                $row.find( '> .panel-body > .row' ).children().each( function( index, element ) {
                        children.push( self.getColumn( $( element ) ) );
                } );
                
                return {
                        type    : 'row',
                        children: children
                };
        };
        
        /**
         * Get column data.
         * 
         * @param {jQuery} $column      Column element.
         * @returns {Object}            Column data.
         */
        F.FsmpbData.prototype.getColumn = function( $column ) {
                var     self            = this,
                        children        = [];
                
                $column
                        .data( 'formData', this.Parent.Column.getColumnData( $column ) )
                        .find( '> .panel > .panel-body' ).children().each( function( index, element ) {
                                var $element = $( element );

                                if ( $element.is( '.row-wrapper' ) ) {
                                        children.push( self.getRow( $element ) );
                                } else {
                                        children.push( self.getElement( $element ) );                                
                                }
                        } );
                
                return $.extend( {
                        type    : 'column',
                        children: children
                }, $column.data() );
        };
        
        /**
         * Get element data.
         * 
         * @param {jQuery} $element     Element.
         * @returns {object}            Element data.
         */
        F.FsmpbData.prototype.getElement = function( $element ) {
                return $element.data();
        };
        
        /* ========================================================================== *
         * Old data
         * ========================================================================== */
        
        /**
         * Generate layut from old data.
         * 
         * @param {array} dataArray     Page builder data.
         */
        F.FsmpbData.prototype.old_generateLayout = function( dataArray ) {
                var self = this;
                                
                $.each( dataArray, function( index, value ) {
                        self.old_generateRow( value, self.Parent.$mainContent );
                } );
        };
        
        F.FsmpbData.prototype.old_generateRow = function( rowData, $container ) {
                var     self            = this,
                        $template       = $( self.Parent.templateRow ).appendTo( $container );
                
                $.each( rowData, function( index, value ) {
                        self.old_generateRowColumn( value, $template.find( '> .panel-body > .row' ) );
                } ); 
        };
        
        F.FsmpbData.prototype.old_generateRowColumn = function( columnData, $container ) {
                var     self            = this,
                        breakpoint      = this.Parent.Column.getBreakpoint(),
                        columnWidth     = 'col-' + breakpoint + '-' + columnData.width,
                        $template       = $( this.Parent.templateCol ).addClass( columnWidth ).appendTo( $container );
                        
                $.each( columnData.subRows, function( index, value ) {      
                        self.old_generateSubRow( value, $template.find( '> .panel > .panel-body' ) );                               
                });      
        };
        
        F.FsmpbData.prototype.old_generateSubRow = function( rowData, $container ) {
                var     self            = this,
                        $template       = $( self.Parent.templateRow ).appendTo( $container );  

                $.each( rowData, function( index, value ) {
                        self.old_generateSubRowColumn( value, $template.find( '> .panel-body > .row' ) );
                } );                                      
                
        };
        
        F.FsmpbData.prototype.old_generateSubRowColumn = function ( columnData, $container ) {
                var     self            = this,
                        breakpoint      = this.Parent.Column.getBreakpoint(),
                        columnWidth     = 'col-' + breakpoint + '-' + columnData.width,
                        $template       = $( this.Parent.templateCol ).addClass( columnWidth ).appendTo( $container );

                $.each( columnData.elements, function( index, value ) {    
                        self.old_generateElement( value, $template.find( '> .panel > .panel-body' )  );  
                });
        };
        
        F.FsmpbData.prototype.old_generateElement = function( elementData, $container ) {
                var $element = $( this.Parent.templateElement ).data( elementData );

                if ( fsmpbElementsInfo[ elementData.args.php_class ] ) { // class exists
                        $element.html(
                                $element.html()
                                        .replace( /{{title}}/ig, fsmpbElementsInfo[ elementData.args.php_class ].name )
                                        .replace( /{{subtitle}}/ig, this.Parent.Helper.getElementSubtitle( elementData.formData ) )
                        );
                } else { // class doesn't exists
                        $element.hide();
                }

                $container.append( $element ); 
        };
        
} ( jQuery, window.FriskaMax = window.FriskaMax || {} );

/**
 * Sample Data
 * 1.0.0 = [[{"width":"8","subRows":[[{"width":"12","elements":[{"type":"widget","modal":"#fsmpb-modal-widget_warta_breaking_news","title":"Breaking News","args":{"php_class":"Warta_Breaking_News","css_class":"widget_warta_breaking_news"},"action":"fsmpb_widget_form_ajax","subtitle":"Breaking News","formData":{"title":"Breaking News","data":"latest","sort":"comments","time_range":"all","top_review":0,"category":1,"tags":"","post_ids":"","count":4,"icon":"fa-angle-double-right","duration":20000,"direction":"left","ignore_sticky":1}},{"type":"widget","modal":"#fsmpb-modal-widget_warta_posts_carousel","title":"Posts Carousel","args":{"php_class":"Warta_Posts_Carousel","css_class":"widget_warta_posts_carousel"},"action":"fsmpb_widget_form_ajax","subtitle":"","formData":{"title":"","data":"post_ids","sort":"comments","time_range":"all","category":1,"tags":"","post_ids":"2149, 1","count":4,"excerpt":200,"interval":8000,"animation":"slide","animation_speed":2000,"ignore_sticky":1,"hide_mobile":1},"sortable.preventClickEvent":true},{"type":"widget","modal":"#fsmpb-modal-widget_warta_ads","title":"Advertisement","args":{"php_class":"Warta_Advertisement","css_class":"widget_warta_ads"},"action":"fsmpb_widget_form_ajax","subtitle":"Advertisement","formData":{"title":"Advertisement","ad_type":"img","img_url":"http://placehold.it/728x90/f0f0f9","ad_url":"http://themeforest.net/item/warta-newsmagazine-wordpress-theme/7066039?ref=friskamax","ad_code":"","hide_on_mobile":1}},{"type":"widget","modal":"#fsmpb-modal-widget_warta_articles","title":"Articles","args":{"php_class":"Warta_Articles","css_class":"widget_warta_articles"},"action":"fsmpb_widget_form_submit_ajax","subtitle":"Latest News","formData":{"layout":1,"title":"Latest News","data":"latest","sort":"comments","time_range":"all","top_review":0,"category":"general","tags":"","post_ids":"","count":4,"excerpt":20,"date_format":"F j, Y","ignore_sticky":1,"meta_date":1,"meta_format":0,"meta_category":1,"meta_categories":0,"meta_author":1,"meta_comments":1,"meta_views":1,"meta_review_score":0}},{"type":"widget","modal":"#fsmpb-modal-widget_warta_articles","title":"Articles","args":{"php_class":"Warta_Articles","css_class":"widget_warta_articles"},"action":"fsmpb_widget_form_ajax","subtitle":"Health","formData":{"layout":2,"title":"Health","data":"category","sort":"comments","time_range":"all","top_review":0,"category":"health-national","tags":"","post_ids":"","count":4,"excerpt":160,"date_format":"F j, Y","ignore_sticky":1,"meta_date":1,"meta_format":0,"meta_category":0,"meta_categories":0,"meta_author":1,"meta_comments":1,"meta_views":1,"meta_review_score":0}}]}],[{"width":"6","elements":[{"type":"widget","modal":"#fsmpb-modal-widget_warta_articles","title":"Articles","args":{"php_class":"Warta_Articles","css_class":"widget_warta_articles"},"action":"fsmpb_widget_form_ajax","subtitle":"Sport","formData":{"layout":3,"title":"Sport","data":"category","sort":"comments","time_range":"all","top_review":0,"category":"sport","tags":"","post_ids":"","count":5,"excerpt":160,"date_format":"F j, Y","ignore_sticky":1,"meta_date":1,"meta_format":0,"meta_category":0,"meta_categories":0,"meta_author":1,"meta_comments":1,"meta_views":1,"meta_review_score":0}}]},{"width":"6","elements":[{"type":"widget","modal":"#fsmpb-modal-widget_warta_articles","title":"Articles","args":{"php_class":"Warta_Articles","css_class":"widget_warta_articles"},"action":"fsmpb_widget_form_ajax","subtitle":"Entertainment","formData":{"layout":3,"title":"Entertainment","data":"category","sort":"comments","time_range":"all","top_review":0,"category":"entertainment","tags":"","post_ids":"","count":5,"excerpt":160,"date_format":"F j, Y","ignore_sticky":1,"meta_date":1,"meta_format":0,"meta_category":0,"meta_categories":0,"meta_author":1,"meta_comments":1,"meta_views":1,"meta_review_score":0},"sortable.preventClickEvent":true}]}],[{"width":"12","elements":[{"type":"widget","modal":"#fsmpb-modal-widget_warta_ads","title":"Advertisement","args":{"php_class":"Warta_Advertisement","css_class":"widget_warta_ads"},"action":"fsmpb_widget_form_ajax","subtitle":"Advertisement","formData":{"title":"Advertisement","ad_type":"img","img_url":"http://placehold.it/728x90/f0f0f9","ad_url":"http://themeforest.net/item/warta-newsmagazine-wordpress-theme/7066039?ref=friskamax","ad_code":"","hide_on_mobile":1},"sortable.preventClickEvent":true}]}],[{"width":"3","elements":[{"type":"widget","modal":"#fsmpb-modal-widget_warta_articles","title":"Articles","args":{"php_class":"Warta_Articles","css_class":"widget_warta_articles"},"action":"fsmpb_widget_form_ajax","subtitle":"Books","formData":{"layout":4,"title":"Books","data":"category","sort":"comments","time_range":"all","top_review":0,"category":"booksmagazines","tags":"","post_ids":"","count":4,"excerpt":160,"date_format":"M j, Y","ignore_sticky":1,"meta_date":1,"meta_format":0,"meta_category":0,"meta_categories":0,"meta_author":0,"meta_comments":1,"meta_views":1,"meta_review_score":0}}]},{"width":"3","elements":[{"type":"widget","modal":"#fsmpb-modal-widget_warta_articles","title":"Articles","args":{"php_class":"Warta_Articles","css_class":"widget_warta_articles"},"action":"fsmpb_widget_form_ajax","subtitle":"Food","formData":{"layout":4,"title":"Food","data":"category","sort":"comments","time_range":"all","top_review":0,"category":"food-drink","tags":"","post_ids":"","count":4,"excerpt":160,"date_format":"M j, Y","ignore_sticky":1,"meta_date":1,"meta_format":0,"meta_category":0,"meta_categories":0,"meta_author":0,"meta_comments":1,"meta_views":1,"meta_review_score":0},"sortable.preventClickEvent":true}]},{"width":"3","elements":[{"type":"widget","modal":"#fsmpb-modal-widget_warta_articles","title":"Articles","args":{"php_class":"Warta_Articles","css_class":"widget_warta_articles"},"action":"fsmpb_widget_form_ajax","subtitle":"Travel","formData":{"layout":4,"title":"Travel","data":"category","sort":"comments","time_range":"all","top_review":0,"category":"travel","tags":"","post_ids":"","count":4,"excerpt":160,"date_format":"M j, Y","ignore_sticky":1,"meta_date":1,"meta_format":0,"meta_category":0,"meta_categories":0,"meta_author":0,"meta_comments":1,"meta_views":1,"meta_review_score":0},"sortable.preventClickEvent":true}]},{"width":"3","elements":[{"type":"widget","modal":"#fsmpb-modal-widget_warta_articles","title":"Articles","args":{"php_class":"Warta_Articles","css_class":"widget_warta_articles"},"action":"fsmpb_widget_form_ajax","subtitle":"Tech","formData":{"layout":4,"title":"Tech","data":"category","sort":"comments","time_range":"all","top_review":0,"category":"technology","tags":"","post_ids":"","count":4,"excerpt":160,"date_format":"M j, Y","ignore_sticky":1,"meta_date":1,"meta_format":0,"meta_category":0,"meta_categories":0,"meta_author":0,"meta_comments":1,"meta_views":1,"meta_review_score":0},"sortable.preventClickEvent":true}]}]]},{"width":"4","subRows":[[{"width":"12","elements":[{"type":"widget","modal":"#fsmpb-modal-widget_warta_social_media","title":"Social Media","args":{"php_class":"Warta_Social_Media","css_class":"widget_warta_social_media"},"action":"fsmpb_widget_form_submit_ajax","subtitle":"Follow Us","formData":{"title":"Follow Us","amazon":"","behance":"http://behance.net/","blogger":"","codepen":"","deviantart":"http://www.deviantart.com/","dribbble":"http://dribbble.com/","facebook":"http://facebook.com/","feedly":"","flickr":"http://www.flickr.com/photos/friskamax","forrst":"","foursquare":"","googleplus":"http://plus.google.com/","github":"","grooveshark":"","instagram":"http://instagram.com/","kickstarter":"https://www.kickstarter.com/","lastfm":"","linkedin":"https://www.linkedin.com/","myspace":"https://myspace.com/","picasa":"http://picasa.google.com/","pinterest":"http://pinterest.com/","quora":"","reddit":"","renren":"","rss":"http://friskamax.com/warta/wp/?feed=rss2","soundcloud":"https://soundcloud.com/","stack-exchange":null,"stack-overflow":null,"steam":"","stumbleupon":"","technorati":"","tencent-weibo":null,"tumblr":"http://tumblr.com/","twitter":"https://twitter.com/friska_max","vimeo":"http://vimeo.com/","vine":"","wikipedia":"","vk":"","weibo":"","wordpress":"","xing":"","yahoo":"","youtube":"http://youtube.com/","zerply":""}},{"type":"widget","modal":"#fsmpb-modal-widget_warta_tabs","title":"Tabs","args":{"php_class":"Warta_Tabs","css_class":"widget_warta_tabs"},"action":"fsmpb_widget_form_ajax","subtitle":"Latest / Popular / Comments","formData":{"title":["Latest","Popular","Comments"],"data":["latest","popular","recent_comments"],"sort":["comments","views","comments"],"time_range":["all","all","all"],"category":[1,1,1],"tags":["","",""],"top_review":[0,0,0],"post_ids":["","",""],"posts_counts":[0,0,0],"comment_excerpt":[50,50,80],"count":[4,4,4],"date_format":["F j, Y","F j, Y","F j, Y"],"ignore_sticky":[1,1,1],"meta_date":[0,0,0],"meta_format":[0,0,0],"meta_category":[1,1,1],"meta_categories":[0,0,0],"meta_author":[0,0,0],"meta_comments":[1,1,1],"meta_views":[1,1,1],"meta_review_score":[0,0,0]}},{"type":"widget","modal":"#fsmpb-modal-widget_warta_tabs","title":"Tabs","args":{"php_class":"Warta_Tabs","css_class":"widget_warta_tabs"},"action":"fsmpb_widget_form_submit_ajax","subtitle":"Top Reviews","formData":{"title":["Top Reviews"],"data":["review"],"sort":["comments"],"time_range":["all"],"category":[1],"tags":[""],"top_review":[1],"post_ids":[""],"posts_counts":[0],"comment_excerpt":[50],"count":[4],"date_format":["F j, Y"],"display_featured_image":[0],"ignore_sticky":[1],"meta_date":[0],"meta_format":[0],"meta_category":[1],"meta_categories":[0],"meta_author":[0],"meta_comments":[1],"meta_views":[1],"meta_review_score":[1]}},{"type":"widget","modal":"#fsmpb-modal-widget_warta_feedburner","title":"Feedburner","args":{"php_class":"Warta_Feedburner","css_class":"widget_warta_feedburner"},"action":"fsmpb_widget_form_ajax","subtitle":"Feedburner","formData":{"title":"Feedburner","feedburner_id":"friskamax/CKyn"}},{"type":"widget","modal":"#fsmpb-modal-widget_warta_ads","title":"Advertisement","args":{"php_class":"Warta_Advertisement","css_class":"widget_warta_ads"},"action":"fsmpb_widget_form_ajax","subtitle":"Advertisement","formData":{"title":"Advertisement","ad_type":"img","img_url":"http://placehold.it/300x250/f0f0f9","ad_url":"http://themeforest.net/item/warta-newsmagazine-wordpress-theme/7066039?ref=friskamax","ad_code":"","hide_on_mobile":1}},{"type":"widget","modal":"#fsmpb-modal-widget_text","title":"Text","args":{"php_class":"WP_Widget_Text","css_class":"widget_text"},"action":"fsmpb_widget_form_ajax","subtitle":"Youtube Video","formData":{"title":"Youtube Video","text":"<iframe width=\"360\" height=\"203\" src=\"//www.youtube.com/embed/3AhivlZO0us\"  allowfullscreen></iframe>","filter":1}},{"type":"widget","modal":"#fsmpb-modal-widget_warta_twitter_feed","title":"Twitter Feed","args":{"php_class":"Warta_Twitter_Feed","css_class":"widget_warta_twitter_feed"},"action":"fsmpb_widget_form_ajax","subtitle":"Twitter Feed","formData":{"title":"Twitter Feed","appearance":"avatar","display_date":1,"display_reply":1,"username":"","count":4,"date_format":"%B %d, %Y"}},{"type":"widget","modal":"#fsmpb-modal-widget_warta_flickr_feed","title":"Flickr Feed","args":{"php_class":"Warta_Flickr_Feed","css_class":"widget_warta_flickr_feed"},"action":"fsmpb_widget_form_ajax","subtitle":"Flickr Feed","formData":{"title":"Flickr Feed","id":"112356465@N05","ids":"","tags":"","tagmode":"all","count":8}},{"type":"widget","modal":"#fsmpb-modal-widget_warta_gallery_carousel","title":"Gallery Carousel","args":{"php_class":"Warta_Gallery_Carousel","css_class":"widget_warta_gallery_carousel"},"action":"fsmpb_widget_form_ajax","subtitle":"Gallery","formData":{"title":"Gallery","gallery_post":1750,"caption_length":60,"hide_mobile":1}},{"type":"widget","modal":"#fsmpb-modal-widget_warta_ads","title":"Advertisement","args":{"php_class":"Warta_Advertisement","css_class":"widget_warta_ads"},"action":"fsmpb_widget_form_ajax","subtitle":"Advertisement","formData":{"title":"Advertisement","ad_type":"img","img_url":"http://placehold.it/300x250/f0f0f9","ad_url":"http://themeforest.net/item/warta-newsmagazine-wordpress-theme/7066039?ref=friskamax","ad_code":"","hide_on_mobile":1},"sortable.preventClickEvent":true},{"type":"widget","modal":"#fsmpb-modal-widget_warta_tabs","title":"Tabs","args":{"php_class":"Warta_Tabs","css_class":"widget_warta_tabs"},"action":"fsmpb_widget_form_ajax","subtitle":"Categories / Popular Tags","formData":{"title":["Categories","Popular Tags"],"data":["list_categories","popular_tags"],"sort":["comments","comments"],"time_range":["all","all"],"category":[1,1],"tags":["",""],"top_review":[1,1],"post_ids":["",""],"posts_counts":[1,1],"comment_excerpt":[50,50],"count":[4,40],"date_format":["F j, Y","F j, Y"],"ignore_sticky":[1,1],"meta_date":[0,0],"meta_format":[0,0],"meta_category":[1,1],"meta_categories":[0,0],"meta_author":[0,0],"meta_comments":[1,1],"meta_views":[1,1],"meta_review_score":[1,1]},"sortable.preventClickEvent":true}]}]]}],[{"width":"12","subRows":[[{"width":"12","elements":[{"type":"widget","modal":"#fsmpb-modal-widget_warta_slider_tabs","title":"Slider Tabs","args":{"php_class":"Warta_Slider_Tabs","css_class":"widget_warta_slider_tabs"},"action":"fsmpb_widget_form_submit_ajax","subtitle":"Gallery","formData":{"title":["Gallery"],"data":["gallery"],"category":["general"],"tags":["headline"],"gallery_post":[2149],"caption_length":[60],"sort":["comments"],"time_range":["all"],"post_ids":[""],"top_review":[0],"count":[18],"date_format":["M j, Y"],"ignore_sticky":[1],"meta_date":[1],"meta_format":[0],"meta_category":[1],"meta_author":[0],"meta_comments":[0],"meta_views":[0],"meta_review_score":[0],"hide_mobile":1}}]}]]}]]
 */
/**
 * Element management 
 * 
 * @requires jQuery
 * @see {@link http://jquery.com/}
 * 
 * @since 1.0.0
 * @package friskamax-library\page-builder\js\element
 * 
 * @param {jQuery} $
 * @param {object} F
 * @param {object} window
 * @param {object} document
 * @param {undefined} undefined
 */
; ( function( $, F, window, document, undefined ) { 'use strict';
        
        /**
         * FsmpbElement constructor.
         * 
         * @since 1.0.0
         * 
         * @param {object} Parent       Instance of Fsmpb (main page builder class).
         */
        F.FsmpbElement = function( Parent ) {
                this.Parent                     = Parent;
                
                this.$modalInsertElement        = $( '#fsmpb-modal-insert-element' );
                
                this.listen();
        };
        
        /**
         * Listen events.
         * 
         * @since 1.0.0
         */
        F.FsmpbElement.prototype.listen = function() {
                var self = this;
                                        
                /* -------------------------------------------------------------------------- *
                 * Modal
                 * -------------------------------------------------------------------------- */
                
                // Element chooser.
                this.$modalInsertElement
                        .on( 'shown.bs.modal', function() { // Helper.resizeElementsHeight() on first tab
                                var $wrapper = $( this ).find( '.tab-pane.active' );
                                self.Parent.Helper.resizeElementsHeight( $wrapper );
                        } )
                        .find( 'a[data-toggle="fsmh-tab"]' ).on( 'shown.bs.tab', function ( event ) { // Helper.resizeElementsHeight() on new activated tab
                                var target = $( event.target ).attr( 'href' );
                                self.Parent.Helper.resizeElementsHeight( $( target ) );
                        } )
                        .end()
                        .on( 'click', '.element', function() { // modal() : show element's form
                                self.modal( $( this ).data() );
                        } );
                        
                /* -------------------------------------------------------------------------- *
                 * Content
                 * -------------------------------------------------------------------------- */
                        
                // Panel.
                this.Parent.$mainContent.on( 'click.fsmpb', '.panel', { self: this }, $.proxy( this.selectPanel ) );
        };
        
        /**
         * Submit new element form.
         * 
         * @since 1.0.0
         * 
         * @param {object} $modal 
         * @param {object} data         Element's data
         * @param {object} $oldElement 
         */
        F.FsmpbElement.prototype.insert = function( $modal, data, $oldElement ) {
                var     self            = this,
                        elementData     = $.extend( {}, data ); // Lets not overwrite the original object.
                
                $modal.on( 'submit.fsmpb', function( event ) {                                          
                        var $this = $( this );  
                        
                        elementData.oldFormData        = $oldElement ? $oldElement.data( 'formData' ) : null;
                        elementData.formData           = $this.serialize();
                        
                        // Set ajax action sanitize function
                        switch( elementData.type ) {
                                case 'widget':
                                        elementData.action = 'fsmpb_widget_sanitize';
                                        break;
                                case 'custom':
                                        elementData.action = 'fsmpb_custom_sanitize_' + elementData.args.php_class;
                                        break;

                                // old version
                                case 'widget_area':
                                        elementData.action = 'fsmpb_custom_sanitize_Fsmpb_Element_Widget_Area';
                                        break;
                        }
                                                 
                        // Sanitize data and insert the element
                        $.post( ajaxurl, elementData, function( response ) {               
                                // Remove unnecessary data
                                delete elementData.oldFormData;
                                delete elementData.action;
                                
                                elementData.formData    = F.Helper.isJSON( response ) ? $.parseJSON( response ) : {};   
                                
                                var     element         = self.Parent.templateElement
                                                                .replace( /{{title}}/ig, fsmpbElementsInfo[ elementData.args.php_class ].name )
                                                                .replace( /{{subtitle}}/ig, self.Parent.Helper.getElementSubtitle( elementData.formData ) ),
                                        $element        = $( element ).data( elementData );                                        
                                                
                                // Insert the element
                                if( $oldElement ) {
                                        $oldElement.replaceWith( $element );
                                } else {
                                        self.Parent.Helper.getColumnTarget().append( $element );
                                        $element.hide().slideDown();
                                }
                                
                                self.Parent.Helper.sortable();
                                self.Parent.$modalLoading.fsmhBsModal( 'hide' ); 
                        } );
                        
                        $this.fsmhBsModal( 'hide' );
                        self.Parent.$modalLoading.fsmhBsModal( 'show' );                                
                        event.preventDefault();
                } ).on( 'hidden.bs.modal', function() {
                        $( this ).parent().remove();
                } );
        };
        
        /**
         * Show Insert Element form.
         * 
         * @since 1.0.0
         * @private
         * 
         * @param {object} data         Element's data.
         * @param {object} $oldElement  Old element.
         */
        F.FsmpbElement.prototype.modal = function( data, $oldElement ) {                
                var     self            = this,
                        elementData     = $.extend( {}, data ); // Lets not overwrite the original element's data.
                
                this.$modalInsertElement.fsmhBsModal( 'hide' );
                this.Parent.$modalLoading.fsmhBsModal( 'show' );
                
                // remove unnecessary sortable data
                delete elementData.sortable;
                delete elementData.sortableItem;
                
                // Set ajax action function
                switch( elementData.type ) {
                        case 'widget':
                                elementData.action = 'fsmpb_widget_form';
                                break;
                        case 'custom':
                                elementData.action = 'fsmpb_custom_form_' + elementData.args.php_class;
                                break;
                                
                        // old version
                        case 'widget_area':
                                elementData.action = 'fsmpb_custom_form_Fsmpb_Element_Widget_Area';
                                break;
                }
                
                // Sending values as POST parameters will be treated as simple text strings,
                // so any boolean values will be converted to string "true" or "false". 
                // That will cause a problem for many widgets, so we use JSON.
                elementData.formData = JSON.stringify( elementData.formData );
                         
                // Get the form
                $.post( ajaxurl, elementData, function( response ) {
                        var $response = $( $.trim( response ) );
                        
                        self.Parent.$body.append( $response );                                                
                        self.Parent.$modalLoading.fsmhBsModal( 'hide' );
                        self.insert(
                                $response.find( '.modal' ).fsmhBsModal( 'show' ), 
                                elementData, 
                                $oldElement
                        );       
                } ); 
        };
        
        /**
         * Adds selected class to panel.
         * 
         * @since 1.0.0
         * @private
         * 
         * @param {object} event Event object.
         */
        F.FsmpbElement.prototype.selectPanel = function( event ) {   
                var     self    = event.data.self,
                        $this   = $( this );
                
                if ( $this.is( $( event.target ).closest( '.panel' ) ) ) {       
                        self.Parent.$mainContent.find( '.panel' ).removeClass( 'selected' );
                        $this.addClass( 'selected' );
                }
        };
        
} ) ( jQuery, window.FriskaMax = window.FriskaMax || {}, window, document );
+function( $, F ) { 'use strict';
        
        F.FsmpbHelper = function( Parent ) {
                this.Parent                             = Parent;
                
                this.$btnInsertSubRow                   = $( '#fsmpb-btn-insert-sub-row' );
                
                this.selectorRowElements                = '> .row > div > .elements';
                this.selectorRowElementsSelected        = '> .row > div > .elements.selected';
                this.selectorSubRowElements             = '.sub-row .elements';
                this.selectorSubRowElementsSelected     = '.sub-row .elements.selected';                
        };
        
        /* -------------------------------------------------------------------------- *
         * General
         * -------------------------------------------------------------------------- */
        
        /**
         * Make Elements, columns and rows sortable.
         */
        F.FsmpbHelper.prototype.sortable = function() {
                var     $rows           = this.Parent.$mainContent.find( '.row-wrapper > .panel-body > .row' ),
                        $columns        = this.Parent.$mainContent.find( '.column-wrapper > .panel > .panel-body' ); 
                
                this.Parent.$mainContent.sortable( {
                        connectWith     : $columns,
                        placeholder     : 'placeholder',
                        handle          : '.panel-heading',
                        start           : function( event, ui ) {
                                ui.placeholder.height( ui.helper.outerHeight() );
                        }
                } );
                
                $rows.sortable( {
                        helper                  : 'clone',
                        connectWith             : $rows,
                        placeholder             : 'placeholder',
                        handle                  : '.panel-heading',
                        start                   : function( event, ui ) {
                                ui.placeholder.attr( 'class', ui.item.attr( 'class' ) + ' placeholder' );
                                ui.placeholder.height( ui.helper.find('> .panel').outerHeight() );
                        }
                } );
                
                $columns.sortable( {
                        connectWith             : $columns.add( this.Parent.$mainContent ),
                        placeholder             : 'placeholder',
                        handle                  : '.panel-heading',
                        start                   : function( event, ui ) {
                                ui.placeholder.height( ui.helper.outerHeight() );
                        }
                } );
        };
        
        /* -------------------------------------------------------------------------- *
         * Element
         * -------------------------------------------------------------------------- */
        
        /**
         * Returns element's subtitle from element settings.
         * @param {object} formData             The element settings.
         * @returns {formData.title|String}     The element title.
         */
        F.FsmpbHelper.prototype.getElementSubtitle = function( formData ) {
                var subtitle = ''; 
                                
                if ( typeof formData.title !== 'undefined' ) {
                        if ( typeof formData.title === 'string' ) {
                                subtitle = formData.title;
                        } else if ( $.isArray( formData.title ) ) {
                                subtitle = formData.title.join( ' / ' );
                        }
                }
                
                return subtitle;
        };
        
        /* -------------------------------------------------------------------------- *
         * Element selector
         * -------------------------------------------------------------------------- */
                
        /**
         * Resize element's height.
         * 
         * @param {object} $wrapper Element's wrapper.
         */
        F.FsmpbHelper.prototype.resizeElementsHeight = function( $wrapper ) {
                var height  = 0;                        

                $wrapper.find( '.element' ).each( function() {
                        var thisHeight = $( this ).height();                                  
                        
                        height = ( thisHeight > height ) ? thisHeight : height;
                } ).height( height );
        };
        
        /* -------------------------------------------------------------------------- *
         * Column
         * -------------------------------------------------------------------------- */
        
        /**
         * Get column target to insert the new element.
         * @returns {jQuery}
         */
        F.FsmpbHelper.prototype.getColumnTarget = function() {
                var $selectedColumn = this.Parent.$mainContent.find( '.column-wrapper > .panel.selected > .panel-body' );
                
                return $selectedColumn.length ? $selectedColumn : this.Parent.$mainContent;
        };
        
} ( jQuery, window.FriskaMax = window.FriskaMax || {} );
/**
 * Page builder control.
 * 
 * @since 1.0.0
 * 
 * @param {jQuery} $
 * @param {object} F
 * @param {undefined} undefined
 */

; ( function( $, F, undefined ) { 'use strict';
        
        F.FsmpbControl = function( Parent ) {
                this.Parent     = Parent;
                
                this.listen();
        };
        
        /**
         * Listen events.
         */
        F.FsmpbControl.prototype.listen = function() {
                var self = this;
                
                // Toggle.
                this.Parent.$mainContent.on( 'click.FsmpbControl.toggle', '.panel-heading > .panel-title', $.proxy( this.toggle ) );
                
                // Delete button.
                this.Parent.$mainContent.on( 'click.FsmpbControl.delete', '.control .delete', $.proxy( this.delete ) );
                
                // Duplicate button.
                this.Parent.$mainContent.on( 'click.FsmpbControl.duplicate', '.control .duplicate', { self: this }, $.proxy( this.duplicate ) );
                
                // Edit button.
                this.Parent.$mainContent.on( 'click.FsmpbControl.edit', '.control .edit', function( event ) {
                        self.edit.call( this, self, event );
                } );
                
                // Increase/decrease width button.
                this.Parent.$mainContent.on( 'click.FsmpbControl.increaseDecrease', '.control .increase, .control .decrease', function( event ) {
                        self.increaseDecrease.call( this, self, event );
                } );
        };       
        
        /**
         * Delete a row, column or element.
         * 
         * @since 1.0.0
         * @private
         * 
         * @param {object} event Event Object.
         */
        F.FsmpbControl.prototype.delete = function( event ) {
                var $this = $( this );
                
                if ( confirm( fsmpbLang.msg.delete ) ) {
                        $this.closest( '.element-wrapper, .column-wrapper, .row-wrapper' ).slideUp( 400, function () {
                                $( this ).remove();
                        } );                        
                }        

                event.preventDefault();
        };
        
        /**
         * Duplicate row or element.
         * 
         * @since 1.0.0
         * @private
         * 
         * @param {object} event Event Object.
         */
        F.FsmpbControl.prototype.duplicate = function( event ) { 
                var     self            = event.data.self,
                        $element        = $( this ).closest( '.element-wrapper, .column-wrapper, .row-wrapper' );
                        
                // Destroy the sortable before deep clone the element
                self.Parent.$mainContent.add( self.Parent.$mainContent.find( '.ui-sortable' ) ).sortable( 'destroy' );

                $element.clone( true )
                        .hide()
                        .insertAfter( $element )
                        .slideDown()
                        .find( '.selected' ).removeClass( 'selected' );

                self.Parent.Helper.sortable();                       
                event.preventDefault();
        };
        
        /**
         * Edit element.
         * 
         * @param {object} self Instance of Fsmpb.
         * @param {object} event Event Object.
         */
        F.FsmpbControl.prototype.edit = function( self, event ) {
                var $element = $( this ).closest( '.element-wrapper, .column-wrapper' );

                if ( $element.is( '.element-wrapper' ) ) {
                        self.Parent.Element.modal( $element.data(), $element );
                } else if( $element.is( '.column-wrapper') ) {
                        self.Parent.Column.modal( $element );
                }
                
                event.preventDefault();
        };
        
        /**
         * Toggle show/hide the panel body.
         * 
         * @param {object} event Event Object.
         */
        F.FsmpbControl.prototype.toggle = function( event ) {
                var     $panel_heading  = $( this ).parent( '.panel-heading' ),
                        $panel_body     = $panel_heading.next( '.panel-body' ),
                        $panel          = $panel_heading.parent( '.panel' );
                
                // Remove min height to avoid jerky animation.
                // http://blog.markstarkman.com/blog/2013/07/25/jquery-jerky-slideup-and-slidedown-with-twitter-bootstrap-well/
                
                if ( $panel_body.is( ':visible' ) && $panel.is( '.selected' ) ) {
                        $panel_body.css( {
                                height          : $panel_body.outerHeight(),
                                minHeight       : 0
                        } );
                        
                        $panel_body.slideUp( 400 );
                        event.preventDefault();
                } else if ( $panel_body.is( ':hidden' ) ) {
                        $panel_body.slideDown( 400, function() {
                                $panel_body.css( {
                                        height          : '',
                                        minHeight       : ''
                                } );
                        } );
                        
                        event.preventDefault();
                }
                
        };
        
        /**
         * increase/decrease the column width..
         * 
         * @param {object} self Instance of Fsmpb.
         * @param {object} event Event Object.
         */
        F.FsmpbControl.prototype.increaseDecrease = function( self, event ) {
                var     $this           = $( this ),
                        $wrapper        = $this.closest( '.column-wrapper' );
                
                if ( $this.is( '.increase' ) ) {
                        self.Parent.Column.increaseWidth( $wrapper );
                } else {
                        self.Parent.Column.decreaseWidth( $wrapper );
                }
                
                event.preventDefault();
        };
        
} ) ( jQuery, window.FriskaMax = window.FriskaMax || {} );
+function( $, F ) { 'use strict';
        
        F.FsmpbRow = function( Parent ) {
                this.Parent             = Parent;                
                this.$modalInsertRow    = $( '#fsmpb-modal-insert-row' );
                
                this.modal();
        };
        
        /**
         * Insert a new row.
         * 
         * @since 1.0.0
         * @private
         * 
         * @param {array} colWidths
         */
        F.FsmpbRow.prototype.insert = function( colWidths ) {
                var     self            = this,
                        $templateRow    = $( self.Parent.templateRow ),
                        $row            = $templateRow.find( '> .panel-body > .row' ); 
                                                
                $.each( colWidths, function( index, value ){       
                        var     $templateCol    = $( self.Parent.templateCol ),
                                columnWidth     = 'col-' + self.Parent.Column.getBreakpoint() + '-' + value;
                                
                        $row.append( $templateCol.addClass( columnWidth ) );
                } );
                
                this.Parent.Helper.getColumnTarget().append( $templateRow );
                this.Parent.Helper.sortable();
                
                $templateRow.hide().slideDown();
        };
        
        /**
         * Modal: Insert new row
         */
        F.FsmpbRow.prototype.modal = function() {
                var self = this;
                
                this.$modalInsertRow.each(function() {                        
                        var     $this           = $(this),
                                $fieldRow       = $this.find( '[name="fsmpb[row]"]' ),
                                $choices        = $this.find( '.columns > div' ),
                                $button         = $this.find( 'button[data-insert]' );
                                
                        // Adds selected class
                        $choices.click( function() {
                                var $thisChoice = $(this);

                                $choices.removeClass( 'selected' );
                                $thisChoice.addClass( 'selected' );
                                $fieldRow.val( $thisChoice.attr( 'title' ) ).focus();
                        } );

                        // Insert the row
                        $button.click( function() {
                                var     colWidths       = $fieldRow.val().split( '-' ),
                                        sum             = 0;

                                // counts the custom row layout
                                $.each( colWidths, function( index, value ) {  
                                        sum += parseInt( value ); 
                                } );

                                if( sum !== 12 ) { // checks the custom row layout
                                        alert( fsmpbLang.msg.invalidValue );
                                } else {
                                        self.insert( colWidths );                                
                                        $this.fsmhBsModal( 'hide' );
                                }
                        } );
                
                        // Submit on enter
                        $this.keypress( function ( event ) {
                                if( event.which === 13 ) {
                                        $button.trigger( 'click' );                                
                                        event.preventDefault();
                                }
                        });
                });
        };
        
} ( jQuery, window.FriskaMax = window.FriskaMax || {} );
+function( $, F ) { 'use strict';        
        
        var Fsmpb = function() {
                this.$body              = $( 'body' );
                this.$container         = $( '#fsmpb-container' );
                this.$mainContent       = $( '#fsmpb-main-content' );
                
                this.$modalLoading      = $( '#fsmpb-modal-loading' );
                
                this.templateElement    = $.trim( $( '#fsmpb-template-element' ).html() );
                this.templateCol        = $.trim( $( '#fsmpb-template-col' ).html() );
                this.templateRow        = $.trim( $( '#fsmpb-template-row' ).html() );
                
                this.Helper             = new F.FsmpbHelper( this );
                this.Control            = new F.FsmpbControl( this );
                this.Row                = new F.FsmpbRow( this );
                this.Column             = new F.FsmpbColumn( this );
                this.Element            = new F.FsmpbElement( this );
                this.Data               = new F.FsmpbData( this );
        };    
        
        F.Instances             = F.Instances || {};
        F.Instances.fsmpb       = new Fsmpb();
                        
} ( jQuery, window.FriskaMax = window.FriskaMax || {} );
+function( $ ) { 'use strict';
        
        var FsmpbSwitch = function() {
                this.$button            = $( '#fsmpb-switch' );
                this.$pageTemplate      = $( '#page_template' );
                                
                this.pageTemplateChange();
                this.click();
                this.init();
        };
        
        /**
         * Show/hide page builder on page template change
         * ==============================================
         */
        FsmpbSwitch.prototype.pageTemplateChange = function() {               
                var self = this;
                
                this.$pageTemplate.on( 'change.fsmpb.switch', function() {
                        if ( fsmpbPageTemplates[ $( this ).val() ] ) {
                                self.$button.show();
                        } else {
                                if( self.$button.hasClass( 'fsmpb-active' ) ) {
                                        self.$button.trigger( 'click' );
                                }                            
                                
                                self.$button.hide();
                        }
                } ).trigger( 'change.fsmpb.switch' );
        };
        
        /**
         * Toggle show/hide page builder.
         * 
         * @since 1.0.0
         * @private
         */
        FsmpbSwitch.prototype.click = function() {                
                this.$button.click( function() {
                        var $this = $( this ).toggleClass( 'fsmpb-active' );
                                                
                        $( 'body' ).toggleClass( 'fsmpb' );
                                                
                        if ( $this.hasClass( 'fsmpb-active' ) ) {                               
                                $this.html( '<span class="dashicons dashicons-grid-view"></span> ' + fsmpbLang.exitPageBuilder );
                        } else {
                                $this.html( '<span class="dashicons dashicons-grid-view"></span> ' + fsmpbLang.pageBuilder );
                        }
                } );
        };
        
        FsmpbSwitch.prototype.init = function() {
                if( $( '#fsmpb-main-content .row' ).length && fsmpbPageTemplates[ this.$pageTemplate.val() ] ) {
                        this.$button.trigger( 'click' );
                }
        };
        
        var fsmpbSwitch = new FsmpbSwitch();
        
        
}( jQuery );
/**
 * Created on : Mar 28, 2014, 4:04:10 PM
 * Author     : Fahri
 */
+function( $ ) { 'use strict';
        
        var FsmpbFullScreen = function() {
                this.$body      = $( 'body' );
                this.$metaBox   = $( '#fsmpb_meta_box .inside' );
                this.$container = $( '#fsmpb-container' );
                this.$button    = this.$container.find( '[data-toggle="full-screen"]' );
                this.$adminMenus= $( '#wpadminbar, #adminmenuback, #adminmenuwrap' );
                 
                this.click();
        };
        
        FsmpbFullScreen.prototype.click = function() {
                var self = this;
                
                this.$button.click(function() {
                        var $this = $(this);
                        
                        self.$container.toggleClass('full-screen');
                        self.$adminMenus.toggle();
                        
                        if( self.$container.hasClass('full-screen') ) { // Activate full screen view                 
                                self.$container.detach().appendTo('body');                                
                                self.$body.addClass( 'fsmpb-full-screen' ); 
                                
                                $this.text(fsmpbLang.exitFullScreen);
                        } else { // Deactivate full screen view
                                self.$container.detach().appendTo( self.$metaBox );     
                                self.$body.removeClass( 'fsmpb-full-screen' ); // Disable scrolling on the body
                                
                                $this.text(fsmpbLang.fullScreen);
                        }
                });
        };
        
        var fsmpbFullScreen = new FsmpbFullScreen;
        
}( jQuery );