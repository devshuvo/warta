+function( $, F ) { 'use strict';
        
        F.FsmpbRow = function( Parent ) {
                this.Parent             = Parent;                
                this.$modalInsertRow    = $( '#fsmpb-modal-insert-row' );
                
                this.modal();
        };
        
        /**
         * Insert a new row.
         * 
         * @since 1.0.0
         * @private
         * 
         * @param {array} colWidths
         */
        F.FsmpbRow.prototype.insert = function( colWidths ) {
                var     self            = this,
                        $templateRow    = $( self.Parent.templateRow ),
                        $row            = $templateRow.find( '> .panel-body > .row' ); 
                                                
                $.each( colWidths, function( index, value ){       
                        var     $templateCol    = $( self.Parent.templateCol ),
                                columnWidth     = 'col-' + self.Parent.Column.getBreakpoint() + '-' + value;
                                
                        $row.append( $templateCol.addClass( columnWidth ) );
                } );
                
                this.Parent.Helper.getColumnTarget().append( $templateRow );
                this.Parent.Helper.sortable();
                
                $templateRow.hide().slideDown();
        };
        
        /**
         * Modal: Insert new row
         */
        F.FsmpbRow.prototype.modal = function() {
                var self = this;
                
                this.$modalInsertRow.each(function() {                        
                        var     $this           = $(this),
                                $fieldRow       = $this.find( '[name="fsmpb[row]"]' ),
                                $choices        = $this.find( '.columns > div' ),
                                $button         = $this.find( 'button[data-insert]' );
                                
                        // Adds selected class
                        $choices.click( function() {
                                var $thisChoice = $(this);

                                $choices.removeClass( 'selected' );
                                $thisChoice.addClass( 'selected' );
                                $fieldRow.val( $thisChoice.attr( 'title' ) ).focus();
                        } );

                        // Insert the row
                        $button.click( function() {
                                var     colWidths       = $fieldRow.val().split( '-' ),
                                        sum             = 0;

                                // counts the custom row layout
                                $.each( colWidths, function( index, value ) {  
                                        sum += parseInt( value ); 
                                } );

                                if( sum !== 12 ) { // checks the custom row layout
                                        alert( fsmpbLang.msg.invalidValue );
                                } else {
                                        self.insert( colWidths );                                
                                        $this.fsmhBsModal( 'hide' );
                                }
                        } );
                
                        // Submit on enter
                        $this.keypress( function ( event ) {
                                if( event.which === 13 ) {
                                        $button.trigger( 'click' );                                
                                        event.preventDefault();
                                }
                        });
                });
        };
        
} ( jQuery, window.FriskaMax = window.FriskaMax || {} );