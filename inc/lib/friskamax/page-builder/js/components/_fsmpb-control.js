/**
 * Page builder control.
 * 
 * @since 1.0.0
 * 
 * @param {jQuery} $
 * @param {object} F
 * @param {undefined} undefined
 */

; ( function( $, F, undefined ) { 'use strict';
        
        F.FsmpbControl = function( Parent ) {
                this.Parent     = Parent;
                
                this.listen();
        };
        
        /**
         * Listen events.
         */
        F.FsmpbControl.prototype.listen = function() {
                var self = this;
                
                // Toggle.
                this.Parent.$mainContent.on( 'click.FsmpbControl.toggle', '.panel-heading > .panel-title', $.proxy( this.toggle ) );
                
                // Delete button.
                this.Parent.$mainContent.on( 'click.FsmpbControl.delete', '.control .delete', $.proxy( this.delete ) );
                
                // Duplicate button.
                this.Parent.$mainContent.on( 'click.FsmpbControl.duplicate', '.control .duplicate', { self: this }, $.proxy( this.duplicate ) );
                
                // Edit button.
                this.Parent.$mainContent.on( 'click.FsmpbControl.edit', '.control .edit', function( event ) {
                        self.edit.call( this, self, event );
                } );
                
                // Increase/decrease width button.
                this.Parent.$mainContent.on( 'click.FsmpbControl.increaseDecrease', '.control .increase, .control .decrease', function( event ) {
                        self.increaseDecrease.call( this, self, event );
                } );
        };       
        
        /**
         * Delete a row, column or element.
         * 
         * @since 1.0.0
         * @private
         * 
         * @param {object} event Event Object.
         */
        F.FsmpbControl.prototype.delete = function( event ) {
                var $this = $( this );
                
                if ( confirm( fsmpbLang.msg.delete ) ) {
                        $this.closest( '.element-wrapper, .column-wrapper, .row-wrapper' ).slideUp( 400, function () {
                                $( this ).remove();
                        } );                        
                }        

                event.preventDefault();
        };
        
        /**
         * Duplicate row or element.
         * 
         * @since 1.0.0
         * @private
         * 
         * @param {object} event Event Object.
         */
        F.FsmpbControl.prototype.duplicate = function( event ) { 
                var     self            = event.data.self,
                        $element        = $( this ).closest( '.element-wrapper, .column-wrapper, .row-wrapper' );
                        
                // Destroy the sortable before deep clone the element
                self.Parent.$mainContent.add( self.Parent.$mainContent.find( '.ui-sortable' ) ).sortable( 'destroy' );

                $element.clone( true )
                        .hide()
                        .insertAfter( $element )
                        .slideDown()
                        .find( '.selected' ).removeClass( 'selected' );

                self.Parent.Helper.sortable();                       
                event.preventDefault();
        };
        
        /**
         * Edit element.
         * 
         * @param {object} self Instance of Fsmpb.
         * @param {object} event Event Object.
         */
        F.FsmpbControl.prototype.edit = function( self, event ) {
                var $element = $( this ).closest( '.element-wrapper, .column-wrapper' );

                if ( $element.is( '.element-wrapper' ) ) {
                        self.Parent.Element.modal( $element.data(), $element );
                } else if( $element.is( '.column-wrapper') ) {
                        self.Parent.Column.modal( $element );
                }
                
                event.preventDefault();
        };
        
        /**
         * Toggle show/hide the panel body.
         * 
         * @param {object} event Event Object.
         */
        F.FsmpbControl.prototype.toggle = function( event ) {
                var     $panel_heading  = $( this ).parent( '.panel-heading' ),
                        $panel_body     = $panel_heading.next( '.panel-body' ),
                        $panel          = $panel_heading.parent( '.panel' );
                
                // Remove min height to avoid jerky animation.
                // http://blog.markstarkman.com/blog/2013/07/25/jquery-jerky-slideup-and-slidedown-with-twitter-bootstrap-well/
                
                if ( $panel_body.is( ':visible' ) && $panel.is( '.selected' ) ) {
                        $panel_body.css( {
                                height          : $panel_body.outerHeight(),
                                minHeight       : 0
                        } );
                        
                        $panel_body.slideUp( 400 );
                        event.preventDefault();
                } else if ( $panel_body.is( ':hidden' ) ) {
                        $panel_body.slideDown( 400, function() {
                                $panel_body.css( {
                                        height          : '',
                                        minHeight       : ''
                                } );
                        } );
                        
                        event.preventDefault();
                }
                
        };
        
        /**
         * increase/decrease the column width..
         * 
         * @param {object} self Instance of Fsmpb.
         * @param {object} event Event Object.
         */
        F.FsmpbControl.prototype.increaseDecrease = function( self, event ) {
                var     $this           = $( this ),
                        $wrapper        = $this.closest( '.column-wrapper' );
                
                if ( $this.is( '.increase' ) ) {
                        self.Parent.Column.increaseWidth( $wrapper );
                } else {
                        self.Parent.Column.decreaseWidth( $wrapper );
                }
                
                event.preventDefault();
        };
        
} ) ( jQuery, window.FriskaMax = window.FriskaMax || {} );