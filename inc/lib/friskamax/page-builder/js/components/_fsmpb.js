+function( $, F ) { 'use strict';        
        
        var Fsmpb = function() {
                this.$body              = $( 'body' );
                this.$container         = $( '#fsmpb-container' );
                this.$mainContent       = $( '#fsmpb-main-content' );
                
                this.$modalLoading      = $( '#fsmpb-modal-loading' );
                
                this.templateElement    = $.trim( $( '#fsmpb-template-element' ).html() );
                this.templateCol        = $.trim( $( '#fsmpb-template-col' ).html() );
                this.templateRow        = $.trim( $( '#fsmpb-template-row' ).html() );
                
                this.Helper             = new F.FsmpbHelper( this );
                this.Control            = new F.FsmpbControl( this );
                this.Row                = new F.FsmpbRow( this );
                this.Column             = new F.FsmpbColumn( this );
                this.Element            = new F.FsmpbElement( this );
                this.Data               = new F.FsmpbData( this );
        };    
        
        F.Instances             = F.Instances || {};
        F.Instances.fsmpb       = new Fsmpb();
                        
} ( jQuery, window.FriskaMax = window.FriskaMax || {} );