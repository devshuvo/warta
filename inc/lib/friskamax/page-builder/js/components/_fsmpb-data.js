+function( $, F ) { 'use strict';
        
        F.FsmpbData = function( Parent ) {
                this.Parent     = Parent;
                this.$fieldMain = $( '[name=fsmpb_main]' );
                
                this.listen(); 
                this.generateLayout(); 
        };
        
        /**
         * Listen events.
         */
        F.FsmpbData.prototype.listen = function() {
                // Submit the Post
                $( '#post' ).on( 'submit.fsmpb', $.proxy( this.submit, this ) );
        };   
        
        /**
         * Submit the data.
         * 
         * @param {Event} event
         */
        F.FsmpbData.prototype.submit = function( event ) {                
                // WordPress required either post title or post content to be filled in order the data can be saved.
                if( this.getData() && ! $( '#title' ).val() ) {
                        alert( fsmpbLang.msg.enterTitle );
                        
                        $( '#title' ).focus();
                        $( '#publish' )
                                .removeClass( 'button-primary-disabled' )       // Reenable publish button
                                .parent().find( '.spinner' ).hide();            // Hide spinner
                        
                        event.preventDefault();
                }
        };     
        
        /* ========================================================================== *
         * Generate layout
         * ========================================================================== */
        
        /**
         * Generate page builder layout from saved data.
         */
        F.FsmpbData.prototype.generateLayout = function() {
                var     self            = this,
                        dataJSON        = this.$fieldMain.val(),
                        dataArray       = F.Helper.isJSON( dataJSON ) ? $.parseJSON( dataJSON ) : [];
                
                // Return if there is no page builder data or the data is broken.
                if ( ! dataJSON || ! dataArray.length ) return;
                
                if ( $.type( dataArray[0].type )  === 'undefined' ) { // In version > 1.0.1, the outermost element is a row and it doesn't have type property.
                        this.old_generateLayout( dataArray );
                } else {
                        $.each( dataArray, function( index, value ) {
                                self.generateElement( value, self.Parent.$mainContent );
                        } );
                }

                this.Parent.Helper.sortable();  
        };
        
        F.FsmpbData.prototype.generateElement = function( elementData, $container ) {
                if ( elementData.type === 'row' ) {
                        this.generateRow( elementData, $container );
                } else {
                        var $element = $( this.Parent.templateElement ).data( elementData ).appendTo( $container );

                        if ( fsmpbElementsInfo[ elementData.args.php_class ] ) { // class exists
                                $element.html(
                                        $element.html()
                                                .replace( /{{title}}/g, fsmpbElementsInfo[ elementData.args.php_class ].name )
                                                .replace( /{{subtitle}}/g, this.Parent.Helper.getElementSubtitle( elementData.formData ) )
                                );
                        } else { // class doesn't exists
                                $element.hide();
                        }
                }
        };
        
        F.FsmpbData.prototype.generateRow = function( rowData, $container ) {
                var     self            = this,
                        $template       = $( this.Parent.templateRow ).appendTo( $container );

                $.each( rowData.children, function( index, value )  {  
                        self.generateColumn( value, $template.find( '> .panel-body > .row' ) );
                } );
        };
        
        F.FsmpbData.prototype.generateColumn = function( columnData, $container ) {
                var     self            = this,
                        $template       = $( this.Parent.templateCol ).appendTo( $container );

                $.each( columnData.formData, function( index, value ) {
                        $template.addClass( 'col-' + index + '-' +value );
                } );

                $.each( columnData.children, function( index, value ) {      
                        self.generateElement( value, $template.find( '> .panel > .panel-body' ) );                               
                });      
        };
        
        /* ========================================================================== *
         * Get data
         * ========================================================================== */
        
        /**
         * Get page builder data from layout and save it to [name=fsmpb_main].
         * 
         * @returns {JSON}      Page builder data.
         */
        F.FsmpbData.prototype.getData = function() {
                var     self            = this,
                        dataArray       = [],
                        dataJSON        = '';
                
                if ( this.Parent.$mainContent.children().length === 0 ) {
                        this.$fieldMain.val( '' );
                        return;
                }
                
                // Remove any sortable data by removing the sortable functionality.
                // The sortable data is unecessary and can cause error when using JSON.stringify (because there's a method in it).
                this.Parent.$mainContent.add( this.Parent.$mainContent.find( '.ui-sortable' ) ).sortable( 'destroy' );
                
                this.Parent.$mainContent.children().each( function( index, element  ) {
                        var $element = $( element );
                        
                        if ( $element.is( '.row-wrapper' ) ) {
                                dataArray.push( self.getRow( $element ) );
                        } else {
                                dataArray.push( self.getElement( $element ) );                                
                        }
                } );
                                
                dataJSON = dataArray.length ? JSON.stringify( dataArray ) : '';
                this.$fieldMain.val( dataJSON );
                
                this.Parent.Helper.sortable();
                
                return dataJSON;
        };
        
        /**
         * Get row data.
         * 
         * @param {jQuery} $row Row element.
         * @returns {object}    Row data.
         */
        F.FsmpbData.prototype.getRow = function( $row ) {
                var     self            = this,
                        children        = [];
                
                $row.find( '> .panel-body > .row' ).children().each( function( index, element ) {
                        children.push( self.getColumn( $( element ) ) );
                } );
                
                return {
                        type    : 'row',
                        children: children
                };
        };
        
        /**
         * Get column data.
         * 
         * @param {jQuery} $column      Column element.
         * @returns {Object}            Column data.
         */
        F.FsmpbData.prototype.getColumn = function( $column ) {
                var     self            = this,
                        children        = [];
                
                $column
                        .data( 'formData', this.Parent.Column.getColumnData( $column ) )
                        .find( '> .panel > .panel-body' ).children().each( function( index, element ) {
                                var $element = $( element );

                                if ( $element.is( '.row-wrapper' ) ) {
                                        children.push( self.getRow( $element ) );
                                } else {
                                        children.push( self.getElement( $element ) );                                
                                }
                        } );
                
                return $.extend( {
                        type    : 'column',
                        children: children
                }, $column.data() );
        };
        
        /**
         * Get element data.
         * 
         * @param {jQuery} $element     Element.
         * @returns {object}            Element data.
         */
        F.FsmpbData.prototype.getElement = function( $element ) {
                return $element.data();
        };
        
        /* ========================================================================== *
         * Old data
         * ========================================================================== */
        
        /**
         * Generate layut from old data.
         * 
         * @param {array} dataArray     Page builder data.
         */
        F.FsmpbData.prototype.old_generateLayout = function( dataArray ) {
                var self = this;
                                
                $.each( dataArray, function( index, value ) {
                        self.old_generateRow( value, self.Parent.$mainContent );
                } );
        };
        
        F.FsmpbData.prototype.old_generateRow = function( rowData, $container ) {
                var     self            = this,
                        $template       = $( self.Parent.templateRow ).appendTo( $container );
                
                $.each( rowData, function( index, value ) {
                        self.old_generateRowColumn( value, $template.find( '> .panel-body > .row' ) );
                } ); 
        };
        
        F.FsmpbData.prototype.old_generateRowColumn = function( columnData, $container ) {
                var     self            = this,
                        breakpoint      = this.Parent.Column.getBreakpoint(),
                        columnWidth     = 'col-' + breakpoint + '-' + columnData.width,
                        $template       = $( this.Parent.templateCol ).addClass( columnWidth ).appendTo( $container );
                        
                $.each( columnData.subRows, function( index, value ) {      
                        self.old_generateSubRow( value, $template.find( '> .panel > .panel-body' ) );                               
                });      
        };
        
        F.FsmpbData.prototype.old_generateSubRow = function( rowData, $container ) {
                var     self            = this,
                        $template       = $( self.Parent.templateRow ).appendTo( $container );  

                $.each( rowData, function( index, value ) {
                        self.old_generateSubRowColumn( value, $template.find( '> .panel-body > .row' ) );
                } );                                      
                
        };
        
        F.FsmpbData.prototype.old_generateSubRowColumn = function ( columnData, $container ) {
                var     self            = this,
                        breakpoint      = this.Parent.Column.getBreakpoint(),
                        columnWidth     = 'col-' + breakpoint + '-' + columnData.width,
                        $template       = $( this.Parent.templateCol ).addClass( columnWidth ).appendTo( $container );

                $.each( columnData.elements, function( index, value ) {    
                        self.old_generateElement( value, $template.find( '> .panel > .panel-body' )  );  
                });
        };
        
        F.FsmpbData.prototype.old_generateElement = function( elementData, $container ) {
                var $element = $( this.Parent.templateElement ).data( elementData );

                if ( fsmpbElementsInfo[ elementData.args.php_class ] ) { // class exists
                        $element.html(
                                $element.html()
                                        .replace( /{{title}}/ig, fsmpbElementsInfo[ elementData.args.php_class ].name )
                                        .replace( /{{subtitle}}/ig, this.Parent.Helper.getElementSubtitle( elementData.formData ) )
                        );
                } else { // class doesn't exists
                        $element.hide();
                }

                $container.append( $element ); 
        };
        
} ( jQuery, window.FriskaMax = window.FriskaMax || {} );

/**
 * Sample Data
 * 1.0.0 = [[{"width":"8","subRows":[[{"width":"12","elements":[{"type":"widget","modal":"#fsmpb-modal-widget_warta_breaking_news","title":"Breaking News","args":{"php_class":"Warta_Breaking_News","css_class":"widget_warta_breaking_news"},"action":"fsmpb_widget_form_ajax","subtitle":"Breaking News","formData":{"title":"Breaking News","data":"latest","sort":"comments","time_range":"all","top_review":0,"category":1,"tags":"","post_ids":"","count":4,"icon":"fa-angle-double-right","duration":20000,"direction":"left","ignore_sticky":1}},{"type":"widget","modal":"#fsmpb-modal-widget_warta_posts_carousel","title":"Posts Carousel","args":{"php_class":"Warta_Posts_Carousel","css_class":"widget_warta_posts_carousel"},"action":"fsmpb_widget_form_ajax","subtitle":"","formData":{"title":"","data":"post_ids","sort":"comments","time_range":"all","category":1,"tags":"","post_ids":"2149, 1","count":4,"excerpt":200,"interval":8000,"animation":"slide","animation_speed":2000,"ignore_sticky":1,"hide_mobile":1},"sortable.preventClickEvent":true},{"type":"widget","modal":"#fsmpb-modal-widget_warta_ads","title":"Advertisement","args":{"php_class":"Warta_Advertisement","css_class":"widget_warta_ads"},"action":"fsmpb_widget_form_ajax","subtitle":"Advertisement","formData":{"title":"Advertisement","ad_type":"img","img_url":"http://placehold.it/728x90/f0f0f9","ad_url":"http://themeforest.net/item/warta-newsmagazine-wordpress-theme/7066039?ref=friskamax","ad_code":"","hide_on_mobile":1}},{"type":"widget","modal":"#fsmpb-modal-widget_warta_articles","title":"Articles","args":{"php_class":"Warta_Articles","css_class":"widget_warta_articles"},"action":"fsmpb_widget_form_submit_ajax","subtitle":"Latest News","formData":{"layout":1,"title":"Latest News","data":"latest","sort":"comments","time_range":"all","top_review":0,"category":"general","tags":"","post_ids":"","count":4,"excerpt":20,"date_format":"F j, Y","ignore_sticky":1,"meta_date":1,"meta_format":0,"meta_category":1,"meta_categories":0,"meta_author":1,"meta_comments":1,"meta_views":1,"meta_review_score":0}},{"type":"widget","modal":"#fsmpb-modal-widget_warta_articles","title":"Articles","args":{"php_class":"Warta_Articles","css_class":"widget_warta_articles"},"action":"fsmpb_widget_form_ajax","subtitle":"Health","formData":{"layout":2,"title":"Health","data":"category","sort":"comments","time_range":"all","top_review":0,"category":"health-national","tags":"","post_ids":"","count":4,"excerpt":160,"date_format":"F j, Y","ignore_sticky":1,"meta_date":1,"meta_format":0,"meta_category":0,"meta_categories":0,"meta_author":1,"meta_comments":1,"meta_views":1,"meta_review_score":0}}]}],[{"width":"6","elements":[{"type":"widget","modal":"#fsmpb-modal-widget_warta_articles","title":"Articles","args":{"php_class":"Warta_Articles","css_class":"widget_warta_articles"},"action":"fsmpb_widget_form_ajax","subtitle":"Sport","formData":{"layout":3,"title":"Sport","data":"category","sort":"comments","time_range":"all","top_review":0,"category":"sport","tags":"","post_ids":"","count":5,"excerpt":160,"date_format":"F j, Y","ignore_sticky":1,"meta_date":1,"meta_format":0,"meta_category":0,"meta_categories":0,"meta_author":1,"meta_comments":1,"meta_views":1,"meta_review_score":0}}]},{"width":"6","elements":[{"type":"widget","modal":"#fsmpb-modal-widget_warta_articles","title":"Articles","args":{"php_class":"Warta_Articles","css_class":"widget_warta_articles"},"action":"fsmpb_widget_form_ajax","subtitle":"Entertainment","formData":{"layout":3,"title":"Entertainment","data":"category","sort":"comments","time_range":"all","top_review":0,"category":"entertainment","tags":"","post_ids":"","count":5,"excerpt":160,"date_format":"F j, Y","ignore_sticky":1,"meta_date":1,"meta_format":0,"meta_category":0,"meta_categories":0,"meta_author":1,"meta_comments":1,"meta_views":1,"meta_review_score":0},"sortable.preventClickEvent":true}]}],[{"width":"12","elements":[{"type":"widget","modal":"#fsmpb-modal-widget_warta_ads","title":"Advertisement","args":{"php_class":"Warta_Advertisement","css_class":"widget_warta_ads"},"action":"fsmpb_widget_form_ajax","subtitle":"Advertisement","formData":{"title":"Advertisement","ad_type":"img","img_url":"http://placehold.it/728x90/f0f0f9","ad_url":"http://themeforest.net/item/warta-newsmagazine-wordpress-theme/7066039?ref=friskamax","ad_code":"","hide_on_mobile":1},"sortable.preventClickEvent":true}]}],[{"width":"3","elements":[{"type":"widget","modal":"#fsmpb-modal-widget_warta_articles","title":"Articles","args":{"php_class":"Warta_Articles","css_class":"widget_warta_articles"},"action":"fsmpb_widget_form_ajax","subtitle":"Books","formData":{"layout":4,"title":"Books","data":"category","sort":"comments","time_range":"all","top_review":0,"category":"booksmagazines","tags":"","post_ids":"","count":4,"excerpt":160,"date_format":"M j, Y","ignore_sticky":1,"meta_date":1,"meta_format":0,"meta_category":0,"meta_categories":0,"meta_author":0,"meta_comments":1,"meta_views":1,"meta_review_score":0}}]},{"width":"3","elements":[{"type":"widget","modal":"#fsmpb-modal-widget_warta_articles","title":"Articles","args":{"php_class":"Warta_Articles","css_class":"widget_warta_articles"},"action":"fsmpb_widget_form_ajax","subtitle":"Food","formData":{"layout":4,"title":"Food","data":"category","sort":"comments","time_range":"all","top_review":0,"category":"food-drink","tags":"","post_ids":"","count":4,"excerpt":160,"date_format":"M j, Y","ignore_sticky":1,"meta_date":1,"meta_format":0,"meta_category":0,"meta_categories":0,"meta_author":0,"meta_comments":1,"meta_views":1,"meta_review_score":0},"sortable.preventClickEvent":true}]},{"width":"3","elements":[{"type":"widget","modal":"#fsmpb-modal-widget_warta_articles","title":"Articles","args":{"php_class":"Warta_Articles","css_class":"widget_warta_articles"},"action":"fsmpb_widget_form_ajax","subtitle":"Travel","formData":{"layout":4,"title":"Travel","data":"category","sort":"comments","time_range":"all","top_review":0,"category":"travel","tags":"","post_ids":"","count":4,"excerpt":160,"date_format":"M j, Y","ignore_sticky":1,"meta_date":1,"meta_format":0,"meta_category":0,"meta_categories":0,"meta_author":0,"meta_comments":1,"meta_views":1,"meta_review_score":0},"sortable.preventClickEvent":true}]},{"width":"3","elements":[{"type":"widget","modal":"#fsmpb-modal-widget_warta_articles","title":"Articles","args":{"php_class":"Warta_Articles","css_class":"widget_warta_articles"},"action":"fsmpb_widget_form_ajax","subtitle":"Tech","formData":{"layout":4,"title":"Tech","data":"category","sort":"comments","time_range":"all","top_review":0,"category":"technology","tags":"","post_ids":"","count":4,"excerpt":160,"date_format":"M j, Y","ignore_sticky":1,"meta_date":1,"meta_format":0,"meta_category":0,"meta_categories":0,"meta_author":0,"meta_comments":1,"meta_views":1,"meta_review_score":0},"sortable.preventClickEvent":true}]}]]},{"width":"4","subRows":[[{"width":"12","elements":[{"type":"widget","modal":"#fsmpb-modal-widget_warta_social_media","title":"Social Media","args":{"php_class":"Warta_Social_Media","css_class":"widget_warta_social_media"},"action":"fsmpb_widget_form_submit_ajax","subtitle":"Follow Us","formData":{"title":"Follow Us","amazon":"","behance":"http://behance.net/","blogger":"","codepen":"","deviantart":"http://www.deviantart.com/","dribbble":"http://dribbble.com/","facebook":"http://facebook.com/","feedly":"","flickr":"http://www.flickr.com/photos/friskamax","forrst":"","foursquare":"","googleplus":"http://plus.google.com/","github":"","grooveshark":"","instagram":"http://instagram.com/","kickstarter":"https://www.kickstarter.com/","lastfm":"","linkedin":"https://www.linkedin.com/","myspace":"https://myspace.com/","picasa":"http://picasa.google.com/","pinterest":"http://pinterest.com/","quora":"","reddit":"","renren":"","rss":"http://friskamax.com/warta/wp/?feed=rss2","soundcloud":"https://soundcloud.com/","stack-exchange":null,"stack-overflow":null,"steam":"","stumbleupon":"","technorati":"","tencent-weibo":null,"tumblr":"http://tumblr.com/","twitter":"https://twitter.com/friska_max","vimeo":"http://vimeo.com/","vine":"","wikipedia":"","vk":"","weibo":"","wordpress":"","xing":"","yahoo":"","youtube":"http://youtube.com/","zerply":""}},{"type":"widget","modal":"#fsmpb-modal-widget_warta_tabs","title":"Tabs","args":{"php_class":"Warta_Tabs","css_class":"widget_warta_tabs"},"action":"fsmpb_widget_form_ajax","subtitle":"Latest / Popular / Comments","formData":{"title":["Latest","Popular","Comments"],"data":["latest","popular","recent_comments"],"sort":["comments","views","comments"],"time_range":["all","all","all"],"category":[1,1,1],"tags":["","",""],"top_review":[0,0,0],"post_ids":["","",""],"posts_counts":[0,0,0],"comment_excerpt":[50,50,80],"count":[4,4,4],"date_format":["F j, Y","F j, Y","F j, Y"],"ignore_sticky":[1,1,1],"meta_date":[0,0,0],"meta_format":[0,0,0],"meta_category":[1,1,1],"meta_categories":[0,0,0],"meta_author":[0,0,0],"meta_comments":[1,1,1],"meta_views":[1,1,1],"meta_review_score":[0,0,0]}},{"type":"widget","modal":"#fsmpb-modal-widget_warta_tabs","title":"Tabs","args":{"php_class":"Warta_Tabs","css_class":"widget_warta_tabs"},"action":"fsmpb_widget_form_submit_ajax","subtitle":"Top Reviews","formData":{"title":["Top Reviews"],"data":["review"],"sort":["comments"],"time_range":["all"],"category":[1],"tags":[""],"top_review":[1],"post_ids":[""],"posts_counts":[0],"comment_excerpt":[50],"count":[4],"date_format":["F j, Y"],"display_featured_image":[0],"ignore_sticky":[1],"meta_date":[0],"meta_format":[0],"meta_category":[1],"meta_categories":[0],"meta_author":[0],"meta_comments":[1],"meta_views":[1],"meta_review_score":[1]}},{"type":"widget","modal":"#fsmpb-modal-widget_warta_feedburner","title":"Feedburner","args":{"php_class":"Warta_Feedburner","css_class":"widget_warta_feedburner"},"action":"fsmpb_widget_form_ajax","subtitle":"Feedburner","formData":{"title":"Feedburner","feedburner_id":"friskamax/CKyn"}},{"type":"widget","modal":"#fsmpb-modal-widget_warta_ads","title":"Advertisement","args":{"php_class":"Warta_Advertisement","css_class":"widget_warta_ads"},"action":"fsmpb_widget_form_ajax","subtitle":"Advertisement","formData":{"title":"Advertisement","ad_type":"img","img_url":"http://placehold.it/300x250/f0f0f9","ad_url":"http://themeforest.net/item/warta-newsmagazine-wordpress-theme/7066039?ref=friskamax","ad_code":"","hide_on_mobile":1}},{"type":"widget","modal":"#fsmpb-modal-widget_text","title":"Text","args":{"php_class":"WP_Widget_Text","css_class":"widget_text"},"action":"fsmpb_widget_form_ajax","subtitle":"Youtube Video","formData":{"title":"Youtube Video","text":"<iframe width=\"360\" height=\"203\" src=\"//www.youtube.com/embed/3AhivlZO0us\"  allowfullscreen></iframe>","filter":1}},{"type":"widget","modal":"#fsmpb-modal-widget_warta_twitter_feed","title":"Twitter Feed","args":{"php_class":"Warta_Twitter_Feed","css_class":"widget_warta_twitter_feed"},"action":"fsmpb_widget_form_ajax","subtitle":"Twitter Feed","formData":{"title":"Twitter Feed","appearance":"avatar","display_date":1,"display_reply":1,"username":"","count":4,"date_format":"%B %d, %Y"}},{"type":"widget","modal":"#fsmpb-modal-widget_warta_flickr_feed","title":"Flickr Feed","args":{"php_class":"Warta_Flickr_Feed","css_class":"widget_warta_flickr_feed"},"action":"fsmpb_widget_form_ajax","subtitle":"Flickr Feed","formData":{"title":"Flickr Feed","id":"112356465@N05","ids":"","tags":"","tagmode":"all","count":8}},{"type":"widget","modal":"#fsmpb-modal-widget_warta_gallery_carousel","title":"Gallery Carousel","args":{"php_class":"Warta_Gallery_Carousel","css_class":"widget_warta_gallery_carousel"},"action":"fsmpb_widget_form_ajax","subtitle":"Gallery","formData":{"title":"Gallery","gallery_post":1750,"caption_length":60,"hide_mobile":1}},{"type":"widget","modal":"#fsmpb-modal-widget_warta_ads","title":"Advertisement","args":{"php_class":"Warta_Advertisement","css_class":"widget_warta_ads"},"action":"fsmpb_widget_form_ajax","subtitle":"Advertisement","formData":{"title":"Advertisement","ad_type":"img","img_url":"http://placehold.it/300x250/f0f0f9","ad_url":"http://themeforest.net/item/warta-newsmagazine-wordpress-theme/7066039?ref=friskamax","ad_code":"","hide_on_mobile":1},"sortable.preventClickEvent":true},{"type":"widget","modal":"#fsmpb-modal-widget_warta_tabs","title":"Tabs","args":{"php_class":"Warta_Tabs","css_class":"widget_warta_tabs"},"action":"fsmpb_widget_form_ajax","subtitle":"Categories / Popular Tags","formData":{"title":["Categories","Popular Tags"],"data":["list_categories","popular_tags"],"sort":["comments","comments"],"time_range":["all","all"],"category":[1,1],"tags":["",""],"top_review":[1,1],"post_ids":["",""],"posts_counts":[1,1],"comment_excerpt":[50,50],"count":[4,40],"date_format":["F j, Y","F j, Y"],"ignore_sticky":[1,1],"meta_date":[0,0],"meta_format":[0,0],"meta_category":[1,1],"meta_categories":[0,0],"meta_author":[0,0],"meta_comments":[1,1],"meta_views":[1,1],"meta_review_score":[1,1]},"sortable.preventClickEvent":true}]}]]}],[{"width":"12","subRows":[[{"width":"12","elements":[{"type":"widget","modal":"#fsmpb-modal-widget_warta_slider_tabs","title":"Slider Tabs","args":{"php_class":"Warta_Slider_Tabs","css_class":"widget_warta_slider_tabs"},"action":"fsmpb_widget_form_submit_ajax","subtitle":"Gallery","formData":{"title":["Gallery"],"data":["gallery"],"category":["general"],"tags":["headline"],"gallery_post":[2149],"caption_length":[60],"sort":["comments"],"time_range":["all"],"post_ids":[""],"top_review":[0],"count":[18],"date_format":["M j, Y"],"ignore_sticky":[1],"meta_date":[1],"meta_format":[0],"meta_category":[1],"meta_author":[0],"meta_comments":[0],"meta_views":[0],"meta_review_score":[0],"hide_mobile":1}}]}]]}]]
 */