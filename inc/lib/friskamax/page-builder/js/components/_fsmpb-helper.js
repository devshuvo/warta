+function( $, F ) { 'use strict';
        
        F.FsmpbHelper = function( Parent ) {
                this.Parent                             = Parent;
                
                this.$btnInsertSubRow                   = $( '#fsmpb-btn-insert-sub-row' );
                
                this.selectorRowElements                = '> .row > div > .elements';
                this.selectorRowElementsSelected        = '> .row > div > .elements.selected';
                this.selectorSubRowElements             = '.sub-row .elements';
                this.selectorSubRowElementsSelected     = '.sub-row .elements.selected';                
        };
        
        /* -------------------------------------------------------------------------- *
         * General
         * -------------------------------------------------------------------------- */
        
        /**
         * Make Elements, columns and rows sortable.
         */
        F.FsmpbHelper.prototype.sortable = function() {
                var     $rows           = this.Parent.$mainContent.find( '.row-wrapper > .panel-body > .row' ),
                        $columns        = this.Parent.$mainContent.find( '.column-wrapper > .panel > .panel-body' ); 
                
                this.Parent.$mainContent.sortable( {
                        connectWith     : $columns,
                        placeholder     : 'placeholder',
                        handle          : '.panel-heading',
                        start           : function( event, ui ) {
                                ui.placeholder.height( ui.helper.outerHeight() );
                        }
                } );
                
                $rows.sortable( {
                        helper                  : 'clone',
                        connectWith             : $rows,
                        placeholder             : 'placeholder',
                        handle                  : '.panel-heading',
                        start                   : function( event, ui ) {
                                ui.placeholder.attr( 'class', ui.item.attr( 'class' ) + ' placeholder' );
                                ui.placeholder.height( ui.helper.find('> .panel').outerHeight() );
                        }
                } );
                
                $columns.sortable( {
                        connectWith             : $columns.add( this.Parent.$mainContent ),
                        placeholder             : 'placeholder',
                        handle                  : '.panel-heading',
                        start                   : function( event, ui ) {
                                ui.placeholder.height( ui.helper.outerHeight() );
                        }
                } );
        };
        
        /* -------------------------------------------------------------------------- *
         * Element
         * -------------------------------------------------------------------------- */
        
        /**
         * Returns element's subtitle from element settings.
         * @param {object} formData             The element settings.
         * @returns {formData.title|String}     The element title.
         */
        F.FsmpbHelper.prototype.getElementSubtitle = function( formData ) {
                var subtitle = ''; 
                                
                if ( typeof formData.title !== 'undefined' ) {
                        if ( typeof formData.title === 'string' ) {
                                subtitle = formData.title;
                        } else if ( $.isArray( formData.title ) ) {
                                subtitle = formData.title.join( ' / ' );
                        }
                }
                
                return subtitle;
        };
        
        /* -------------------------------------------------------------------------- *
         * Element selector
         * -------------------------------------------------------------------------- */
                
        /**
         * Resize element's height.
         * 
         * @param {object} $wrapper Element's wrapper.
         */
        F.FsmpbHelper.prototype.resizeElementsHeight = function( $wrapper ) {
                var height  = 0;                        

                $wrapper.find( '.element' ).each( function() {
                        var thisHeight = $( this ).height();                                  
                        
                        height = ( thisHeight > height ) ? thisHeight : height;
                } ).height( height );
        };
        
        /* -------------------------------------------------------------------------- *
         * Column
         * -------------------------------------------------------------------------- */
        
        /**
         * Get column target to insert the new element.
         * @returns {jQuery}
         */
        F.FsmpbHelper.prototype.getColumnTarget = function() {
                var $selectedColumn = this.Parent.$mainContent.find( '.column-wrapper > .panel.selected > .panel-body' );
                
                return $selectedColumn.length ? $selectedColumn : this.Parent.$mainContent;
        };
        
} ( jQuery, window.FriskaMax = window.FriskaMax || {} );