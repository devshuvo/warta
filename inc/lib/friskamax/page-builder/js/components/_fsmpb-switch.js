+function( $ ) { 'use strict';
        
        var FsmpbSwitch = function() {
                this.$button            = $( '#fsmpb-switch' );
                this.$pageTemplate      = $( '#page_template' );
                                
                this.pageTemplateChange();
                this.click();
                this.init();
        };
        
        /**
         * Show/hide page builder on page template change
         * ==============================================
         */
        FsmpbSwitch.prototype.pageTemplateChange = function() {               
                var self = this;
                
                this.$pageTemplate.on( 'change.fsmpb.switch', function() {
                        if ( fsmpbPageTemplates[ $( this ).val() ] ) {
                                self.$button.show();
                        } else {
                                if( self.$button.hasClass( 'fsmpb-active' ) ) {
                                        self.$button.trigger( 'click' );
                                }                            
                                
                                self.$button.hide();
                        }
                } ).trigger( 'change.fsmpb.switch' );
        };
        
        /**
         * Toggle show/hide page builder.
         * 
         * @since 1.0.0
         * @private
         */
        FsmpbSwitch.prototype.click = function() {                
                this.$button.click( function() {
                        var $this = $( this ).toggleClass( 'fsmpb-active' );
                                                
                        $( 'body' ).toggleClass( 'fsmpb' );
                                                
                        if ( $this.hasClass( 'fsmpb-active' ) ) {                               
                                $this.html( '<span class="dashicons dashicons-grid-view"></span> ' + fsmpbLang.exitPageBuilder );
                        } else {
                                $this.html( '<span class="dashicons dashicons-grid-view"></span> ' + fsmpbLang.pageBuilder );
                        }
                } );
        };
        
        FsmpbSwitch.prototype.init = function() {
                if( $( '#fsmpb-main-content .row' ).length && fsmpbPageTemplates[ this.$pageTemplate.val() ] ) {
                        this.$button.trigger( 'click' );
                }
        };
        
        var fsmpbSwitch = new FsmpbSwitch();
        
        
}( jQuery );