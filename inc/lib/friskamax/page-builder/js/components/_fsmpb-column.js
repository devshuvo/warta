+function( $, F ) { 'use strict';
        
        F.FsmpbColumn = function( Parent ) {
                this.Parent             = Parent;                
                
                this.$modalColumn       = $( '#fsmpb-modal-column' );
                this.$saveButton        = this.$modalColumn.find( '[data-save]' );
                this.$columnFields      = this.$modalColumn.find( '.column-field' );
                
                this.$column            = null;
                
                this.listen();
        };
        
        /**
         * Listen events.
         */
        F.FsmpbColumn.prototype.listen = function() {
                this.$saveButton.on( 'click', { self: this }, F.FsmpbColumn.prototype.save );
        };
        
        /**
         * Show edit column form.
         * @param {jQuery} $column Old element object.
         */
        F.FsmpbColumn.prototype.modal = function( $column ) {
                var     self            = this,  
                        columnData      = this.getColumnData( $column );
                
                this.$columnFields.val( '' );
                
                $.each( columnData, function( index, value ) {
                        self.$modalColumn.find( 'input[name=' + index + ']' ).val( value );
                } );
                
                this.$modalColumn.fsmhBsModal( 'show' );
                this.$column = $column;
        };
        
        /**
         * Save the column data.
         * @param {Event} event
         */
        F.FsmpbColumn.prototype.save = function( event ) {
                var     self    = event.data.self,
                        data    = {},
                        ok      = false;
                
                self.$columnFields.each( function( index, element ) {   
                        var     $element        = $( element ),
                                name            = $element.attr( 'name' ),
                                value           = parseInt( $element.val() );
                        
                        if ( ! isNaN( value ) ) {      
                                if ( value > 12 )       value = 12;
                                else if ( value < 1 )   value = 1;
                                
                                data[ name ]    = value;
                                ok              = true;
                        } else {
                                data[ name ] = '';
                        }
                } );
                
                if ( ! ok ) {
                        alert( fsmpbLang.msg.invalidColumn );
                        return;
                }
                
                self.changeWidth( self.$column, data );
                self.$modalColumn.fsmhBsModal( 'hide' );
        };
        
        /**
         * Increase the column width.
         * 
         * @param {jQuery} $column      The element.
         */
        F.FsmpbColumn.prototype.increaseWidth = function( $column ) {
                var data = this.getSingleColumnData( $column );
                
                $.each( data, function( index, value ) {
                        data[ index ] = value < 12 ? value + 1 : value;
                } );
                
                this.changeWidth( $column, data );
        };
        
        /**
         * Decrease the column width.
         * 
         * @param {jQuery} $column      The element.
         */
        F.FsmpbColumn.prototype.decreaseWidth = function( $column ) {
                var data = this.getSingleColumnData( $column );
                
                $.each( data, function( index, value ) {
                        data[ index ] = value > 1 ? value - 1 : value; 
                } );
                
                this.changeWidth( $column, data );
        };
        
        /**
         * Get the column data.
         * 
         * @param {jQuery} $column      The element.
         * @returns {object}            The column data. 
         * {
         *      lg: #,
         *      md: #,
         *      sm: #,
         *      xs: #
         * }
         */
        F.FsmpbColumn.prototype.getColumnData = function( $column ) {
                var     classname       = $column.attr( 'class' ).replace( /column-wrapper/gi, '' ),
                        columns         = $.trim( classname ).split( ' ' ),
                        data            = {};
                
                $.each( columns, function( index, Element ) {
                        if ( Element.indexOf( 'col-lg-' ) >= 0 ) {
                                data.lg = Element.replace( 'col-lg-', '' );
                                data.lg = parseInt( data.lg );
                        } else if ( Element.indexOf( 'col-md-' ) >= 0 ) {
                                data.md = Element.replace( 'col-md-', '' );
                                data.md = parseInt( data.md );
                        } else if ( Element.indexOf( 'col-sm-' ) >= 0 ) {
                                data.sm = Element.replace( 'col-sm-', '' );
                                data.sm = parseInt( data.sm );
                        } else if ( Element.indexOf( 'col-xs-' ) >= 0 ) {
                                data.xs = Element.replace( 'col-xs-', '' );
                                data.xs = parseInt( data.xs );
                        }
                } ); 
                
                return data;
        };
        
        /**
         * Get a single column data. The largest screen size will be used.
         * 
         * @param {jQuery} $column      The element.
         * @returns {object}            The arguments.
         */
        F.FsmpbColumn.prototype.getSingleColumnData = function( $column ) {
                var     columnData      = this.getColumnData( $column ),
                        data            = {};
                
                if ( columnData.lg )            data.lg = columnData.lg;
                else if ( columnData.md )       data.md = columnData.md;
                else if ( columnData.sm )       data.sm = columnData.sm;
                else if ( columnData.xs )       data.xs = columnData.xs;
                
                return data;
        };
        
        /**
         * Change the column width.
         * 
         * @param {jQuery} $column              The element.
         * @param {object} newColumnData        New column data.
         */
        F.FsmpbColumn.prototype.changeWidth = function( $column, newColumnData ) {
                var columnData = this.getColumnData( $column );
                
                $.each( newColumnData, function( index, value ) {
                        var     oldClass        = 'col-' + index + '-' + columnData[index],
                                newClass        = 'col-' + index + '-' + value;
                        
                        $column.removeClass( oldClass );
                        
                        if ( value ) $column.addClass( newClass );                                
                } );
        };
        
        /**
         * Get screen breakpoint based on page template that being used.
         * 
         * @returns {String}
         */
        F.FsmpbColumn.prototype.getBreakpoint = function() {
                var $pageTemplate = $( '#page_template' );
                
                if ( ! $pageTemplate.length ) return 'sm';
                
                return fsmpbPageTemplates[ $pageTemplate.val() ].breakpoint;
        };
        
} ( jQuery, window.FriskaMax = window.FriskaMax || {} );