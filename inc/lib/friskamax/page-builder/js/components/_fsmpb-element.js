/**
 * Element management 
 * 
 * @requires jQuery
 * @see {@link http://jquery.com/}
 * 
 * @since 1.0.0
 * @package friskamax-library\page-builder\js\element
 * 
 * @param {jQuery} $
 * @param {object} F
 * @param {object} window
 * @param {object} document
 * @param {undefined} undefined
 */
; ( function( $, F, window, document, undefined ) { 'use strict';
        
        /**
         * FsmpbElement constructor.
         * 
         * @since 1.0.0
         * 
         * @param {object} Parent       Instance of Fsmpb (main page builder class).
         */
        F.FsmpbElement = function( Parent ) {
                this.Parent                     = Parent;
                
                this.$modalInsertElement        = $( '#fsmpb-modal-insert-element' );
                
                this.listen();
        };
        
        /**
         * Listen events.
         * 
         * @since 1.0.0
         */
        F.FsmpbElement.prototype.listen = function() {
                var self = this;
                                        
                /* -------------------------------------------------------------------------- *
                 * Modal
                 * -------------------------------------------------------------------------- */
                
                // Element chooser.
                this.$modalInsertElement
                        .on( 'shown.bs.modal', function() { // Helper.resizeElementsHeight() on first tab
                                var $wrapper = $( this ).find( '.tab-pane.active' );
                                self.Parent.Helper.resizeElementsHeight( $wrapper );
                        } )
                        .find( 'a[data-toggle="fsmh-tab"]' ).on( 'shown.bs.tab', function ( event ) { // Helper.resizeElementsHeight() on new activated tab
                                var target = $( event.target ).attr( 'href' );
                                self.Parent.Helper.resizeElementsHeight( $( target ) );
                        } )
                        .end()
                        .on( 'click', '.element', function() { // modal() : show element's form
                                self.modal( $( this ).data() );
                        } );
                        
                /* -------------------------------------------------------------------------- *
                 * Content
                 * -------------------------------------------------------------------------- */
                        
                // Panel.
                this.Parent.$mainContent.on( 'click.fsmpb', '.panel', { self: this }, $.proxy( this.selectPanel ) );
        };
        
        /**
         * Submit new element form.
         * 
         * @since 1.0.0
         * 
         * @param {object} $modal 
         * @param {object} data         Element's data
         * @param {object} $oldElement 
         */
        F.FsmpbElement.prototype.insert = function( $modal, data, $oldElement ) {
                var     self            = this,
                        elementData     = $.extend( {}, data ); // Lets not overwrite the original object.
                
                $modal.on( 'submit.fsmpb', function( event ) {                                          
                        var $this = $( this );  
                        
                        elementData.oldFormData        = $oldElement ? $oldElement.data( 'formData' ) : null;
                        elementData.formData           = $this.serialize();
                        
                        // Set ajax action sanitize function
                        switch( elementData.type ) {
                                case 'widget':
                                        elementData.action = 'fsmpb_widget_sanitize';
                                        break;
                                case 'custom':
                                        elementData.action = 'fsmpb_custom_sanitize_' + elementData.args.php_class;
                                        break;

                                // old version
                                case 'widget_area':
                                        elementData.action = 'fsmpb_custom_sanitize_Fsmpb_Element_Widget_Area';
                                        break;
                        }
                                                 
                        // Sanitize data and insert the element
                        $.post( ajaxurl, elementData, function( response ) {               
                                // Remove unnecessary data
                                delete elementData.oldFormData;
                                delete elementData.action;
                                
                                elementData.formData    = F.Helper.isJSON( response ) ? $.parseJSON( response ) : {};   
                                
                                var     element         = self.Parent.templateElement
                                                                .replace( /{{title}}/ig, fsmpbElementsInfo[ elementData.args.php_class ].name )
                                                                .replace( /{{subtitle}}/ig, self.Parent.Helper.getElementSubtitle( elementData.formData ) ),
                                        $element        = $( element ).data( elementData );                                        
                                                
                                // Insert the element
                                if( $oldElement ) {
                                        $oldElement.replaceWith( $element );
                                } else {
                                        self.Parent.Helper.getColumnTarget().append( $element );
                                        $element.hide().slideDown();
                                }
                                
                                self.Parent.Helper.sortable();
                                self.Parent.$modalLoading.fsmhBsModal( 'hide' ); 
                        } );
                        
                        $this.fsmhBsModal( 'hide' );
                        self.Parent.$modalLoading.fsmhBsModal( 'show' );                                
                        event.preventDefault();
                } ).on( 'hidden.bs.modal', function() {
                        $( this ).parent().remove();
                } );
        };
        
        /**
         * Show Insert Element form.
         * 
         * @since 1.0.0
         * @private
         * 
         * @param {object} data         Element's data.
         * @param {object} $oldElement  Old element.
         */
        F.FsmpbElement.prototype.modal = function( data, $oldElement ) {                
                var     self            = this,
                        elementData     = $.extend( {}, data ); // Lets not overwrite the original element's data.
                
                this.$modalInsertElement.fsmhBsModal( 'hide' );
                this.Parent.$modalLoading.fsmhBsModal( 'show' );
                
                // remove unnecessary sortable data
                delete elementData.sortable;
                delete elementData.sortableItem;
                
                // Set ajax action function
                switch( elementData.type ) {
                        case 'widget':
                                elementData.action = 'fsmpb_widget_form';
                                break;
                        case 'custom':
                                elementData.action = 'fsmpb_custom_form_' + elementData.args.php_class;
                                break;
                                
                        // old version
                        case 'widget_area':
                                elementData.action = 'fsmpb_custom_form_Fsmpb_Element_Widget_Area';
                                break;
                }
                
                // Sending values as POST parameters will be treated as simple text strings,
                // so any boolean values will be converted to string "true" or "false". 
                // That will cause a problem for many widgets, so we use JSON.
                elementData.formData = JSON.stringify( elementData.formData );
                         
                // Get the form
                $.post( ajaxurl, elementData, function( response ) {
                        var $response = $( $.trim( response ) );
                        
                        self.Parent.$body.append( $response );                                                
                        self.Parent.$modalLoading.fsmhBsModal( 'hide' );
                        self.insert(
                                $response.find( '.modal' ).fsmhBsModal( 'show' ), 
                                elementData, 
                                $oldElement
                        );       
                } ); 
        };
        
        /**
         * Adds selected class to panel.
         * 
         * @since 1.0.0
         * @private
         * 
         * @param {object} event Event object.
         */
        F.FsmpbElement.prototype.selectPanel = function( event ) {   
                var     self    = event.data.self,
                        $this   = $( this );
                
                if ( $this.is( $( event.target ).closest( '.panel' ) ) ) {       
                        self.Parent.$mainContent.find( '.panel' ).removeClass( 'selected' );
                        $this.addClass( 'selected' );
                }
        };
        
} ) ( jQuery, window.FriskaMax = window.FriskaMax || {}, window, document );