/**
 * Created on : Mar 28, 2014, 4:04:10 PM
 * Author     : Fahri
 */
+function( $ ) { 'use strict';
        
        var FsmpbFullScreen = function() {
                this.$body      = $( 'body' );
                this.$metaBox   = $( '#fsmpb_meta_box .inside' );
                this.$container = $( '#fsmpb-container' );
                this.$button    = this.$container.find( '[data-toggle="full-screen"]' );
                this.$adminMenus= $( '#wpadminbar, #adminmenuback, #adminmenuwrap' );
                 
                this.click();
        };
        
        FsmpbFullScreen.prototype.click = function() {
                var self = this;
                
                this.$button.click(function() {
                        var $this = $(this);
                        
                        self.$container.toggleClass('full-screen');
                        self.$adminMenus.toggle();
                        
                        if( self.$container.hasClass('full-screen') ) { // Activate full screen view                 
                                self.$container.detach().appendTo('body');                                
                                self.$body.addClass( 'fsmpb-full-screen' ); 
                                
                                $this.text(fsmpbLang.exitFullScreen);
                        } else { // Deactivate full screen view
                                self.$container.detach().appendTo( self.$metaBox );     
                                self.$body.removeClass( 'fsmpb-full-screen' ); // Disable scrolling on the body
                                
                                $this.text(fsmpbLang.fullScreen);
                        }
                });
        };
        
        var fsmpbFullScreen = new FsmpbFullScreen;
        
}( jQuery );