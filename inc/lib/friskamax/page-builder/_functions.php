<?php
/**
 * Page Builder functions.
 * 
 * @package friskamax-library\page-builder
 * @since 1.0.0
 */

/* -------------------------------------------------------------------------- *
 * Conditional
 * -------------------------------------------------------------------------- */

/**
 * Is the current post using page builder?
 * 
 * @since 1.0.0
 * 
 * @return boolean
 */
function fsmpb_is_page_builder() {
        
        return Fsmpb_Display::get_instance()->is_pb();
        
}

/**
 * Is old data?
 * 
 * @since 1.0.0
 * 
 * @return boolean
 */
function fsmpb_is_old_data() {
        
        return Fsmpb_Display::get_instance()->is_old();
        
}

/* -------------------------------------------------------------------------- *
 * Body class
 * -------------------------------------------------------------------------- */

/**
 * Adds custom classes to the array of body classes.
 *
 * @since 1.0.2
 * 
 * @param array $classes Classes for the body element.
 * @return array
 */
function fsmpb_filter_body_class( $classes ) {
 
        if ( is_page() && fsmpb_is_page_builder() ) {
                
                $classes[] = 'page-builder';
                
        }
        
        return $classes;
        
}
add_filter( 'body_class', 'fsmpb_filter_body_class' );

/* -------------------------------------------------------------------------- *
 * Content
 * -------------------------------------------------------------------------- */

if ( ! function_exists( 'fsmpb_get_content' ) ) :
        
/**
 * Get the Page Builder content.
 * 
 * @since 1.0.0
 * @return string HTML page builder content.
 */
function fsmpb_get_content() {
        
        $return = '';
        
        if ( Fsmpb_Display::get_instance()->is_pb() ) {
                
                ob_start();
                Fsmpb_Display::get_instance()->display();
                
                $return = ob_get_clean();
                
        }
        
        /**
         * Filter the HTML Page builder content.
         * 
         * @since 1.0.2
         * 
         * @param string $return        HTML Page builder content.
         * @param object $fsmpb_display Instance of Fsmpb_Display class.
         */
        return apply_filters( 'fsmpb_get_content', $return, Fsmpb_Display::get_instance() );
        
}

endif;

if ( ! function_exists( 'fsmpb_content' ) ) :
        
/**
 * Display the page builder content.
 * 
 * @see fsmpb_get_content()
 * @since 1.0.0
 * @return type
 */
function fsmpb_content() {
        
        echo fsmpb_get_content();
        
}

endif; 