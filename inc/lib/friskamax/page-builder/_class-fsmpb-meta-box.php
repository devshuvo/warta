<?php
/**
 * FriskaMax Page Builder Meta Box.
 * 
 * @since 1.0.0
 * @package friskamax-library\page-builder
 */

/**
 * FriskaMax Page Builder Meta Box class.
 * 
 * @since 1.0.0
 */
class Fsmpb_Meta_Box {  
        /**
         * Instance of Fsmpb_Meta_Box_Register class.
         * 
         * @since 1.0.0
         * @access private
         * @var object
         */
        public $Register;

        /**
         * Instance of Fsmpb_Meta_Box_Elements class.
         * 
         * @since 1.0.0
         * @access private
         * @var object
         */
        public $Elements;

        /**
         * Instance of Fsmpb_Meta_Box_Display class.
         * 
         * @since 1.0.0
         * @access private
         * @var object 
         */
        public $Display;
        
        /**
         * Constructor's arguments.
         * 
         * @since 1.0.0
         * @access private
         * @var object 
         */
        public $r;

        /**
         * Constructor.
         * 
         * @since 1.0.0
         * @access private
         * 
         * @param array $args {
         *      Optional. An array of arguments.
         * 
         *      @type string $capability                User capability that allowed to use the page builder.
         *      @type array $post_types                 Allowed post types to display the page builder.
         *      @type array $custom_element_classes     Custom element classes, should has method: form, sanitize and display.
         *      @type array $page_templates             Page templates that can be used by page builder. @see Fsmpb_Meta_Box_Register::$page_templates
         *      @type array $element_page               Arguments for Page element. @see Fsmpb_Element_Page::$args
         *      @type array $element_widget             Arguments for Widget element. @see Fsmpb_Display::$widget_args
         *      @type array $element_widget_area        Arguments for Widget Area element. @see Fsmpb_Element_Widget_Area::$args
         * }
         */                
        public function __construct( $args = array() ) {       
                                
                $this->r                                = friskamax_parse_args( $args, array(
                                                                'capability'                    => 'unfiltered_html',
                                                                'post_types'                    => array( 'page' ),
                                                                'custom_element_classes'        => array( 'Fsmpb_Element_Widget_Area' ),
                                                                'page_templates'                => array(),
                                                                'element_page'                  => array(),
                                                                'element_widget'                => array(),
                                                                'element_widget_area'           => array(),
                                                        ), 'Fsmpb_Meta_Box_r' );                
                $this->r->custom_element_classes        = wp_parse_args( $this->r->custom_element_classes, array( 
                                                                'Fsmpb_Element_Widget_Area', 
                                                                'Fsmpb_Element_Page', 
                                                        ) );
                
                $this->Register = new Fsmpb_Meta_Box_Register( $this );
                $this->Elements = new Fsmpb_Meta_Box_Elements( $this );   
                $this->Display  = new Fsmpb_Meta_Box_Display( $this ); 
                
        }       
}