<?php

/**
 * FriskaMax Page Builder Display.
 * 
 * Used to display the page builder content.
 * 
 * @since 1.0.0
 * @package friskamax-library\page-builder\display
 */

if ( ! class_exists( 'Fsmpb_Display' ) ) :
/**
 * Class FriskaMax Page Builder Display.
 * 
 * @since 1.0.0
 */
class Fsmpb_Display {        
        /** 
         * Instance of Fsmpb_Display. 
         * 
         * @since 1.0.2
         * @access protected
         * @var object 
         */
        protected static $instance;
        
        /** 
         * Layout data.
         * 
         * @since 1.0.0
         * @var JSON  
         */
        public $data;

        /** 
         * Page ID.
         * 
         * @since 1.0.0
         * @var int  
         */
        public $id;

        /**
         * The widget ID.
         * 
         * Auto increment.
         * 
         * @since 1.0.0
         * @var int 
         */
        public $widget_id = 1;

        /**
         * Arguments of the Widget element.
         * 
         * @since 1.0.0 
         * @var array {
         *      @type array $the_widget_args    Third argument or the_widget() function.
         *                                      @see the_widget()
         * }
         */
        public $widget_args = array();
        
        /* -------------------------------------------------------------------------- *
         * Setup
         * -------------------------------------------------------------------------- */

        /**
         * Initialize.
         * 
         * @since 1.0.0
         * @global object $fsmpb_meta_box      Instance of Fsmpb_Meta_Box class.
         */
        public function __construct() {
                global $fsmpb_meta_box;
                
                $this->id                               = get_the_ID();
                $this->data                             = get_post_meta( $this->id, 'fsmpb_main', true );
                $this->data                             = json_decode( $this->data );
                $this->widget_args                      = $fsmpb_meta_box->r->element_widget;
                $this->widget_args[ 'the_widget_args' ] = isset( $this->widget_args[ 'the_widget_args' ] ) 
                                                        ? $this->widget_args[ 'the_widget_args' ]
                                                        : array();
                $this->widget_args[ 'the_widget_args' ] = wp_parse_args( $this->widget_args[ 'the_widget_args' ], array(
                        'before_widget' => '<section id="%1$s" class="widget %2$s">',
                        'after_widget'  => '</section>',
                        'before_title'  => '<header class="widget-header"><h1 class="widget-title">',
                        'after_title'   => '</h1></header>',
                        'is_pb'         => true // Only used on Warta theme.
                ) );
        }
        
        /* -------------------------------------------------------------------------- *
         * Display
         * -------------------------------------------------------------------------- */

        /**
         * Display the page builder content.
         * 
         * @since 1.0.0
         */
        public function display() {       
                echo $this->get();              
        }

        /**
         * Display an element with widget type.
         * 
         * @since 1.0.0
         * 
         * @param object $element       Element data.
         */
        public function display_widget( $element ) {
                if ( ! class_exists( $element->args->php_class ) ) return;
                
                $widget_obj             = $GLOBALS[ 'wp_widget_factory' ]->widgets[ $element->args->php_class ];                        
                $r                      = $this->widget_args[ 'the_widget_args' ];
                $r[ 'widget_id' ]       = $widget_obj->widget_options[ 'classname' ] . '-' . $this->id . '-' . $this->widget_id++;                        
                $r[ 'before_widget' ]   = sprintf( $r[ 'before_widget' ], $r[ 'widget_id' ], $widget_obj->widget_options[ 'classname' ] );
                
                // Convert to array. WordPress uses array settings.
                // Use json_decode() because it can convert multidimensional array.
                $form_data              = json_decode( json_encode( $element->formData ), true );
                
                the_widget( $element->args->php_class, $form_data, $r );
        }
        
        /* -------------------------------------------------------------------------- *
         * Get
         * -------------------------------------------------------------------------- */
        
        /**
         * Get the page builder content.
         * 
         * @since 1.0.0
         * 
         * @return string
         */
        public function get() {    
                if ( ! $this->is_pb() ) return;
                if ( $this->is_old() )  return $this->old_get();    
                
                $return = '';
                
                foreach ( $this->data as $element ) {
                        $return .= $this->get_element( array( 'element' => $element ) );
                }
                
                /**
                 * Filter the page builder content.
                 * 
                 * @since 1.0.0
                 * 
                 * @param string $return        Page builder content.
                 * @param json $data            Page builder data.
                 * @param object $this          Instance of Fsmpb_Display.
                 */
                return apply_filters( 'Fsmpb_Display__get', $return, $this->data, $this );
        }

        /**
         * Get an element.
         * 
         * @since 1.0.0
         * 
         * @param array $args {
         *      An array of arguments.
         * 
         *      @type array $element    Element data.
         *      @type string $before    Optional. Opening tag.
         *      @type string $after     Optional. Closing tag.
         * }
         * @return string
         */
        public function get_element( $args = array() ) {
                $r              = friskamax_parse_args( $args, array(
                                        'element'       => null,
                                        'before'        => '',
                                        'after'         => ''
                                ), 'Fsmpb_Display__get_sub_row_columns_r' );
                $instances      = $GLOBALS['fsmpb_meta_box']->Elements->instances;
                
                if ( $r->element->type == 'row' )  return $this->get_row( array( 'row' => $r->element ) );

                ob_start();
                
                switch ( $r->element->type ) { 
                        case 'widget':
                                $this->display_widget( $r->element );
                                break;
                        case 'custom':
                                if ( class_exists( $r->element->args->php_class ) ) {
                                        $instances[ $r->element->args->php_class ]->display( $r->element->formData );
                                }
                                break;
                        case 'widget_area': // Version < 1.0.0
                                $instances['Fsmpb_Element_Widget_Area']->display( $r->element->formData );
                                break;
                }
                
                $return = $r->before;
                $return .= ob_get_clean();
                $return .= $r->after;
                
                /**
                 * Filter the element.
                 * 
                 * @since 1.0.0
                 * 
                 * @param string $return        Page builder element.
                 * @param object $r             Arguments.
                 * @param object $this          Instance of Fsmpb_Display.
                 */
                return apply_filters( 'Fsmpb_Display__get_elements', $return, $r, $this );
        }
        
        /**
         * Get a row.
         * 
         * @since 1.0.0
         * 
         * @param array $args {
         *      An array of arguments.
         * 
         *      @type string $row       Row data.
         *      @type string $before    Optional. Opening tag.
         *      @type string $after     Optional. Closing tag.
         * }
         * @return string
         */
        public function get_row( $args = array() ) {
                $r      = friskamax_parse_args( $args, array(
                                'row'           => array(),
                                'before'        => '<div class="row">',
                                'after'         => '</div>'
                        ), 'Fsmpb_Display__get_row_r' );
                $return = $r->before;
                
                foreach ( $r->row->children as $column ) {
                        $return .= $this->get_column( array( 'column' => $column ) );
                }   
                
                $return .= $r->after;
                
                /**
                 * Filter the row.
                 * 
                 * @since 1.0.0
                 * 
                 * @param string $return        Row.
                 * @param object $r             Arguments.
                 * @param object $this          Instance of Fsmpb_Display.
                 */
                return apply_filters( 'Fsmpb_Display__get_row', $return, $r, $this );
        }

        /**
         * Returns a column.
         * 
         * @since 1.0.0
         * 
         * @param array $args {
         *      An array of arguments.
         * 
         *      @type array $column     Columns data.
         *      @type string $before    Optional. Opening tag.
         *      @type string $after     Optional. Closing tag.
         * }
         * @return string
         */
        public function get_column( $args = array() ) {
                $r      = friskamax_parse_args( $args, array(
                                'column'        => array(),
                                'before'        => '<div class="%s">',
                                'after'         => '</div>'
                        ), 'Fsmpb_Display__get_columns_r' );                
                $class  = '';
                
                foreach ( $r->column->formData as $key => $value ) {
                        $class .= 'col-' . $key . '-' . $value . ' ';
                } 
                                
                $return = sprintf( $r->before, $class );

                foreach ( $r->column->children as $element ) {
                        $return .= $this->get_element( array( 'element' => $element ) );           
                }       
                
                $return .= $r->after;                  
                
                /**
                 * Filter the column.
                 * 
                 * @since 1.0.0
                 * 
                 * @param string $return        Column.
                 * @param objecy $r             Arguments.
                 * @param object $this          Instance of Fsmpb_Display.
                 */
                return apply_filters( 'Fsmpb_Display__get_columns', $return, $r, $this );                      
        }
        
        /* -------------------------------------------------------------------------- *
         * Helper Functions
         * -------------------------------------------------------------------------- */

        /**
         * Get instance of Fsmpb_Display.
         * 
         * @since 1.0.2
         * 
         * @return object
         */
        public static function get_instance() {
                if ( ! self::$instance ) {
                        self::$instance = new self;
                }

                return self::$instance;
        }
        
        /**
         * Determine wheter the current page using page builder or not.
         * 
         * @since 1.0.0
         * 
         * @return boolean
         */
        public function is_pb() {
                return ( boolean ) $this->data;
        }
        
        /**
         * Determine whether the data is old or new.
         * 
         * The old data was used in beta version.
         * 
         * @since 1.0.0
         * 
         * @return boolean
         */
        public function is_old() {
                return ! isset( $this->data[0]->type );
        }
        
        /* -------------------------------------------------------------------------- *
         * Old Data
         * -------------------------------------------------------------------------- */
        
        /**
         * Return the page builder content from old data.
         * 
         * Formerly know as Fsmpb_Display::get()
         * 
         * @since beta
         * @deprecated since version 1.0.0
         * @access private
         * 
         * @return string
         */
        private function old_get() {
                return apply_filters( 'Fsmpb_Display__old_get', $this->old_get_rows(), $this );
        }
        
        /**
         * Get the rows from old data.
         * 
         * Formerly know as Fsmpb_Display::get_rows()
         * 
         * @since beta
         * @deprecated since version 1.0.0
         * @access private
         * 
         * @param array $args {
         *      Optional. An array of arguments.
         * 
         *      @type string $before    Opening tag.
         *      @type string $after     Closing tag.
         * }
         * @return string
         */
        private function old_get_rows( $args = array() ) {
                $r      = friskamax_parse_args( $args, array(
                                'before'        => '<div class="row">',
                                'after'         => '</div>'
                        ), 'Fsmpb_Display__old_get_rows_r' );
                $return = '';
                
                foreach ( $this->data as $columns ) {
                        $return .= $r->before;
                        $return .= $this->old_get_row_columns( array( 'columns' => $columns ) );
                        $return .= $r->after;
                }   
                
                return apply_filters( 'Fsmpb_Display__old_get_rows', $return, $r, $this );
        }
        
        /**
         * Get the row's columns from old data.
         * 
         * Formerly know as Fsmpb_Display::get_row_columns()
         * 
         * @since beta
         * @deprecated since version 1.0.0
         * @access private
         * 
         * @param array $args {
         *      An array of arguments.
         * 
         *      @type array $columns    Row columns data.
         *      @type string $before    Optional. Opening tag.
         *      @type string $after     Optional. Closing tag.
         * }
         * @return string
         */
        private function old_get_row_columns( $args = array() ) {
                $r      = friskamax_parse_args( $args, array(
                                'columns'       => array(),
                                'before'        => '<div class="col-md-%1$d col-sm-%2$d">',
                                'after'         => '</div>'
                        ), 'Fsmpb_Display__old_get_columns_r' );
                $return = '';
                
                $this->old_set_sm_widths( $r->columns );

                foreach ( $r->columns as $column ) {
                        $return .= sprintf( $r->before, (int) $column->width, $column->sm_width ); 
                        $return .= $this->old_get_sub_rows( array( 'sub_rows' => $column->subRows ) );
                        $return .= $r->after;                     
                }                  
                
                return apply_filters( 'Fsmpb_Display__old_get_columns', $return, $r, $this );                      
        }
        
        /**
         * Returns the sub rows from old data.
         * 
         * Formerly know as Fsmpb_Display::get_sub_rows()
         * 
         * @since beta
         * @deprecated since version 1.0.0
         * @access private
         * 
         * @param array $args {          
         *      An array of Arguments.
         * 
         *      @type array $sub_rows   Sub rows data.
         *      @type string $before    Optional. Opening tag.
         *      @type string $after     Optional. Closing tag.
         * }
         * @return string
         */
        private function old_get_sub_rows( $args = array() ) {
                $r      = friskamax_parse_args( $args, array(
                                'sub_rows'      => array(),
                                'before'        => '<div class="row">',
                                'after'         => '</div>'
                        ), 'Fsmpb_Display__old_get_sub_rows_r' );
                $return = '';
                
                foreach ( $r->sub_rows as $sub_row_columns ) {
                        $return .= $r->before; 
                        $return .= $this->old_get_sub_row_columns( array( 'sub_row_columns' => $sub_row_columns ) );
                        $return .= $r->after;   
                }
                
                return apply_filters( 'Fsmpb_Display__old_get_sub_rows', $return, $r, $this );  
        }
        
        /**
         * Returns sub row's columns from old data.
         * 
         * Formerly know as Fsmpb_Display::get_sub_row_columns()
         * 
         * @since beta
         * @deprecated since version 1.0.0
         * @access private
         * 
         * @param array $args {
         *      An array of arguments.
         * 
         *      @type array $sub_row_columns    Sub row columns data.
         *      @type string $before            Optional. Opening tag.
         *      @type string $after             Optional. Closing tag.
         * }
         * @return string
         */
        private function old_get_sub_row_columns( $args = array() ) {
                $r      = friskamax_parse_args( $args, array(
                                'sub_row_columns'       => array(),
                                'before'                => '<div class="col-sm-%1$d">',
                                'after'                 => '</div>'
                        ), 'Fsmpb_Display__old_get_sub_row_columns_r' );
                $return = '';
                
                foreach ( $r->sub_row_columns as $sub_row_column ) {
                        $return .= sprintf( $r->before, (int) $sub_row_column->width ); 
                        $return .= $this->old_get_elements( array( 'elements' => $sub_row_column->elements ) );
                        $return .= $r->after;                     
                }                  
                
                return apply_filters( 'Fsmpb_Display__old_get_sub_row_columns', $return, $r, $this );
        }
        
        /**
         * Returns the elements from old data.
         * 
         * Formerly know as Fsmpb_Display::get_elements()
         * 
         * @since beta
         * @deprecated since version 1.0.0
         * @access private
         * 
         * @param array $args {
         *      An array of arguments.
         * 
         *      @type array $elements   Elements data.
         *      @type string $before    Optional. Opening tag.
         *      @type string $after     Optional. Closing tag.
         * }
         * @return string
         */
        private function old_get_elements( $args = array() ) {
                $r              = friskamax_parse_args( $args, array(
                                        'elements'      => array(),
                                        'before'        => '',
                                        'after'         => ''
                                ), 'Fsmpb_Display__get_sub_row_columns_r' );
                $instances      = $GLOBALS['fsmpb_meta_box']->Elements->instances;

                ob_start();
                
                foreach ( $r->elements as $element ) {
                        switch ( $element->type ) { 
                                case 'widget':
                                        $this->display_widget( $element );
                                        break;
                                case 'custom':
                                        if ( class_exists( $element->args->php_class ) ) {
                                                $instances[ $element->args->php_class ]->display( $element->formData );
                                        }
                                        break;
                                case 'widget_area': // Version < 1.0.0
                                        $instances['Fsmpb_Element_Widget_Area']->display( $element->formData );
                                        break;
                        }
                }
                
                $return = $r->before;
                $return .= ob_get_clean();
                $return .= $r->after;
                
                return apply_filters( 'Fsmpb_Display__old_get_elements', $return, $r, $this );
        }

        /**
         * Calculate column width for small devices.
         * 
         * Formerly know as Fsmpb_Display::set_sm_widths()
         * 
         * @since beta
         * @deprecated since version 1.0.0
         * @access private
         * 
         * @param array $cols   Column data.
         */
        private function old_set_sm_widths( &$cols ) {
                if( count($cols) <= 2 ) { // Set sm width to 12 if there's only 1 or 2 columns
                        foreach( array_keys( $cols ) as $key ) {
                                $cols[$key]->sm_width = 12;
                        }
                } else {
                        $sm_widths_1    = array();
                        $sm_widths_2    = array();

                        // Break into 2 rows
                        foreach ( $cols as $col ) {
                                if( array_sum($sm_widths_1) < 12 ) {
                                        $sm_widths_1[] = $col->width * 2;
                                } else {
                                        $sm_widths_2[] = $col->width * 2;
                                }
                        }                                

                        // Move middle column to $temp_width
                        if( array_sum( $sm_widths_1 ) > 12 ) {
                                $temp_width     = array_pop($sm_widths_1);
                        } elseif ( array_sum( $sm_widths_1 ) < 12 ) {
                                $temp_width     = array_pop( $sm_widths_1 );
                        }                                

                        if( isset( $temp_width ) ) {
                                // Move middle columns to narrowest row 
                                if( array_sum( $sm_widths_1 ) > array_sum( $sm_widths_2 ) ) {
                                        array_unshift( $sm_widths_2, $temp_width );
                                } else {
                                        $sm_widths_1[] = $temp_width; 
                                }

                                // Make the width 1st row to be 12
                                while( array_sum( $sm_widths_1 ) != 12) {                                                
                                        foreach ( $sm_widths_1 as $key => $value ) {
                                                if( array_sum( $sm_widths_1 ) > 12 ) {
                                                        $sm_widths_1[$key] -= 1;
                                                } else if( array_sum( $sm_widths_1 ) < 12 ) {
                                                        $sm_widths_1[$key] += 1;
                                                }
                                        }                                                         
                                }

                                // Make the width 2nd row to be 12
                                while ( array_sum( $sm_widths_2 ) != 12 ) {   
                                        foreach ( $sm_widths_2 as $key => $value ) {
                                                if( array_sum( $sm_widths_2 ) > 12 ) {
                                                        $sm_widths_2[$key] -= 1;
                                                } else if(array_sum( $sm_widths_2 ) < 12) {
                                                        $sm_widths_2[$key] += 1;
                                                }
                                        }                                                        
                                }
                        }   

                        // Set the sm widths
                        $i = 0;
                        foreach ( $sm_widths_1 as $key => $value ) {
                                $cols[$i++]->sm_width = $value;
                        }
                        foreach ( $sm_widths_2 as $key => $value ) {
                                $cols[$i++]->sm_width = $value;
                        }                                
                }                        
        }
}
endif; // ! class_exists( 'Fsmpb_Display' )