<?php
/**
 * FriskaMax library.
 * 
 * @version 1.0.5
 * @package friskaMax-library
 */

/** General functions. */
require_once dirname( __FILE__ ) . '/functions/functions.php';

/** CSS and JS helper. */
require_once dirname( __FILE__ ) . '/helper/helper.php';

/** Form. */
require_once dirname( __FILE__ ) . '/form/form.php';

/** Widget. */
require_once dirname( __FILE__ ) . '/widget/widget.php';

/**
 * Setup the library.
 * 
 * @since 1.0.2
 * @access private
 * 
 * @global object $fsmpb_meta_box               Instance of Fsmpb_Meta_Box.
 * @global object $friskamax_custom_sidebar     Instance of Friskamax_Custom_Sidebar.
 */
function friskamax_library_setup() {
        global $fsmpb_meta_box, $friskamax_custom_sidebar;
        
        $dir = dirname( __FILE__ );
        
        /* -------------------------------------------------------------------------- *
         * Page Builder
         * -------------------------------------------------------------------------- */
        
        // Since version 1.0.2, theme should use add_theme_support( 'friskamax-page-builder' );
        if ( ! isset( $fsmpb_meta_box ) && current_theme_supports( 'friskamax-page-builder' ) ) {
                require_once $dir . '/page-builder/page-builder.php';
                
                $theme_support_args     = get_theme_support( 'friskamax-page-builder' );
                $fsmpb_args             = isset( $theme_support_args[ 0 ] ) ? $theme_support_args[ 0 ] : array();
                $fsmpb_meta_box         = new Fsmpb_Meta_Box( $fsmpb_args );
        }
}
// Priority 20, because themes also hook into after_setup_theme with priority 10
// to register theme supports.
add_action( 'after_setup_theme', 'friskamax_library_setup', 20 );