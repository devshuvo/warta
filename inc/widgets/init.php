<?php
/**
 * Warta widgets initialization
 *
 * @package Warta
 */

/**
 * Load widgets
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
require get_template_directory() . '/inc/widgets/slider-tabs.php';
require get_template_directory() . '/inc/widgets/articles.php';
require get_template_directory() . '/inc/widgets/ads.php';
require get_template_directory() . '/inc/widgets/social-media.php';
require get_template_directory() . '/inc/widgets/gallery-carousel.php';
require get_template_directory() . '/inc/widgets/feedburner.php';
require get_template_directory() . '/inc/widgets/tabs.php';
require get_template_directory() . '/inc/widgets/flickr-feed.php';
require get_template_directory() . '/inc/widgets/sub-categories.php';
require get_template_directory() . '/inc/widgets/twitter-feed.php';
//require get_template_directory() . '/inc/widgets/default.php';

/**
 * Register widgets
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
function warta_register_widgets() {    
    /**
     * Twitter feed widget
     * -------------------------------------------------------------------------
     */
    $friskamax_warta = get_option('friskamax_warta');
    if( !!$friskamax_warta ) {
                $consumer_key       = trim( $friskamax_warta['twitter_consumer_key'] ); 
                $consumer_secret    = trim( $friskamax_warta['twitter_consumer_secret'] ); 
                $access_token       = trim( $friskamax_warta['twitter_access_token'] ); 
                $access_secret      = trim( $friskamax_warta['twitter_access_token_secret'] );  

                if( !!$consumer_key && !!$consumer_secret && !!$access_token && !!$access_secret ) {
                        register_widget( 'Warta_Twitter_Feed' );
                }    
    }
    
    /**
     * Warta widgets
     * -------------------------------------------------------------------------
     */
    register_widget( 'Warta_Slider_Tabs' );
    register_widget( 'Warta_Articles' );
    register_widget( 'Warta_Advertisement' );
    register_widget( 'Warta_Social_Media' );
    register_widget( 'Warta_Gallery_Carousel' );
    register_widget( 'Warta_Feedburner' );
    register_widget( 'Warta_Tabs' );
    register_widget( 'Warta_Flickr_Feed' );
    register_widget( 'Warta_Sub_Categories' );
}
add_action( 'widgets_init', 'warta_register_widgets' );

/**
 * Enqueue widget scripts on admin page
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
function warta_widget_scripts() {
    wp_enqueue_style( 'warta-widgets', get_template_directory_uri() . '/css/admin/widgets.css'  );
    wp_enqueue_script( 'warta-widgets', get_template_directory_uri() . '/js/admin/widgets.js' , array('jquery'), '1.0', TRUE);
}
add_action( 'sidebar_admin_page', 'warta_widget_scripts' );