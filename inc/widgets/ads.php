<?php
/**
 * Advertisement Widget
 * 
 * @package Warta
 */

/**
 * Adds Advertisement widget.
 * =============================================================================
 */
class Warta_Advertisement extends WP_Widget {

	/**
	 * Register widget with WordPress.
     * =====================================================================
	 */
	function __construct() {
		parent::__construct(
			'warta_ads', // Base ID
			__('[Warta] Advertisement', 'warta'), // Name
			array( 'description' => __( 'Advertisement.', 'warta' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
     * =====================================================================
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
            extract($instance);
            
            $title      = apply_filters( 'widget_title', $title );
            $classes    = $hide_on_mobile ? ' hidden-xs' : '';
            $classes    .= $hide_on_mobile && ( $ad_type == 'img' ) ? ' no-mobile' : '';
            $id         = 'id' . rand();
            ?>

            <!-- Widget Wrapper
            ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            <section class="widget col-md-12 <?php echo $classes ?>" id="<?php echo $id ?>">
            
                <!-- Widget Header
                ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                <?php if( !empty($title) ) echo $args['before_title'] . $title . $args['after_title'] ?>
                
                <!-- Widget Content
                ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                <div class="frame thick">

                    <?php if( $ad_type === 'img' ) : // Banner image ?>
                    
                        <a href="<?php echo esc_url($ad_url) ?>">
                            
                            <?php if( $hide_on_mobile ) : ?>
                            
                                <div data-src="<?php echo esc_url($img_url) ?>" data-alt="<?php echo esc_attr($title) ?>"></div>
                            
                            <?php else : ?>
                                
                                <img src="<?php echo esc_url($img_url) ?>" alt="<?php echo esc_attr($title) ?>">
                            
                            <?php endif ?>
                        </a>
                    
                    <?php else :  ?>
                                <?php if($hide_on_mobile) : ?>
                                        <script>if( jQuery(window).width() <= 768 ) jQuery('#<?php echo $id ?>').remove();</script>
                                <?php endif ?>
                    <?php echo $ad_code; endif; ?>
                        
                </div>
                <?php echo warta_image_shadow() ?>
            
            </section><!--.widget-->

            <?php
	}

	/**
	 * Back-end widget form.
         * =====================================================================
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {                        
		$defaults   = array(
                        'title'         => __( 'New title', 'warta' ),
                        'ad_type'       => 'img',
                        'hide_on_mobile'=> 1,
                        'img_url'       => '',
                        'ad_url'        => '',
                        'hide_on_mobile'=> 0,
                        'ad_code'       => ''
                    );
        $args       = wp_parse_args( $instance, $defaults );

        extract($args); 

        $is_img     = $ad_type === 'img';
        $is_js      = $ad_type === 'js';
?>

                <!--Widget Form Wrapper
                ------------------------------------------------------------ -->
                <div class="warta-widget">
                    
                    <!--Title
                    -------------------------------------------------------- -->
                    <p>
                        <label><?php _e('Title:', 'warta') ?> 
                            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
                        </label>
                    </p>
                    
                    <!--Advertisement Type
                    -------------------------------------------------------- -->
                    <p>
                        <label><?php _e('Advertisement type:', 'warta') ?>
                            <select name="<?php echo $this->get_field_name( 'ad_type' ) ?>" class="widefat warta-option-choice">
                                <option value="img" <?php selected( $ad_type, 'img' ) ?>><?php _e('Image', 'warta') ?></option>
                                <option value="js" <?php selected( $ad_type, 'js' ) ?>><?php _e('JavaScript', 'warta') ?></option>
                            </select>
                        </label>
                    </p>

                    <!--Image URL
                    -------------------------------------------------------- -->
                    <p class="warta-option warta-option-img <?php if(!$is_img) echo 'hidden"' ?>">
                        <label><?php _e('Image URL:', 'warta') ?> 
                            <input class="widefat" name="<?php echo $this->get_field_name( 'img_url' ); ?>" type="url" value="<?php echo esc_url( $img_url ); ?>">
                        </label>
                        <small><?php _e('Recommended image size on main section is 730px wide and in sidebar section is 340px wide.', 'warta') ?></small>
                    </p>

                    <!--Advertisement URL
                    -------------------------------------------------------- -->
                    <p class="warta-option warta-option-img <?php if(!$is_img) echo 'hidden"' ?>">
                        <label><?php _e('Advertisement URL:', 'warta') ?> 
                            <input class="widefat" name="<?php echo $this->get_field_name( 'ad_url' ); ?>" type="url" value="<?php echo esc_url( $ad_url ); ?>">
                        </label>
                    </p>

                    <!--Advertisement Code
                    -------------------------------------------------------- -->
                    <p class="warta-option warta-option-js <?php if(!$is_js) echo 'hidden"' ?>">
                        <label><?php _e('Advertisement code:', 'warta') ?> 
                            <textarea rows="8" class="widefat" name="<?php echo $this->get_field_name( 'ad_code' ); ?>" type="text"><?php echo esc_textarea( $ad_code ) ?></textarea>
                        </label>
                        <small><?php _e('Please use responsive ad unit in order to be displayed correctly in all screen sizes. '
                                . 'If you\'re using Google Adsense, you can find the tutorial <a target="_blank" href="https://support.google.com/adsense/answer/3213689">here</a>.', 'warta') ?>
                        </small>
                    </p>
                    
                    <!--Hide on mobile
                    -------------------------------------------------------- -->
                    <p>
                        <label>
                            <input name="<?php echo $this->get_field_name( 'hide_on_mobile' ); ?>" type="checkbox" value="1" <?php checked($hide_on_mobile, 1) ?>>
                            <?php _e('Hide on mobile devices', 'warta') ?> 
                        </label><br>
                        <small><?php _e('Recommended for better performance.', 'warta') ?></small>
                    </p>
                    
                </div><!--.warta-widget-->
                
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
         * =====================================================================
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title']          = sanitize_text_field( $new_instance['title'] );
        $instance['ad_type']        = sanitize_text_field( $new_instance['ad_type'] );
        $instance['img_url']        = esc_html( $new_instance['img_url'] );
        $instance['ad_url']         = esc_html( $new_instance['ad_url'] );
		$instance['hide_on_mobile'] = (int) $new_instance['hide_on_mobile'];
                
        if ( current_user_can('unfiltered_html') )
                $instance['ad_code'] =  $new_instance['ad_code'];
		else
                $instance['ad_code'] = stripslashes( wp_filter_post_kses( addslashes($new_instance['ad_code']) ) ); // wp_filter_post_kses() expects slashed
                
		return $instance;
	}

} // class Warta_Advertisement