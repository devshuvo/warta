<?php
/**
 * Warta Feedburner Widget initialization
 *
 * @package Warta
 */

/**
 * Adds Feedburner widget.
 * =============================================================================
 */
class Warta_Feedburner extends WP_Widget {

	/**
	 * Register widget with WordPress.
     * =====================================================================
	 */
	function __construct() {
		parent::__construct(
			'warta_feedburner', // Base ID
			__('[Warta] Feedburner', 'warta'), // Name
			array( 'description' => __( 'Feedburner email subscription.', 'warta' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
     * =====================================================================
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
                extract($instance);            
                
                $title = apply_filters( 'widget_title', $title );
		?>

                <!-- FEEDBURNER WIDGET
                ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                <section class="<?php warta_widget_class( $args['id'] ) ?> feedburner">

                    <!-- Widget Header
                    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                    <?php if ( ! empty( $title ) ) echo $args['before_title'] . $title . $args['after_title']; ?>

                    <!-- Widget Content
                    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                    <form action="http://feedburner.google.com/fb/a/mailverify" 
                            method="post" 
                            target="popupwindow" 
                            onsubmit="window.open(
                              'http://feedburner.google.com/fb/a/mailverify?uri=<?php echo esc_attr( $feedburner_id ) ?>', 
                              'popupwindow', 
                              'scrollbars=yes,width=550,height=520'
                      );return true">
                          <div class="input-group">
                              <i class="fa fa-envelope"></i>
                              <input type="email" name="email" class="input-light" placeholder="Enter your email address" />                                        
                          </div>
                          <input type="hidden" value="<?php echo esc_attr( $feedburner_id ) ?>" name="uri"/>
                          <input type="hidden" name="loc" value="en_US"/>
                          <input type="submit" value="Subscribe" />   
                      </form>
                </section>

                <?php warta_add_clearfix( $args['id'], 6) ?>
                    
                <?php
	}

	/**
	 * Back-end widget form.
     * =====================================================================
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$defaults   = array(
                        'title'         => __( 'New title', 'warta' ),
                        'feedburner_id' => ''
                    );
        $args       = wp_parse_args( $instance, $defaults );

        extract($args); 
?>
		
                <!--Widget Form Wrapper
                ------------------------------------------------------------ -->
                <div class="warta-widget">
                    
                    <!--Title
                    -------------------------------------------------------- -->
                    <p>
                        <label><?php _e('Title:', 'warta') ?> 
                            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
                        </label>
                    </p>
                    
                    <p>
                        <label><?php _e('Feedburner ID:', 'warta') ?>
                            <input class="widefat" name="<?php echo $this->get_field_name('feedburner_id') ?>" value="<?php echo esc_attr( $feedburner_id ) ?>">
                        </label>
                        <small><?php _e('Example: If your Feedburner URL is http://feeds.feedburner.com/YourFeedburnerURI, then <strong>YourFeedburnerURI</strong> is your ID', 'warta') ?></small>
                    </p>
                    
                </div>
                    
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
         * =====================================================================
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title']          = sanitize_text_field( $new_instance['title'] );
		$instance['feedburner_id']  = sanitize_text_field( $new_instance['feedburner_id'] );

		return $instance;
	}

} // class Warta_Feedburner