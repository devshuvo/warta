<?php
/**
 * Article v2 widget layout
 * 
 * @package Warta
 */

$featured_media     = warta_match_featured_media();

        if( $counter++ === 0 ) : // first article ?>

                <article class="<?php echo warta_is_main( $args['id'] ) ? 'col-sm-6' : 'col-sm-12' ?> article-medium">    
<?php                   
                        warta_featured_image( array(
                                'size'                  => 'medium',
                                'featured_media'        => $featured_media,
                                'caption'               => TRUE,
                        )); // featured image
?>                       
                        <p><?php echo warta_the_excerpt_max_charlength($excerpt) ?></p><!--excerpt--> 
<?php       
                        if( !$hide_post_meta_all ) {
                                echo warta_posted_on(array(
                                        'meta_date'         => $meta_date,
                                        'date_format'       => $date_format,
                                        'meta_format'       => $meta_format,
                                        'meta_comments'     => $meta_comments,
                                        'meta_views'        => $meta_views,
                                        'meta_category'     => $meta_category,
                                        'meta_author'       => $meta_author,
                                        'meta_review_score' => $meta_review_score,
                                ));
                        } // post meta
?>

                        <hr class="<?php if( warta_is_main( $args['id'] ) ) echo 'visible-xs' ?>">

                </article><!--1st col-->

                <div class="<?php echo warta_is_main( $args['id'] ) ? 'col-sm-6' : 'col-sm-12' ?>">

<?php   else: // > 1 articles 

                        require __DIR__ . '/article-tiny.php';

        endif; // article counter 