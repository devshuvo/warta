<?php
/**
 * Article tiny template
 * 
 * @package Warta
 */
?>
<article class="article-tiny">
<?php                   
        warta_featured_image( array(
                'size'      => 'tiny',
        )); // featured image
?>  
        <h5><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h5><!--title-->
<?php                   
        if( !$hide_post_meta_all ) {
                echo warta_posted_on(array(
                        'meta_date'         => $meta_date,
                        'date_format'       => $date_format,
                        'meta_format'       => $meta_format,
                        'meta_comments'     => $meta_comments,
                        'meta_views'        => $meta_views,
                        'meta_category'     => $meta_category,
                        'meta_author'       => $meta_author,
                        'meta_review_score' => $meta_review_score,
                ));
        } // post meta
?>
        <hr>

</article><!--.article-tiny-->