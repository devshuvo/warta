<?php
/**
 * Article v1 widget layout
 * 
 * @package Warta
 */

global  $content_width, 
        $wp_embed;

$content_width                  = 360;
$friskamax_warta_var['sidebar_counter']   += warta_is_full( $args['id'] ) ? 6 : 0; // Column counter. Used for clearfix.
?>

<section class="headline <?php warta_widget_class( $args['id'], 12) ?>">

    <?php if( !empty($title) ) : ?>

        <!-- Widget Header
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
        <header class="clearfix">
                <h4><?php echo strip_tags( $title ) ?></h4>            
<?php           echo $widget_more_link ?>
        </header><!--header-->

    <?php endif ?>

<?php 
    /**
     * Widget Contents
     * -------------------------------------------------------------------------
     */
    while ( $the_query->have_posts() ) : 
        $the_query->the_post();
    
        $format             = get_post_format() ? get_post_format() : 'standard';
        $hide_post_meta_all = get_post_meta( get_the_ID(), 'friskamax_hide_post_meta_all', true );
        $template           = __DIR__ . "/articles-1-{$format}.php";
        
        if(file_exists($template)) {
            require $template;
        } else { 
            require __DIR__ . "/articles-1-standard.php";
        }
        
    endwhile; // $the_query->have_posts() 
?>

</section><!--.widget-->