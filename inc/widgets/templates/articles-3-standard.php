<?php
/**
 * Article v3 widget standard layout
 * 
 * @package Warta
 */

        if($counter++ === 0) : // first article ?>

                <article class="article-medium">    
<?php                   
                        warta_featured_image( array(
                                'size'      => 'medium',
                                'caption'   => TRUE
                        )); // featured image
?>                        
                        <p><?php echo warta_the_excerpt_max_charlength($excerpt) ?></p><!--excerpt-->  
<?php                   
                        if( !$hide_post_meta_all ) {
                                echo warta_posted_on(array(
                                        'meta_date'         => $meta_date,
                                        'date_format'       => $date_format,
                                        'meta_format'       => $meta_format,
                                        'meta_comments'     => $meta_comments,
                                        'meta_views'        => $meta_views,
                                        'meta_category'     => $meta_category,
                                        'meta_author'       => $meta_author,
                                        'meta_review_score' => $meta_review_score,
                                ));
                        }  
?>
                        <hr>

                </article><!--.article-medium-->

<?php   else: // > 1 articles 

                require __DIR__ . '/article-tiny.php';

        endif; // article counter 