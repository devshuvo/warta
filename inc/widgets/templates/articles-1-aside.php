<?php
/**
 * Article v1 widget aside layout
 * 
 * @package Warta
 */
?>
<article class="article-medium">

        <div class="row">

                <div class="<?php echo ( warta_is_sidebar($args['id']) || warta_is_full($args['id'])  ) ? 'col-sm-12' : 'col-sm-6' ?>">
<?php                   
                        warta_featured_image( array(
                                'size' => 'medium'
                        )); // featured image
?>          
                </div><!--1st col-->

                <div class="<?php echo ( warta_is_sidebar($args['id']) || warta_is_full($args['id']) ) ? 'col-sm-12' : 'col-sm-6' ?>">
<?php       
                        if( !$hide_post_meta_all ) {
                                echo warta_posted_on(array(
                                        'meta_date'         => $meta_date,
                                        'date_format'       => $date_format,
                                        'meta_format'       => $meta_format,
                                        'meta_comments'     => $meta_comments,
                                        'meta_views'        => $meta_views,
                                        'meta_category'     => $meta_category,
                                        'meta_author'       => $meta_author,
                                        'meta_review_score' => $meta_review_score,
                                ));
                        } // post meta            
            
                        the_content(); // content
?>
                </div><!--2nd col-->

        </div><!--.row-->

</article><!--.article-medium-->
