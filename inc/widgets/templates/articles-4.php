<?php
/**
 * Article v2 widget layout
 * 
 * @package Warta
 */

$friskamax_warta_var['sidebar_counter'] += warta_is_main( $args['id'] ) ? 3 : 0; // Column counter. Used for clearfix.
$friskamax_warta_var['sidebar_counter'] += warta_is_full( $args['id'] ) ? 6 : 0; // Column counter. Used for clearfix.
?>

<section class="<?php warta_widget_class( $args['id'], 3) ?>">

<?php   if( !empty($title) ) : ?>

                <header class="clearfix">
                        <h4><?php echo esc_html($title) ?></h4>           
<?php                   echo $widget_more_link ?>
                </header>

<?php   endif; // title 

        while ( $the_query->have_posts() ) : 
                $the_query->the_post(); 
                
                $format = get_post_format() ? get_post_format() : 'standard'; 
                
                if( $counter++ === 0 ) : // first article ?>

                        <article class="article-small">                                     
<?php                   
                                warta_featured_image( array(
                                        'size'      => 'small',
                                )); // featured image
?>

                                <h5><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h5><!--title-->
<?php 
                                echo warta_posted_on(array(
                                        'meta_date'         => $meta_date,
                                        'date_format'       => $date_format,
                                        'meta_format'       => $meta_format,
                                        'meta_author'       => $meta_author,
                                        'meta_comments'     => $meta_comments,
                                        'meta_views'        => $meta_views,
                                        'meta_category'     => $meta_category,
                                        'meta_categories'   => $meta_categories,
                                        'meta_review_score' => $meta_review_score,
                                )); // post meta
?>                                    
                                <hr>

                        </article><!--.article-small-->

<?php           else : // > 1 articles ?>

                        <article>                                    
                                <h5><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h5><!--title-->
<?php 
                                        echo warta_posted_on(array(
                                                'meta_date'         => $meta_date,
                                                'date_format'       => $date_format,
                                                'meta_format'       => $meta_format,
                                                'meta_author'       => $meta_author,
                                                'meta_comments'     => $meta_comments,
                                                'meta_views'        => $meta_views,
                                                'meta_category'     => $meta_category,
                                                'meta_categories'   => $meta_categories,
                                                'meta_review_score' => $meta_review_score,
                                        )); // post meta
?>
                                <hr>                                    
                        </article>

<?php           endif; // articles counter 

        endwhile; // $the_query->have_posts() ?>

</section><!--.widget-->