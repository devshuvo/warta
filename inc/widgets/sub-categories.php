<?php

/**
 * Sub Categories Widget
 * 
 * @package Warta
 */

/**
 * Adds Sub Categories widget.
 */
class Warta_Sub_Categories extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'warta_sub_categories', // Base ID
			__('[Warta] Sub Categories', 'warta'), // Name
			array( 'description' => __( 'Displays sub categories of a selected parent category.', 'warta' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
        extract($instance);
                
		$title      = apply_filters( 'widget_title', $title );
        $categories = get_categories( array( 'parent' => $category ) );

?>
                
                <section class="<?php warta_widget_class( $args['id'] ) ?>">
<?php                   echo $args['before_title'] ?>
                                <a href="<?php echo esc_url( get_category_link($category) ) ?>">
<?php                                   if ( ! empty( $title ) )  
                                                echo $title; 
                                        else
                                                echo get_cat_name( $category );
?>	
                                </a>
<?php                   echo $args['after_title'] ?>

                        <ul class="categories">

                            <?php foreach( $categories as $category ) : ?>

                                <li>
                                    <a href="<?php echo esc_url( get_category_link( $category->term_id ) ) ?>" title="<?php echo esc_attr( sprintf( __( "View %d posts under %s category", 'warta' ), $category->count, $category->name ) ) ?>">
                                        <i class="fa <?php echo is_rtl() ? 'fa-angle-left' : 'fa-angle-right' ?>"></i> <?php echo strip_tags( $category->name ) ?>
                                    </a>
                                </li>

                            <?php endforeach ?>

                        </ul>
                    
                </section>
                
		<?php warta_add_clearfix( $args['id'], $col = 6 );
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$defaults   = array(
                        'title'         => __( 'New title', 'warta' ),
                        'category'      => 0
                    );
        $args       = wp_parse_args( $instance, $defaults );

        extract($args);                 
?>
		
                <!--Widget Form Wrapper
                ------------------------------------------------------------ -->
                <div class="warta-widget">
                    
                    <!--Title
                    -------------------------------------------------------- -->
                    <p>
                        <label><?php _e('Title:', 'warta') ?> 
                            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
                        </label>
                    </p>
                    
                    <!--Category
                    -------------------------------------------------------- -->
                    <p>
                        <label><?php _e('Parent Category:', 'warta') ?>
                            <select name="<?php echo $this->get_field_name( 'category' ) ?>" class="widefat">
<?php 
                                    $categories = get_categories( array( 'parent' => 0 ) ); 
                                    foreach ($categories as $cat) :  ?>
                                            <option value="<?php echo $cat->term_id ?>" <?php selected($category, $cat->term_id) ?>>
<?php                                                       echo $cat->cat_name ?>
                                            </option>
<?php                               endforeach; ?>
                            </select>
                        </label>
                    </p>
                                        
                </div>
                
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title']          = strip_tags( $new_instance['title'] );
		$instance['category']       = (int) $new_instance['category'];

		return $instance;
	}

} // class Sub Categories