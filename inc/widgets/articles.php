<?php
/**
 * Articles Widget
 * 
 * @package Warta
 */

/**
 * Adds Articles widget.
 * =============================================================================
 */
class Warta_Articles extends WP_Widget {

	/**
	 * Register widget with WordPress.
     * =====================================================================
	 */
	function __construct() {
		parent::__construct(
			'warta_articles', // Base ID
			__('[Warta] Articles', 'warta'), // Name
			array( 'description' => __( 'Articles box.', 'warta' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
     * =====================================================================
	 *
	 * @see WP_Widget::widget()
     * 
     * @global array $friskamax_warta_var Warta theme variables
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) { 
            global $friskamax_warta_var;    
            
            extract($instance);
                        
            $title          = apply_filters( 'widget_title', $title );
            $query_args     = array( 
                                'posts_per_page'        => $count,
                                'ignore_sticky_posts'   => $ignore_sticky
                            ); // query arguments
            $counter        = 0; // articles counter
            $template       = __DIR__ . "/templates/articles-{$layout}.php";

            /**
             * What kind of data to retrieve
             * -----------------------------------------------------------------
             */ 
            switch ( $data ) {                
                case 'category':
                    $query_args['cat']  = $category;
                    break;
                
                case 'tags':
                    $query_args['tax_query'] = array(
                            array(
                                    'taxonomy'  => 'post_tag',
                                    'field'     => 'slug',
                                    'terms'     => explode(',', $tags)
                            )
                    );
                    break;
                
                case 'review':
                    $query_args['meta_key']     = 'friskamax_review_total';  
                    $query_args['meta_value']   = 0;
                    $query_args['meta_compare'] = '>';                  
                    
                    if( $top_review ) {
                        $query_args['orderby']  = 'meta_value_num';
                    }
                    
                    break;
               
                case 'popular':
                    if( $sort  === 'comments' ) {    
                        $query_args['orderby']  = 'comment_count'; 
                    } else {
                        $query_args['meta_key'] = 'warta_post_views_count';
                        $query_args['orderby']  = 'meta_value_num';
                    }                    
                    break;
                    
                case 'post_ids':
                    $query_args['post__in']     = explode(',', $post_ids);
                    break;

                default:
                    break;                
            }    
            
            // Time Range
            if( ( $data == 'review' || $data == 'popular' ) && $time_range != 'all' ) {
                        switch ($time_range) {
                                case 'year':
                                        $query_args['date_query'] = array(
                                                    array( 'year' => date('Y') ),
                                        );
                                        break;
                                case 'month':
                                        $query_args['date_query'] = array(
                                                    array( 
                                                        'year' => date('Y'),
                                                        'month'=> date('n')
                                                    ),
                                        );
                                        break;
                                case 'week':
                                        $query_args['date_query'] = array(
                                                    array( 
                                                        'year' => date('Y'),
                                                        'week' => date('W')
                                                    ),
                                        );
                                        break;
                        }
            }

            $the_query = new WP_Query( $query_args );

            if ( $the_query->have_posts() && file_exists( $template ) ) {
                    /**
                     * Widget more link
                     * ---------------------------------------------------------
                     */
                    ob_start();
                    
                    if( $data == 'category' ) : ?>
                            <a href="<?php echo esc_url( get_category_link( $category ) ) ?>" class="control" title="<?php _e('More posts', 'warta') ?>">
                                    <i class="fa fa-plus"></i>
                            </a>
<?php               elseif( $data == 'latest' && get_option('page_for_posts') ) : ?>                            
                            <a href="<?php echo esc_url( get_page_link( get_option('page_for_posts') ) ) ?>" class="control" title="<?php _e('More posts', 'warta') ?>">
                                    <i class="fa fa-plus"></i>
                            </a>
<?php               endif;

                    $widget_more_link = ob_get_clean();
                
                    require $template; // get the template
                    warta_add_clearfix( $args['id'] ); // adds clearfix
            }       
            
            wp_reset_postdata(); // Restore original Post Data
	}

	/**
	 * Back-end widget form.
         * =====================================================================
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {                  
                $defaults   = array(
                                'title'                 => __( 'New title', 'warta' ),
                                'layout'                => 1,
                                'data'                  => 'latest',
                                'category'              => 0,
                                'tags'                  => '',
                                'sort'                  => '',
                                'time_range'            => 'all',
                                'post_ids'              => '',
                                'top_review'            => 0,
                                'count'                 => 4,
                                'excerpt'               => 160,
                                'ignore_sticky'         => 1,
                                'date_format'           => 'F j, Y',
                                'meta_date'             => 1,
                                'meta_format'           => 0,
                                'meta_comments'         => 1,
                                'meta_views'            => 1,
                                'meta_category'         => 1,
                                'meta_categories'       => 0,
                                'meta_author'           => 0,
                                'meta_review_score'     => 0,
                            );
                $args       = wp_parse_args( $instance, $defaults );
                
                extract($args);  
                
                $is_category    = $data === 'category';
                $is_tags        = $data === 'tags';
                $is_popular     = $data === 'popular';
                $is_post_ids    = $data === 'post_ids';
                $is_review      = $data === 'review';
                
		?>
                    
                <!--Widget Form Wrapper
                ------------------------------------------------------------ -->
                <div class="warta-widget">
                    
                    <!--Layout
                    -------------------------------------------------------- -->
                    <p>
                        <?php _e('Layout:', 'warta') ?><br>
                        <label class="warta-image-radio">
                            <input type="radio" name="<?php echo $this->get_field_name( 'layout' ); ?>" value="1" <?php checked($layout, 1) ?>>
                            <img src="<?php echo get_template_directory_uri() . '/img/admin/layout-article-v1.jpg' ?>">
                        </label>
                        <label class="warta-image-radio">
                            <input type="radio" name="<?php echo $this->get_field_name( 'layout' ); ?>" value="2" <?php checked($layout, 2) ?>>
                            <img src="<?php echo get_template_directory_uri() . '/img/admin/layout-article-v2.jpg' ?>">
                        </label>
                        <label class="warta-image-radio">
                            <input type="radio" name="<?php echo $this->get_field_name( 'layout' ); ?>" value="3" <?php checked($layout, 3) ?>>
                            <img src="<?php echo get_template_directory_uri() . '/img/admin/layout-article-v3.jpg' ?>">
                        </label>
                        <label class="warta-image-radio">
                            <input type="radio" name="<?php echo $this->get_field_name( 'layout' ); ?>" value="4" <?php checked($layout, 4) ?>>
                            <img src="<?php echo get_template_directory_uri() . '/img/admin/layout-article-v4.jpg' ?>">
                        </label>
                    </p>

                    <!--Title
                    -------------------------------------------------------- -->
                    <p>
                        <label><?php _e( 'Title:', 'warta' ); ?>
                            <input class="widefat" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" value="<?php echo sanitize_text_field( $title ); ?>">
                        </label> 
                    </p>

                    <!--Data
                    -------------------------------------------------------- -->
                    <p>
                        <label><?php _e('Data:', 'warta') ?>
                            <select name="<?php echo $this->get_field_name( 'data' ) ?>" class="widefat warta-option-choice">
                                <option value="latest" <?php selected( $data, 'latest' ) ?>><?php _e('Latest posts', 'warta') ?></option>
                                <option value="popular" <?php selected( $data, 'popular' ) ?>><?php _e('Popular posts', 'warta') ?></option>
                                <option value="category" <?php selected( $data, 'category' ) ?>><?php _e('Posts by category', 'warta') ?></option>
                                <option value="tags" <?php selected( $data, 'tags' ) ?>><?php _e('Posts by tags', 'warta') ?></option>
                                <option value="review" <?php selected( $data, 'review' ) ?>><?php _e('Review posts', 'warta') ?></option>
                                <option value="post_ids" <?php selected( $data, 'post_ids' ) ?>><?php _e('Posts by IDs', 'warta') ?></option>
                            </select>
                        </label>
                    </p>

                    <!--Category: categories list
                    ======================================================== -->
                    <p class="warta-option 
                       warta-option-category <?php if(!$is_category) echo 'hidden"' ?>">
                        <label><?php _e('Category:', 'warta') ?>
                            <?php wp_dropdown_categories(array(
                                'name'          => $this->get_field_name( 'category' ),
                                'class'         => 'widefat',
                                'selected'      => $category,
                                'hierarchical'  => TRUE
                            )) ?>
                        </label>
                    </p>

                    <!--Tags: selected tags
                    ======================================================== -->
                    <p class="warta-option 
                       warta-option-tags <?php if(!$is_tags) echo 'hidden"' ?>">
                        <label><?php _e('Tags:', 'warta') ?>
                            <input type="text" name="<?php echo $this->get_field_name( 'tags' ) ?>" value="<?php echo esc_attr( $tags ) ?>" class="widefat">
                        </label>
                        <small><?php _e('Enter the tag slugs, separated by commas.', 'warta') ?></small>
                    </p>

                    <!--Popular: Sort by
                    ======================================================== -->
                    <p class="warta-option 
                       warta-option-popular <?php if(!$is_popular) echo 'hidden' ?>">
                        <label><?php _e('Sort by:', 'warta') ?>
                            <select name="<?php echo $this->get_field_name( 'sort' ); ?>" class="widefat">
                                <option value="comments" <?php selected($sort, 'comments') ?>><?php _e('Comments count', 'warta') ?></option>
                                <option value="views" <?php selected($sort, 'views') ?>><?php _e('Views count', 'warta') ?></option>
                            </select>
                        </label>
                    </p>

                    <!--Popular: time range
                    ======================================================== -->
                    <p class="warta-option 
                       warta-option-popular 
                       warta-option-review
                               <?php if(!$is_popular && !$is_review) echo 'hidden' ?>">
                                <label><?php _e('Time range:', 'warta') ?>
                                    <select name="<?php echo $this->get_field_name( 'time_range' ); ?>" class="widefat">
                                        <option value="all" <?php selected($time_range, 'all') ?>><?php _e('All time', 'warta') ?></option>
                                        <option value="year" <?php selected($time_range, 'year') ?>><?php _e('This year', 'warta') ?></option>
                                        <option value="month" <?php selected($time_range, 'month') ?>><?php _e('This month', 'warta') ?></option>
                                        <option value="week" <?php selected($time_range, 'week') ?>><?php _e('This week', 'warta') ?></option>
                                    </select>
                                </label>
                    </p>

                    <!--Post IDs: The posts IDs
                    ======================================================== -->
                    <p class="warta-option 
                       warta-option-post-ids <?php if(!$is_post_ids) echo 'hidden' ?>">
                        <label><?php _e('Post IDs:', 'warta') ?>
                            <input class="widefat" name="<?php echo $this->get_field_name( 'post_ids' ); ?>" type="text" value="<?php echo esc_attr( $post_ids ); ?>">
                        </label>
                        <small><?php _e('Enter the post IDs, separated by commas.', 'warta') ?></small>
                    </p>

                    <!--Review: Top review
                    ======================================================== -->
                    <p class="warta-option 
                       warta-option-review <?php if(!$is_review) echo 'hidden' ?>">
                        <label>
                            <input name="<?php echo $this->get_field_name( 'top_review' ); ?>" type="checkbox" value="1" <?php checked($top_review, 1) ?>>
                            <?php _e('Sort by review score', 'warta') ?>
                        </label>
                    </p>

                    <!--Number of Items to Show
                    -------------------------------------------------------- -->
                    <p>
                        <label><?php _e('Number of items to show:', 'warta') ?>
                            <input class="widefat" name="<?php echo $this->get_field_name( 'count' ); ?>" type="number" min="1" max="20" value="<?php echo esc_attr( $count ); ?>">
                        </label>
                    </p>

                    <!--Excerpt Length
                    -------------------------------------------------------- -->
                    <p>
                        <label><?php _e('Excerpt Length:', 'warta') ?>
                            <input class="widefat" name="<?php echo $this->get_field_name( 'excerpt' ); ?>" type="number" min="100" max="1000" value="<?php echo esc_attr( $excerpt ); ?>">
                        </label>
                        <small><?php _e('How many characters do you want to show?', 'warta') ?></small>
                    </p>

                    <!--Date Format
                    -------------------------------------------------------- -->
                    <p>
                        <label><?php _e('Date format:', 'warta') ?>
                            <input class="widefat" name="<?php echo $this->get_field_name( 'date_format' ); ?>" type="text" value="<?php echo esc_attr( $date_format ); ?>">
                        </label>
                        <small><?php _e('You can use the <a href="http://php.net/date" target="_blank">table of date format characters on the PHP website</a> as a reference '
                                . 'for building date format strings.', 'warta') ?> </small>
                    </p>
                    
                    <!--Ignore Sticky
                    -------------------------------------------------------- -->
                    <p>
                        <label>
                            <input type="checkbox" name="<?php echo $this->get_field_name('ignore_sticky') ?>" value="1" <?php checked($ignore_sticky, 1) ?>>
                            <?php _e('Ignore sticky posts', 'warta') ?>
                        </label>
                    </p>

                    <!--Post Meta
                    -------------------------------------------------------- -->
                    <p>
                        <?php _e('Post meta:', 'warta') ?><br>
                        <label>
                            <input type="checkbox" name="<?php echo $this->get_field_name( 'meta_date' ); ?>" value="1" <?php checked($meta_date, '1') ?>"> 
                            <?php _e('Date', 'warta') ?>
                        </label><br>
                        <label>
                            <input type="checkbox" name="<?php echo $this->get_field_name( 'meta_format' ); ?>" value="1" <?php checked($meta_format, '1') ?>"> 
                            <?php _e('Post format', 'warta') ?>
                        </label><br>
                        <label>
                            <input type="checkbox" name="<?php echo $this->get_field_name( 'meta_category' ); ?>" value="1" <?php checked($meta_category, '1') ?>"> 
                            <?php _e('First category', 'warta') ?>
                        </label><br>
                        <label>
                            <input type="checkbox" name="<?php echo $this->get_field_name( 'meta_categories' ); ?>" value="1" <?php checked($meta_categories, '1') ?>"> 
                            <?php _e('All categories', 'warta') ?>
                        </label><br>
                        <label>
                            <input type="checkbox" name="<?php echo $this->get_field_name( 'meta_author' ); ?>" value="1" <?php checked($meta_author, '1') ?>"> 
                            <?php _e('Author', 'warta') ?>
                        </label><br>
                        <label>
                            <input type="checkbox" name="<?php echo $this->get_field_name( 'meta_comments' ); ?>" value="1" <?php checked($meta_comments, '1') ?>"> 
                            <?php _e('Comments count', 'warta') ?>
                        </label><br>
                        <label>
                            <input type="checkbox" name="<?php echo $this->get_field_name( 'meta_views' ); ?>" value="1" <?php checked($meta_views, '1') ?>"> 
                            <?php _e('Views count', 'warta') ?>
                        </label><br>
                        <label>
                            <input type="checkbox" name="<?php echo $this->get_field_name( 'meta_review_score' ); ?>" value="1" <?php checked($meta_review_score, '1') ?>"> 
                            <?php _e('Review score', 'warta') ?>
                        </label><br>
                    </p>
                    
                </div><!--.warta-widget-->
                    
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
         * =====================================================================
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title']              = sanitize_text_field( $new_instance['title'] );
		$instance['layout']             = (int) $new_instance['layout'];
		$instance['data']               = sanitize_text_field( $new_instance['data'] );
		$instance['category']           = (int) $new_instance['category'];
		$instance['tags']               = sanitize_text_field( $new_instance['tags'] );
		$instance['sort']               = sanitize_text_field( $new_instance['sort'] );
		$instance['time_range']         = sanitize_text_field( $new_instance['time_range'] );
		$instance['post_ids']           = sanitize_text_field( $new_instance['post_ids'] );
		$instance['top_review']         = (int) $new_instance['top_review'];
		$instance['ignore_sticky']      = (int) $new_instance['ignore_sticky'];
		$instance['count']              = (int) $new_instance['count'];
		$instance['excerpt']            = (int) $new_instance['excerpt'];
		$instance['date_format']        = sanitize_text_field( $new_instance['date_format'] );
		$instance['meta_date']          = (int) $new_instance['meta_date'];
		$instance['meta_format']        = (int) $new_instance['meta_format'];
		$instance['meta_comments']      = (int) $new_instance['meta_comments'];
		$instance['meta_views']         = (int) $new_instance['meta_views'];
		$instance['meta_category']      = (int) $new_instance['meta_category'];
		$instance['meta_categories']    = (int) $new_instance['meta_categories'];
		$instance['meta_author']        = (int) $new_instance['meta_author'];
		$instance['meta_review_score']  = (int) $new_instance['meta_review_score'];

		return $instance;
	}

} // class Warta_Articles