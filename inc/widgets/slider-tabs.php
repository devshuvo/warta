<?php
/**
 * Slider Tabs Widget
 * 
 * @package Warta
 */

/**
 * Adds Slider_Tabs widget.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
class Warta_Slider_Tabs extends WP_Widget {

    /**
     * Register widget with WordPress.
     * =========================================================================
     */
    function __construct() {
        parent::__construct(
            'warta_slider_tabs', // Base ID
            __('[Warta] Slider Tabs', 'warta'), // Name
            array( 'description' => __( 'Items slider in togglable tabs.', 'warta' ), ) // Args
        );
    }

    /**
     * Front-end display of widget.
     * =========================================================================
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget( $args, $instance ) { 
                global $friskamax_warta;

                extract($instance);

                $w_header   = '<ul class="nav nav-tabs">'; // widget header
                $w_content  = '<div class="tab-content">'; // widget content
                
                // Loops every tabs
                for($i = 0; $i < count($title); $i++) {
                        $active         = $i === 0 ? 'active' : '';
                        $current        = $i === 0 ? 'current' : '';
                        $in             = $i === 0 ? 'in' : '';
                        $gallery_class  = $data[$i] === 'gallery' ? 'class="da-thumbs"' : '';
                        $article_class  = $data[$i] !== 'gallery' ? 'article' : '';
                        $w_id           = $args['widget_id'] . rand();

                        $query_args     = array( 
                                                'posts_per_page'        => $count[$i],
                                                'ignore_sticky_posts'   => $ignore_sticky[$i]
                                        );

                        // Query args
                        switch ( $data[$i] ) {
                            case 'category':
                                $query_args['cat']  = $category[$i];
                                break;
                            
                            case 'tags':
                                $query_args['tax_query'] = array(
                                        array(
                                                'taxonomy'  => 'post_tag',
                                                'field'     => 'slug',
                                                'terms'     => explode(',', $tags[$i])
                                        )
                                );
                                break;

                            case 'review':
                                $query_args['meta_key']     = 'friskamax_review_total';
                                $query_args['meta_value']   = 0;
                                $query_args['meta_compare'] = '>';

                                if( $top_review[$i] ) {
                                    $query_args['orderby']  = 'meta_value_num';
                                }

                                break;

                            case 'gallery':
                                $query_args['p']    = $gallery_post[$i];
                                break;

                            case 'popular':
                                if( $sort[$i]    === 'comments' ) {    
                                    $query_args['orderby']  = 'comment_count'; 
                                } else {
                                    $query_args['meta_key'] = 'warta_post_views_count';
                                    $query_args['orderby']  = 'meta_value_num';
                                }
                                break;

                            case 'post_ids':
                                $query_args['post__in']     = explode(',', $post_ids[$i]);
                                break;
                        }
                        
                        // Time range
                        if( $time_range[$i] != 'all' && ( $data[$i] == 'review' || $data[$i] == 'popular' ) ) {
                                switch ($time_range[$i]) {
                                        case 'year':
                                                $query_args['date_query'] = array(
                                                            array( 'year' => date('Y') ),
                                                );
                                                break;
                                        case 'month':
                                                $query_args['date_query'] = array(
                                                            array( 
                                                                'year' => date('Y'),
                                                                'month'=> date('n')
                                                            ),
                                                );
                                                break;
                                        case 'week':
                                                $query_args['date_query'] = array(
                                                            array( 
                                                                'year' => date('Y'),
                                                                'week' => date('W')
                                                            ),
                                                );
                                                break;
                                }
                        }

                        $the_query  = new WP_Query( $query_args );

                        $w_header       .= '<li class="' . $active . '"><a href="#' . $w_id . '" data-toggle="tab">' . strip_tags( $title[$i] ) . '</a></li>';
                        if(is_rtl()) {
                            $w_header   .= '<li class="control ' .  $current . '"><a href="#' . $w_id . '" data-slide="prev"><span class="fa fa-chevron-left"></span></a></li>'
                                        . '<li class="control ' .  $current . '"><a href="#' . $w_id . '" data-slide="next"><span class="fa fa-chevron-right"></span></a></li>';
                        } else {
                            $w_header   .= '<li class="control ' .  $current . '"><a href="#' . $w_id . '" data-slide="next"><span class="fa fa-chevron-right"></span></a></li>'
                                        . '<li class="control ' .  $current . '"><a href="#' . $w_id . '" data-slide="prev"><span class="fa fa-chevron-left"></span></a></li>';
                        }
                                                   
                        $w_content      .= '<div class="tab-pane fade ' . $active . ' ' . $in . '" id="' . $w_id . '">'
                                                . '<div class="slider-container ' . $article_class . '">'
                                                        . '<ul ' . $gallery_class . '>';

                        // Get the items
                        if ( $the_query->have_posts() ) {
                                while ( $the_query->have_posts() ) { 
                                        $the_query->the_post(); 

                                        $format                 = get_post_format() ? get_post_format() : 'standard';
                                        $hide_post_meta_all     = get_post_meta( get_the_ID(), 'friskamax_hide_post_meta_all', true );

                                        /**
                                         * Gallery Items
                                         * -------------------------------------
                                         */
                                        if( $data[$i] === 'gallery' && get_post_gallery() ) {                                                
                                                                                                
                                                $matches_gallery = warta_match_gallery();
                                                
                                                $attachments = get_posts( array(
                                                        'include'               => implode(',', $matches_gallery['image_ids']), 
                                                        'post_status'           => 'inherit', 
                                                        'post_type'             => 'attachment', 
                                                        'post_mime_type'        => 'image',
                                                ) );

                                                // Retrieve image attachements
                                                if ( $attachments ) {
                                                        foreach ( $attachments as $attachment ) {  
                                                                $caption                = wptexturize( $attachment->post_excerpt );
                                                                $caption_excerpt        = $caption
                                                                                        ? '<span>' . warta_the_excerpt_max_charlength( $caption_length[$i], $caption) . '</span>'
                                                                                        : '';
                                                                $attachment_huge        = wp_get_attachment_image_src( $attachment->ID, 'huge');
                                                                $attachment_gallery     = wp_get_attachment_image_src( $attachment->ID, 'gallery');

                                                                $w_content 
                                                                        .= '<li>'
                                                                                . '<a href="' . esc_url( $attachment_huge[0] ) . '" title="' . esc_attr( $caption ) . '" data-lightbox-gallery="' . $w_id . '">';

                                                                // Hide on mobile devices
                                                                if( $hide_mobile)
                                                                        $w_content      .= '<div data-src="' . esc_url( $attachment_gallery[0] ) . '" data-alt="' . esc_attr( $caption ) . '"></div>';
                                                                else
                                                                        $w_content      .= '<img src="' . esc_url( $attachment_gallery[0] ) . '" alt="' . esc_attr( $caption ) . '">';

                                                                $w_content              .= '<div class="image-caption">' . $caption_excerpt . '</div>'
                                                                                        . '<span class="image-light"></span>'
                                                                                . '</a>'
                                                                        . '</li>';
                                                        }
                                                }
                                        } 
                                        
                                        /**
                                         * Article items
                                         * -------------------------------------
                                         */
                                        else {                                       
                                                $w_content      .= '<li class="article-small">';

                                                if(has_post_thumbnail()) { // featured image
                                                        $attachment_sm  = wp_get_attachment_image_src( get_post_thumbnail_id(), 'small');
                                                        $w_content      .= '<a href="' . esc_url( get_permalink() ) . '" class="image">';

                                                        // Hide on mobile devices
                                                        if( $hide_mobile )
                                                                $w_content      .= '<div data-src="' . esc_url( $attachment_sm[0] ) . '" data-alt="' . get_the_title() . '"></div>';
                                                        else
                                                                $w_content      .= '<img src="' . esc_url( $attachment_sm[0] ) . '" alt="' . get_the_title() . '">';

                                                        $w_content              .= '<div class="image-light"></div>'
                                                                                . '<div class="link">'
                                                                                        . '<span class="dashicons dashicons-format-' . $format . '"></span>'
                                                                                . '</div>'
                                                                        . '</a>'; // .image
                                                } else { // post format placeholder
                                                        $w_content      .= '<div class="image">'
                                                                                . '<div class="format-placeholder dashicons dashicons-format-' . $format . '"></div>'
                                                                                . '<div class="image-light"></div>'
                                                                        . '</div>'; // .image
                                                }

                                                $w_content              .= '<a href="' . esc_url( get_permalink() ) . '"><h5>' . get_the_title() . '</h5></a>';
                                                
                                                if( !$hide_post_meta_all ) {
                                                        $w_content      .= warta_posted_on( array(
                                                                                'meta_date'             => $meta_date[$i],
                                                                                'date_format'           => $date_format[$i],
                                                                                'meta_format'           => $meta_format[$i],
                                                                                'meta_comments'         => $meta_comments[$i],
                                                                                'meta_views'            => $meta_views[$i],
                                                                                'meta_category'         => $meta_category[$i],
                                                                                'meta_author'           => $meta_author[$i],
                                                                                'meta_review_score'     => $meta_review_score[$i],
                                                                        ) ); // post meta
                                                }
                                                
                                                $w_content      .= '</li>'; // article-small
                                        }
                                }
                        } 

                        wp_reset_postdata(); // Restore original Post Data            

                        $w_content                      .= '</ul>'
                                                . '</div>' // slider-container
                                        . '</div>'; // Tab items container
                }

                $w_header   .= '</ul>';     // widget header
                $w_content  .= '</div>';    // widget content


                // widget classes
                if( warta_is_sidebar( $args['id'] ) ) {
                        $widget_class = 'widget col-sm-6 col-md-12';
                } else if( warta_is_footer( $args['id'] ) ) {
                        $widget_class = $friskamax_warta['footer_layout'] == 1 ? 'col-md-2 col-sm-4' : 'col-md-3 col-sm-6';
                } else {
                        $widget_class = 'widget col-sm-12';
                }
                ?>

                <section id="slider-tabs-<?php echo rand() ?>" class="<?php if( $hide_mobile ) echo 'no-mobile' ?> slider-tabs <?php echo $widget_class ?>">
                        <?php echo $w_header . $w_content ?>
                </section>
        
<?php
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form( $instance ) {             
        $defaults   = array( 
                        'title'                 => array( __( 'New title', 'warta' ) ),
                        'data'                  => array( 'latest' ),
                        'category'              => array( 0 ),
                        'tags'                  => array( '' ),
                        'gallery_post'          => array( 0 ),
                        'caption_length'        => array( 60 ),
                        'sort'                  => array( 'comments' ),
                        'time_range'            => array( 'all' ),
                        'post_ids'              => array( '' ),
                        'top_review'            => array( 0 ),
                        'count'                 => array( 12 ),
                        'ignore_sticky'         => array( 1 ),
                        'date_format'           => array( 'M j, Y' ),
                        'meta_date'             => array( 1 ),
                        'meta_format'           => array( 0 ),
                        'meta_comments'         => array( 0 ),
                        'meta_views'            => array( 0 ),
                        'meta_category'         => array( 1 ),
                        'meta_author'           => array( 0 ),
                        'meta_review_score'     => array( 0 ),
                        'hide_mobile'           => 1,
                    );
        $args       = wp_parse_args( $instance, $defaults );

        extract($args);        
        ?>

        <!--Title that appears on widget settings page-->
        <input id="<?php echo $this->get_field_id( 'title' ); ?>"  type="hidden" value="<?php echo esc_attr(implode( ' / ', $title) ) ?>">

        <!--Widget Form Wrapper
        -------------------------------------------------------------------- -->
        <div class="warta-widget">
            
            <?php 
                for ($i = 0; $i < count($title); $i++): 
                    $is_category    = $data[$i] === 'category';
                    $is_tags        = $data[$i] === 'tags';
                    $is_popular     = $data[$i] === 'popular';
                    $is_latest      = $data[$i] === 'latest';
                    $is_gallery     = $data[$i] === 'gallery';
                    $is_review      = $data[$i] === 'review';
                    $is_post_ids    = $data[$i] === 'post_ids';
            ?>
            
                <!--Tab wrapper
                ------------------------------------------------------------ -->
                <div class="warta-tab">

                    <!--Title
                    -------------------------------------------------------- -->
                    <p>
                        <label><?php _e( 'Title:', 'warta' ); ?>
                            <input class="widefat" name="<?php echo $this->get_field_name( 'title' ); ?>[]" type="text" value="<?php echo sanitize_text_field( $title[$i] ); ?>">
                        </label> 
                    </p>

                    <!--Data
                    -------------------------------------------------------- -->
                    <p>
                        <label><?php _e('Data:', 'warta') ?>
                            <select name="<?php echo $this->get_field_name( 'data' ) ?>[]" class="widefat warta-option-choice">
                                <option value="latest" <?php selected( $data[$i], 'latest' ) ?>><?php _e('Latest posts', 'warta') ?></option>
                                <option value="popular" <?php selected( $data[$i], 'popular' ) ?>><?php _e('Popular posts', 'warta') ?></option>
                                <option value="category" <?php selected( $data[$i], 'category' ) ?>><?php _e('Posts by category', 'warta') ?></option>
                                <option value="tags" <?php selected( $data[$i], 'tags' ) ?>><?php _e('Posts by tags', 'warta') ?></option>
                                <option value="gallery" <?php selected( $data[$i], 'gallery' ) ?>><?php _e('Gallery images', 'warta') ?></option>
                                <option value="review" <?php selected( $data[$i], 'review' ) ?>><?php _e('Review posts', 'warta') ?></option>
                                <option value="post_ids" <?php selected( $data[$i], 'post_ids' ) ?>><?php _e('Posts by IDs', 'warta') ?></option>
                            </select>
                        </label>
                    </p>

                    <!--Category: list categories
                    ======================================================== -->
                    <p class="warta-option 
                       warta-option-category <?php if(!$is_category) echo 'hidden"' ?>">
                        <label><?php _e('Category:', 'warta') ?>
                            <?php wp_dropdown_categories(array(
                                'name'          => $this->get_field_name( 'category' ) . '[]',
                                'class'         => 'widefat',
                                'selected'      => $category[$i],
                                'hierarchical'  => TRUE
                            )) ?>
                        </label>
                    </p>

                    <!--Tags: selected tags
                    ======================================================== -->
                    <p class="warta-option 
                       warta-option-tags <?php if(!$is_tags) echo 'hidden"' ?>">
                        <label><?php _e('Tags:', 'warta') ?>
                            <input type="text" name="<?php echo $this->get_field_name( 'tags' ) ?>[]" value="<?php echo esc_attr( $tags[$i] ) ?>" class="widefat">
                        </label>
                        <small><?php _e('Enter the tag slugs, separated by commas.', 'warta') ?></small>
                    </p>

                    <!--Gallery:  gallery posts
                    ======================================================== -->
                    <p class="warta-option 
                       warta-option-gallery <?php if(!$is_gallery) echo 'hidden' ?>">
                        <label><?php _e('Gallery post:', 'warta') ?>
                            <select name="<?php echo $this->get_field_name( 'gallery_post' ) ?>[]" class="widefat">
                                <?php 
                                    // The Query
                                    $the_query = new WP_Query( array(
                                        'tax_query' => array(
                                            array(
                                                'taxonomy'  => 'post_format',
                                                'field'     => 'slug',
                                                'terms'     => array( 'post-format-gallery' )
                                            )
                                        )
                                    ) );

                                    // The Loop
                                    if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                            
                                        <option value="<?php the_ID() ?>" <?php selected($gallery_post[$i], get_the_ID()) ?>><?php the_title() ?></option>
                                            
                                    <?php endwhile; endif;

                                wp_reset_postdata() // Restore original Post Data : ?>
                            </select>
                        </label>
                    </p>

                    <!--Gallery:  caption length
                    ======================================================== -->
                    <p class="warta-option 
                       warta-option-gallery <?php if(!$is_gallery) echo 'hidden' ?>">
                        <label><?php _e('Caption Length:', 'warta') ?>
                            <input class="widefat" name="<?php echo $this->get_field_name( 'caption_length' ); ?>[]" type="number" min="1" max="100" value="<?php echo (int) $caption_length[$i] ?>">
                        </label>
                        <small>How many characters of the caption do you want to show?</small>
                    </p>

                    <!--Popular: Sort By
                    ======================================================== -->
                    <p class="warta-option 
                       warta-option-popular <?php if(!$is_popular) echo 'hidden' ?>">
                        <label><?php _e('Sort by:', 'warta') ?>
                            <select name="<?php echo $this->get_field_name( 'sort' ); ?>[]" class="widefat">
                                <option value="comments" <?php selected($sort[$i], 'comments') ?>><?php _e('Comments count', 'warta') ?></option>
                                <option value="views" <?php selected($sort[$i], 'views') ?>><?php _e('Views count', 'warta') ?></option>
                            </select>
                        </label>
                    </p>
                    
                    <!--Popular: time range
                    ======================================================== -->
                    <p class="warta-option 
                       warta-option-popular 
                       warta-option-review
                               <?php if( !$is_popular && !$is_review ) echo 'hidden' ?>">
                                <label><?php _e('Time range:', 'warta') ?>
                                    <select name="<?php echo $this->get_field_name( 'time_range' ); ?>[]" class="widefat">
                                        <option value="all"     <?php selected($time_range[$i], 'all') ?>>      <?php _e('All time', 'warta') ?>        </option>
                                        <option value="year"    <?php selected($time_range[$i], 'year') ?>>     <?php _e('This year', 'warta') ?>       </option>
                                        <option value="month"   <?php selected($time_range[$i], 'month') ?>>    <?php _e('This month', 'warta') ?>      </option>
                                        <option value="week"    <?php selected($time_range[$i], 'week') ?>>     <?php _e('This week', 'warta') ?>       </option>
                                    </select>
                                </label>
                    </p>
                    
                    <!--Post IDs: The posts IDs
                    ======================================================== -->
                    <p class="warta-option 
                       warta-option-post-ids <?php if(!$is_post_ids) echo 'hidden' ?>">
                        <label><?php _e('Post IDs:', 'warta') ?>
                            <input class="widefat" name="<?php echo $this->get_field_name( 'post_ids' ); ?>[]" type="text" value="<?php echo esc_attr( $post_ids[$i] ); ?>">
                        </label>
                        <small><?php _e('Enter the post IDs, separated by commas.', 'warta') ?></small>
                    </p>
                    
                    <!--Review: Top review
                    ======================================================== -->
                    <p class="warta-option 
                       warta-option-review <?php if(!$is_review) echo 'hidden' ?>">
                        <label>
                            <input type="checkbox" name="warta_checkbox[]" <?php checked($top_review[$i], 1) ?>">
                            <input name="<?php echo $this->get_field_name( 'top_review' ); ?>[]" type="hidden" value="<?php echo $top_review[$i] ? 1 : 0 ?>">
                            <?php _e('Sort by review score', 'warta') ?>
                        </label>
                    </p>

                    <!--Number of Items to Show
                    -------------------------------------------------------- -->
                    <p class="warta-option 
                       warta-option-category
                       warta-option-popular
                       warta-option-latest
                       warta-option-post-ids 
                       warta-option-review
                       warta-option-tags 
                            <?php if(   !$is_popular && 
                                        !$is_latest && 
                                        !$is_category && 
                                        !$is_post_ids && 
                                        !$is_review &&
                                        !$is_tags ) echo 'hidden' ?>">
                        <label><?php _e('Number of items to show:', 'warta') ?>
                            <input class="widefat" name="<?php echo $this->get_field_name( 'count' ); ?>[]" type="number" min="6" max="30" value="<?php echo esc_attr( $count[$i] ); ?>">
                        </label>
                    </p>

                    <!--Date Format
                    -------------------------------------------------------- -->
                    <p class="warta-option 
                       warta-option-category
                       warta-option-popular
                       warta-option-latest
                       warta-option-post-ids 
                       warta-option-review 
                       warta-option-tags 
                           <?php if(    !$is_popular && 
                                        !$is_latest && 
                                        !$is_category && 
                                        !$is_post_ids && 
                                        !$is_review &&
                                        !$is_tags ) echo 'hidden' ?>">
                        <label><?php _e('Date format:', 'warta') ?>
                            <input class="widefat" name="<?php echo $this->get_field_name( 'date_format' ); ?>[]" type="text" value="<?php echo esc_attr( $date_format[$i] ); ?>">
                        </label>
                        <small><?php _e('You can use the <a href="http://php.net/date" target="_blank">table of date format characters on the PHP website</a> as a reference '
                                . 'for building date format strings.', 'warta') ?> </small>
                    </p>
                    
                    <!--Ignore Sticky
                    -------------------------------------------------------- -->
                    <p class="warta-option 
                       warta-option-category
                       warta-option-popular
                       warta-option-latest
                       warta-option-post-ids 
                       warta-option-review 
                       warta-option-tags 
                           <?php if(    !$is_popular && 
                                        !$is_latest && 
                                        !$is_category && 
                                        !$is_post_ids && 
                                        !$is_review &&
                                        !$is_tags ) echo 'hidden' ?>">
                        <label>
                            <input type="checkbox" name="warta_checkbox[]" <?php checked($ignore_sticky[$i], '1') ?>">
                            <input type="hidden" name="<?php echo $this->get_field_name( 'ignore_sticky' ); ?>[]" value="<?php echo $ignore_sticky[$i] ? 1 : 0 ?>"> 
                            <?php _e('Ignore sticky posts', 'warta') ?>
                        </label>
                    </p>
                    
                    <!--Post Meta
                    -------------------------------------------------------- -->
                    <p class="warta-option 
                       warta-option-category
                       warta-option-popular
                       warta-option-latest
                       warta-option-post-ids 
                       warta-option-review 
                       warta-option-tags 
                           <?php if(    !$is_popular && 
                                        !$is_latest && 
                                        !$is_category && 
                                        !$is_post_ids && 
                                        !$is_review &&
                                        !$is_tags ) echo 'hidden' ?>">
                        <?php _e('Post meta:', 'warta') ?><br>
                        <label>
                            <input type="checkbox" name="warta_checkbox[]" <?php checked($meta_date[$i], '1') ?>">
                            <input type="hidden" name="<?php echo $this->get_field_name( 'meta_date' ); ?>[]" value="<?php echo $meta_date[$i] ? 1 : 0 ?>"> 
                            <?php _e('Date', 'warta') ?>
                        </label><br>
                        <label>
                            <input type="checkbox" name="warta_checkbox[]" <?php checked($meta_format[$i], '1') ?>">
                            <input type="hidden" name="<?php echo $this->get_field_name( 'meta_format' ); ?>[]" value="<?php echo $meta_format[$i] ? 1 : 0 ?>"> 
                            <?php _e('Post format', 'warta') ?>
                        </label><br>
                        <label>
                            <input type="checkbox" name="warta_checkbox[]" <?php checked($meta_category[$i], '1') ?>">
                            <input type="hidden" name="<?php echo $this->get_field_name( 'meta_category' ); ?>[]" value="<?php echo $meta_category[$i] ? 1 : 0 ?>"> 
                            <?php _e('Category', 'warta') ?>
                        </label><br>
                        <label>
                            <input type="checkbox" name="warta_checkbox[]" <?php checked($meta_author[$i], '1') ?>">
                            <input type="hidden" name="<?php echo $this->get_field_name( 'meta_author' ); ?>[]" value="<?php echo $meta_author[$i] ? 1 : 0 ?>"> 
                            <?php _e('Author', 'warta') ?>
                        </label><br>
                        <label>
                            <input type="checkbox" name="warta_checkbox[]" <?php checked($meta_comments[$i], '1') ?>">
                            <input type="hidden" name="<?php echo $this->get_field_name( 'meta_comments' ); ?>[]" value="<?php echo $meta_comments[$i] ? 1 : 0 ?>"> 
                            <?php _e('Comments count', 'warta') ?>
                        </label><br>
                        <label>
                            <input type="checkbox" name="warta_checkbox[]" <?php checked($meta_views[$i], '1') ?>">
                            <input type="hidden" name="<?php echo $this->get_field_name( 'meta_views' ); ?>[]" value="<?php echo $meta_views[$i] ? 1 : 0 ?>"> 
                            <?php _e('Views count', 'warta') ?>
                        </label><br>
                        <label>
                            <input type="checkbox" name="warta_checkbox[]" <?php checked($meta_review_score[$i], '1') ?>">
                            <input type="hidden" name="<?php echo $this->get_field_name( 'meta_review_score' ); ?>[]" value="<?php echo $meta_review_score[$i] ? 1 : 0 ?>"> 
                            <?php _e('Review score', 'warta') ?>
                        </label>
                    </p>

                    <!--Delete Tab
                    -------------------------------------------------------- -->
                    <a href="#" class="warta-tab-delete">Delete tab</a>

                </div><!--.warta-tab-->  
            
            <?php endfor; ?>
                  
            <!--Add Tab
            ---------------------------------------------------------------- -->
            <a href="#" class="warta-tab-add">Add tab</a>            
            
            <!--Hide on mobile devices
            -------------------------------------------------------- -->
            <p>
                <label>
                    <input type="checkbox" name="<?php echo $this->get_field_name( 'hide_mobile' ); ?>" value="1" <?php checked( $hide_mobile, 1) ?>> 
                    <?php _e('Hide on mobile devices', 'warta') ?>
                </label><br>
                <small><?php _e('Recommended for better performance.', 'warta') ?></small>
            </p>
            <hr>
            
        </div><!--.warta-widget-->
        
        <?php
    }

    /**
     * Sanitize widget form values as they are saved.
     * =========================================================================
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        
        extract($new_instance);
        
        for ($i = 0; $i < count($title); $i++) {
            $instance['title'][$i]              = empty($title[$i]) ? __('New title', 'warta') : sanitize_text_field( $title[$i] );
            $instance['data'][$i]               = sanitize_text_field( $data[$i] );
            $instance['category'][$i]           = (int) $category[$i];
            $instance['tags'][$i]               = sanitize_text_field( $tags[$i] );
            $instance['gallery_post'][$i]       = (int) $gallery_post[$i];
            $instance['caption_length'][$i]     = (int) $caption_length[$i];
            $instance['sort'][$i]               = sanitize_text_field( $sort[$i] );
            $instance['time_range'][$i]         = sanitize_text_field( $time_range[$i] );
            $instance['post_ids'][$i]           = sanitize_text_field( $post_ids[$i] );
            $instance['top_review'][$i]         = (int) $top_review[$i];
            $instance['count'][$i]              = (int) $count[$i];
            $instance['ignore_sticky'][$i]      = (int) $ignore_sticky[$i];
            $instance['date_format'][$i]        = sanitize_text_field( $date_format[$i] );
            $instance['meta_date'][$i]          = (int) $meta_date[$i];
            $instance['meta_format'][$i]        = (int) $meta_format[$i];
            $instance['meta_comments'][$i]      = (int) $meta_comments[$i];
            $instance['meta_views'][$i]         = (int) $meta_views[$i];
            $instance['meta_category'][$i]      = (int) $meta_category[$i];
            $instance['meta_author'][$i]        = (int) $meta_author[$i];
            $instance['meta_review_score'][$i]  = (int) $meta_review_score[$i];
        }
        
        $instance['hide_mobile'] = (int) $hide_mobile;

        return $instance;
    }

} // class Warta_Slider_Tabs