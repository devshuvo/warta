<?php
/**
 * Tabs Widget
 * 
 * @package Warta
 */

/**
 * Adds Foo_Widget widget.
 * =============================================================================
 */
class Warta_Tabs extends WP_Widget {

	/**
	 * Register widget with WordPress.
     * =====================================================================
	 */
	function __construct() {
		parent::__construct(
			'warta_tabs', // Base ID
			__('[Warta] Tabs', 'warta'), // Name
			array( 'description' => __( 'Togglable tabs ', 'warta' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
     * =====================================================================
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
                extract($instance);
                
                $w_header   = '<ul class="nav nav-tabs">';  // widget header
                $w_content  = '<div class="tab-content">';  // widget content

                // Loop every tabs
                for( $i = 0; $i < count($title); $i++ ) {
                        $active         = $i === 0 ? 'active' : '';
                        $in             = $i === 0 ? 'in' : '';
                        $article_class  = $data[$i] !== 'gallery' ? 'article' : '';
                        $w_id           = $args['widget_id'] . rand(); // widget unique ID, required for .slider-tabs

                        $query_args     = array( 
                                                'posts_per_page'        => $count[$i],
                                                'ignore_sticky_posts'   => $ignore_sticky[$i]
                                        );

                        // Query args
                        switch ( $data[$i] ) {
                                case 'category':
                                        $query_args['cat'] = $category[$i];
                                        break;
                                    
                                case 'tags':
                                        $query_args['tax_query'] = array(
                                                array(
                                                        'taxonomy'  => 'post_tag',
                                                        'field'     => 'slug',
                                                        'terms'     => explode(',', $tags[$i])
                                                )
                                        );
                                        break;

                                case 'review':
                                        $query_args['meta_key']     = 'friskamax_review_total'; 
                                        $query_args['meta_value']   = 0;
                                        $query_args['meta_compare'] = '>';      

                                        if( $top_review[$i] ) {
                                                $query_args['orderby']  = 'meta_value_num';
                                        }
                                        break;

                                case 'popular':
                                        if( $sort[$i] === 'comments' ) {    
                                                $query_args['orderby']  = 'comment_count'; 
                                        } else {
                                                $query_args['meta_key'] = 'warta_post_views_count';
                                                $query_args['orderby']  = 'meta_value_num';
                                        }                                        
                                        break;

                                case 'post_ids':
                                        $query_args['post__in']     = explode(',', $post_ids[$i]);
                                        break;
                        }
                        
                        // Time range
                        if( $time_range[$i] != 'all' && ( $data[$i] == 'review' || $data[$i] == 'popular' ) ) {
                                switch ($time_range[$i]) {
                                        case 'year':
                                                $query_args['date_query'] = array(
                                                            array( 'year' => date('Y') ),
                                                );
                                                break;
                                        case 'month':
                                                $query_args['date_query'] = array(
                                                            array( 
                                                                'year' => date('Y'),
                                                                'month'=> date('n')
                                                            ),
                                                );
                                                break;
                                        case 'week':
                                                $query_args['date_query'] = array(
                                                            array( 
                                                                'year' => date('Y'),
                                                                'week' => date('W')
                                                            ),
                                                );
                                                break;
                                }
                        }

                        $the_query  = new WP_Query( $query_args );

                        $w_header   .= '<li class="' . $active . '"><a href="#' . $w_id . '" data-toggle="tab">' . strip_tags( $title[$i] ) . '</a></li>'; // Tab Titles 
                        $w_content  .= '<div class="tab-pane fade ' . $active . ' ' . $in . '" id="' . $w_id . '">'; // Tab items container

                        /**
                         * List Categories
                         * -----------------------------------------------------
                         */
                        if( $data[$i] === 'list_categories' ) {
                                $categories     = get_categories( array( 'parent' => 0 ) );
                                $w_content      .= '<ul class="categories">'; // categories wrapper

                                foreach( $categories as $category ) {
                                        $w_content      .= '<li>'
                                                                . '<a href="' . esc_url( get_category_link( $category->term_id ) ) 
                                                                        . '" title="' . esc_attr( sprintf( __( "View %d posts under %s category", 'warta' ), $category->count, $category->name ) ) . '">';
                                        if(is_rtl())
                                                $w_content              .= '<i class="fa fa-angle-left"></i> ' . strip_tags( $category->name );
                                        else
                                                $w_content              .= '<i class="fa fa-angle-right"></i> ' . strip_tags( $category->name );
                                        if($posts_counts[$i]) // posts counts
                                                $w_content              .= '<span class="post-counts">' . $category->count . '</span>';

                                        $w_content              .= '</a>'
                                                        . '</li>';
                                }
                                
                                $w_content      .= '</ul>'; // categories wrapper
                        } 
                        /**
                         * Popular tags
                         * -----------------------------------------------------
                         */
                        else if( $data[$i] === 'popular_tags' ) {
                                $tags = get_tags( array(
                                        'orderby'   => 'count',
                                        'order'     => 'DESC',
                                        'number'    => $count[$i]
                                ) );

                                $w_content      .= '<ul class="tags clearfix">'; // tags wrapper                    

                                foreach ( $tags as $tag ) {
                                    $w_content          .= '<li>'
                                                                . '<a href="' . esc_url( get_tag_link( $tag->term_id ) ) . '" title="' . esc_attr( sprintf( __( "View %d posts with %s tag", 'warta' ), $tag->count, $tag->name ) ) . '">'
                                                                        . strip_tags( $tag->name )
                                                                . '</a>'
                                                        . '</li>';
                                }
                                $w_content      .= '</ul>'; // tags wrapper

                        } 
                        /**
                         * Recent comments
                         * -----------------------------------------------------
                         */
                        else if( $data[$i] === 'recent_comments' ) {
                                $comments       = get_comments( array(
                                                        'status'    => 'approve',
                                                        'type'      => 'comment',
                                                        'number'    => $count[$i]
                                                ) );  // retrieve the comments 
                                $layer          = warta_is_footer( $args['id'] ) ? 'dark' : 'light'; // layer style
                                $w_content      .= '<ul class="recent-comments clearfix">'; // comments wrapper

                                foreach($comments as $comment) { 
                                        $w_content      .= '<li>'
                                                                . '<div class="avatar">'
                                                                        . '<a href="' . esc_url( $comment->comment_author_url ) . '" class="' . $layer . '" title="' . esc_attr( $comment->comment_author ) . '">'
                                                                                . get_avatar( $comment->comment_author_email , 75)
                                                                                . '<div class="layer"></div>'
                                                                        . '</a>'
                                                                . '</div>'
                                                                . '<div class="content">'
                                                                        . '<div class="comment-content">'
                                                                                . '<a href="' . esc_url(get_permalink( $comment->comment_post_ID )) . '">' . warta_the_excerpt_max_charlength( $comment_excerpt[$i], $comment->comment_content ) . '</a>'
                                                                        . '</div>'
                                                                        . '<div class="comment-meta">'
                                                                                . '<a href="' . esc_url( $comment->comment_author_url ) . '"><i class="fa fa-user"></i> ' . strip_tags( $comment->comment_author ) . '</a>&nbsp;'
                                                                                . '<a href="' . esc_url(get_permalink( $comment->comment_post_ID )) . '"><i class="fa fa-clock-o"></i> ' . get_comment_date ( $date_format[$i], $comment->comment_ID ) . '</a>'
                                                                        . '</div>'
                                                                . '</div>'
                                                        . '</li>';
                                }

                                $w_content      .= '</ul>'; // comment wrapper
                        }
                        /**
                         * Articles
                         * -----------------------------------------------------
                         */
                        else if ( $the_query->have_posts() ) {
                                while ( $the_query->have_posts() ) { 
                                        $the_query->the_post(); 
                                        
                                        $classes                = warta_is_footer( $args['id'] ) ? 'image dark' : 'image';
                                        $format                 = get_post_format() ? get_post_format() : 'standard';
                                        $hide_post_meta_all     = get_post_meta( get_the_ID(), 'friskamax_hide_post_meta_all', true ); 
                                        $w_content              .= '<article class="article-tiny">'; 

                                        if(has_post_thumbnail()) {
                                                $w_content              .= '<a href="' . esc_url( get_permalink() ) . '" class="' . $classes . '">'
                                                                                . get_the_post_thumbnail( NULL, 'thumbnail' )
                                                                                . '<div class="image-light"></div>'
                                                                                . '<div class="link">'
                                                                                        . '<span class="dashicons dashicons-format-' . $format . '"></span>'
                                                                                . '</div>'
                                                                                . ( warta_is_footer( $args['id'] ) ? '<div class="layer"></div>' : '' )
                                                                        . '</a>';
                                        } else {
                                                $w_content              .= '<div class="image">'
                                                                                . '<div class="format-placeholder dashicons dashicons-format-' . $format . '"></div>'
                                                                                . '<div class="image-light"></div>'
                                                                        . '</div>';
                                        }

                                        $w_content                      .= '<a href="' . esc_url( get_permalink() ) . '"><h5>' . get_the_title() . '</h5></a>';
                                        
                                        if( !$hide_post_meta_all ) {
                                                $w_content              .= warta_posted_on( array(
                                                                                'meta_date'         => $meta_date[$i],
                                                                                'date_format'       => $date_format[$i],
                                                                                'meta_format'       => $meta_format[$i],
                                                                                'meta_comments'     => $meta_comments[$i],
                                                                                'meta_views'        => $meta_views[$i],
                                                                                'meta_category'     => $meta_category[$i],
                                                                                'meta_categories'   => $meta_categories[$i],
                                                                                'meta_author'       => $meta_author[$i],
                                                                                'meta_review_score' => $meta_review_score[$i],
                                                                            ) );
                                        }
                                        
                                        $w_content                      .= '<hr>'
                                                                . '</article>'; 
                                }
                        } 

                    wp_reset_postdata();        // Restore original Post Data            

                    $w_content  .= '</div>';    // .tab-pane, tab item container

                } // Loop every tabs

                $w_header   .= '</ul>';     // widget header
                $w_content  .= '</div>';    // widget content

            ?>

            <section class="<?php warta_widget_class( $args['id'] ) ?>">
                <?php echo $w_header . $w_content ?>
            </section>

            <?php warta_add_clearfix( $args['id'], 6) ?>

            <?php
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$defaults   = array( 
                        'title'                 => array( __( 'New title', 'warta' )),
                        'data'                  => array( 'latest' ),
                        'category'              => array( 0 ),
                        'tags'                  => array( '' ),
                        'post_ids'              => array( '' ),
                        'sort'                  => array( 'comments' ),
                        'time_range'            => array( 'all' ),
                        'top_review'            => array( 0 ),
                        'count'                 => array( 4 ),
                        'comment_excerpt'       => array( 50 ),
                        'posts_counts'          => array( 0 ),
                        'date_format'           => array( 'F j, Y' ),
                        'ignore_sticky'         => array( 1 ),
                        'meta_date'             => array( 1 ),
                        'meta_format'           => array( 0 ),
                        'meta_comments'         => array( 0 ),
                        'meta_views'            => array( 0 ),
                        'meta_category'         => array( 1 ),
                        'meta_categories'       => array( 0 ),
                        'meta_author'           => array( 0 ),
                        'meta_review_score'     => array( 0 ),
                    );
        $args       = wp_parse_args( $instance, $defaults );

        extract($args); 
?>

                <!--Title that appears on widget settings page-->
                <input id="<?php echo $this->get_field_id( 'title' ); ?>"  type="hidden" value="<?php echo esc_attr(implode( ' / ', $title) ) ?>">

                <!--Widget Form Wrapper
                ------------------------------------------------------------ -->
                <div class="warta-widget">

                    <?php 
                        for ($i = 0; $i < count($title); $i++): 
                            $is_category        = $data[$i] === 'category';
                            $is_tags            = $data[$i] === 'tags';
                            $is_review          = $data[$i] === 'review';
                            $is_popular         = $data[$i] === 'popular';
                            $is_latest          = $data[$i] === 'latest';
                            $is_list_categories = $data[$i] === 'list_categories';
                            $is_popular_tags    = $data[$i] === 'popular_tags';
                            $is_recent_comments = $data[$i] === 'recent_comments';
                            $is_post_ids        = $data[$i] === 'post_ids';
                    ?>

                        <!--Tab wrapper
                        ---------------------------------------------------- -->
                        <div class="warta-tab">

                            <!--Title
                            ------------------------------------------------ -->
                            <p>
                                <label><?php _e( 'Title:', 'warta' ); ?>
                                    <input class="widefat" name="<?php echo $this->get_field_name( 'title' ); ?>[]" type="text" value="<?php echo sanitize_text_field( $title[$i] ); ?>">
                                </label> 
                            </p>

                            <!--Data
                            ------------------------------------------------ -->
                            <p>
                                <label><?php _e('Data:', 'warta') ?>
                                    <select name="<?php echo $this->get_field_name( 'data' ) ?>[]" class="widefat warta-option-choice">
                                        <option value="latest" <?php selected( $data[$i], 'latest' ) ?>><?php _e('Latest posts', 'warta') ?></option>
                                        <option value="popular" <?php selected( $data[$i], 'popular' ) ?>><?php _e('Popular posts', 'warta') ?></option>
                                        <option value="category" <?php selected( $data[$i], 'category' ) ?>><?php _e('Posts by category', 'warta') ?></option>
                                        <option value="tags" <?php selected( $data[$i], 'tags' ) ?>><?php _e('Posts by tags', 'warta') ?></option>
                                        <option value="review" <?php selected( $data[$i], 'review' ) ?>><?php _e('Review posts', 'warta') ?></option>
                                        <option value="post_ids" <?php selected( $data[$i], 'post_ids' ) ?>><?php _e('Posts by selected IDs', 'warta') ?></option>
                                        <option value="list_categories" <?php selected( $data[$i], 'list_categories' ) ?>><?php _e('List of main categories', 'warta') ?></option>
                                        <option value="popular_tags" <?php selected( $data[$i], 'popular_tags' ) ?>><?php _e('Popular tags', 'warta') ?></option>
                                        <option value="recent_comments" <?php selected( $data[$i], 'recent_comments' ) ?>><?php _e('Recent comments', 'warta') ?></option>
                                    </select>
                                </label>
                            </p>

                            <!--Category
                            ------------------------------------------------ -->
                            <p class="warta-option 
                               warta-option-category <?php if( !$is_category ) echo 'hidden"' ?>">
                                <label><?php _e('Category:', 'warta') ?>
                                    <?php wp_dropdown_categories(array(
                                        'name'          => $this->get_field_name( 'category' ) . '[]',
                                        'class'         => 'widefat',
                                        'selected'      => $category[$i],
                                        'hierarchical'  => TRUE
                                    )) ?>
                                </label>
                            </p>

                            <!--Tags: selected tags
                            ======================================================== -->
                            <p class="warta-option 
                               warta-option-tags <?php if(!$is_tags) echo 'hidden"' ?>">
                                <label><?php _e('Tags:', 'warta') ?>
                                    <input type="text" name="<?php echo $this->get_field_name( 'tags' ) ?>[]" value="<?php echo esc_attr( $tags[$i] ) ?>" class="widefat">
                                </label>
                                <small><?php _e('Enter the tag slugs, separated by commas.', 'warta') ?></small>
                            </p>
                            
                            <!--Popular: Sort By
                            ================================================ -->
                            <p class="warta-option 
                               warta-option-popular <?php if( !$is_popular ) echo 'hidden' ?>">
                                <label><?php _e('Sort by:', 'warta') ?>
                                    <select name="<?php echo $this->get_field_name( 'sort' ); ?>[]" class="widefat">
                                        <option value="comments" <?php selected($sort[$i], 'comments') ?>><?php _e('Comments count', 'warta') ?></option>
                                        <option value="views" <?php selected($sort[$i], 'views') ?>><?php _e('Views count', 'warta') ?></option>
                                    </select>
                                </label>
                            </p>
                    
                            <!--Popular: time range
                            ======================================================== -->
                            <p class="warta-option 
                                   warta-option-popular
                                   warta-option-review
                                        <?php if(!$is_popular && !$is_review) echo 'hidden' ?>">
                                        <label><?php _e('Time range:', 'warta') ?>
                                            <select name="<?php echo $this->get_field_name( 'time_range' ); ?>[]" class="widefat">
                                                <option value="all"     <?php selected($time_range[$i], 'all') ?>>      <?php _e('All time', 'warta') ?>        </option>
                                                <option value="year"    <?php selected($time_range[$i], 'year') ?>>     <?php _e('This year', 'warta') ?>       </option>
                                                <option value="month"   <?php selected($time_range[$i], 'month') ?>>    <?php _e('This month', 'warta') ?>      </option>
                                                <option value="week"    <?php selected($time_range[$i], 'week') ?>>     <?php _e('This week', 'warta') ?>       </option>
                                            </select>
                                        </label>
                            </p>

                            <!--List categories: Show Post Counts
                            ================================================ -->
                            <p class="warta-option 
                               warta-option-list-categories <?php if( !$is_list_categories ) echo 'hidden' ?>">
                                <label>
                                    <input type="checkbox" name="warta_checkbox[]" <?php checked($posts_counts[$i], '1') ?>">
                                    <input type="hidden" name="<?php echo $this->get_field_name( 'posts_counts' ); ?>[]" value="<?php echo $posts_counts[$i] ? 1 : 0 ?>"> 
                                    <?php _e('Show post counts', 'warta') ?>
                                </label>
                            </p>
                            
                            <!--Recent Comments: Comment Excerpt
                            ================================================ -->
                            <p class="warta-option 
                               warta-option-recent-comments <?php if( !$is_recent_comments ) echo 'hidden' ?>">
                                <label><?php _e( 'Excerpt length:', 'warta' ); ?>
                                    <input class="widefat" name="<?php echo $this->get_field_name( 'comment_excerpt' ); ?>[]" type="number" min="20" max="500" value="<?php echo (int) $comment_excerpt[$i] ?>">
                                </label> 
                                <small><?php _e('How many characters do you want to show?', 'warta') ?></small>
                            </p>
                            
                            <!--Post IDs: The posts IDs
                            ======================================================== -->
                            <p class="warta-option 
                               warta-option-post-ids <?php if(!$is_post_ids) echo 'hidden' ?>">
                                <label><?php _e('Post IDs:', 'warta') ?>
                                    <input class="widefat" name="<?php echo $this->get_field_name( 'post_ids' ); ?>[]" type="text" value="<?php echo esc_attr( $post_ids[$i] ); ?>">
                                </label>
                                <small><?php _e('Enter the post IDs, separated by commas.', 'warta') ?></small>
                            </p>
                    
                            <!--Review: Top review
                            ======================================================== -->
                            <p class="warta-option 
                               warta-option-review <?php if(!$is_review) echo 'hidden' ?>">
                                <label>
                                    <input type="checkbox" name="warta_checkbox[]" <?php checked($top_review[$i], 1) ?>">
                                    <input name="<?php echo $this->get_field_name( 'top_review' ); ?>[]" type="hidden" value="<?php echo $top_review[$i] ? 1 : 0 ?>">
                                    <?php _e('Sort by review score', 'warta') ?>
                                </label>
                            </p>

                            <!--Number of Items to Show
                            ------------------------------------------------ -->
                            <p class="warta-option 
                               warta-option-latest
                               warta-option-popular 
                               warta-option-category
                               warta-option-popular-tags
                               warta-option-recent-comments
                               warta-option-post-ids
                               warta-option-review 
                               warta-option-tags 
                                   <?php if(    !$is_latest && 
                                                !$is_popular && 
                                                !$is_category && 
                                                !$is_popular_tags && 
                                                !$is_recent_comments && 
                                                !$is_post_ids && 
                                                !$is_review &&
                                                !$is_tags ) echo 'hidden' ?>"
                            >
                                <label><?php _e('Number of items to show:', 'warta') ?>
                                    <input class="widefat" name="<?php echo $this->get_field_name( 'count' ); ?>[]" type="number" min="6" max="30" value="<?php echo esc_attr( $count[$i] ); ?>">
                                </label>
                            </p>

                            <!--Date Format
                            -------------------------------------------------------- -->
                            <p class="warta-option 
                               warta-option-latest
                               warta-option-popular
                               warta-option-category
                               warta-option-recent-comments
                               warta-option-post-ids
                               warta-option-review 
                               warta-option-tags  
                                   <?php if(    !$is_latest && 
                                                !$is_popular && 
                                                !$is_category && 
                                                !$is_recent_comments && 
                                                !$is_post_ids && 
                                                !$is_review &&
                                                !$is_tags ) echo 'hidden' ?>">
                                <label><?php _e('Date format:', 'warta') ?>
                                    <input class="widefat" name="<?php echo $this->get_field_name( 'date_format' ); ?>[]" type="text" value="<?php echo esc_attr( $date_format[$i] ); ?>">
                                </label>
                                <small><?php _e('You can use the <a href="http://php.net/date" target="_blank">table of date format characters on the PHP website</a> as a reference '
                                . 'for building date format strings.', 'warta') ?> </small>
                            </p>
                            
                            <!--Ignore Sticky
                            -------------------------------------------------------- -->
                            <p class="warta-option 
                               warta-option-category
                               warta-option-popular
                               warta-option-latest 
                               warta-option-post-ids
                               warta-option-review
                               warta-option-tags  
                                    <?php if(   !$is_popular && 
                                                !$is_latest && 
                                                !$is_category && 
                                                !$is_post_ids && 
                                                !$is_review && 
                                                !$is_tags ) echo 'hidden' ?>">
                                <label>
                                    <input type="checkbox" name="warta_checkbox[]" <?php checked($ignore_sticky[$i], '1') ?>">
                                    <input type="hidden" name="<?php echo $this->get_field_name( 'ignore_sticky' ); ?>[]" value="<?php echo $ignore_sticky[$i] ? 1 : 0 ?>"> 
                                    <?php _e('Ignore sticky posts', 'warta') ?>
                                </label>
                            </p>

                            <!--Post Meta
                            -------------------------------------------------------- -->
                            <p class="warta-option 
                               warta-option-latest
                               warta-option-popular
                               warta-option-category
                               warta-option-post-ids
                               warta-option-review
                               warta-option-tags    
                                    <?php if(   !$is_latest && 
                                                !$is_popular && 
                                                !$is_category && 
                                                !$is_post_ids && 
                                                !$is_review &&
                                                !$is_tags ) echo 'hidden' ?>">
                                <?php _e('Post meta:', 'warta') ?><br>
                                <label>
                                    <input type="checkbox" name="warta_checkbox[]" <?php checked($meta_date[$i], '1') ?>">
                                    <input type="hidden" name="<?php echo $this->get_field_name( 'meta_date' ); ?>[]" value="<?php echo $meta_date[$i] ? 1 : 0 ?>"> 
                                    <?php _e('Date', 'warta') ?>
                                </label><br>
                                <label>
                                    <input type="checkbox" name="warta_checkbox[]" <?php checked($meta_format[$i], '1') ?>">
                                    <input type="hidden" name="<?php echo $this->get_field_name( 'meta_format' ); ?>[]" value="<?php echo $meta_format[$i] ? 1 : 0 ?>"> 
                                    <?php _e('Post format', 'warta') ?>
                                </label><br>
                                <label>
                                    <input type="checkbox" name="warta_checkbox[]" <?php checked($meta_category[$i], '1') ?>">
                                    <input type="hidden" name="<?php echo $this->get_field_name( 'meta_category' ); ?>[]" value="<?php echo $meta_category[$i] ? 1 : 0 ?>"> 
                                    <?php _e('First category', 'warta') ?>
                                </label><br>
                                <label>
                                    <input type="checkbox" name="warta_checkbox[]" <?php checked($meta_categories[$i], '1') ?>"> 
                                    <input type="hidden" name="<?php echo $this->get_field_name( 'meta_categories' ); ?>[]" value="<?php echo $meta_categories[$i] ? 1 : 0 ?>"> 
                                    <?php _e('All categories', 'warta') ?>
                                </label><br>
                                <label>
                                    <input type="checkbox" name="warta_checkbox[]" <?php checked($meta_author[$i], '1') ?>">
                                    <input type="hidden" name="<?php echo $this->get_field_name( 'meta_author' ); ?>[]" value="<?php echo $meta_author[$i] ? 1 : 0 ?>"> 
                                    <?php _e('Author', 'warta') ?>
                                </label><br>
                                <label>
                                    <input type="checkbox" name="warta_checkbox[]" <?php checked($meta_comments[$i], '1') ?>">
                                    <input type="hidden" name="<?php echo $this->get_field_name( 'meta_comments' ); ?>[]" value="<?php echo $meta_comments[$i] ? 1 : 0 ?>"> 
                                    <?php _e('Comments count', 'warta') ?>
                                </label><br>
                                <label>
                                    <input type="checkbox" name="warta_checkbox[]" <?php checked($meta_views[$i], '1') ?>">
                                    <input type="hidden" name="<?php echo $this->get_field_name( 'meta_views' ); ?>[]" value="<?php echo $meta_views[$i] ? 1 : 0 ?>"> 
                                    <?php _e('Views count', 'warta') ?>
                                </label><br>
                                <label>
                                    <input type="checkbox" name="warta_checkbox[]" <?php checked($meta_review_score[$i], '1') ?>">
                                    <input type="hidden" name="<?php echo $this->get_field_name( 'meta_review_score' ); ?>[]" value="<?php echo $meta_review_score[$i] ? 1 : 0 ?>"> 
                                    <?php _e('Review score', 'warta') ?>
                                </label>
                            </p>

                            <!--Delete Tab
                            -------------------------------------------------------- -->
                            <a href="#" class="warta-tab-delete">Delete tab</a>

                        </div><!--.warta-tab-->  

                    <?php endfor; ?>

                    <!--Add Tab
                    ---------------------------------------------------------------- -->
                    <a href="#" class="warta-tab-add">Add tab</a>
                    <hr>

                </div><!--.warta-widget-->
                
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
         * =====================================================================
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
        
        extract($new_instance);

        for ($i = 0; $i < count($title); $i++) {
            $instance['title'][$i]              = empty($title[$i]) ? __('New title', 'warta') : sanitize_text_field( $title[$i] );
            $instance['data'][$i]               = sanitize_text_field( $data[$i] );
            $instance['category'][$i]           = (int) $category[$i];
            $instance['tags'][$i]               = sanitize_text_field( $tags[$i] );
            $instance['post_ids'][$i]           = sanitize_text_field( $post_ids[$i] );
            $instance['sort'][$i]               = sanitize_text_field( $sort[$i] );
            $instance['time_range'][$i]         = sanitize_text_field( $time_range[$i] );
            $instance['top_review'][$i]         = (int) $top_review[$i];
            $instance['count'][$i]              = (int) $count[$i];
            $instance['comment_excerpt'][$i]    = (int) $comment_excerpt[$i];
            $instance['posts_counts'][$i]       = (int) $posts_counts[$i];
            $instance['date_format'][$i]        = sanitize_text_field( $date_format[$i] );
            $instance['ignore_sticky'][$i]      = (int) $ignore_sticky[$i];
            $instance['meta_date'][$i]          = (int) $meta_date[$i];
            $instance['meta_format'][$i]        = (int) $meta_format[$i];
            $instance['meta_comments'][$i]      = (int) $meta_comments[$i];
            $instance['meta_views'][$i]         = (int) $meta_views[$i];
            $instance['meta_category'][$i]      = (int) $meta_category[$i];
            $instance['meta_categories'][$i]    = (int) $meta_categories[$i];
            $instance['meta_author'][$i]        = (int) $meta_author[$i];
            $instance['meta_review_score'][$i]  = (int) $meta_review_score[$i];
        }

        return $instance;
	}

} // class Warta_Tabs