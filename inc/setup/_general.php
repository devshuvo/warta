<?php

/**
 * General setup.
 * 
 * @since 1.7.5
 * @package warta\setup
 */

if ( ! function_exists( 'warta_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 * 
 * @since 1.0.0
 */
function warta_setup() {                
        /* -------------------------------------------------------------------------- *
         * Translation
         * -------------------------------------------------------------------------- */
        
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on Warta, use a find and replace
         * to change 'warta' to the name of your theme in all the template files
         */
        load_theme_textdomain( 'warta', get_template_directory() . '/languages' );
        
        /* -------------------------------------------------------------------------- *
         * Theme support
         * -------------------------------------------------------------------------- */

        /*
         * Add default posts and comments RSS feed links to head.
         */
        add_theme_support( 'automatic-feed-links' );
        
        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support( 'title-tag' );
    
        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
         */
        add_theme_support( 'post-thumbnails' );
        
        /*
         * Enable support for Post Formats.
         * 
         * @link http://codex.wordpress.org/Post_Formats
         */
        add_theme_support( 'post-formats', array( 
                'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' 
        ) ); 
        
        /*
         * Declaring WooCommerce support. 
         */
        add_theme_support( 'woocommerce' );
        
        /*
         * Enable support for FriskaMax Page Builder.
         */
        add_theme_support( 'friskamax-page-builder', array(
                // Page templates that allowed to use the page builder and its breakpoint.
                'page_templates'                => array(       
                        'default'                       => array( 'breakpoint' => 'sm' ),
                        'template-page-full-width.php'  => array( 'breakpoint' => 'md' )
                ),
                
                // Arguments for Widget element.
                'element_widget'                => array(               
                        'the_widget_args'       => array(               // Third argument of the_widget() function.
                                'before_title'  => '<header class="clearfix"><h4 class="widget-title">',
                                'after_title'   => '</h4></header>',
                                'id'            => 'singular-before-content-section'
                        )
                ),
                
                // Arguments for Widget Area element.
                'element_widget_area'           => array(               
                        'before'                => '<div class="row">', // Content to prepend to the widget area.
                        'after'                 => '</div>',            // Content to append to the widget area.
                        'register_sidebar_args' => array(               // Arguments for register_sidebar() function.
                                'before_widget' => '<section id="%1$s" class="col-md-12 widget %2$s">',
                                'after_widget'  => '</section>',
                                'before_title'  => '<header class="clearfix"><h4 class="widget-title">',
                                'after_title'   => '</h4></header>',
                        )
                )
        ) );
        
        /*
         * Enabling WooCommerce product gallery features.
         * 
         * @link https://github.com/woocommerce/woocommerce/wiki/Enabling-product-gallery-features-(zoom,-swipe,-lightbox)-in-3.0.0
         */
        add_theme_support( 'wc-product-gallery-zoom' );
        add_theme_support( 'wc-product-gallery-lightbox' );
        add_theme_support( 'wc-product-gallery-slider' );
        
        /* -------------------------------------------------------------------------- *
         * Menus
         * -------------------------------------------------------------------------- */

        /*
         * This theme uses wp_nav_menu() in 3 locations.
         */
        register_nav_menus( array(
                'top'   => __( 'Top Menu', 'warta' ),
                'main'  => __( 'Main Menu', 'warta' ),
                'footer'=> __( 'Footer Menu', 'warta' ),
        ) );
        
        /* -------------------------------------------------------------------------- *
         * TinyMCE
         * -------------------------------------------------------------------------- */
        
        /*
         * Adds editor stylesheet.
         */
        add_editor_style( 'css/editor-style.css' );
        
        /* -------------------------------------------------------------------------- *
         * Image sizes
         * -------------------------------------------------------------------------- */
        
        /*
         * Registers new image sizes.
         */
        add_image_size( 'small', 165, 90, true );
        add_image_size( 'gallery', 180, 180, true );
        add_image_size( 'huge', 1366 );
}
endif; // ! function_exists( 'warta_setup' ) 
add_action( 'after_setup_theme', 'warta_setup' );



if ( ! function_exists( 'warta_after_switch_theme' ) ) :
/**
 * Set some settings after activate the theme.
 * 
 * @since 1.5.9
 */
function warta_after_switch_theme() {
        /**
         * Update image sizes.
         */
        update_option( 'thumbnail_size_w',      95 );
        update_option( 'thumbnail_size_h',      75 );
        update_option( 'thumbnail_crop',        1 );
        update_option( 'medium_size_w',         350 );
        update_option( 'medium_size_h',         0 );
        update_option( 'medium_crop',           0 );
        update_option( 'large_size_w',          730 );
        update_option( 'large_size_h',          0 );
        update_option( 'large_crop',            0 );
}
endif;
add_action( "after_switch_theme", "warta_after_switch_theme" );



if ( ! function_exists( 'warta_scripts' ) ) :
/**
 * Enqueue scripts and styles.
 * 
 * @since 1.0.0
 */
function warta_scripts() {           
    $theme_version = is_child_theme() ? wp_get_theme()->parent()->get( 'Version' ) : wp_get_theme()->get( 'Version' );
    
    wp_enqueue_style('font-awesome', get_template_directory_uri() . "/css/font-awesome.min.css", array(), '4.1.0');
    
    // Main stylesheet
    wp_enqueue_style( 'warta-style', get_template_directory_uri() . "/css/style.min.css", array(), filemtime( get_template_directory() . "/css/style.min.css" ) );
        
    // RTL stylesheet
    if ( is_rtl() ) {
        wp_enqueue_style( 'warta-style-rtl', get_template_directory_uri() . '/css/rtl.min.css', array( 'warta-style' ), $theme_version );
    }
        
    // Child theme stylesheet
    if ( is_child_theme() ) {
        wp_enqueue_style( 'warta-style-child', get_stylesheet_uri(), array( 'warta-style' ), wp_get_theme()->get( 'Version' ) );
    }
    
    // Modernizr
    wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/modernizr.min.js', array(), '3.2.0' );
        
    // Main script
    wp_enqueue_script( 'warta-script', get_template_directory_uri() . '/js/script.min.js' , array('jquery', 'jquery-ui-core', 'jquery-effects-core'), $theme_version, true );
        
    // Main script initialization
    wp_enqueue_script( 'warta-script-init', get_template_directory_uri() . '/js/init.min.js' , array( 'warta-script' ), $theme_version, true );
        
    // Localize AJAX url
    wp_localize_script( 'warta-script', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
                
    // Threaded Comments.
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
endif; // warta_scripts
add_action( 'wp_enqueue_scripts', 'warta_scripts' );