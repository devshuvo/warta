<?php

/**
 * Theme setup.
 * 
 * @since 1.7.0
 * @package warta\setup
 */

/**
 * Theme variables.
 */
require_once dirname( __FILE__ ) . '/_variables.php';

/**
 * General setup.
 */
require_once dirname( __FILE__ ) . '/_general.php';