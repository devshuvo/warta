<?php
/**
 * Archive loop layout version 3
 * @package Warta
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( "article-large" ); ?>>
<?php   
        $featured_image = new Warta_Featured_Image();
        $featured_image->display_large_icons();
        
        echo '<div class="content">';
        warta_template_archive_large_featured_media();
        warta_template_archive_large_content();  
        warta_article_footer();     
        echo '</div>';
?>                                
</article>