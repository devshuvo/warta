<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package Warta
 */

// Get $friskamax_warta global variables
global $friskamax_warta_var;

$friskamax_warta_var['sidebar_counter'] = 0;
?>

<aside class="col-md-4">
    
    <div class="row">

        <?php do_action( 'before_sidebar' ); ?>

        <?php 
            if      ( $friskamax_warta_var['page'] === 'home' && dynamic_sidebar( 'home-sidebar-section' ) ) : 
            elseif  ( $friskamax_warta_var['page'] === 'archive' && dynamic_sidebar( 'archive-sidebar-section' ) ) : 
            elseif  ( $friskamax_warta_var['page'] === 'singular' && dynamic_sidebar( 'singular-sidebar-section' ) ) : 
            elseif  ( $friskamax_warta_var['page'] === 'page' && dynamic_sidebar( 'page-sidebar-section' ) ) : 
            elseif  ( dynamic_sidebar( 'default-sidebar-section' ) ) : 
            endif; 
        ?>

    </div><!--.row-->

</aside><!--aside-->               