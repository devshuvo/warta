<?php
/**
 * @package Warta
 */

global $friskamax_warta;

$format                 = get_post_format() ? get_post_format() : 'standard'; // Set default post format
$hide_featured_image    = get_post_meta( get_the_ID(), 'friskamax_hide_featured_image_main', true );
$hide_post_meta_all     = get_post_meta( get_the_ID(), 'friskamax_hide_post_meta_all', true );
$hide_post_meta_main    = get_post_meta( get_the_ID(), 'friskamax_hide_post_meta_main', true );
$hide_post_meta         = $hide_post_meta_all || $hide_post_meta_main;

$rest_content           = '';
$featured_media         = warta_match_featured_media('^', $rest_content);
?>

<article id="post-<?php the_ID(); ?>" 
<?php   post_class(
                'article-large entry-content clearfix ' . 
                ( !get_the_content() ? 'no-padding-bottom no-border-bottom' : '' 
        ) ); 
?>>
        
<?php   
        /**
         * Featured image
         * ---------------------------------------------------------------------
         */
        if( has_post_thumbnail() && !$hide_featured_image ) : 
                warta_featured_image( array( 'page' => 'singular' ) ); ?>         
                <div class="margin-bottom-15"></div>
        
<?php                   
        /**
         * Doesn't have featured image
         * ---------------------------------------------------------------------
         */
        else: // has_post_thumbnail() 
                if( !$hide_post_meta ) {
                        echo '<hr class="no-margin-top">';                
                        echo warta_post_meta();                
                        echo '<hr>';
                } // post meta
                
                
                if( get_post_meta( get_the_ID(), 'friskamax_review_total', true ) ) { 
                        if( $featured_media ) {
                                echo "<div class='featured-media'>{$featured_media}</div>";
                
                                warta_review_box(); 


                                $rest_content = apply_filters( 'the_content', $rest_content );
                                $rest_content = str_replace( ']]>', ']]&gt;', $rest_content );

                                echo $rest_content;
                        } else {
                                warta_review_box();
                                the_content(); 
                        }                                
                } else {
                        the_content(); 
                } // content
                                
        endif; // has_post_thumbnail() 
        
        wp_link_pages( array(
                'before'        => '<div class="page-links">' . __( 'Pages:', 'warta' ),
                'after'         => '</div>',
                'link_before'   => '<span>',
                'link_after'    => '</span>'
        ) ); // Pages
?>
</article><!-- .article-large -->