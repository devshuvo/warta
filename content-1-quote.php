<?php
/**
 * @package Warta
 */

global $friskamax_warta;

$hide_post_meta_all = get_post_meta( get_the_ID(), 'friskamax_hide_post_meta_all', true );
$matches_blockquote = warta_match_quote();

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'article-large ' . ( !has_post_thumbnail() ? 'no-image' : '' ) ); ?>>

<?php   
        /**
         * Featured image
         * =====================================================================
         */
        if( has_post_thumbnail() ) : 
                warta_featured_image(); 
        
        /**
         * No featured image
         * =====================================================================
         */
        else: // has_post_thumbnail()  ?>
        
                <a href="<?php the_permalink() ?>" class="title <?php if( $hide_post_meta_all ) { echo 'margin-bottom-15'; } ?>">
                        <h4><?php the_title() ?></h4>
                </a><!--title-->
        
<?php           if( !$hide_post_meta_all ) {
                        echo warta_post_meta('archive');
                } // post meta

        endif; // has_post_thumbnail() || $matches_image 

        /**
         * Content
         * =====================================================================
         */
        if($matches_blockquote) {
                $content        = apply_filters( 'the_content', get_the_content() );
                $display_more   = strlen( trim($content) ) > strlen( $matches_blockquote );

                echo $matches_blockquote;
        } else {
                echo '<p>' . warta_the_excerpt_max_charlength($excerpt) . '</p>';
        } 
        
        /**
         * Footer
         * =====================================================================
         */
        warta_article_footer( array(
                'read_more' => $display_more
        ));
        
?>
        
</article><!--.article-large-->